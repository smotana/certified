#!/usr/bin/env bash

# Set the contents of this file as metadata with key "shutdown-script" inside an instance template

WORKSPACE=/var/lib/google/veruv

${WORKSPACE}/shutdown-script.sh
