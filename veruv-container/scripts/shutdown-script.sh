#!/usr/bin/env bash

# Upload this file to gs://com-smotana-veruv-bootstrap/shutdown-script.sh

echo "VERUV SHUTDOWN SCRIPT -- started"

WORKSPACE=/var/lib/google/veruv

echo "Pulling secrets"
source ${WORKSPACE}/secrets.sh

echo "Shutting down tomcat gracefully"
curl -d "218C1D5531C444F49A7FBBEEB6175A05" -H "Content-Type: text/plain" -X POST http://127.0.0.1:80/api/shutdown

echo "VERUV SHUTDOWN SCRIPT -- finished"
