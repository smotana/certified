#!/usr/bin/env bash
### Veruv: Webserver and Docker network hardening

iptables -F INPUT

# Basic rules
iptables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -m conntrack --ctstate INVALID -j DROP
iptables -A INPUT -i lo -j ACCEPT
iptables -A INPUT -p icmp -j ACCEPT

# Accept SSH connections with rate limit
iptables -A INPUT -p tcp -m tcp --dport 22 -m conntrack --ctstate NEW -m recent --set --name SSHRATELIMIT --rsource
iptables -A INPUT -p tcp -m tcp --dport 22 -m conntrack --ctstate NEW -m recent --update --seconds 60 --hitcount 5 --name SSHRATELIMIT --rsource -j DROP
iptables -A INPUT -p tcp -m tcp --dport 22 -m state --state NEW -j ACCEPT

# Drop all other INPUT traffic
iptables -A INPUT -j DROP

# Docker containers
# Mainly disallow access from/to Veruv subnet
# Defined in DockerManagerImpl.java:
#  Docker default subnet:       172.17.0.0/16
#  Veruv subnet:                172.28.0.0/16
#  Veruv Webserver container:   172.28.48.0
#  Veruv Browser containers:    172.28.48.0/24

iptables -F DOCKER-USER

# Allow existing connections
iptables -A DOCKER-USER -m state --state ESTABLISHED,RELATED -j ACCEPT

# Allow Webserver from GCloud Health Check and Load Balancer IPs 130.211.0.0/22,35.191.0.0/16 and Veruv public ip 35.244.237.5
# The webserver is running somewhere on the default docker bridge so allow to any host in that subnet
# https://cloud.google.com/load-balancing/docs/https/#firewall_rules
# https://stackoverflow.com/a/53093920
iptables -A DOCKER-USER -p tcp -m tcp  -s 130.211.0.0/22,35.191.0.0/16,5.244.237.5 -d 172.17.0.0/16 --dport 8080  -j RETURN
# Disallow all incoming traffic to default subnet
iptables -A DOCKER-USER ! -s 172.17.0.0/16 -d 172.17.0.0/16 -j DROP

# Allow webserver to access containers
iptables -A DOCKER-USER -p tcp -m tcp -s 172.28.48.0 -d 172.28.48.0/24 --dport 4444 -j RETURN
# Disallow all incoming and cross communication in veruv net
iptables -A DOCKER-USER -d 172.28.0.0/16 -j DROP
# Drop access from veruv net to other private ranges
#  10.0.0.0/8,172.16.0.0/12,192.168.0.0/16 - private ip ranges
#  169.254.0.0/16 - Used by metadata.google.internal
iptables -A DOCKER-USER -s 172.28.0.0/16 -d 10.0.0.0/8,172.16.0.0/12,192.168.0.0/16,169.254.0.0/16 -j DROP

# Return to apply Docker built in rules
iptables -A DOCKER-USER -j RETURN
