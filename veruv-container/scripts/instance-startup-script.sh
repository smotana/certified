#!/usr/bin/env bash

# Set the contents of this file as metadata with key "startup-script" inside an instance template

WORKSPACE=/var/lib/google/veruv

docker run --rm -v=${WORKSPACE}:/mnt/out google/cloud-sdk:alpine \
    gsutil cp gs://com-smotana-veruv-bootstrap/startup-script.sh /mnt/out/startup-script.sh

chmod 700 ${WORKSPACE}/startup-script.sh
${WORKSPACE}/startup-script.sh

docker run --rm -v=${WORKSPACE}:/mnt/out google/cloud-sdk:alpine \
    gsutil cp gs://com-smotana-veruv-bootstrap/shutdown-script.sh /mnt/out/shutdown-script.sh
chmod 700 ${WORKSPACE}/shutdown-script.sh
