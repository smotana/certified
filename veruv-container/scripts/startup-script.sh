#!/usr/bin/env bash

# Upload this file to gs://com-smotana-veruv-bootstrap/startup-script.sh

echo "VERUV STARTUP SCRIPT -- started"
set -e

WORKSPACE=/var/lib/google/veruv

echo "Pulling secrets"
docker run --rm -v=${WORKSPACE}:/mnt/out google/cloud-sdk:alpine \
    gsutil cp gs://com-smotana-veruv-bootstrap/secrets.sh /mnt/out/secrets.sh
source ${WORKSPACE}/secrets.sh

echo "Applying iptables rules"
docker run --rm -v=${WORKSPACE}:/mnt/out google/cloud-sdk:alpine \
    gsutil cp gs://com-smotana-veruv-bootstrap/iptables.sh /mnt/out/iptables.sh
chmod 700 ${WORKSPACE}/iptables.sh
${WORKSPACE}/iptables.sh

echo "Determining max browsers"
HOST_MEM_TOTAL=$(awk '$1~/MemTotal:/ {print $2;}' /proc/meminfo)
MAX_BROWSER_SESSIONS_PER_HOST=$(expr $(((${HOST_MEM_TOTAL} - 1000000) / 500000)))
echo "Allowing ${MAX_BROWSER_SESSIONS_PER_HOST} concurrent browsers"

echo "Creating conf file from template"
docker run --rm -v=${WORKSPACE}:/mnt/out google/cloud-sdk:alpine \
    gsutil cp gs://com-smotana-veruv-bootstrap/config-prod.cfg.template /mnt/out/config-prod.cfg.template
python -c 'secrets = dict({"MAX_BROWSER_SESSIONS_PER_HOST":"'"${MAX_BROWSER_SESSIONS_PER_HOST}"'"}.items() + {a:b.decode("string_escape").replace("\n", "\\n").strip("\"") for a, b in [i.strip("\n").split("=", 1) for i in open("'"${WORKSPACE}/secrets.sh"'") if not i.strip().startswith("#") and "=" in i]}.items()); \
    import re; print re.sub(r"\${(.*?)}", lambda x: secrets[x.group(1)], open("'"${WORKSPACE}/config-prod.cfg.template"'").read());' \
    > ${WORKSPACE}/config-prod.cfg

echo "Logging in to Docker registry"
HOME=${WORKSPACE}/ docker login -u _json_key -p "${GCLOUD_CREDENTIALS_STARTUP}" https://us.gcr.io
echo "Pulling browser docker image"
HOME=${WORKSPACE}/ docker pull \
    us.gcr.io/com-smotana-veruv/veruv-browser:latest
echo "Pulling webserver docker image"
HOME=${WORKSPACE}/ docker pull \
    us.gcr.io/com-smotana-veruv/veruv-webserver:latest
echo "Logging out of Docker registry"
HOME=${WORKSPACE}/ docker logout https://us.gcr.io

echo "Downloading Veruv jar"
docker run --rm -v=${WORKSPACE}:/mnt/out google/cloud-sdk:alpine \
    gsutil cp gs://com-smotana-veruv-bootstrap/veruv-server.war /mnt/out/ROOT.war

if [ "$suppress_docker_container_start" != true ] ; then
    echo "Starting webserver"
    docker run \
        -d \
        --rm \
        --name=veruv-webserver \
        -e VERUV_ENVIRONMENT=PRODUCTION_GOOGLE_CLOUD \
        -p 80:8080 \
        -v /var/run/docker.sock:/var/run/docker.sock \
        -v ${WORKSPACE}/ROOT.war:/usr/local/tomcat/webapps/ROOT.war \
        -v ${WORKSPACE}/config-prod.cfg:/opt/veruv/config-prod.cfg \
        us.gcr.io/com-smotana-veruv/veruv-webserver
else
    echo "Not starting webserver due to 'suppress_docker_container_start=true'"
fi

echo "VERUV STARTUP SCRIPT -- finished"
