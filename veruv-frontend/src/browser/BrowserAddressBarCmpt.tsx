import * as React from 'react';
import * as BrowserApi from  '../server/browserApi';
import {
  FormControl,
  Button,
} from 'react-bootstrap';
import KeyboardIcon from '@material-ui/icons/KeyboardRounded';
import LockIcon from '@material-ui/icons/LockRounded';
import GoIcon from '@material-ui/icons/ArrowForwardRounded';
import NavigateBackIcon from '@material-ui/icons/ArrowBackRounded';
import NavigateForwardIcon from '@material-ui/icons/ArrowForwardRounded';
import RefreshIcon from '@material-ui/icons/RefreshRounded';
import ScreenshotIcon from '@material-ui/icons/PhotoCameraRounded';
import FullscreenIcon from '@material-ui/icons/FullscreenRounded';
import FullscreenExitIcon from '@material-ui/icons/FullscreenExitRounded';

enum FullscreenButtonState {
  Hidden = 'hidden',
  Maximize = 'maximize',
  Minimize = 'minimize',
}

interface Props {
  inputValue?:string;
  controlsHidden?:boolean;
  snapshotDisabled?:boolean;
  fullscreenButtonState?:FullscreenButtonState;
  onFullscreenToggle?:() => void;
  formStyle?:React.CSSProperties;
  onBack?:() => void;
  onForward?:() => void;
  onRefresh?:() => void;
  onGo?:() => void;
  onKeyDown?:React.KeyboardEventHandler<FormControl>;
  onChange?:React.FormEventHandler<FormControl>;
  onTakeScreenshot?:() => void;
  inputRef?:(instance: HTMLInputElement) => void;
  icon?:'check' | 'lock'
  hiddenInputToFocusForKeyboard?:React.RefObject<HTMLInputElement>
}

class BrowserAddressBarCmpt extends React.Component<Props> {
  inputRef:HTMLInputElement|undefined = undefined;
  readonly styles = {
    form: {
      display: 'flex',
      alignItems: 'center',
      margin: '0px',
      verticalAlign: 'top',
      padding: '5px',
      height: '40px',
      borderBottom: "1px solid #ccc",
    },
    addressBarInputContainer: {
      border: "1px solid #ccc",
      borderRadius: '50px',
      display: 'flex',
      alignItems: 'center',
      height: '100%',
      width: '100%',
      padding: '10px 4px 10px 10px',
    },
    addressBarInput: {
      padding: '0px',
      margin: '0px',
      height: 'auto',
      width: '100%',
      border: '0',
      outline: 'none',
      boxShadow: 'none',
    },
    addressBarComponent: {
      marginLeft: '5px',
      marginRight: '5px',
      display: 'table-cell',
      borderBottomRightRadius: '0px',
      borderBottomLeftRadius: '0px',
      border: '1px',
    },
    addressBarButtonGroup: {
      backgroundColor: '#fff',
      display: 'flex',
    },
    addressBarButton: {
      outline: 'none',
      borderRadius: '50%',
      border: '0px',
      fontSize: '20px',
      width: '28px',
      height: '28px',
      padding: '2px',
    },
    goButton: {
      width: '20px',
      height: '20px',
      fontSize: '15px',
    },
    screenshotButton: {
      padding: '0px 12px',
      height: '28px',
      display: 'flex',
      alignItems: 'center',
    },
    buttonIcon: {
      verticalAlign: 'baseline',
    },
  };

  render() {
    return (
      <div style={{...this.styles.form, ...this.props.formStyle}}>
          <div style={{...this.styles.addressBarComponent, ...this.styles.addressBarButtonGroup}}>
            <Button
              disabled={this.props.controlsHidden}
              className="visible-xs"
              style={this.styles.addressBarButton}
              onClick={e => this.handleShowKeyboard()}>
              <KeyboardIcon titleAccess='Show keyboard' fontSize='inherit' style={this.styles.buttonIcon}/>
            </Button>
            <Button
              disabled={this.props.controlsHidden}
              style={this.styles.addressBarButton}
              onClick={e => this.props.onBack && this.props.onBack()}>
              <NavigateBackIcon titleAccess='Back' fontSize='inherit' style={this.styles.buttonIcon}/>
            </Button>
            <Button
              disabled={this.props.controlsHidden}
              style={this.styles.addressBarButton}
              onClick={e => this.props.onForward && this.props.onForward()}>
              <NavigateForwardIcon titleAccess='Forward' fontSize='inherit' style={this.styles.buttonIcon}/>
            </Button>
            <Button
              disabled={this.props.controlsHidden}
              style={this.styles.addressBarButton}
              onClick={e => this.props.onRefresh && this.props.onRefresh()}>
              <RefreshIcon titleAccess='Refresh' fontSize='inherit' style={this.styles.buttonIcon}/>
            </Button>
            {this.props.fullscreenButtonState && this.props.fullscreenButtonState !== FullscreenButtonState.Hidden && (
              <Button
                disabled={this.props.controlsHidden}
                style={this.styles.addressBarButton}
                onClick={e => this.props.onFullscreenToggle && this.props.onFullscreenToggle()}>
                {this.props.fullscreenButtonState === FullscreenButtonState.Maximize
                  ? (
                    <FullscreenIcon titleAccess='Maximize' fontSize='inherit' style={this.styles.buttonIcon}/>
                  )
                  : (
                    <FullscreenExitIcon titleAccess='Maximize' fontSize='inherit' style={this.styles.buttonIcon}/>
                  )}
              </Button>
            )}
          </div>
          <div style={{...this.styles.addressBarComponent, ...this.styles.addressBarInputContainer}}>
            <LockIcon fontSize='inherit' style={{marginRight: '10px'}} />
            <FormControl
              name="addressBarInput"
              style={this.styles.addressBarInput}
              placeholder='Type an address'
              // autoFocus={true}
              type="text"
              value={this.props.inputValue}
              inputRef={this.props.inputRef}
              onKeyDown={this.props.onKeyDown}
              onChange={this.props.onChange} />
            <Button
              style={{...this.styles.addressBarButton, ...this.styles.goButton, marginLeft: '10px'}}
              onClick={e => this.props.onGo && this.props.onGo()}>
              <GoIcon titleAccess='Go' fontSize='inherit' style={this.styles.buttonIcon}/>
            </Button>
          </div>
          <div style={{...this.styles.addressBarComponent,
            ...{transition: 'max-width 500ms ease-out, opacity 500ms ease-out'},
            ...(this.props.controlsHidden ? {
              maxWidth: '0px',
              opacity: 0,
            } : {
              maxWidth: '200px',
              opacity: 1,
            })}}>
            <Button
              style={{...this.styles.screenshotButton, ...(this.props.controlsHidden ? {display: 'none'} : null)}}
              bsStyle='primary'
              disabled={this.props.snapshotDisabled}
              onClick={e => this.props.onTakeScreenshot && this.props.onTakeScreenshot()}>
              <ScreenshotIcon titleAccess='Screenshot' fontSize='inherit'/>
              <span className="hidden-xs" style={{marginLeft: '10px'}}>Snapshot</span>
            </Button>
          </div>
      </div>
    );
  }

  handleShowKeyboard() {
    if(this.props.hiddenInputToFocusForKeyboard && this.props.hiddenInputToFocusForKeyboard.current) {
      this.props.hiddenInputToFocusForKeyboard!.current!.focus();
    }
  }
}

export { FullscreenButtonState };
export default BrowserAddressBarCmpt;
