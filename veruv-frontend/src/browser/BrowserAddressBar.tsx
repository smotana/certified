import React, { Component } from 'react';
import { NavigateType, Verification } from  '../server/serverApi';
import * as BrowserApi from  '../server/browserApi';
import BrowserAddressBarCmpt, { FullscreenButtonState } from "./BrowserAddressBarCmpt";
import ReactGA from 'react-ga';

interface Props {
  socket:BrowserApi.BrowserSocket;
  startPage?:string;
  style?:React.CSSProperties;
  hiddenInputToFocusForKeyboard?:React.RefObject<HTMLInputElement>
  onTakeScreenshot:() => void;
}

interface State {
  browserUrl:string;
  url:string;
  controlsHidden:boolean;
  snapshotWaitingForServer:boolean
  fullscreenButtonState:FullscreenButtonState;
}

class BrowserAddressBar extends Component<Props, State> implements BrowserApi.BrowserEventHandler {
  inputRef:HTMLInputElement|undefined = undefined;
  readonly styles = {
    canvas: {
      outline: 'none',
    },
  };

  constructor(props) {
    super(props);

    var fullscreenButtonState:FullscreenButtonState = this.props.socket.canChangeDimensions()
      ? FullscreenButtonState.Maximize
      : FullscreenButtonState.Hidden;

    this.state = {
      browserUrl: '',
      url: props.startPage || '',
      controlsHidden: true,
      snapshotWaitingForServer: false,
      fullscreenButtonState: fullscreenButtonState,
    };

    props.socket.addHandler(this);
  }

  render() {
    return (
      <BrowserAddressBarCmpt
        inputValue={this.state.url}
        controlsHidden={this.state.controlsHidden}
        snapshotDisabled={this.state.snapshotWaitingForServer}
        fullscreenButtonState={this.state.fullscreenButtonState}
        onFullscreenToggle={() => this.handleFullscreenToggle()}
        formStyle={this.props.style}
        onBack={() => this.handleAddressBarNavigate(NavigateType.BACK)}
        onForward={() => this.handleAddressBarNavigate(NavigateType.FORWARD)}
        onRefresh={() => this.handleAddressBarNavigate(NavigateType.REFRESH)}
        onGo={() => this.handleAddressBarGo()}
        onKeyDown={e => this.handleAddressBarKeyDown(e)}
        onChange={e => this.handleAddressBarChange(e)}
        onTakeScreenshot={() => this.handleAddressBarTakeScreenshot()}
        inputRef={ref => this.inputRef = ref}
        hiddenInputToFocusForKeyboard={this.props.hiddenInputToFocusForKeyboard}
      />
    );
  }

  handleFullscreenToggle() {
    if(!this.props.socket.isConnected()) {
      return;
    }
    switch(this.state.fullscreenButtonState) {
      case FullscreenButtonState.Hidden:
        break;
      case FullscreenButtonState.Maximize:
        this.props.socket.maximizeDimensions();
        this.setState({fullscreenButtonState: FullscreenButtonState.Minimize})
        break;
      case FullscreenButtonState.Minimize:
        this.props.socket.minimizeDimensions();
        this.setState({fullscreenButtonState: FullscreenButtonState.Maximize})
        break;
    }
  }

  handleAddressBarTakeScreenshot() {
    this.setState({snapshotWaitingForServer: true});
    this.props.onTakeScreenshot();
  }

  verification?(verification:Verification, requestId:string, verificationId:string):void {
    this.setState({snapshotWaitingForServer: false});
  }

  handleAddressBarChange(event) {
    if(!this.inputRef) {
      return;
    }
    this.setState({url: this.inputRef.value});
  }

  handleAddressBarKeyDown(event) {
    if(event.keyCode != 13) {
      return;
    }
    this.handleAddressBarGo();
  }

  handleAddressBarGo() {
    if(!this.inputRef) {
      return;
    }
    var url = this.inputRef.value;
    if(!url) {
      return;
    }
    if(this.props.socket.isConnected()) {
      this.props.socket.navigateUrl(url);

      ReactGA.event({
        category: 'browser',
        action: 'navigate',
        label: 'GO',
      });
    } else {
      this.props.socket.connect({url: url});

      ReactGA.event({
        category: 'browser',
        action: 'open',
        label: 'addressBar',
      });
    }
  }

  handleAddressBarNavigate(navigateType:NavigateType) {
    this.props.socket.navigate(navigateType);

    ReactGA.event({
      category: 'browser',
      action: 'navigate',
      label: navigateType,
    });
  }

  browserNavigated(url:string):void {
    this.setState({
      browserUrl: url,
      url: url
    });
  }

  browserAcquired?():void {
    this.setState({
      browserUrl: '',
      url: '',
      controlsHidden: false,
      snapshotWaitingForServer: false,
    });
    if(this.inputRef) {
      this.inputRef.focus();
    }
  }

  closed?(reason:string):void {
    this.setState({
      controlsHidden: true,
      snapshotWaitingForServer: false,
    });
  }
}

export default BrowserAddressBar;
