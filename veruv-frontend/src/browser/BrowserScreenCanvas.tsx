import * as React from 'react';
import * as BrowserApi from  '../server/browserApi';

interface Props {
  canvasStyle?:React.CSSProperties;
  canvasRef?:React.RefObject<HTMLCanvasElement>;
  tabIndex?:number;
  onClick?:React.MouseEventHandler<HTMLCanvasElement>;
  onMouseMove?:React.MouseEventHandler<HTMLCanvasElement>;
  onKeyDown?:React.KeyboardEventHandler<HTMLElement>;
  onKeyUp?:React.KeyboardEventHandler<HTMLElement>;
  onKeyPress?:React.KeyboardEventHandler<HTMLElement>;
  /** call focus() to open virtual keyboard */
  hiddenInputToFocusForKeyboard?:React.RefObject<HTMLInputElement>;
}

class BrowserScreenCanvas extends React.Component<Props> {
  readonly styles = {
    canvas: {
      outline: 'none',
      display: 'block',
    },
    hidden: {
      width: '0px',
      height: '0px',
      overflow: 'hidden',
    },
    input: {
      caretColor: 'transparent',
      color: 'transparent',
      textShadow: '0 0 0 transparent',
      opacity: 0,
      position: 'absolute' as 'absolute',
      top: '50%',
      left: '50%',
      height: '50px',
      width: '50px',
    },
  };

  shouldComponentUpdate() {
    return false; // Never redraw canvas that needs to retain state
  }

  render() {
    return (
      <div>
        <div style={this.styles.hidden}>
          <input type='text'
            style={this.styles.input}
            ref={this.props.hiddenInputToFocusForKeyboard}
            onKeyDown={this.props.onKeyDown}
            onKeyUp={this.props.onKeyUp}
            onKeyPress={this.props.onKeyPress}
          />
        </div>
        <canvas
          style={{...this.styles.canvas, ...this.props.canvasStyle}}
          ref={this.props.canvasRef}
          tabIndex={this.props.tabIndex}
          onClick={this.props.onClick}
          onMouseMove={this.props.onMouseMove}
          onKeyDown={this.props.onKeyDown}
          onKeyUp={this.props.onKeyUp}
          onKeyPress={this.props.onKeyPress}
        />
      </div>
    );
  }
}

export default BrowserScreenCanvas;
