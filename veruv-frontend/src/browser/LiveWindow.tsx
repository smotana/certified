import * as React from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';
import {
  Alert,
  Button,
  Fade,
} from 'react-bootstrap';

interface Props {
  style?:React.CSSProperties;
  contentWidth?:number;
  contentHeight?:number;
  // Progress bar
  showProgressBar?:boolean;
  // Message
  messageShow?:boolean;
  messageContent?:string|React.ReactNode;
  messageStyle?:string;
  messageButtonPrimaryShow?:boolean;
  messageButtonPrimaryTitle?:string;
  messageButtonPrimaryClick?:()=>void;
  messageButtonSecondaryShow?:boolean;
  messageButtonSecondaryTitle?:string;
  messageButtonSecondaryClick?:()=>void;
}

/** Live Window allows showing progress bar and messages */
class LiveWindow extends React.Component<Props> {
  styles = {
    overlayContainer: {
      position: "relative" as 'relative',
      transition: 'width 500ms ease-out, height 500ms ease-out',
    },
    overlayAlertMsg: {
      pointerEvents: 'none' as 'none',
      width: '100%',
      position: 'absolute' as 'absolute',
      top: 0,
      left: 0,
      zIndex: 11,
    },
    alertMsg: {
      pointerEvents: 'all' as 'all',
      marginTop: '40px',
      borderRadius: '0px',
    },
    overlayProgressBar: {
      pointerEvents: 'none' as 'none',
      width: '100%',
      position: 'absolute' as 'absolute',
      top: 0,
      left: 0,
      zIndex: 12,
      height: '2px'
    },
    scrollableContent: {
      overflowX: 'scroll' as 'scroll',
      WebkitOverflowScrolling: 'touch' as 'touch',
      maxWidth: '100%',
    },
  }

  render() {
    return (
        <div style={{...this.styles.overlayContainer, ...this.props.style}}>
          <Fade in={this.props.showProgressBar} appear mountOnEnter unmountOnExit>
            <div>
              <LinearProgress style={this.styles.overlayProgressBar}/>
            </div>
          </Fade>
          <Fade in={this.props.messageShow} appear mountOnEnter unmountOnExit>
            <div>
              <div style={this.styles.overlayAlertMsg}>
                <Alert bsStyle={this.props.messageStyle} style={this.styles.alertMsg}>
                  {this.props.messageContent}
                  {(this.props.messageButtonPrimaryShow || this.props.messageButtonSecondaryShow) ? (<p><br />
                    {this.props.messageButtonPrimaryShow ? (
                      <Button
                        bsStyle={this.props.messageStyle}
                        onClick={() => this.props.messageButtonPrimaryClick && this.props.messageButtonPrimaryClick()}>
                        {this.props.messageButtonPrimaryTitle}
                      </Button>
                    ) : null}
                    {this.props.messageButtonSecondaryShow ? (
                      <Button
                        bsStyle={this.props.messageButtonPrimaryShow ? 'default' : this.props.messageStyle}
                        style={{marginLeft: '10px'}}
                        onClick={() => this.props.messageButtonSecondaryClick && this.props.messageButtonSecondaryClick()}>
                        {this.props.messageButtonSecondaryTitle}
                      </Button>
                    ) : null}
                  </p>) : null}
                </Alert>
              </div>
            </div>
          </Fade>
          <div style={{...this.styles.scrollableContent, width: this.props.contentWidth, height: this.props.contentHeight}}>
            {this.props.children}
          </div>
        </div>
    );
  }
}

export default LiveWindow;
