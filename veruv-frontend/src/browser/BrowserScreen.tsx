import * as React from 'react';
import * as BrowserApi from  '../server/browserApi';
import BrowserScreenCanvas from './BrowserScreenCanvas';

interface Props {
  socket:BrowserApi.BrowserSocket;
  style?:React.CSSProperties;
  hiddenInputToFocusForKeyboard?:React.RefObject<HTMLInputElement>
}

class BrowserScreen extends React.Component<Props> implements BrowserApi.BrowserEventHandler {
  readonly MOUSE_MOVE_DEBOUNCE = 50;
  readonly NON_PRINTABLE_CHARS_WITH_KEY_UP_DOWN = new Set([
    16, // Shift
    18, // Alt
    17, // Ctrl
    91, // Left Command/WinKey
    93, // Right Command
  ]);
  readonly ALLOWED_NON_PRINTABLE_CHARS = new Set([
    ...this.NON_PRINTABLE_CHARS_WITH_KEY_UP_DOWN,
    13, // Enter
    8, // Backspace
    46, // Delete
    9, // Tab
    112, // F1
    113, // F2
    114, // F3
    115, // F4
    116, // F5
    117, // F6
    118, // F7
    119, // F8
    120, // F9
    121, // F10
    122, // F11
    123, // F12
    27, // Esc
    38, // Arrow Up
    40, // Arrow Down
    37, // Arrow Left
    39, // Arrow Right
    33, // Page up
    34, // Page down
    36, // Home
    35, // End
  ]);
  canvasRef:React.RefObject<HTMLCanvasElement> = React.createRef();
  charsBuffer:number[] = [];
  keysFlushScheduled:boolean = false;
  mouseMoveDebounceTask?;

  constructor(props:Props) {
    super(props);

    props.socket.addHandler(this);
  }

  componentDidMount() {
    this.screenDimensions!(this.props.socket.getDimensions());
  }

  componentWillUnmount() {
    this.mouseMoveDebounceTask && clearTimeout(this.mouseMoveDebounceTask);
  }

  render() {
    return (
      <BrowserScreenCanvas
        canvasStyle={this.props.style}
        canvasRef={this.canvasRef}
        tabIndex={0}
        onClick={e => this.handleCanvasClick(e)}
        onMouseMove={e => this.handleCanvasMouseMove(e)}
        onKeyDown={e => this.handleCanvasKeyDownUp(e, true)}
        onKeyUp={e => this.handleCanvasKeyDownUp(e, false)}
        onKeyPress={e => this.handleCanvasKeyPress(e)}
        hiddenInputToFocusForKeyboard={this.props.hiddenInputToFocusForKeyboard}
      />
    );
  }

  screenDimensions?(dimensions:BrowserApi.ScreenDimensions):void {
    if(!this.canvasRef.current) {
      return;
    }
    this.canvasRef.current.width = dimensions.width;
    this.canvasRef.current.height = dimensions.height;
  }

  handleCanvasClick(event:React.MouseEvent<HTMLElement>) {
    if(!this.props.socket.isBrowserReady()) {
      return;
    }
    var rect = this.canvasRef.current!.getBoundingClientRect();
    var xPos = event.clientX - rect.left;
    var yPos = event.clientY - rect.top;
    this.props.socket.mouseClick(xPos, yPos, false);
  }

  handleCanvasMouseMove(event:React.MouseEvent<HTMLElement>) {
    if(!this.props.socket.isBrowserReady()) {
      return;
    }
    var rect = this.canvasRef.current!.getBoundingClientRect();
    var xPos = event.clientX - rect.left;
    var yPos = event.clientY - rect.top;
    this.mouseMoveDebounceTask && clearTimeout(this.mouseMoveDebounceTask);
    this.mouseMoveDebounceTask = setTimeout(() => {
      this.props.socket.mouseMove(xPos, yPos);
    }, this.MOUSE_MOVE_DEBOUNCE);
  }

  flushKeysSchedule() {
    if(this.keysFlushScheduled || this.charsBuffer.length == 0 || !this.props.socket.isBrowserReady()){
      return;
    }
    this.keysFlushScheduled = true;
    setTimeout(() => {
      this.keysFlushScheduled = false;
      this.flushKeysNow();
    }, 100);
  }

  flushKeysNow() {
    if(this.charsBuffer.length == 0 || !this.props.socket.isBrowserReady()){
      return;
    }
    this.props.socket.keyboardPrint(this.charsBuffer);
    this.charsBuffer = [];
  }

  handleCanvasKeyPress(event:React.KeyboardEvent<HTMLElement>) {
    var charCode = event.charCode || event.which || event.keyCode;
    if(!charCode) {
      return;
    }

    this.charsBuffer.push(charCode);
    this.flushKeysSchedule();
    event.preventDefault();
  }

  handleCanvasKeyDownUp(event:React.KeyboardEvent<HTMLElement>, keyDown:boolean) {
    var keyCode = event.keyCode || event.which || event.charCode;
    if(!this.ALLOWED_NON_PRINTABLE_CHARS.has(keyCode)) {
      return;
    }
    var pressType:BrowserApi.PressType;
    if(this.NON_PRINTABLE_CHARS_WITH_KEY_UP_DOWN.has(keyCode)) {
      pressType = keyDown ? BrowserApi.PressType.Down : BrowserApi.PressType.Up;
    } else {
      if(!keyDown) {
        return;
      }
      pressType = BrowserApi.PressType.Press;
    }

    this.flushKeysNow();
    this.props.socket.keyboardPress(keyCode, pressType);
    event.preventDefault();
  }

  frame(xPos:number, yPos:number, width:number, height:number, image:string):void {
    var imageContainer = new Image();
    if(!this.canvasRef.current) {
      throw new Error("Canvas reference is null");
    }
    var context:CanvasRenderingContext2D = this.canvasRef.current.getContext('2d')!;
    imageContainer.onload = () => context.drawImage(imageContainer, xPos, yPos, width, height);
    imageContainer.src = 'data:image/png;base64,' + image;
  }
}

export default BrowserScreen;
