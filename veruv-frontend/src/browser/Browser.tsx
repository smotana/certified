import * as React from 'react';
import ReactGA from 'react-ga';
import * as BrowserApi from  '../server/browserApi';
import BrowserScreen from './BrowserScreen';
import BrowserAddressBar from './BrowserAddressBar';
import LiveWindow from './LiveWindow';
import ReCAPTCHA from 'react-google-recaptcha';
import withAuthentication, { AuthenticatorProps } from '../server/auth/authenticator';
import { Verification } from '../server/serverApi';
import SiteBridge, { Handler as BridgeHandler } from '../extension/siteBridge';

interface Props {
  style?;
  socket:BrowserApi.BrowserSocket;
  startPage?:string;
}

type MessageContent = {
  type:'text',
  value:string,
}|{
  type:'recaptcha',
  key:string,
  callback:(challengeResult:string)=>void,
}

interface State {
  loading:boolean;
  messageShow:boolean;
  messageContent?:MessageContent;
  messageStyle?:string;
  messageShowReconnectButton?:boolean;
  messageShowHideButton?:boolean;
  screenDimensions:BrowserApi.ScreenDimensions;
  expiryTime?:number;
}

class Browser extends React.Component<Props & AuthenticatorProps, State> implements BrowserApi.BrowserEventHandler, BridgeHandler {
  readonly styles = {
    browser: {
      display: 'inline-block',
      textAlign: 'center' as 'center',
      border: "1px solid #ccc",
      borderRadius: '4px',
      backgroundColor: 'white',
      boxShadow: 'rgba(0, 9, 152, 0.1) 1px 5px 9px 4px',
    },
    lockLabel: {
      width: "50px",
    },
    challengeContainer: {
      display: 'inline-block',
      marginTop: '10px',
    }
  }
  readonly hiddenInputToFocusForKeyboard?:React.RefObject<HTMLInputElement> = React.createRef();
  expiryTask?;
  recaptchaRef:React.RefObject<ReCAPTCHA> = React.createRef();

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      messageShow: false,
      screenDimensions: props.socket.getDimensions(),
    };

    (window as any).recaptchaOptions = {
      useRecaptchaNet: true,
    };

    this.props.socket.addHandler(this);
  }

  componentDidMount() {
    if(this.props.startPage) {
      this.props.socket.connect({url: this.props.startPage});
    }
    SiteBridge.getInstance().registerSiteHandler('Browser', this);
  }

  componentWillUnmount() {
    this.props.socket.disconnect("Closing page");
    SiteBridge.getInstance().deregisterSiteHandler('Browser');
    this.expiryTask && clearTimeout(this.expiryTask);
  }

  render() {
    var messageContent;
    if(this.state.messageContent) {
      switch(this.state.messageContent.type) {
        case 'text':
          messageContent = this.state.messageContent.value;
          break;
        case 'recaptcha':
          messageContent = (
            <div>
              <div>Please solve a Captcha before continuing:</div>
              <div style={this.styles.challengeContainer}>
                <ReCAPTCHA
                  ref={this.recaptchaRef}
                  sitekey={this.state.messageContent.key}
                  onChange={result => this.recaptchaOnChange(result)}
                />
              </div>
            </div>
          );
          break;
        default:
          throw new Error(`Unknown message content ${this.state.messageContent}`);
      }
    }
    return (
      <div style={{...this.styles.browser, ...this.props.style,
        maxWidth: '100%',
        width: this.state.screenDimensions.width + 2}}>
        <BrowserAddressBar
          socket={this.props.socket}
          startPage={this.props.startPage}
          style={{width: '100%'}}
          hiddenInputToFocusForKeyboard={this.hiddenInputToFocusForKeyboard}
          onTakeScreenshot={() => this.handleAddressBarTakeScreenshot()}
          />
        <div>
          <LiveWindow
            style={{
              maxWidth: '100%',
              height: this.state.screenDimensions.height
            }}
            contentWidth={this.state.screenDimensions.width}
            contentHeight={this.state.screenDimensions.height}
            showProgressBar={this.state.loading}
            messageShow={this.state.messageShow}
            messageContent={messageContent}
            messageStyle={this.state.messageStyle}
            messageButtonPrimaryShow={this.state.messageShowReconnectButton}
            messageButtonPrimaryTitle='Reconnect'
            messageButtonPrimaryClick={() => this.handleRetry()}
            messageButtonSecondaryShow={this.state.messageShowHideButton}
            messageButtonSecondaryTitle='Hide'
            messageButtonSecondaryClick={() => this.clearMessage()}>
            <BrowserScreen
              socket={this.props.socket}
              hiddenInputToFocusForKeyboard={this.hiddenInputToFocusForKeyboard}
            />
          </LiveWindow>
        </div>
      </div>
    );
  }

  handleAddressBarTakeScreenshot() {
    this.showMessage({type:'text', value:'Taking a screenshot...'});
    this.props.socket.verificationRequest(this.props.token);

    ReactGA.event({
      category: 'browser',
      action: 'snapshot',
      label: 'create',
    });
  }

  verification?(verification:Verification, requestId:string, verificationId:string):void {
    this.showMessage({type:'text', value:'Screenshot taken'}, 'success', false, false, 800);
  }

  recaptchaOnChange(result:string|null) {
    if(!this.state.messageContent || this.state.messageContent.type !== 'recaptcha') {
      return;
    }

    if(!result) {
      this.recaptchaRef.current && this.recaptchaRef.current.reset();
      return;
    }

    this.clearMessage();

    this.state.messageContent.callback(result);
  }

  handleRetry() {
    this.clearMessage();
    this.props.socket.connect();
  }

  showMessage(content:MessageContent, style = 'info', showReconnectButton = false, showHideButton = false, clearTimeout:number = 0) {
    this.setState({
      messageShow: true,
      messageContent: content,
      messageStyle: style,
      messageShowReconnectButton: showReconnectButton,
      messageShowHideButton: showHideButton
    });
    if(clearTimeout) {
      this.expiryTask = setTimeout(() => {
        if(this.state.messageShow
          && this.state.messageContent
          && this.messageContentEquals(this.state.messageContent, content)
          && this.state.messageStyle === style) {
          this.clearMessage();
        }
      }, clearTimeout);
    }
  }

  messageContentEquals(l:MessageContent, r:MessageContent):boolean {
    if(l.type === 'text' && r.type === 'text') {
      return l.value === r.value;
    }
    if(l.type === 'recaptcha' && r.type === 'recaptcha') {
      return l.key === r.key;
    }
    return false;
  }

  clearMessage() {
    this.setState({
      messageShow: false
    });
  }

  screenDimensions?(dimensions:BrowserApi.ScreenDimensions):void {
    this.setState({screenDimensions: {
      width: dimensions.width,
      height: dimensions.height,
    }});
  }

  browserInQueue?(timeInQueueEstimate:number, timeInQueueEstimateUnit:string, usersInQueue:number):void {
    var msgReason, msgTimeLeft;

    if(usersInQueue == 0) {
      msgReason = 'Starting a secure browser just for you...';
    } else {
      msgReason = `Waiting for ${usersInQueue} users in queue...`
    }

    if(timeInQueueEstimateUnit === 'SECONDS') {
      if(timeInQueueEstimate < 5) {
        msgTimeLeft = 'Few seconds remaining';
      } else {
        msgTimeLeft = `~${timeInQueueEstimate} seconds remaining`;
      }
    } else {
      msgTimeLeft = `${timeInQueueEstimate} ${timeInQueueEstimateUnit} remaining`;
    }

    this.showMessage({type:'text', value:`${msgReason} (${msgTimeLeft})`});
  }

  loadingChanged?(isLoading:boolean):void {
    this.setState({loading: isLoading});
  }

  browserChallenge?(challengeType:string, challengeKey:string, onSolvedCallback:(challengeResult:string)=>void):void {
    if(challengeType !== 'RECAPTCHA_V2') {
      throw new Error(`Unknown challenge ${challengeType}`);
    }

    this.showMessage({type:'recaptcha', key:challengeKey, callback:onSolvedCallback}, 'warning');
  }

  browserUserNotify?(bsStyle:string, text:string):void {
    this.showMessage({type:'text', value:text}, bsStyle, false, true);
  }

  browserAcquired?():void {
    this.clearMessage();

    // Show "successfully connected" message
    var connectedText = "Connected"
    this.showMessage({type:'text', value:connectedText}, 'success', false, false, 800);
  }

  closed?(reason:string):void {
    this.expiryTask && clearTimeout(this.expiryTask);
    this.showMessage({type:'text', value:reason}, 'danger', true, false);
  }

  error?(reason:string):void {
    this.showMessage({type:'text', value:reason}, 'warning', true, true);
  }

  handleMessage?(type: string, data: any):void {
    if(type === 'open') {
      if(!this.isTypeBrowserSession(data)) {
        this.showMessage({type:'text', value:'Failed to open site from browser extension'}, 'danger', false, true);
        SiteBridge.getInstance().sendMessageToExtension('open-err');
        return;
      }
      SiteBridge.getInstance().sendMessageToExtension('open-ack');
      this.props.socket.connect(data);

      ReactGA.event({
        category: 'browser',
        action: 'open',
        label: 'addon',
      });
    }
  }
  isTypeBrowserSession(object: any): object is BrowserApi.BrowserSession {
    return object;
  }
}

export default withAuthentication(Browser);
