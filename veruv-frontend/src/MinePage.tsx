import React, { Component } from 'react';
import {
  Alert, Panel, Button
 } from 'react-bootstrap';
import { withRouter, RouteComponentProps } from 'react-router';
import { connect } from 'react-redux';
import { Action, Status } from './server/action';
import SnapshotSelection from './editor/SnapshotSelection';
import Editor from './editor/Editor';
import HorizontalPanels from './content/HorizontalPanels';
import { ContentLink, Content } from './content/contentApi';
import withAuthentication, { AuthenticatorProps } from './server/auth/authenticator';
import TipIcon from '@material-ui/icons/InfoOutlined';
import setTitle from './util/titleUtil';

interface Props {
  action:Action;
  // Store props
  mySnapshotsStatus;
  mySnapshots;
}

interface State {
  liveSnapshotId?:string;
}

class MinePage extends Component<Props & RouteComponentProps & AuthenticatorProps, State> {
  readonly styles = {
  }

  constructor(props) {
    super(props)

    this.state = {};
  }

  componentDidMount() {
    setTitle('My created snapshots');
    if (this.props.mySnapshotsStatus == Status.NONE) {
      this.props.action.getMySnapshots();
    }
  }

  render() {
    var content;
    switch(this.props.mySnapshotsStatus) {
      case Status.FULFILLED:
        if(!this.props.mySnapshots || Object.keys(this.props.mySnapshots).length <= 0) {
          content = (<Alert bsStyle='info'>
            <p>You have no snapshots</p>
            <ContentLink content={{link: {
              text: 'Create',
              path: '/create',
              anchor: 'browser',
            }} as Content}/>
          </Alert>);
          break;
        }
        var selectedContent
        if(this.state.liveSnapshotId) {
          var liveSnapshot = this.props.mySnapshots[this.state.liveSnapshotId];
          selectedContent = (
            <Editor
              action={this.props.action}
              snapshotId={this.state.liveSnapshotId}
              snapshot={liveSnapshot}
            />
          );
        }

        content = (
          <div style={{display: 'inline-block'}}>
            <SnapshotSelection
              onSelect={id => this.handleSnapshotSelect(id)}
              snapshots={this.props.mySnapshots}
            />
            {selectedContent}
          </div>
        )
        break;
      case Status.NONE:
      case Status.PENDING:
        content = (<Alert bsStyle='info'>Loading...</Alert>);
        break;
      case Status.REJECTED:
      default:
        content = (<Alert bsStyle='danger'>Oops, failed to load your snapshots</Alert>);
        break;
    }

    var registerReminder;
    if(!this.props.isAuthenticated) {
      registerReminder = (
        <div>
          <Panel>
            <Panel.Body>
              <div style={{display:'flex', alignItems:'center'}}>
                <TipIcon />
                <div style={{flexGrow:1, margin:'0 10px 0 10px'}}>
                  Snapshots are saved in your browser session. Register so you don't lose your snapshots.
                </div>
                <ContentLink content={{link: {text: "Register", path: '/members'}}} />
              </div>
            </Panel.Body>
          </Panel>
        </div>
      );
    }
    return (
      <div>
        <HorizontalPanels
          xsColCnt={1}
          smColCnt={2}
          maxWidth={1024}
          contents={[
            {title: (<h1>My snapshots</h1>)},
            {description: registerReminder},
          ]}
        />
        {content}
      </div>
    );
  }

  handleSnapshotSelect(id) {
    this.setState({liveSnapshotId: id});
  }
}

const mapStateToProps = (state, ownProps) => {
  var snapshotsById = {};
  state.mySnapshots.ids.forEach(id => snapshotsById[id] = state.verificationById[id]);
  state.mySnapshots.createdIds.forEach(id => snapshotsById[id] = state.verificationById[id]);
  return {
    mySnapshotsStatus: state.mySnapshots.status,
    mySnapshots: snapshotsById,
  }
}

export default withAuthentication(withRouter(connect(mapStateToProps)(MinePage)));
