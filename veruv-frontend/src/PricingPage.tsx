import React, { Component } from 'react';
import PricingBuilder from './components/Pricing'
import setTitle from './util/titleUtil';


class PricingPage extends Component {
  readonly styles = {
    finePrint: {
      fontSize: '0.7em',
    },
  }

  componentDidMount() {
    setTitle('Pricing');
  }

  render() {
    var builder = new PricingBuilder();
    builder.setPanelStyle({
      maxWidth: '170px',
    });

    var colFree = builder.createColumn('Public use');
    // var colHosting = builder.createColumn('One time', 'Single hosting of your snapshot');
    // var colSingle = builder.createColumn('Subscription', 'Single user access');
    var colApi = builder.createColumn('Organization', 'Custom usage based on your needs');

    builder.addRowText(colFree, 'Unlimited†', 'snapshots');
    // builder.addRowText(colHosting, '1', 'snapshot');
    builder.addRowText([/*colSingle, */colApi], 'Unlimited', 'snapshots');

    builder.addRowText(colFree, '90 days', 'snapshot expiry');
    // builder.addRowText([colHosting/*, colSingle*/], '90 Days', 'snapshot expiry');
    builder.addRowText(colApi, 'Custom', 'data retention');

    builder.addRowText([colFree/*, colHosting*/], 'Shared', 'Browser instance availability');
    // builder.addRowText(colSingle, '1 Dedicated¶', 'private browser');
    builder.addRowText(colApi, 'Dedicated¶', 'Browser instance availability');

    builder.addRowText([colApi], 'Whitelabel', 'Customize look and embed');
    builder.addRowText([colApi], 'API', 'Developer access');

    builder.addRowPrice(colFree, 'Free');
    // builder.addRowPrice(colHosting, '$3', 'USD');
    // builder.addRowPrice(colSingle, '$24', 'USD / month');
    // builder.addRowPrice(colApi, '?');

    builder.addRowButton(colFree, 'Use now', 'create');
    // builder.addRowButton([colHosting/*, colSingle*/], 'Get started', 'account');
    builder.addRowButton(colApi, 'Request a quote', 'contact');
    
    return (
      <div>
        <div style={{
          margin: 'auto',
          maxWidth: '850px',
        }}>
          {builder.build(2)}
        </div>
        <div style={this.styles.finePrint}>
          <div>† Free usage tier is limited for unreasonable excessive usage to ensure shared resources are fairly distributed.</div>
          <div>¶ Dedicated private browsers are always ready for your use.</div>
          {/* <div>§ Snapshot request is a link containing a one-time use for taking a snapshot. Snapshot can only be viewed by the requester.</div> */}
          <div>Payment fees are charged by our payment processor Stripe.</div>
          <div>Service may be suspended if the service is used for abuse, spam or illegal content</div>
          {/* <div>Reimbursement for breaching our SLA is a prorated amount of monthly fee.</div> */}
        </div>
      </div>
    );
  }
}

export default PricingPage;
