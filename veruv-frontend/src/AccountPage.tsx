import React, { Component } from 'react';
import withAuthentication, { AuthenticatorProps } from './server/auth/authenticator';
import {
  Form,
  FormGroup,
  Col,
  Button,
  Collapse,
  Alert,
} from 'react-bootstrap';
import AuthenticatedBlock from './server/auth/AuthenticatedBlock';
import InputEdit from './components/InputEdit';
import { Action, actionRef } from './server/action';
import PasswordStrengthMeterAsync from './components/PasswordStrengthMeterAsync';
import { ErrorResponse } from './server/serverApi';
import setTitle from './util/titleUtil';

enum ItemState {
  NONE = 'NONE',
  SAVED = 'SAVED',
}

interface State {
  error?:string;
  isSubmitting:boolean;
  emailState:ItemState;
  passwordState:ItemState;
  newEmail?:string,
  newPass?:string,
  passwordFocused?:boolean,
}

class AccountPage extends Component<AuthenticatorProps, State> {
  constructor(props) {
    super(props);

    this.state = {
      isSubmitting: false,
      emailState: ItemState.NONE,
      passwordState: ItemState.NONE,
    };
  }

  componentDidMount() {
    setTitle('Account');
  }

  render() {
    let hasAnyChanged = this.state.newEmail || this.state.newPass;
    return (
      <AuthenticatedBlock inline={true}>
        <div style={{maxWidth:'512px', margin:'auto'}}>
          <h1>Account settings</h1>
          <Collapse in={this.state.error !== undefined}>
            <div>
              <Alert bsStyle="danger" style={{textAlign:'center'}}>
                {this.state.error}
              </Alert>
            </div>
          </Collapse>
          <Form horizontal onSubmit={this.onSave.bind(this)}>
            <InputEdit
              name='Email'
              value={this.props.email}
              defaultState={this.inputStateToValidationState(this.state.emailState)}
              onChange={this.emailChanged.bind(this)}
              disabled={this.state.isSubmitting}
            />
            <InputEdit
              name='Password'
              type='password'
              value=''
              defaultState={this.inputStateToValidationState(this.state.passwordState)}
              placeholder='~ Change password ~'
              onChange={this.passChanged.bind(this)}
              disabled={this.state.isSubmitting}
              clearAfterEnabling={true}
              onFocus={e => this.setState({passwordFocused: true})}
              onBlur={e => this.setState({passwordFocused: false})}
            >
              <PasswordStrengthMeterAsync
                forceHide={!this.state.passwordFocused}
                style={{
                  width:'95%',
                  borderRadius: '0px 0px 4px 4px',
                }}
                password={this.state.newPass}
                userInputs={['veruv', this.props.email, this.state.newEmail]}
              />
            </InputEdit>
            <FormGroup>
              <Col smOffset={2} sm={10}>
                <Button
                  type="submit"
                  disabled={this.state.isSubmitting || !hasAnyChanged}
                >Save</Button>
              </Col>
            </FormGroup>
          </Form>
        </div>
      </AuthenticatedBlock>
    );
  }

  emailChanged(hasChanged, newEmail) {
    this.setState({
      newEmail: hasChanged ? newEmail : null,
    });
  }

  passChanged(hasChanged, newPass) {
    this.setState({
      newPass: hasChanged ? newPass : null,
    });
  }

  onSave(event) {
    event.preventDefault();
    if(!this.props.isAuthenticated || !this.props.token) {
      return;
    }

    this.setState({isSubmitting: true});
    let newEmail = this.state.newEmail;
    let newPass = this.state.newPass;
    this.props.email !== newEmail

    var promises:Promise<void>[] = [];
    if(newEmail && this.props.email !== newEmail) {
      promises.push(actionRef.get().changeEmail(
        this.props.token,
        newEmail,
      ));
    }
    if(newPass) {
      promises.push(actionRef.get().changePass(
        this.props.token,
        newPass,
      ));
    }

    Promise.all(promises).then(() => {
      this.setState({
        isSubmitting: false,
        emailState: newEmail ? ItemState.SAVED : ItemState.NONE,
        passwordState: newPass ? ItemState.SAVED : ItemState.NONE,
        newEmail:undefined,
        newPass:undefined,
      });
    }).catch((err) => {
      var errorStr;
      if(err instanceof ErrorResponse) {
        errorStr = err.userFacingMessage || err.code;
      } else {
        errorStr = "Failed communicating with server";
      }

      this.setState({
        isSubmitting: false,
        error: errorStr,
        emailState: ItemState.NONE,
        passwordState: ItemState.NONE,
      });
    });
  }

  inputStateToValidationState(inputState) {
    switch(inputState) {
      case 'SAVE_FULFILLED':
        return 'success';
        case 'NONE':
        default:
          return null;
    }
  }
}

export default withAuthentication(AccountPage);
