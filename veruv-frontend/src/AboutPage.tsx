import React, { Component } from 'react';
import {
  Grid,
  Row,
  Col,
  Nav,
  NavItem,
} from 'react-bootstrap';
import {
  Route,
  Switch,
  withRouter,
  RouteComponentProps,
} from 'react-router-dom'
import PrivacyPolicyPage from './PrivacyPolicyPage';
import PageNotFoundPage from './PageNotFoundPage';
import { LinkNavItem } from './util/LinkContainer';
import TermsOfServicePage from './TermsOfServicePage';
import BugBountyPage from './BugBountyPage';
import SecurityPage from './SecurityPage';
import PrivacyIcon from '@material-ui/icons/VisibilityOffRounded';
import SecurityIcon from '@material-ui/icons/SecurityRounded';
import TermsIcon from '@material-ui/icons/NotesRounded';
import ContactIcon from '@material-ui/icons/PersonRounded';
import BountyIcon from '@material-ui/icons/BugReportRounded';
import setTitle from './util/titleUtil';
import ContactPage from './ContactPage';

interface Props {
  // Router matching
  match;
}

class AboutPage extends Component<Props & RouteComponentProps> {
  readonly styles = {
    content: {
      textAlign: 'left' as 'left',
      maxWidth: '1224px',
      margin: 'auto',
      fontSize: '1em',
    },
    paragraph: {
    },
    hr: {
      marginTop: "40px",
      marginBottom: "30px",
      width: "10%",
    },
  }

  componentDidMount() {
    if(this.props.location.pathname === '/about' || this.props.location.pathname === '/about/') {
      setTitle('About');
    }
  }

  render() {
    return (
      <div style={this.styles.content}>
        <Grid fluid>
          <Row>
            <Col xs={12} sm={2}>
              <Nav
                bsStyle="pills"
                stacked
                activeKey={this.props.match.params.page}
              >
                <LinkNavItem eventKey='security' to="/about/security">
                  <SecurityIcon fontSize='inherit' />
                  &nbsp;
                  Security
                </LinkNavItem>
                {/* <LinkNavItem eventKey='bug-bounty' to="/about/bug-bounty">
                  <BountyIcon fontSize='inherit' />
                  &nbsp;
                  Bug bounty
                </LinkNavItem> */}
                <LinkNavItem eventKey='privacy-policy' to="/about/privacy-policy">
                  <PrivacyIcon fontSize='inherit' />
                  &nbsp;
                  Privacy policy
                </LinkNavItem>
                <LinkNavItem eventKey='terms-of-service' to="/about/terms-of-service">
                  <TermsIcon fontSize='inherit' />
                  &nbsp;
                  Terms of service
                </LinkNavItem>
                <LinkNavItem eventKey='contact' to="/about/contact">
                  <ContactIcon fontSize='inherit' />
                  &nbsp;
                  Contact
                </LinkNavItem>
              </Nav>
            </Col>
            <Col xs={12} sm={10}>
              <Switch>
                <Route path="/about/privacy-policy" exact render={props => (
                  <PrivacyPolicyPage {...props} />
                )} />
                {/* <Route path="/about/bug-bounty" exact render={props => (
                  <BugBountyPage {...props} />
                )} /> */}
                <Route path="/about/terms-of-service" exact render={props => (
                  <TermsOfServicePage {...props} />
                )} />
                <Route path="/about/security" exact render={props => (
                  <SecurityPage {...props} />
                )} />
                <Route path="/about/contact" exact render={props => (
                  <ContactPage {...props} />
                )} />
                <Route path="/about" exact render={props => (
                  <div />
                )} />
                <Route render={props => (
                  <PageNotFoundPage {...props} />
                )} />
              </Switch>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default withRouter(AboutPage);
