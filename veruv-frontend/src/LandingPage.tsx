import React, { Component } from 'react';
import {
  Jumbotron,
  FormControl,
  FormGroup,
  InputGroup,
  Button,
  Panel,
} from 'react-bootstrap';
import HorizontalPanels from './content/HorizontalPanels';
import Hr from './Hr';
import withAuthentication, { AuthenticatorProps } from './server/auth/authenticator';
import SecurityIcon from '@material-ui/icons/SecurityRounded';
import PrivacyIcon from '@material-ui/icons/VisibilityOffRounded';
import OpenSourceIcon from '@material-ui/icons/CodeRounded';
import BountyIcon from '@material-ui/icons/BugReportRounded';
import ScrollAnchor from './components/ScrollAnchor';
import CircleImage from './components/CircleImage';
import AnimAnchor from './components/AnimAnchor';
import AnimBubble from './components/AnimBubble';
import AnimCycleContent from './components/AnimCycleContent';
import setTitle from './util/titleUtil';
import VeruvAddonButton from './components/VeruvAddonButton';
import ReactGA from 'react-ga';
import { Link } from 'react-router-dom';
import ProofOfFunds from './usecase/ProofOfFunds';
import { UseCase } from './usecase/UseCaseUtil';
import IncomeVerification from './usecase/IncomeVerification';
import TranscriptGmail from './usecase/TranscriptGmail';
import TranscriptFacebook from './usecase/TranscriptFacebook';
import GoogleAnalytics from './usecase/GoogleAnalytics';
import AccountOwnership from './usecase/AccountOwnership';

class LandingPage extends Component<AuthenticatorProps> {
  readonly styles = {
    heading: {
      margin: '50px 0px 150px',
    },
    createContent: {
      padding: '24px',
      borderRadius: '54px',
      boxShadow: 'rgba(0, 9, 152, 0.1) 1px 5px 9px 4px',
      background: 'white',
      width: '330px',
      margin: '15px',
      alignSelf: 'baseline',
    },
    exampleTextPrimary: {
      display: 'inline-block',
      fontSize: '18px',
      color: 'black',
      marginLeft: '6px',
      marginRight: '6px',
      whiteSpace: 'nowrap' as 'nowrap',
    },
    exampleTextSecondary: {
      display: 'inline-block',
      fontSize: '14px',
      color: 'rgb(101, 101, 101);',
      marginLeft: '6px',
      marginRight: '6px',
      whiteSpace: 'nowrap' as 'nowrap',
    }
  }
  createUrlRef:HTMLInputElement|undefined = undefined;
  requestUrlRef:HTMLInputElement|undefined = undefined;
  requestNoteRef:HTMLInputElement|undefined = undefined;

  componentDidMount() {
    setTitle();
  }

  render() {
    var spaceHiddenOnXs = (<div className="hidden-xs" style={{marginTop:'64px'}}>&nbsp;</div>);
    return (
      <div>
        <Jumbotron style={{
          ...this.styles.heading,
          overflowX: 'hidden',
          backgroundColor: 'unset',
          padding: '10px',
        }}>
          <img
            style={{
              width:'400px',
              maxWidth:'70%',
            }}
            src='/img/veruv-url.png'
          />
          <h1>
            Certified snapshot of any site.
            <div style={{
              width: '100%',
              whiteSpace: 'nowrap',
            }}>
              Verify&nbsp;
              <AnimCycleContent
                style={{
                  color: '#09009e',
                }}
                radius='360px'
                offsetDegrees={8}
                showTime={1000}
                transitionTime={400}
                stopOnHover={true}
                visibleLayerCount={1}
              >
                {[
                  'domain ownership.', // ownership
                  'private message.', // transcript
                  'bank balance.', // finances
                  'package delivery.',
                  'website analytics.',
                  'social media account.', // ownership
                  'received email.', // transcript
                  'purchase transaction.', // finances
                  'game stats.',
                ]}
              </AnimCycleContent>
            </div>
          </h1>
          <h2 style={{maxWidth: '800px', margin: 'auto'}}>
            Use our online secure browser to access and snapshot your information.
            Share the certified snapshot as a link to establish trust.
          </h2>
        </Jumbotron>
        <HorizontalPanels
          smColCnt={3}
          xsColCnt={1}
          spaceBetweenContent={25}
          maxWidth={1024}
          contents={[
            {description:(
              <div style={{
                maxWidth:'256px',
              }}>
                <h3>Find your info</h3>
                <h5>Use our embedded browser to securely navigate to your information.</h5>
                <CircleImage scale radius='240px' src='/img/browse.png' />
              </div>
            )},
            {description:(
              <div style={{
                maxWidth:'256px',
                margin:'0px auto',
              }}>
                {spaceHiddenOnXs}
                <h3>Redact content</h3>
                <h5>Remove any sensitive information you don't want to share.</h5>
                <CircleImage scale radius='240px' src='/img/edit.png' />
              </div>
            )},
            {description:(
              <div style={{
                maxWidth:'256px',
                marginLeft:'auto',
                marginRight:'unset',
              }}>
                {spaceHiddenOnXs}{spaceHiddenOnXs}
                <h3>Share it</h3>
                <h5>Send your personalized link to others as proof of your information.</h5>
                <CircleImage scale radius='240px' src='/img/share.png' />
              </div>
            )},
        ]} />
        <Hr length='0px' />
        <div style={{
          margin: 'auto',
          display: 'flex',
          flexWrap: 'wrap',
          alignItems: 'center',
          placeContent: 'center',
        }}>
          <div style={{
            ...this.styles.createContent,
          }}>
            <h2>Try it</h2>
            Enter a website to open our secure browser.
            <FormGroup style={{margin: '20px'}}>
              <InputGroup>
                <FormControl
                  name="typeUrl"
                  placeholder="type a URL"
                  type="text"
                  inputRef={ref => this.createUrlRef = ref}
                  onKeyDown={e => {
                    if(e.keyCode === 13) this.handleCreateKeySubmit();
                  }}
                />
                <InputGroup.Button>
                  <Button
                    onClick={e => this.handleCreateKeySubmit()}
                  >Go</Button>
                </InputGroup.Button>
              </InputGroup>
            </FormGroup>
          </div>
          <Hr
            className="hidden-xs"
            vertical={true}
            margins='20px'
            length='200px'
          >Or</Hr>
          <div className="visible-xs" style={{flexBasis: '100%'}}>
            <Hr
              vertical={false}
              margins='20px'
              length='50%'
            >Or</Hr>
          </div>
          <div style={{
            ...this.styles.createContent,
          }}>
            <h2>Open instantly</h2>
            Addon to transfer your current browser session so you don't have to relogin.
            <VeruvAddonButton
              style={{
                margin: '0px auto',
              }}
              trackOnOpen={clientBrowser => {
                ReactGA.event({
                  category: 'landingPage',
                  action: 'openExtension',
                  label: clientBrowser,
                });
              }}
            />
          </div>
        </div>
        <Hr length='0px' />
        <h2>What can I verify?</h2>
        <HorizontalPanels
          xsColCnt={1}
          smColCnt={2}
          lgColCnt={4}
          spaceBetweenContent={96}
          maxWidth={1680}
          contents={[
            {description:
              <div>
                <AnimAnchor>
                  <AnimBubble size='500px' x='100px' y='0px' />
                </AnimAnchor>
                {this.useCaseList([
                  {useCase: TranscriptGmail.useCase, title: 'Notarized Gmail email'},
                  {title: 'Transcript', primary: true},
                  {useCase: TranscriptFacebook.useCase, title: 'Facebook message'},
                ])}
                {this.sampleSnapshot('https://www.messenger.com/t/john.doe', '/img/message.png', '500px', '500px')}
              </div>},
            {description:
              <div>
                {spaceHiddenOnXs}
                {this.useCaseList([
                  {title: 'Receipt'},
                  {useCase: ProofOfFunds.useCase, title: 'Proof of funds'},
                  {title: 'Finances', primary: true},
                  {useCase: IncomeVerification.useCase, title: 'Income verification'},
                  {title: 'Online transaction'},
                  {title: 'Package delivery'},
                ])}
                <AnimAnchor>
                  <AnimBubble size='600px' x='50px' y='0px' />
                </AnimAnchor>
                {this.sampleSnapshot('https://secure.bankofamerica.com/myaccounts/acctDetails.go', '/img/financials.png', '500px', '500px')}
              </div>},
            {description:
              <div>
                {this.useCaseList([
                  {useCase: GoogleAnalytics.useCase, title: 'Google Analytics'},
                  {title: 'Data', primary: true},
                  {title: 'Results'},
                  {title: 'Game stats'},
                ])}
                {this.sampleSnapshot('https://analytics.google.com/analytics/web/', '/img/data.png', '500px', '500px')}
                <AnimAnchor>
                  <AnimBubble size='700px' x='0px' y='0px' />
                </AnimAnchor>
              </div>},
            {description:
              <div>
                {spaceHiddenOnXs}
                <AnimAnchor>
                  <AnimBubble size='300px' x='100px' y='100px' />
                </AnimAnchor>
                {this.useCaseList([
                  {useCase: AccountOwnership.useCase, title: 'Account ownership'},
                  {title: 'Identity', primary: true},
                  {title: 'Photo verification'},
                ])}
                {this.sampleSnapshot('https://www.facebook.com/', '/img/ownership.png', '500px', '500px')}
              </div>},
        ]} />
        <Hr length='0px' margins='100px' />
        <HorizontalPanels
          maxContentWidth={300}
          spaceBetweenContent={150}
          maxWidth={1680}
          xsColCnt={1}
          smColCnt={2}
          lgColCnt={4}
          contents={[
            {title: 'Browser security', description: 'Our secure browser runs inside an isolated container which is torn down after your session ends. We keep your browsing safe.',
              image: {icon: (<SecurityIcon fontSize='large' />)},
              link: {text: "Security details", path: '/about/security', anchor: 'security',
              trackerEvent: {
                category: 'landingPage',
                action: 'click',
                label: 'security',
              }}},
            {title: 'Data privacy', description: 'We take your privacy seriously. We do not store your browsing history nor any passwords. See our privacy for more details.',
              image: {icon: (<PrivacyIcon fontSize='large' />)},
              link: {text: "Privacy Policy", path: '/about/privacy-policy', anchor: 'privacy',
              trackerEvent: {
                category: 'landingPage',
                action: 'click',
                label: 'privacy',
              }}},
            {title: 'Open Source', description: 'For transparency, we have open sourced parts of our system. Additional parts are planned in the future.',
              image: {icon: (<OpenSourceIcon fontSize='large' />)},
              link: {text: "Source code", path: 'https://gitlab.com/smotana', isExternal: true,
              trackerEvent: {
                category: 'landingPage',
                action: 'click',
                label: 'opensource',
              }}},
            // {title: 'Bounty program', description: 'We are operating a bounty program to encourage and reward researchers for disclosing potential vulnerabilities to us.',
            //   image: {icon: (<BountyIcon fontSize='large' />)},
            //   link: {text: "Program details", path: '/about/bug-bounty', anchor: 'bounty',
            //   trackerEvent: {
            //     category: 'landingPage',
            //     action: 'click',
            //     label: 'bounty',
            //   }}},
        ]} />
      </div>
    );
  }

  useCaseList(useCaseList:({useCase:UseCase; title?:string; primary?:boolean}|{useCase?:UseCase; title:string; primary?:boolean})[]) {
    return useCaseList
      .map(item => (
      <div style={item.primary ? this.styles.exampleTextPrimary : this.styles.exampleTextSecondary}>
        {(item.useCase )
          ? (<Link to={'/usecase/' + item.useCase.name}>{item.title || item.useCase.title}</Link>)
          : item.title}
      </div>
    ))
  }

  sampleSnapshot(url:string, imgSrc:string, maxWidth:string, maxHeight:string) {
    return (
      <Panel bsStyle="default" style={{
        width:'100%',
        height:'100%',
        maxWidth: maxWidth,
        maxHeight: maxHeight,
        margin: 'auto',
      }}>
        <Panel.Heading style={{
          padding: '5px 10px',
        }}>
          <Panel.Title style={{
            color: 'rgba(101, 101, 101, 0.8)',
            overflow: 'hidden',
            fontSize: '1em',
          }}>
            {url}
          </Panel.Title>
        </Panel.Heading>
        <Panel.Body style={{
          padding: '0px',
          position: 'relative',
          overflow: 'hidden',
        }}>
          <img src={imgSrc} style={{
            display: 'block',
            maxWidth: '100%',
            maxHeight: '100%',
            opacity: 0.4,
          }} />
          {/* <div style={{
            transform: 'translate(-50%, -50%) rotate(-45deg)',
            fontSize: '64px',
            fontWeight: 'bolder',
            color: '#00000040',
            position: 'absolute',
            top: '50%',
            left: '50%',
          }}>SAMPLE</div> */}
        </Panel.Body>
      </Panel>
    );
  }

  handleCreateKeySubmit() {
    if(!this.createUrlRef){
      return;
    }
    this.props.history.push({
      pathname: '/create',
      state: {
        ...ScrollAnchor.scrollToState('browser'),
        startPage: this.createUrlRef.value,
      }
    });

    ReactGA.event({
      category: 'browser',
      action: 'open',
      label: 'tryIt',
    });
  }
}

export default withAuthentication(LandingPage);
