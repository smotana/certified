import MockWebSocket from './mockWebSocket';
import {
  Verification,
  NavigateType,
} from './serverApi';
import detectBrowserDimensions from '../util/browserDetect';
import TokenManager, { ResourceType } from './tokenmanager';

enum PressType {
  Up = "UP",
  Down = "DOWN",
  Press = "PRESS"
}

interface _BrowserSession {
  url:string;
  userAgent?:string;
  cookies?:any;
  localStorage?:any;
}

interface _ScreenDimensions {
  width:number;
  height:number;
}

function calcScreenDimensions(browserDimensions:ScreenDimensions):ScreenDimensions {
  browserDimensions.width -= 2; // adjust for 2px of borders TODO remove borders when full screen

  // Limit max and min
  var newWidth = Math.max(Math.min(browserDimensions.width, BrowserSocket.BROWSER_WIDTH_MAX), BrowserSocket.BROWSER_WIDTH_MIN);
  var newHeight = Math.max(Math.min(browserDimensions.height, BrowserSocket.BROWSER_HEIGHT_MAX), BrowserSocket.BROWSER_HEIGHT_MIN);
  // Limit width height ratio
  newHeight = Math.max(Math.min(newHeight, Math.floor(newWidth * BrowserSocket.BROWSER_HEIGHT_RATIO_DIFF_MAX)), Math.ceil(newWidth / BrowserSocket.BROWSER_HEIGHT_RATIO_DIFF_MAX));

  return {width: newWidth, height: newHeight};
}

interface _BrowserEventHandler {
  screenDimensions?(dimensions:ScreenDimensions):void;
  browserInQueue?(timeInQueueEstimate:number, timeInQueueEstimateUnit:string, usersInQueue:number):void;
  browserChallenge?(challengeType:string, challengeKey:string, onSolvedCallback:(challengeResult:string)=>void):void;
  browserUserNotify?(bsStyle:string, text:string):void;
  browserAcquired?():void;
  browserNavigated?(url:string):void;

  frame?(xPos:number, yPos:number, width:number, height:number, image:string):void;

  verification?(verification:Verification, requestId:string, verificationId:string):void;

  closed?(reason:string):void;
  error?(reason:string):void;

  loadingChanged?(isLoading:boolean):void;
}

class BrowserSocket {
  /** Also change on server side in SanitizerScreenDimensions.java */
  static readonly BROWSER_WIDTH_MAX:number = 1680;
  static readonly BROWSER_WIDTH_MIN:number = 1024;
  static readonly BROWSER_HEIGHT_MAX:number = 1200;
  static readonly BROWSER_HEIGHT_MIN:number = 768;
  static readonly BROWSER_HEIGHT_RATIO_DIFF_MAX:number = 2;
  static readonly BROWSER_MIN_DIMENSIONS:ScreenDimensions = {width: BrowserSocket.BROWSER_WIDTH_MIN, height: BrowserSocket.BROWSER_HEIGHT_MIN};
  static readonly BROWSER_MAX_DIMENSIONS:ScreenDimensions = {width: BrowserSocket.BROWSER_WIDTH_MAX, height: BrowserSocket.BROWSER_HEIGHT_MAX};
  handlers:BrowserEventHandler[] = [];
  socket?:WebSocket
  browserAcquired:boolean = false;
  /* For now use min dimensions as default, use current browser dimensions with "calcScreenDimensions(detectBrowserDimensions())" */
  minDimensions:ScreenDimensions = BrowserSocket.BROWSER_MIN_DIMENSIONS;
  dimensions:ScreenDimensions = {width: this.minDimensions.width, height: this.minDimensions.height};
  session:BrowserSession = {url: 'https://start.duckduckgo.com'};

  addHandler(handler:BrowserEventHandler) {
    this.handlers.push(handler);
  }

  connect(session?:BrowserSession) {
    this.disconnect("Reconnecting");

    this.handlers.forEach(handler => handler.loadingChanged && handler.loadingChanged(true));

    if(session) {
      session.url = this._normalizeUrl(session.url);
      this.session = session;
      this.handlers.forEach(handler => handler.browserNavigated && handler.browserNavigated(this.session.url));
    }

    this.handlers.forEach(handler => handler.screenDimensions && handler.screenDimensions(this.dimensions));

    if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
      this.socket = new MockWebSocket(this._getWebsocketUrl());
    } else {
      this.socket = new WebSocket(this._getWebsocketUrl());
    }

    this.socket.onopen = () => {
      this._send(this._genBrowserRequest(this.session, this.dimensions));
    };
    this.socket.onclose = (event) => {
      if(event.reason) {
        this.handlers.forEach(handler => handler.closed && handler.closed(event.reason));
      } else if(event.code) {
        this.handlers.forEach(handler => handler.closed && handler.closed(`${this._getWebsocketReasonForCode(event.code)} (Code #${event.code})`));
      } else {
        this.handlers.forEach(handler => handler.closed && handler.closed("Disconnected"));
      }
      this.handlers.forEach(handler => handler.loadingChanged && handler.loadingChanged(false));
    };
    this.socket.onerror = (event) => {
      console.log("Websocket error", event);
      this.handlers.forEach(handler => handler.error && handler.error("Connection to server received error"));
    };
    this.socket.onmessage = (event) => {
      var msg = JSON.parse(event.data);
      console.log('RECV:', msg)
      this._handleMessage(msg);
    };
  }

  getDimensions():ScreenDimensions {
    return this.dimensions;
  }

  canChangeDimensions():boolean {
    return BrowserSocket.BROWSER_MAX_DIMENSIONS.width !== this.minDimensions.width
      || BrowserSocket.BROWSER_MAX_DIMENSIONS.height !== this.minDimensions.height;
  }

  minimizeDimensions() {
    this._send(this._genScreenResize(this.minDimensions));
  }

  maximizeDimensions() {
    this._send(this._genScreenResize(BrowserSocket.BROWSER_MAX_DIMENSIONS));
  }

  isConnected() {
    return this.socket && this.socket.readyState == WebSocket.OPEN;
  }

  isBrowserReady() {
    return this.isConnected() && this.browserAcquired;
  }

  ping() {
    this._send(this._genPing());
  }

  navigateUrl(url:string) {
    if(!this.isBrowserReady()) {
      return;
    }
    this.handlers.forEach(handler => handler.loadingChanged && handler.loadingChanged(true));
    this.session.url = this._normalizeUrl(url);
    this._send(this._genActionNavigateUrl(this.session.url));
  }

  navigate(navigateType:NavigateType) {
    if(!this.isBrowserReady()) {
      return;
    }
    this._send(this._genActionNavigate(navigateType));
  }

  mouseMove(x:number, y:number) {
    if(!this.isBrowserReady()) {
      return;
    }
    this._send(this._genActionMouseMove(x, y));
  }

  mouseClick(x:number, y:number, doubleClick:boolean) {
    if(!this.isBrowserReady()) {
      return;
    }
    this._send(this._genActionMouseClick(x, y, doubleClick));
  }

  keyboardPrint(charCode:number[]) {
    if(!this.isBrowserReady()) {
      return;
    }
    this._send(this._genActionKeyboardKeyPrint(charCode));
  }

  keyboardPress(keyCode:number, pressType:PressType) {
    if(!this.isBrowserReady()) {
      return;
    }
    this._send(this._genActionKeyboardKeyPress(keyCode, pressType));
  }

  verificationRequest(tokenOpt:string|undefined) {
    if(!this.isBrowserReady()) {
      return;
    }
    this._send(this._genVerificationRequest(tokenOpt));
  }

  disconnect(reason:string) {
    if(this.socket) {
      if(this.socket.readyState == WebSocket.OPEN) {
        this._send(this._genClosed("Client disconnected"));
        this.handlers.forEach(handler => handler.closed && handler.closed(reason));
      }
      this.socket.close();
      this.socket.onopen = null;
      this.socket.onclose = null;
      this.socket.onerror = null;
      this.socket.onmessage = null;
      this.handlers.forEach(handler => handler.loadingChanged && handler.loadingChanged(false));
      this.browserAcquired = false;
      this.socket = undefined;
    }
  }

  _normalizeUrl(url:string) {
    if(!url.startsWith('http://') && !url.startsWith('https://')){
      url = 'http://' + url;
    }
    return url;
  }

  _handleMessage(msg:any) {
    switch (msg.__TYPE__) {
      case 'Ping':
        this._send(this._genPong());
        break;
      case 'Pong':
        console.log("Received ping from server");
        break;
      case 'VerificationCreated':
        if(msg.token) {
          TokenManager.getInstance().updateResourceToken(ResourceType.SNAPSHOT, msg.verificationId, msg.token);
        }
        this.handlers.forEach(handler => handler.verification && handler.verification(msg.verification, msg.requestId, msg.verificationId));
        break;
      case 'Frame':
        var dimensionsChanged = false;
        if(msg.sw || this.dimensions.width < msg.x + msg.w) {
          this.dimensions.width = Math.max(msg.sw || null, msg.x + msg.w);
          dimensionsChanged = true;
        }
        if(msg.sh || this.dimensions.height < msg.y + msg.h) {
          this.dimensions.height = Math.max(msg.sh || null, msg.y + msg.h);
          dimensionsChanged = true;
        }
        if(dimensionsChanged) {
          this.handlers.forEach(handler => handler.screenDimensions && handler.screenDimensions(this.dimensions));
        }
        this.handlers.forEach(handler => handler.frame && handler.frame(msg.x, msg.y, msg.w, msg.h, msg.d));
        break;
      case 'BrowserInQueue':
        this.handlers.forEach(handler => handler.browserInQueue && handler.browserInQueue(msg.timeInQueueEstimate, msg.timeInQueueEstimateUnit, msg.usersInQueue));
        break;
      case 'BrowserUserNotify':
        this.handlers.forEach(handler => handler.browserUserNotify && handler.browserUserNotify(msg.type, msg.text));
        break;
      case 'BrowserAcquired':
        this.browserAcquired = true;
        this.handlers.forEach(handler => handler.browserAcquired && handler.browserAcquired());
        break;
      case 'RequestChallenge':
        this.handlers.forEach(handler => handler.browserChallenge && handler.browserChallenge(msg.type, msg.key, (challengeResult:string) => {
          this._send(this._genBrowserRequest(this.session, this.dimensions, {
            type: msg.type,
            result: challengeResult,
          }));
        }));
        break;
      case 'BrowserReadyState':
        this.handlers.forEach(handler => handler.loadingChanged && handler.loadingChanged(!msg.isComplete));
        break;
      case 'BrowserNavigated':
        this.session.url = msg.url;
        this.handlers.forEach(handler => handler.browserNavigated && handler.browserNavigated(msg.url));
        break;
      case 'Error':
        this.handlers.forEach(handler => handler.error && handler.error(msg.errorText));
        break;
      default:
        console.log("Unknown message from server:", msg);
        this.disconnect("Communication error");
        break;
    }
  }

  _send(msg:any) {
    console.log('SEND:', msg)
    if(!this.socket || this.socket.readyState != WebSocket.OPEN) {
      throw new Error("Not connected");
    }
    this.socket.send(JSON.stringify(msg));
  }

  _getWebsocketReasonForCode(code:number) {
    switch(code) {
      case 1000:
        return "Disconnected";
      case 1001:
        return "Server shutting down";
      case 1012:
        return "Server restarting";
      case 1013:
        return "Server overloaded, try again later";
      case 1002:
      case 1003:
      case 1004:
      case 1005:
      case 1006:
      case 1007:
      case 1008:
      case 1009:
      case 1010:
      case 1011:
      case 1015:
      default:
        return "Communication error";
    }
  }

  _getWebsocketUrl():string {
    var location = window.location
    var url;
    if (location.protocol === "https:") {
      url = "wss:";
    } else {
      url = "ws:";
    }
    url += "//" + location.host + "/api/browser";
    return url;
  }

  _genBrowserRequest(session:BrowserSession, screenDimensions:ScreenDimensions, challenge?:{type:string, result:string}) {
    return {
      __TYPE__: 'BrowserRequest',
      startUrl: session.url,
      screenWidth: screenDimensions.width,
      screenHeight: screenDimensions.height,
      captchaType: challenge && challenge.type,
      captchaResult: challenge && challenge.result,
      userAgent: session.userAgent && session.userAgent,
      cookies: session.cookies && session.cookies,
      localStorage: session.localStorage && session.localStorage,
    };
  }

  _genError(errorText:string) {
    return {
      __TYPE__: 'Error',
      errorText: errorText,
    };
  }

  _genClosed(reason:string) {
    return {
      __TYPE__: 'Closed',
      reason: reason,
    };
  }

  _genPing() {
    return {
      __TYPE__: 'Ping',
    };
  }

  _genPong() {
    return {
      __TYPE__: 'Pong',
    };
  }

  _genScreenResize(screenDimensions:ScreenDimensions) {
    return {
      __TYPE__: 'ActionScreenResize',
      screenWidth: screenDimensions.width,
      screenHeight: screenDimensions.height,
    };
  }

  _genActionNavigateUrl(url:string) {
    return {
      __TYPE__: 'ActionNavigateUrl',
      url: url,
    };
  }

  _genActionNavigate(navigateType:NavigateType) {
    return {
      __TYPE__: 'ActionNavigate',
      navigateType: navigateType,
    };
  }

  _genActionMouseMove(x:number, y:number) {
    return {
      __TYPE__: 'ActionMouseMove',
      x: x,
      y: y,
    };
  }

  _genActionMouseClick(x:number, y:number, doubleClick:boolean) {
    return {
      __TYPE__: 'ActionMouseClick',
      x: x,
      y: y,
      doubleClick: doubleClick,
    };
  }

  _genActionKeyboardKeyPrint(charCode:number[]) {
    return {
      __TYPE__: 'ActionKeyboardKeyPrint',
      c: charCode
    };
  }

  _genActionKeyboardKeyPress(keyCode:number, pressType:PressType) {
    return {
      __TYPE__: 'ActionKeyboardKeyPress',
      k: keyCode,
      t: pressType
    };
  }

  _genVerificationRequest(tokenOpt:string|undefined) {
    return {
      __TYPE__: 'VerificationRequest',
      token: tokenOpt,
    };
  }
}

export type BrowserEventHandler = _BrowserEventHandler;
export type BrowserSession = _BrowserSession;
export type ScreenDimensions = _ScreenDimensions;
export {
  BrowserSocket,
  PressType
};
