import mockSnapshot from './mockSnapshot';

class MockWebSocket implements WebSocket {
  readonly LATENCY = 300;
  CLOSED: number = WebSocket.CLOSED;
  CLOSING: number = WebSocket.CLOSING;
  CONNECTING: number = WebSocket.CONNECTING;
  OPEN: number = WebSocket.OPEN;
  PIXEL_RED: string = 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8z8DwHwAFBQIAX8jx0gAAAABJRU5ErkJggg==';
  PIXEL_GREEN: string = 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+M/wHwAEBgIApD5fRAAAAABJRU5ErkJggg==';
  PIXEL_WHITE: string = 'R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';

  onclose: ((this: WebSocket, ev: CloseEvent) => any) | null = null;
  onerror: ((this: WebSocket, ev: Event) => any) | null = null;
  onmessage: ((this: WebSocket, ev: MessageEvent) => any) | null = null;
  onopen: ((this: WebSocket, ev: Event) => any) | null = null;

  static firstTimeOpening:boolean = true;
  binaryType;
  bufferedAmount;
  extensions;
  protocol;
  url;
  readyState: number = this.CLOSED;

  constructor(url:string) {
    setTimeout(() => {
      this.readyState = this.OPEN;
      this.onopen && this.onopen(new Event("open"));
    }, 100);
  }

  close(code?: number | undefined, reason?: string | undefined): void {
    console.log("Closing connection");
  }

  send(msgStr: any): void {
    if(this.readyState != this.OPEN) {
      throw new Error("Sending msg to server when disconnected");
    }
    var msg = JSON.parse(msgStr);
    switch (msg.__TYPE__) {
      case 'Ping':
        this._sendToClient("Pong");
        break;
      case 'Pong':
        console.log("Received ping from client");
        break;
      case 'Closed':
        this.readyState = this.CLOSED;
        break;
      case 'BrowserRequest':
        if(!MockWebSocket.firstTimeOpening) {
          if(msg.captchaType !== 'RECAPTCHA_V2' || !msg.captchaResult){
            // Challenge on subsequent times
            this._sendToClient("RequestChallenge", {
              type: 'RECAPTCHA_V2',
              key: '6Lcran4UAAAAADDeMJaDUJ5aQX5TaEKEFK6h1bN4',
            });
            break;
          }
        }
        MockWebSocket.firstTimeOpening = false;
        setTimeout(() => {
          this._sendToClient("BrowserInQueue", {
            timeInQueueEstimate: 0,
            timeInQueueEstimateUnit: 'SECONDS',
            usersInQueue: 0
          });
        }, 500);
        setTimeout(() => {
          this._sendToClient("BrowserAcquired", {expiryTimeRemaining: 30000});
        }, 1000);
        setTimeout(() => {
          this._sendFrame(0, 0, msg.screenWidth, msg.screenHeight, this.PIXEL_WHITE, msg.screenWidth, msg.screenHeight);
        }, 1100);
        setTimeout(() => {
          this._sendToClient("BrowserReadyState", {isComplete: true});
        }, 1120);
        break;
      case 'ActionNavigateUrl':
        setTimeout(() => {
          this._sendToClient("BrowserNavigated", {url: msg.url});
        }, 2000);
        break;
      case 'ActionMouseMove':
        this._sendFrame(msg.x-1, msg.y-1, 3, 3, this.PIXEL_RED);
        break;
      case 'ActionMouseClick':
        this._sendFrame(msg.x-4, msg.y-4, 9, 9, this.PIXEL_GREEN);
        break;
      case 'ActionKeyboardKeyPress':
        console.log("Keyboard key press:", msg);
        break;
      case 'ActionKeyboardKeyPrint':
        console.log("Keyboard key print:", msg);
        break;
      case 'ActionScreenResize':
        console.log("Screen resize:", msg);
        this._sendFrame(0, 0, msg.screenWidth, msg.screenHeight, this.PIXEL_WHITE, msg.screenWidth, msg.screenHeight);
        break;
      case 'VerificationRequest':
        var snapshot = mockSnapshot();
        this._sendToClient("VerificationCreated", {verification: snapshot, requestId: msg.requestId, verificationId: snapshot.id});
        break;
      case 'Error':
        console.log("Error from client: ", msg.errorText);
        break;
      default:
        console.log("Unknown message from client:", msg);
        this.readyState = this.CLOSED;
        break;
    }
  }

  addEventListener(type: any, listener: any, options?: any) {
    throw new Error("Method not implemented.");
  }

  removeEventListener(type: any, listener: any, options?: any) {
    throw new Error("Method not implemented.");
  }

  dispatchEvent(event: Event): boolean {
    throw new Error("Method not implemented.");
  }

  _sendFrame(xPos:number, yPos:number, width:number, height:number, img:string=this.PIXEL_GREEN, screenWidth:number|undefined=undefined, screenHeight:number|undefined=undefined) {
    setTimeout(() => {
      this._sendToClient("Frame", {
        x: xPos,
        y: yPos,
        w: width,
        h: height,
        d: img,
        sw: screenWidth,
        sh: screenHeight,
      });
    }, 100);
  }

  async _sendToClient(type:string, props:any = {}) {
    await new Promise(resolve => setTimeout(resolve, this.LATENCY));
    props.__TYPE__ = type;
    this.onmessage && this.onmessage({
      data: JSON.stringify(props),
      lastEventId: '',
      origin: '',
      ports: [],
      source: null,
      bubbles: false,
      cancelBubble: false,
      cancelable: false,
      composed: false,
      currentTarget: null,
      defaultPrevented: false,
      eventPhase: 0,
      isTrusted: false,
      returnValue: false,
      srcElement: null,
      target: null,
      timeStamp: 0,
      type: '',
      composedPath: () => [],
      initEvent: () => {},
      preventDefault: () => {},
      stopImmediatePropagation: () => {},
      stopPropagation: () => {},
      AT_TARGET: 0,
      BUBBLING_PHASE: 0,
      CAPTURING_PHASE: 0,
      NONE: 0,
    });
  }
}

export default MockWebSocket;