import { Server, Message, AccountInfo, ErrorResponse, Visibility, Verification } from './serverApi';
import mockSnapshot from './mockSnapshot';
import randomUuid from '../util/uuid';
import { PurchaseResult, Product } from '../payment/paymentApi';

class MockServer implements Server {
  readonly LATENCY = 300;
  readonly snapshots:{[id:string]: Verification} = {
    'a': mockSnapshot('a', true),
    'b': mockSnapshot('b', true),
    'c': mockSnapshot('c', false),
    'd': mockSnapshot('d', false),
    'e': mockSnapshot('e', false, Visibility.PASSWORD),
  };

  constructor(apiUrl:string) {
  }

  async send(msg:Message):Promise<any | null> {
    console.log('SEND:', msg);
    await new Promise(resolve => setTimeout(resolve, this.LATENCY));
    var result = this.process(msg);
    if(result) {
      console.log('RECV:', result);
    }
    return result;
  }

  process(msg:Message):any|null {
    if(msg.subApi === '/verification/get') {
      var snapshot:Verification = this.snapshots[msg.body.verificationId];
      if(!snapshot) {
        throw new ErrorResponse(404, 'not-found', 'Not found');
      }
      if(snapshot.visibility === Visibility.PASSWORD
        && msg.body.passOpt !== "a") {
          throw new ErrorResponse(403, 'unauthorized-password-required', 'Password is required for access');
        }
      return snapshot;
    }
    if(msg.subApi === '/verification/applyEdits') {
      return mockSnapshot(msg.body.verificationId, true);
    }
    if(msg.subApi === '/verification/getMySnapshots') {
      return {snapshots: Object['values'](this.snapshots)
        .filter(s => s.isMine)};
    }
    if(msg.subApi === '/auth/login'
      || msg.subApi === '/auth/token'
      || msg.subApi === '/auth/register') {
      const accountInfo:AccountInfo = {
        token: randomUuid(),
        email: msg.body.email || 'your_email@example.com',
      };
      return accountInfo;
    }
    if(msg.subApi === '/payment/pay') {
      const purchaseResult:PurchaseResult = {
        referenceId: msg.body.amountInCents > 0 ? randomUuid() : undefined,
        [Product.EXTEND_EXPIRY]: {
          snapshotId: msg.body.metadata.snapshotId,
          expiry: new Date().getTime() + 90*24*60*60*1000,
          canExtendExpiry: false,
        },
      };
      return purchaseResult;
    }
  }
}

export {MockServer};