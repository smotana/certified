import React from 'react';
import {
  ServerApi,
  Server,
  Verification,
  Visibility,
  SnapshotEdits,
  AccountInfo as _AccountInfo,
  ErrorResponse,
} from './serverApi';
import { MockServer } from './mockServer';
import { combineReducers, Store } from "redux";
import { ProductionServer } from './productionServer';
import CryptoJS from 'crypto-js';
import TokenManager, { ResourceType } from './tokenmanager';
import { PurchaseInfo, PurchaseResult, Product } from '../payment/paymentApi';
import { detectEnv, Environment } from '../util/detectEnv';

enum MySnapshotRequestsAction {
  GET_MY_SNAPSHOT_REQUESTS = 'GET_MY_SNAPSHOT_REQUESTS',
}
enum MySnapshotsAction {
  GET_MY_SNAPSHOTS = 'GET_MY_SNAPSHOTS',
}
enum VerificationAction {
  GET_VERIFICATION = 'GET_VERIFICATION',
  CREATED_VERIFICATION = 'CREATED_VERIFICATION',
  DELETE_VERIFICATION = 'DELETE_VERIFICATION',
  VERIFICATION_SET_VISIBILITY = 'VERIFICATION_SET_VISIBILITY',
  VERIFICATION_APPLY_EDITS = 'VERIFICATION_APPLY_EDITS',
}
enum AuthAction {
  REGISTER_USER = 'REGISTER_USER',
  AUTHENTICATE_USER = 'AUTHENTICATE_USER',
  VERIFY_TOKEN = 'VERIFY_TOKEN',
  LOGOUT = 'LOGOUT',
  UPDATE_INFO = 'UPDATE_INFO',
}
enum PaymentAction {
  PAY = 'PAY',
}

class Action implements ServerApi {
  readonly server:Server;
  readonly store:Store

  constructor(store:Store) {
    this.store = store;
    
    if (detectEnv() !== Environment.DEVELOPMENT_FRONTEND) {
      this.server = new ProductionServer(this._getWebsocketUrl());
    } else {
      this.server = new MockServer(this._getWebsocketUrl());
    }
  }

  static initialState():any {
    var accountInfo = TokenManager.getInstance().getAccountInfo();
    if(!accountInfo) {
      return undefined;
    }
    var initialAuthAction = {
      type: `${AuthAction.REGISTER_USER}_${Status.FULFILLED}`,
      payload: accountInfo,
    };
    return reducers(undefined, initialAuthAction);
  }

  /*
   * Verification
   */

  getVerification(id:string, pass:string|undefined=undefined):Promise<Verification> {
    return this._dispatch({
      type: VerificationAction.GET_VERIFICATION,
      meta: {verificationId: id},
      payload: this.server.send({
        subApi: `/verification/get`,
        method: 'POST',
        body: {
          __TYPE__: 'VerificationGet',
          verificationId: id,
          token: TokenManager.getInstance().getResourceOrUserToken(ResourceType.SNAPSHOT, id),
          passOpt: pass && this._saltHashPassword(pass),
        }
      }),
    });
  }

  getMySnapshots():Promise<Verification[]> {
    return this._dispatch({
      type: MySnapshotsAction.GET_MY_SNAPSHOTS,
      payload: this.server.send({
        subApi: `/verification/getMySnapshots`,
        method: 'POST',
        body: {
          __TYPE__: 'VerificationGetMine',
          token: TokenManager.getInstance().getUserToken(),
          snapshotTokens: TokenManager.getInstance().getAllResourceTokens(ResourceType.SNAPSHOT)
        }
      }),
    })
    .then(verificationGetMineResponse => {
      if(verificationGetMineResponse && verificationGetMineResponse.removeTokens) {
        verificationGetMineResponse.removeTokens.forEach(tokenStr => {
          TokenManager.getInstance().removeResourceTokenByToken(ResourceType.SNAPSHOT, tokenStr);
        });
      }
      return verificationGetMineResponse;
    });
  }

  /**
   * This is a special case where we want to populate the store
   * with an action that was created in the BrowserApi initially.
   */
  createdVerification(id:string, verification:Verification):Promise<Verification> {
    return this._dispatch({
      type: VerificationAction.CREATED_VERIFICATION,
      meta: {
        verificationId: id
      },
      payload: Promise.resolve(verification),
    });
  }

  deleteVerification(id:string):Promise<void> {
    return this._dispatch({
      type: VerificationAction.DELETE_VERIFICATION,
      meta: {verificationId: id},
      payload: this.server.send({
        subApi: '/verification/delete',
        method: 'POST',
        body: {
          __TYPE__: 'VerificationDelete',
          verificationId: id,
          token: TokenManager.getInstance().getResourceOrUserToken(ResourceType.SNAPSHOT, id),
        }
      }),
    });
  }

  setVisibility(id:string, visibility:Visibility, passOpt?:string):Promise<void> {
    return this._dispatch({
      type: VerificationAction.VERIFICATION_SET_VISIBILITY,
      meta: {
        verificationId: id,
        visibility: visibility
      },
      payload: this.server.send({
        subApi: '/verification/setVisibility',
        method: 'POST',
        body: {
          __TYPE__: 'VerificationUpdateVisibility',
          verificationId: id,
          visibility: visibility,
          token: TokenManager.getInstance().getResourceOrUserToken(ResourceType.SNAPSHOT, id),
          pass: passOpt && this._saltHashPassword(passOpt),
        }
      }),
    });
  }

  applyEdits(id:string, edits:SnapshotEdits):Promise<Verification> {
    return this._dispatch({
      type: VerificationAction.VERIFICATION_APPLY_EDITS,
      meta: {verificationId: id},
      payload: this.server.send({
        subApi: '/verification/applyEdits',
        method: 'POST',
        body: {
          __TYPE__: 'VerificationApplyEdits',
          verificationId: id,
          crop: edits.crop,
          redactions: edits.redactions,
          note: edits.note,
          token: TokenManager.getInstance().getResourceOrUserToken(ResourceType.SNAPSHOT, id),
        }
      }),
    });
  }

  /*
   * Auth
   */

  register(email:string, password:string):Promise<AccountInfo> {
    return this._dispatch({
      type: AuthAction.REGISTER_USER,
      payload: this.server.send({
        subApi: `/auth/register`,
        method: 'POST',
        body: {
          __TYPE__: 'Register',
          email: email,
          passwordSaltedHashed: this._saltHashPassword(password),
        }
      }),
    })
    .then(accountInfo => {
      TokenManager.getInstance().updateAccountInfo(accountInfo);

      // Refresh snapshots, also transfer ownership of any snapshots created while not logged in
      this.getMySnapshots();

      return accountInfo;
    });
  }

  login(email:string, password:string):Promise<AccountInfo> {
    return this._dispatch({
      type: AuthAction.AUTHENTICATE_USER,
      payload: this.server.send({
        subApi: `/auth/login`,
        method: 'POST',
        body: {
          __TYPE__: 'Login',
          email: email,
          passwordSaltedHashed: this._saltHashPassword(password),
        }
      }),
    })
    .then(accountInfo => {
      TokenManager.getInstance().updateAccountInfo(accountInfo);

      // Refresh snapshots, also transfer ownership of any snapshots created while not logged in
      this.getMySnapshots();

      return accountInfo;
    });
  }

  verifyToken():Promise<AccountInfo|undefined> {
    var token = this.store.getState().user.token;
    if(!token) {
      return Promise.resolve(undefined);
    }
    return this._dispatch({
      type: AuthAction.VERIFY_TOKEN,
      payload: this.server.send({
        subApi: `/auth/token`,
        method: 'POST',
        body: {
          __TYPE__: 'VerifyToken',
          token: token,
        }
      }),
    })
    .catch((err) => {
      if(err instanceof ErrorResponse
        && err.httpCode === 403) {
        return this.logout();
      }
      throw err;
    })
    .then(accountInfo => {
      TokenManager.getInstance().updateAccountInfo(accountInfo);
      return accountInfo;
    });
  }

  changeEmail(token:string, newEmail:string):Promise<void> {
    return this._dispatch({
      type: AuthAction.UPDATE_INFO,
      meta: {
        email: newEmail,
      },
      payload: this.server.send({
        subApi: `/auth/email/update`,
        method: 'POST',
        body: {
          __TYPE__: 'EmailUpdate',
          token: token,
          newEmail: newEmail, 
        }
      }),
    });
  }

  changePass(token:string, newPass:string):Promise<void> {
    return this._dispatch({
      type: AuthAction.UPDATE_INFO,
      meta: {/* Nothing to update */},
      payload: this.server.send({
        subApi: `/auth/pass/update`,
        method: 'POST',
        body: {
          __TYPE__: 'PassUpdate',
          token: token,
          newPass: this._saltHashPassword(newPass),
        }
      }),
    });
  }

  logout():Promise<void> {
    return this._dispatch({
      type: AuthAction.LOGOUT,
    })
    .then(() => {
      TokenManager.getInstance().removeAccountInfo();
    });
  }

  /*
   * Payment
   */

  pay(purchaseInfo:PurchaseInfo, paymentToken?:stripe.Token):Promise<PurchaseResult> {
    var token:string|undefined = undefined;
    switch(purchaseInfo.product) {
      case Product.EXTEND_EXPIRY:
        var snapshotId = purchaseInfo.metadata!['snapshotId'];
        token = TokenManager.getInstance().getResourceOrUserToken(ResourceType.SNAPSHOT, snapshotId);
        break;
    }

    return this._dispatch({
      type: PaymentAction.PAY,
      payload: this.server.send({
        subApi: `/payment/pay`,
        method: 'POST',
        body: {
          __TYPE__: 'Payment',
          product: purchaseInfo.product,
          paymentToken: paymentToken && paymentToken.id,
          amountInCents: purchaseInfo.amountInCents,
          currency: purchaseInfo.currency,
          token: token,
          metadata: purchaseInfo.metadata,
        }
      }),
    });
  }

  /*
   * Other
   */

  ping():Promise<void> {
    return this._dispatch({
      type: 'PING',
      payload: {
        subApi: '/ping',
        method: 'GET'
      },
    });
  }

  async _dispatch(msg:any):Promise<any>{
    var result = await this.store.dispatch(msg);
    return result.value;
  }

  _getWebsocketUrl():string {
    var location = window.location
    var url = location.protocol + "//" + location.host + "/api";
    return url;
  }

  /**
   * Warning: changing this method will result in everyone's passwords to change
   */
  _saltHashPassword(password:string):string {
    var hash = CryptoJS.SHA512(password + "salt:2B0ACD390BB44D1DA0E9E0A42B4FBEAD");
    var hashStr = hash.toString(CryptoJS.enc.Base64);
    return hashStr;
  }
}

function reducerMySnapshots(state = {
  status: Status.NONE,
  ids: [],
  createdIds: [],
}, action) {
  switch (action.type) {
    case `${MySnapshotsAction.GET_MY_SNAPSHOTS}_${Status.REJECTED}`:
      return {
        ...state,
        status: Status.REJECTED
      };
    case `${MySnapshotsAction.GET_MY_SNAPSHOTS}_${Status.PENDING}`:
      return {
        ...state,
        status: Status.PENDING
      };
    case `${VerificationAction.GET_VERIFICATION}_${Status.FULFILLED}`:
      if(action.payload.isMine === true) {
        return {
          ...state,
          ids: [...state.createdIds, action.meta.verificationId],
        };
      }
      return state;
    case `${VerificationAction.CREATED_VERIFICATION}_${Status.FULFILLED}`:
      return {
        ...state,
        ids: [...state.createdIds, action.meta.verificationId],
      };
    case `${MySnapshotsAction.GET_MY_SNAPSHOTS}_${Status.FULFILLED}`:
      return {
        ...state,
        status: Status.FULFILLED,
        ids: action.payload.snapshots.map(snapshot => snapshot.id)
      };
    case `${VerificationAction.DELETE_VERIFICATION}_${Status.FULFILLED}`:
      return {
        ...state,
        ids: state.ids.filter(id => id !== action.meta.verificationId),
      };
default:
      return state;
  }
}

enum Status {
  PENDING = 'PENDING',
  FULFILLED = 'FULFILLED',
  REJECTED = 'REJECTED',
  // Custom Statuses
  NOTFOUND = 'NOTFOUND',
  PASSWORD = 'PASSWORD',
  NONE = 'NONE',
}
function reducerVerificationById(state = {}, action) {
  switch (action.type) {
    case `${VerificationAction.GET_VERIFICATION}_${Status.REJECTED}`:
    case `${VerificationAction.CREATED_VERIFICATION}_${Status.REJECTED}`:
      var status;
      switch(action.payload.code) {
        case 'not-found':
          status = Status.NOTFOUND;
          break;
        case 'unauthorized-password-required':
          status = Status.PASSWORD;
          break;
        default:
          status = Status.REJECTED;
          break;
      }
      return {...state, ...{
        [action.meta.verificationId]: {status: status}
      }};
    case `${VerificationAction.GET_VERIFICATION}_${Status.PENDING}`:
    case `${VerificationAction.CREATED_VERIFICATION}_${Status.PENDING}`:
      return {...state, ...{
        [action.meta.verificationId]: {status: Status.PENDING}
      }};
    case `${VerificationAction.GET_VERIFICATION}_${Status.FULFILLED}`:
    case `${VerificationAction.CREATED_VERIFICATION}_${Status.FULFILLED}`:
    case `${VerificationAction.VERIFICATION_APPLY_EDITS}_${Status.FULFILLED}`:
      return {...state, ...{
        [action.meta.verificationId]: {
          ...{status: Status.FULFILLED},
          ...action.payload}
      }};
    case `${MySnapshotsAction.GET_MY_SNAPSHOTS}_${Status.FULFILLED}`:
      var snapshots = {};
      action.payload.snapshots.forEach(snapshot => {
        snapshots[snapshot.id] = {
          ...{status: Status.FULFILLED},
          ...snapshot};
      });
      return {...state, ...snapshots};
    case `${VerificationAction.DELETE_VERIFICATION}_${Status.FULFILLED}`:
      return {...state, ...{
        [action.meta.verificationId]: {status: Status.NONE}
      }};
    case `${VerificationAction.VERIFICATION_SET_VISIBILITY}_${Status.FULFILLED}`:
      return {...state, ...{
        [action.meta.verificationId]: {
          ...state[action.meta.verificationId],
          ...{visibility: action.meta.visibility}
      }}};
    case `${PaymentAction.PAY}_${Status.FULFILLED}`:
      var extendExpiryResult = (action.payload as PurchaseResult)[Product.EXTEND_EXPIRY];
      if(extendExpiryResult) {
        return {...state, ...{
          [extendExpiryResult.snapshotId]: {
            ...state[extendExpiryResult.snapshotId],
            ...{expiry: extendExpiryResult.expiry},
            ...{canExtendExpiry: extendExpiryResult.canExtendExpiry}
        }}};
      } else {
        return state;
      }
  default:
      return state;
  }
}

enum AuthState {
  // User not authenticated
  NONE = 'NONE',
  IN_PROGRESS = 'IN_PROGRESS',
  AUTHENTICATION_FAILED = 'AUTHENTICATION_FAILED',
  AUTHENTICATED = 'AUTHENTICATED',
}
const reducerAuthDefault = {
  state: AuthState.NONE,
}
function reducerAuth(state = reducerAuthDefault, action) {
  switch (action.type) {
    case `${AuthAction.AUTHENTICATE_USER}_${Status.PENDING}`:
    case `${AuthAction.REGISTER_USER}_${Status.PENDING}`:
      return {state: AuthState.IN_PROGRESS};
    case `${AuthAction.REGISTER_USER}_${Status.FULFILLED}`:
    case `${AuthAction.AUTHENTICATE_USER}_${Status.FULFILLED}`:
    case `${AuthAction.VERIFY_TOKEN}_${Status.FULFILLED}`:
      return {
        ...action.payload,
        state: AuthState.AUTHENTICATED,
      };
    case `${AuthAction.UPDATE_INFO}_${Status.PENDING}`:
      return {
        ...state,
        updateInProgress: true,
      };
    case `${AuthAction.UPDATE_INFO}_${Status.REJECTED}`:
      return {
        ...state,
        updateInProgress: false,
      };
    case `${AuthAction.UPDATE_INFO}_${Status.FULFILLED}`:
      return {
        ...state,
        updateInProgress: false,
        ...action.meta,
      };
    case `${AuthAction.REGISTER_USER}_${Status.REJECTED}`:
    case `${AuthAction.AUTHENTICATE_USER}_${Status.REJECTED}`:
    case `${AuthAction.VERIFY_TOKEN}_${Status.REJECTED}`:
      return {
        error: action.payload.error,
        state: AuthState.AUTHENTICATION_FAILED,
      };
    case AuthAction.LOGOUT:
      return reducerAuthDefault;
    default:
      return state
  }
}


const reducers = combineReducers({
    mySnapshots: reducerMySnapshots,
    verificationById: reducerVerificationById,
    user: reducerAuth,
});

const dispatchWithPromise = (dispatch, action) => {
  dispatch(action);
  return action.payload;
}

class ActionRef {
  refPromise:Promise<Action>;
  refResolve:((val:Action)=>void)|undefined = undefined;
  ref?:Action;

  constructor() {
    this.refPromise = new Promise(resolve => {
      this.refResolve = resolve;
    })
  }

  get():Action {
    return this.ref!;
  }

  getPromise():Promise<Action> {
    return this.refPromise;
  }

  set(action:Action) {
    this.ref = action;
    this.refResolve && this.refResolve(action);
  }
}
var actionRef = new ActionRef();

export type AccountInfo = _AccountInfo
export {
  Action,
  actionRef,
  Status,
  reducers,
  dispatchWithPromise,
  AuthState,
};
