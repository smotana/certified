import { Message, Server, ErrorResponse } from "./serverApi";
// window.fetch polyfill
import 'whatwg-fetch'

class ProductionServer implements Server {
  apiUrl: string;

  constructor(apiUrl:string) {
    this.apiUrl = apiUrl;
  }

  async send(msg:Message): Promise<any | null> {
    var response:Response = await fetch(this.apiUrl + msg.subApi, {
      method: msg.method || 'GET',
      body: JSON.stringify(msg.body)
    });
    if(!response.ok) {
      var errorMsg;
      try {
        errorMsg = JSON.parse(await response.text());
      } catch(e) {
        throw new Error(response.statusText);
      }
      throw new ErrorResponse(response.status, errorMsg.code, errorMsg.userFacingMessage);
    }
    if(!response.ok) {
      throw new Error(response.statusText);
    }
    var body = await response.text(); 
    return body.length > 0 ? JSON.parse(body) : undefined;
  }
}

export {ProductionServer};