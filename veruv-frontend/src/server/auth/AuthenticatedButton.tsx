import React, { Component } from 'react';
import withAuthentication, { AuthenticatorProps } from './authenticator';
import {
  Button,
  Popover,
  Overlay,
} from 'react-bootstrap';
import LoginOrRegister from './LoginOrRegister';
import Hr from '../../Hr';

interface Props {
  style?:React.CSSProperties;
  disabled?:boolean;
  onAuthenticatedSubmit?:()=>void;
  buttonText?:string;
  buttonBsStyle?:'primary'|'success'|'info'|'warning'|'danger'|'link';
  overlayPlacement?:'top'|'right'|'bottom'|'left';
}

interface State {
  isOpen:boolean;
}

class AuthenticatedButton extends Component<Props & AuthenticatorProps, State> {
  readonly styles = {
    loginRegisterTitleContainer: {
      display: 'flex',
      justifyContent: 'space-around'
    },
    loginRegisterContainer: {
      display: 'flex',
      justifyContent: 'space-around'
    },
  };
  buttonRef:React.RefObject<Button> = React.createRef();
  loginPasswordRef:HTMLInputElement|undefined = undefined;
  registerPasswordRef:HTMLInputElement|undefined = undefined;
  registerRetypePasswordRef:HTMLInputElement|undefined = undefined;

  constructor(props) {
    super(props);
    
    this.state = {
      isOpen: false,
    }
  }

  render() {
    return (
        <div>
          <Button
            style={this.props.style}
            bsStyle={this.props.buttonBsStyle}
            disabled={this.props.disabled}
            onClick={event => this.handleButtonClick(event)}
            ref={this.buttonRef}
          >
            {this.props.buttonText || 'Login or Register'}
          </Button>
          <Overlay
            show={this.state.isOpen && !this.props.disabled}
            target={this.buttonRef.current || undefined}
            placement={this.props.overlayPlacement || 'bottom'}
            rootClose
            onHide={() => this.setState({isOpen: false})}
          >
            <Popover
              id="popover"
              title={(
                <div style={this.styles.loginRegisterTitleContainer}>
                  <div style={{width: '100%',textAlign: 'center'}}>Login</div>
                  <Hr vertical={true} length='50%' />
                  <div style={{width: '100%',textAlign: 'center'}}>Register</div>
                </div>
              )}
              style={{maxWidth: '100%'}}
            >
            <LoginOrRegister
              onAuthenticated={this.props.onAuthenticatedSubmit}
            />
            </Popover>
          </Overlay>
        </div>
    );
  }

  handleButtonClick(event) {
    if(this.props.isAuthenticated) {
      this.props.onAuthenticatedSubmit && this.props.onAuthenticatedSubmit();
    } else {
      this.setState({isOpen: true});
    }
  }
}

export default withAuthentication(AuthenticatedButton);
