import React, { Component } from 'react';
import withAuthentication, { AuthenticatorProps } from './authenticator';

import AuthenticatedButton from './AuthenticatedButton';
import LoginOrRegister from './LoginOrRegister';
import LocalStorage from '../localstorage';

interface Props {
  style?:React.CSSProperties;
  inline?:boolean;
}

class AuthenticatedBlock extends Component<Props & AuthenticatorProps> {
  localStorage = new LocalStorage();

  render() {
    if(this.props.isAuthenticated) {
      if(this.props.style) {
        return (
          <div style={this.props.style}>
            {this.props.children}
          </div>
        );
      } else {
        return this.props.children;
      }
    } else {
      return (
        <div style={Object.assign({}, styles.root, this.props.style)}>
          <h4 style={{marginBottom: '50px'}}>
            Please login or sign up to continue
          </h4>
          {this.props.inline ? (
            <LoginOrRegister />
          ) : (
            <AuthenticatedButton
              buttonText='Login or Register'
              buttonBsStyle='primary'
              overlayPlacement="bottom"
            />
          )}
        </div>
      );
    }
  }
}

const styles = {
  root: {
    textAlign: 'center',
  },
};

export default withAuthentication(AuthenticatedBlock);
