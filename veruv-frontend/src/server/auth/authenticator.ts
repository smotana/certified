import { connect } from 'react-redux';
import { actionRef, AuthState, AccountInfo } from '../action';
import { withRouter, RouteComponentProps } from 'react-router-dom';

interface Props {
  isAuthenticated:boolean;
  state:AuthState;
  token?:string;
  displayName:string;
  email?:string;
}

interface Funs {
  login:(email:string, password:string) => Promise<AccountInfo>,
  register:(email:string, password:string) => Promise<AccountInfo>,
  logout:() => Promise<void>,
}

interface _AuthenticatorProps extends RouteComponentProps, Props, Funs {}

const mapStateToProps = (state, ownProps) => {
  return {
    isAuthenticated: state.user.state === AuthState.AUTHENTICATED,
    state: state.user.state,
    token: state.user.token,
    displayName: state.user.email || 'Anonymous',
    email: state.user.email,
  } as Props;
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    register: (email, password) => {
      return actionRef.get().register(email, password);
    },
    login: (email, password) => {
      return actionRef.get().login(email, password);
    },
    logout: () => {
      return actionRef.get().logout();
    },
  } as Funs;
}

function getDisplayName(Child) {
  return Child.displayName || Child.name || 'Unknown';
}

export type AuthenticatorProps = _AuthenticatorProps;
export default function withAuthentication(Child) {
  let WithAuthentication = withRouter(connect(mapStateToProps, mapDispatchToProps)(Child));
  WithAuthentication.displayName = `${getDisplayName(Child)}(WithAuthentication)`;
  return WithAuthentication;
}
