import React, { Component } from 'react';
import withAuthentication, { AuthenticatorProps } from './authenticator';
import {
  Button,
  Form,
  FormGroup,
  FormControl,
  Collapse,
  Alert,
  Col,
  ControlLabel,
} from 'react-bootstrap';
import Hr from '../../Hr';
import { ErrorResponse } from '../serverApi';
import PasswordStrengthMeterAsync from '../../components/PasswordStrengthMeterAsync';

interface Props {
  style?:React.CSSProperties;
  disabled?:boolean;
  onAuthenticated?:()=>void;
}

interface State {
  isSubmitting:boolean;
  loginSubmitEnabled?:boolean
  registerSubmitEnabled?:boolean
  registerPasswordFocused?:boolean;
}

interface ValidState {
  error?:string;
  loginEmailValid:boolean;
  loginPasswordValid:boolean;
  registerEmailValid:boolean;
  registerPasswordValid:boolean;
  registerRetypePasswordValid:boolean;
}

class LoginOrRegister extends Component<Props & AuthenticatorProps, State & ValidState> {
  /** Do not change requirements without considering backwards compatibility of existing passwords */
  static readonly PASSWORD_MIN_LENGTH = 5;
  readonly styles = {
    loginRegisterContainer: {
      display: 'flex',
      alignItems: 'center',
      alignContent: 'center',
      justifyContent: 'center',
    },
    loginRegister: {
      textAlign: 'center' as 'center',
    },
  };
  loginEmailRef:HTMLInputElement|undefined = undefined;
  loginPasswordRef:HTMLInputElement|undefined = undefined;
  registerEmailRef:HTMLInputElement|undefined = undefined;
  registerPasswordRef:HTMLInputElement|undefined = undefined;
  registerRetypePasswordRef:HTMLInputElement|undefined = undefined;

  constructor(props) {
    super(props);
    
    this.state = {
      isSubmitting: false,
      loginEmailValid: true,
      loginPasswordValid: true,
      registerEmailValid: true,
      registerPasswordValid: true,
      registerRetypePasswordValid: true,
    }
  }

  render() {
    return this.props.isAuthenticated ? (
      <div>Logged in as {this.props.email}</div>
    ) : (
      <div>
        <Collapse in={this.state.error !== undefined}>
          <div>
            <Alert bsStyle="danger" style={{textAlign:'center'}}>
              {this.state.error}
            </Alert>
          </div>
        </Collapse>
        <div style={this.styles.loginRegisterContainer}>
          <Form horizontal onSubmit={e => this.handleLogin(e)} style={this.styles.loginRegister}>
            <FormGroup validationState={this.state.loginEmailValid ? null : 'error'}>
              <Col componentClass={ControlLabel} sm={4}>
                Email
              </Col>
              <Col sm={8}>
                <FormControl
                  name="loginEmail"
                  type="email"
                  componentClass="input"
                  placeholder="Email"
                  disabled={this.props.disabled || this.state.isSubmitting}
                  inputRef={ref => this.loginEmailRef = ref}
                  onChange={() => this.loginValid(false, true, false)}
                />
              </Col>
            </FormGroup>
            <FormGroup validationState={this.state.loginPasswordValid ? null : 'error'}>
              <Col componentClass={ControlLabel} sm={4}>
                Password
              </Col>
              <Col sm={8}>
                <FormControl
                  name="loginPassword"
                  type="password"
                  componentClass="input"
                  placeholder="Password"
                  disabled={this.props.disabled || this.state.isSubmitting}
                  inputRef={ref => this.loginPasswordRef = ref}
                  onChange={() => this.loginValid(false, false, true)}
                />
              </Col>
            </FormGroup>
            <FormGroup>
              <Col smOffset={4} sm={8}>
                <Button
                  type='submit'
                  disabled={this.props.disabled || this.state.isSubmitting || this.state.loginSubmitEnabled}
                >Login</Button>
              </Col>
            </FormGroup>
          </Form>
          <Hr vertical={true} length='60px' />
          <Form horizontal onSubmit={e => this.handleRegister(e)} style={this.styles.loginRegister}>
            <FormGroup validationState={this.state.registerEmailValid ? null : 'error'}>
              <Col componentClass={ControlLabel} sm={4}>
                Email
              </Col>
              <Col sm={8}>
                <FormControl
                  name="registerEmail"
                  type="email"
                  componentClass="input"
                  placeholder="Email"
                  disabled={this.props.disabled || this.state.isSubmitting}
                  inputRef={ref => this.registerEmailRef = ref}
                  onChange={() => this.registerValid(false, true, false, false)}
                />
              </Col>
            </FormGroup>
            <FormGroup validationState={this.state.registerPasswordValid ? null : 'error'}>
              <Col componentClass={ControlLabel} sm={4}>
                Password
              </Col>
              <Col sm={8}>
                <FormControl
                  name="registerPassword"
                  type="password"
                  componentClass="input"
                  placeholder="New password"
                  disabled={this.props.disabled || this.state.isSubmitting}
                  inputRef={ref => this.registerPasswordRef = ref}
                  onChange={() => this.registerValid(false, false, true, false)}
                  onFocus={e => this.setState({registerPasswordFocused: true})}
                  onBlur={e => this.setState({registerPasswordFocused: false})}
                />
                <PasswordStrengthMeterAsync
                  forceHide={!this.state.registerPasswordFocused}
                  style={{
                    width:'95%',
                    borderRadius: '0px 0px 4px 4px',
                  }}
                  password={this.registerPasswordRef && this.registerPasswordRef.value}
                  userInputs={['veruv', this.registerEmailRef && this.registerEmailRef.value]}
                />
              </Col>
            </FormGroup>
            <FormGroup validationState={this.state.registerRetypePasswordValid ? null : 'error'}>
              <Col componentClass={ControlLabel} sm={4}>
                Retype
              </Col>
              <Col sm={8}>
                <FormControl
                  name="registerPasswordRetype"
                  type="password"
                  componentClass="input"
                  placeholder="Retype password"
                  disabled={this.props.disabled || this.state.isSubmitting}
                  inputRef={ref => this.registerRetypePasswordRef = ref}
                  onChange={() => this.registerValid(false, false, false, true)}
                />
              </Col>
            </FormGroup>
            <FormGroup>
              <Col smOffset={4} sm={8}>
                <Button
                  type='submit'
                  disabled={this.props.disabled || this.state.isSubmitting || this.state.registerSubmitEnabled}
                >Register</Button>
              </Col>
            </FormGroup>
          </Form>
        </div>
      </div>
    );
  }

  emailValid(email):boolean {
    return (email
      && email.indexOf('@') !== -1
      && email.length > 3);
  }

  passValid(pass):boolean {
    return pass && pass.length >= LoginOrRegister.PASSWORD_MIN_LENGTH;
  }

  loginValid(showError:boolean=true, validateEmail:boolean=true, validatePass:boolean=true) {
    var newState:ValidState = {
      loginEmailValid: this.state.loginEmailValid,
      loginPasswordValid: this.state.loginPasswordValid,
      registerEmailValid: true,
      registerPasswordValid: true,
      registerRetypePasswordValid: true,
    };
    var valid:boolean = true;

    if(validatePass) {
      var passValid = this.passValid(this.loginPasswordRef && this.loginPasswordRef.value);
      if(showError && !passValid) {
        newState.error = "Login password is invalid";
      }
      newState.loginPasswordValid = passValid;
      valid = valid && passValid;
    }

    if(validateEmail) {
      var emailValid = this.emailValid(this.loginEmailRef && this.loginEmailRef.value);
      if(showError && !emailValid) {
        newState.error = "Login email is invalid";
      }
      newState.loginEmailValid = emailValid;
      valid = valid && emailValid;
    }

    this.setState(newState);
    return valid;
  }

  handleLogin(event) {
    event.preventDefault();
    if(!this.loginValid()
      || !this.loginEmailRef
      || !this.loginPasswordRef) {
      return;
    }

    let email = this.loginEmailRef.value;
    let password = this.loginPasswordRef.value;
    this.loginPasswordRef.value = "";

    this.setState({isSubmitting: true});
    this.props.login(email, password)
      .then(() => {
        this.setState({isSubmitting: false});
      })
      .catch((err) => {
        var errorStr;
        if(err instanceof ErrorResponse) {
          errorStr = err.userFacingMessage || err.code;
        } else {
          errorStr = "Failed communicating with server";
        }
        this.setState({
          isSubmitting: false,
          error: errorStr,
        });
      })
      .then(this.props.onAuthenticated);
  }

  registerValid(showError:boolean=true, validateEmail:boolean=true, validatePass:boolean=true, validateRetypePass:boolean=true) {
    var newState:ValidState = {
      loginEmailValid: true,
      loginPasswordValid: true,
      registerEmailValid: this.state.registerEmailValid,
      registerPasswordValid: this.state.registerPasswordValid,
      registerRetypePasswordValid: this.state.registerRetypePasswordValid,
    };
    var valid:boolean = true;

    if(validateRetypePass) {
      var pass = this.registerPasswordRef && this.registerPasswordRef.value;
      var passRetype = this.registerRetypePasswordRef && this.registerRetypePasswordRef.value;
      var passRetypeValid = true;
      if(pass != passRetype) {
        passRetypeValid = false;
        if(showError) {
          newState.error = "Passwords do not match";
        }
      } else if(!this.passValid(passRetype)) {
        passRetypeValid = false;
      }
      newState.registerRetypePasswordValid = passRetypeValid;
      valid = valid && passRetypeValid;
    }

    if(validatePass) {
      var passValid = this.passValid(this.registerPasswordRef && this.registerPasswordRef.value);
      if(showError && !passValid) {
        newState.error = "Registration password is too short";
      }
      newState.registerPasswordValid = passValid;
      valid = valid && passValid;
    }

    if(validateEmail) {
      var emailValid = this.emailValid(this.registerEmailRef && this.registerEmailRef.value);
      if(showError && !emailValid) {
        newState.error = "Registration email is invalid";
      }
      newState.registerEmailValid = emailValid;
      valid = valid && emailValid;
    }

    this.setState(newState);
    return valid;
  }

  handleRegister(event) {
    event.preventDefault();
    if(!this.registerValid()
      || !this.registerEmailRef
      || !this.registerPasswordRef) {
      return;
    }

    let email = this.registerEmailRef.value;
    let password = this.registerPasswordRef.value;

    this.setState({isSubmitting: true});
    this.props.register(email, password)
      .then(() => {
        this.setState({isSubmitting: false});
      })
      .catch((err) => {
        var errorStr;
        if(err instanceof ErrorResponse) {
          errorStr = err.userFacingMessage || err.code;
        } else {
          errorStr = "Failed communicating with server";
        }
        this.setState({
          isSubmitting: false,
          error: errorStr,
        });
      })
      .then(this.props.onAuthenticated);
  }
}

export default withAuthentication(LoginOrRegister);
