import LocalStorage from './localstorage';
import { AccountInfo } from './serverApi';

enum ResourceType {
  SNAPSHOT = 'SNAPSHOT',
  SNAPSHOT_REQUEST = 'SNAPSHOT_REQUEST',
}

class TokenManager {
  static readonly ACCOUNT_INFO_STORAGE_KEY = "ACCOUNT_INFO";
  static readonly SNAPSHOT_ID_TO_TOKEN_STORAGE_KEY = "SNAPSHOT_ID_TO_TOKEN";
  static readonly RESOURCE_TOKEN_PREFIX_STORAGE_KEY = "RESOURCE_TOKEN_";
  static instance:TokenManager;

  static getInstance():TokenManager {
    return this.instance || (this.instance = new this());
  }

  getUserToken():string|undefined {
    var accountInfo = this.getAccountInfo();
    return accountInfo && accountInfo.token;
  }

  getAccountInfo():AccountInfo|undefined {
    return LocalStorage.getInstance().get(TokenManager.ACCOUNT_INFO_STORAGE_KEY);
  }

  updateAccountInfo(accountInfo:AccountInfo) {
    LocalStorage.getInstance().set(TokenManager.ACCOUNT_INFO_STORAGE_KEY, accountInfo);
  }

  removeAccountInfo() {
    LocalStorage.getInstance().unset(TokenManager.ACCOUNT_INFO_STORAGE_KEY);
  }

  getResourceOrUserToken(resourceType:ResourceType, resourceId:string):string|undefined {
    var resourceToken = LocalStorage.getInstance().objectGet(
      TokenManager.RESOURCE_TOKEN_PREFIX_STORAGE_KEY + resourceType,
      resourceId);
    return resourceToken || this.getUserToken();
  }

  getResourceToken(resourceType:ResourceType, resourceId:string):string|undefined {
    return LocalStorage.getInstance().objectGet(
      TokenManager.RESOURCE_TOKEN_PREFIX_STORAGE_KEY + resourceType,
      resourceId);
  }

  getAllResourceIds(resourceType:ResourceType):string[]|undefined {
    return Object.keys(LocalStorage.getInstance().object(TokenManager.RESOURCE_TOKEN_PREFIX_STORAGE_KEY + resourceType));
  }

  getAllResourceTokens(resourceType:ResourceType):string[]|undefined {
    return Object['values'](LocalStorage.getInstance().object(TokenManager.RESOURCE_TOKEN_PREFIX_STORAGE_KEY + resourceType));
  }

  updateResourceToken(resourceType:ResourceType, resourceId:string, token:string) {
    LocalStorage.getInstance().objectSet(
      TokenManager.RESOURCE_TOKEN_PREFIX_STORAGE_KEY + resourceType,
      resourceId,
      token);
  }

  removeResourceTokenByResourceId(resourceType:ResourceType, resourceId:string) {
    LocalStorage.getInstance().objectSet(
      TokenManager.RESOURCE_TOKEN_PREFIX_STORAGE_KEY + resourceType,
      resourceId,
      undefined);
  }

  removeResourceTokenByToken(resourceType:ResourceType, token:string) {
    var resourceIds = this.getAllResourceIds(resourceType);
    if(!resourceIds) {
      return;
    }
    for (var resourceId of resourceIds) {
      if(this.getResourceToken(resourceType, resourceId) === token) {
        this.removeResourceTokenByResourceId(resourceType, resourceId);
        return;
      }
    }
  }

  removeAllResourceTokens(resourceType:ResourceType) {
    return LocalStorage.getInstance().unset(
      TokenManager.RESOURCE_TOKEN_PREFIX_STORAGE_KEY + resourceType);
  }
}

export { ResourceType }
export default TokenManager;
