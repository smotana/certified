import * as React from 'react';
import PublicIcon from '@material-ui/icons/PublicRounded';
import PasswordIcon from '@material-ui/icons/LockRounded';
import PrivateIcon from '@material-ui/icons/VisibilityOffRounded';
import { PurchaseResult, PurchaseInfo } from '../payment/paymentApi';

interface _Message {
  subApi:string;
  method:string;
  body?:any;
}

class ErrorResponse {
  httpCode:number;
  code:string;
  userFacingMessage?:string;

  constructor(httpCode:number, code:string, userFacingMessage:string|undefined = undefined) {
    this.httpCode = httpCode;
    this.code = code;
    this.userFacingMessage = userFacingMessage;
  }
}

enum Visibility {
  PRIVATE = 'PRIVATE',
  PASSWORD = 'PASSWORD',
  PUBLIC = 'PUBLIC',
}
const visiblityStyle = {
  display: 'inline-flex',
  alignItems: 'baseline',
};
const visibilityToUserString = {
  [Visibility.PUBLIC]: (
    <span style={visiblityStyle}>
      <PublicIcon fontSize='inherit' style={{margin: 'auto'}} />
      &nbsp;
      Public
    </span>
  ),
  [Visibility.PASSWORD]: (
    <span style={visiblityStyle}>
      <PasswordIcon fontSize='inherit' style={{margin: 'auto'}} />
      &nbsp;
      Password
    </span>
  ),
  [Visibility.PRIVATE]: (
    <span style={visiblityStyle}>
      <PrivateIcon fontSize='inherit' style={{margin: 'auto'}} />
      &nbsp;
      Only you
    </span>
  ),
}

interface _Verification {
  id:string,
  ts:number;
  expiry?:number;
  url:string;
  screenshot:string;
  note:string;
  visibility:Visibility;
  isMine:boolean;
  canExtendExpiry:boolean;
}
const verificationExpiryPretty = (ver:Verification|undefined):string => {
  if(!ver) {
    return "Does not exist";
  }
  if(!ver.expiry) {
    return "Does not expire";
  }

  var delta = Math.abs(ver.expiry - Date.now()) / 1000;
  var days = Math.floor(delta / 86400);

  delta -= days * 86400;
  var hours = Math.floor(delta / 3600) % 24;
  
  delta -= hours * 3600;
  var minutes = Math.floor(delta / 60) % 60;

  delta -= minutes * 60;
  var seconds = delta % 60;

  if(days > 0) {
    return `${days} days ${hours} hours`;
  }
  if(hours > 0) {
    return `${hours} hours ${minutes} minutes`;
  }
  if(minutes > 0) {
    return `${minutes} minutes ${seconds} seconds`;
  }
  if(seconds > 0) {
    return `${seconds} seconds`;
  }
  return `Already expired`;
}

interface _AccountInfo {
  token:string;
  email:string;
}

enum NavigateType {
  FORWARD = 'FORWARD',
  BACK = 'BACK',
  REFRESH = 'REFRESH'
}

interface _Server {
  send(msg:Message):Promise<any | null>;
}

interface _ServerApi {
  /*
   * Verification
   */
  getVerification(id:string, pass:string|undefined):Promise<Verification>;
  getMySnapshots():Promise<Verification[]>;
  deleteVerification(id:string):Promise<void>;
  setVisibility(id:string, visibility:Visibility, passOpt?:string):Promise<void>;
  applyEdits(id:string, edits:SnapshotEdits):Promise<Verification>;

  /*
   * Auth
   */
  register(username:string, email:string, password:string):Promise<AccountInfo>;
  login(username:string|undefined, email:string|undefined, password:string):Promise<AccountInfo>;
  verifyToken():Promise<AccountInfo|undefined>;
  changeEmail(token:string, newEmail:string):Promise<void>;
  changePass(token:string, newPass:string):Promise<void>;
  logout(token:string):Promise<void>;

  /*
   * Payment
   */
  pay(purchaseInfo:PurchaseInfo, paymentToken?:stripe.Token):Promise<PurchaseResult>;

  /*
   * Other
   */
  ping():Promise<void>;
}

interface _Point {
  x:number;
  y:number
}

interface _Box {
  p1:Point;
  p2:Point;
}

interface _SnapshotEdits {
  /** Contents outside of the crop box are redacted */
  crop?:Box;
  /** Contents inside of the redaction box are redacted */
  redactions?:Box[];
  /** Note attached to the image */
  note?:string;
}

export type ServerApi = _ServerApi;
export type Server = _Server;
export type Message = _Message;
export type Verification = _Verification;
export type Point = _Point;
export type Box = _Box;
export type SnapshotEdits = _SnapshotEdits;
export type AccountInfo = _AccountInfo;
export {
  ErrorResponse,
  verificationExpiryPretty,
  Visibility,
  visibilityToUserString,
  NavigateType,
};
