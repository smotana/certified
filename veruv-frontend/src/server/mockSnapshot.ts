import { Verification, Visibility } from './serverApi';
import randomUuid from '../util/uuid';

export default (
  idOpt:string|undefined = undefined,
  isMineOpt:boolean|undefined = undefined,
  visibility:Visibility = Visibility.PRIVATE):Verification => {

  return {
    id: idOpt || randomUuid().substring(0,6),
    isMine: isMineOpt === undefined ? false : isMineOpt,
    canExtendExpiry: true,
    ts: new Date().getTime(),
    expiry: new Date().getTime() + 4*60*60*1000,
    url: 'https://corporatefinanceinstitute.com/resources/knowledge/accounting/three-financial-statements/',
    visibility: visibility,
    note: "This is my note\nthis is my note",
    screenshot: 'iVBORw0KGgoAAAANSUhEUgAAAMgAAACWCAYAAACb3McZAAABPUlEQVR42u3TMQEAAAjDMOZfNCAAARxJNTTVG3CKQcAgYBAwCBgEDAIGAYOAQcAggEHAIGAQMAgYBAwCBgGDgEHAIAYBg4BBwCBgEDAIGAQMAgYBgwAGAYOAQcAgYBAwCBgEDAIGAYMABgGDgEHAIGAQMAgYBAwCBgEMAgYBg4BBwCBgEDAIGAQMAgYBDAIGAYOAQcAgYBAwCBgEDAIYBAwCBgGDgEHAIGAQMAgYBAwCGAQMAgYBg4BBwCBgEDAIGAQMYhAwCBgEDAIGAYOAQcAgYBAwCGAQMAgYBAwCBgGDgEHAIGAQMIhBwCBgEDAIGAQMAgYBg4BBwCCAQcAgYBAwCBgEDAIGAYOAQcAggEHAIGAQMAgYBAwCBgGDgEEAg4BBwCBgEDAIGAQMAgYBg4BBAIOAQcAgYBAwCHw11TQrelMcSVQAAAAASUVORK5CYII='
  };
};
