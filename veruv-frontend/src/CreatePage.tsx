import React, { Component } from 'react';
import {
  Panel,
  Fade,
} from 'react-bootstrap';
import { connect } from 'react-redux';
import { Action } from './server/action';
import Browser from './browser/Browser';
import * as BrowserApi from  './server/browserApi';
import Hr from './Hr';
import { Verification } from './server/serverApi';
import Editor from './editor/Editor';
import SnapshotSelection from './editor/SnapshotSelection';
import { withRouter, RouteComponentProps } from 'react-router';
import ScrollAnchor from './components/ScrollAnchor';
import HorizontalPanels from './content/HorizontalPanels';
import { ContentLink } from './content/contentApi';
import SecurityIcon from '@material-ui/icons/SecurityRounded';
import PrivacyIcon from '@material-ui/icons/VisibilityOffRounded';
import TipIcon from '@material-ui/icons/InfoOutlined';
import setTitle from './util/titleUtil';

interface Props {
  action:Action;
  // Store props
  verificationById
}

interface State {
  takeScreenshotDisabled:boolean;
  createdSnapshotIds:string[];
  liveSnapshotId?:string;
}

class CreatePage extends Component<Props & RouteComponentProps, State> implements BrowserApi.BrowserEventHandler {
  // TODO clean up unused styles
  readonly styles = {
    header: {
      marginTop: '48px',
      maxWidth: '1024px',
      margin: 'auto',
      padding: '20px',
    },
    editor: {
      display: 'inline-block',
    },
  };
  readonly socket:BrowserApi.BrowserSocket;
  readonly editorScrollRef:React.RefObject<ScrollAnchor> = React.createRef();
  hasScrolledToFirstSnapshot:boolean = false;

  constructor(props) {
    super(props);

    this.state = {
      takeScreenshotDisabled: true,
      createdSnapshotIds: [],
    };

    this.socket = new BrowserApi.BrowserSocket();
    this.socket.addHandler(this);
  }

  componentDidMount() {
    setTitle('Create a certified snapshot');
    if(this.props.location.state && this.props.location.state.startPage) {
      // Clear start page from session to prevent
      // it from triggerring on page refresh
      this.props.history.replace({
        ...this.props.location,
        state: {
          ...this.props.location.state,
          startPage: undefined,
        }
      });
    }
  }

  render() {
    var liveSnapshotId:string|undefined = this.state.liveSnapshotId || this.state.createdSnapshotIds[0];
    var hasSnapshots:boolean = this.state.createdSnapshotIds.length > 0;
    var createdSnapshotsMap = this.state.createdSnapshotIds.reduce((map, id) => {
      map[id] = this.props.verificationById[id];
      return map;
    }, {});

    return (
      <div>
        <div style={this.styles.header}>
          <ScrollAnchor scrollOnStateName='browser' />
          <HorizontalPanels
            style={{marginTop: '0px'}}
            xsColCnt={1}
            smColCnt={2}
            contents={[
              {title: (<h1>Verify your information</h1>), description: (
                <div style={{textAlign:'left'}}>
                  <h5>To create a valid snapshot:</h5>
                  <ol>
                    <li>All of the data you want to certify must be visible</li>
                    <li>The identity/source/owner of the data must also be visible</li>
                    <li>Click 'Snapshot' button</li>
                  </ol>
                </div>
              )},
              {description: (
                <div>
                  <Panel>
                    <Panel.Body>
                      <div style={{display:'flex', alignItems:'center'}}>
                        <PrivacyIcon />
                        <div style={{flexGrow:1, margin:'0 5px 0 5px'}}>
                          We never record your browser session including your history and passwords
                        </div>
                        <ContentLink content={{link: {text: "Learn more", path: '/about/privacy-policy', anchor: 'browser'}}} />
                      </div>
                    </Panel.Body>
                  </Panel>
                  <Panel>
                    <Panel.Body>
                      <div style={{display:'flex', alignItems:'center'}}>
                        <SecurityIcon />
                        <div style={{flexGrow:1, margin:'0 5px 0 5px'}}>
                          This browser runs inside a secure isolated container to protect your data
                        </div>
                        <ContentLink content={{link: {text: "Learn more", path: '/about/security', anchor: 'security'}}} />
                      </div>
                    </Panel.Body>
                  </Panel>
                </div>
              )},
            ]}
          />
        </div>
        <Browser
          startPage={this.props.location.state && this.props.location.state.startPage}
          socket={this.socket}
        />
        <Fade
          mountOnEnter
          unmountOnExit
          in={hasSnapshots}
        >
          <div>
            <Hr />
            <div style={this.styles.header}>
              <ScrollAnchor ref={this.editorScrollRef} />
              <HorizontalPanels
                xsColCnt={1}
                smColCnt={2}
                contents={[
                  {title: (<h1>Edit & Share</h1>), description: (
                    <div style={{textAlign:'left'}}>
                      <h5>How to use:</h5>
                      <ol>
                        <li>Paint over sensitive information</li>
                        <li>Configure access permissions</li>
                        <li>Share link</li>
                      </ol>
                    </div>
                  )},
                  {description: (
                    <div>
                      <Panel>
                        <Panel.Body>
                          <div style={{display:'flex', alignItems:'center'}}>
                            <TipIcon />
                            <div style={{flexGrow:1, marginRight:'5px'}}>
                              Redact sensitive information but keep as much other content as possible to retain legibility.
                            </div>
                          </div>
                        </Panel.Body>
                      </Panel>
                    </div>
                  )},
                ]}
              />
            </div>
            <div style={this.styles.editor}>
              <SnapshotSelection
                defaultSelectedSnapshotId={liveSnapshotId}
                onSelect={id => this.handleSnapshotSelect(id)}
                snapshots={createdSnapshotsMap}
              />
              <Editor
                action={this.props.action}
                snapshotId={liveSnapshotId}
                snapshot={this.props.verificationById[liveSnapshotId]}
                onLoaded={() => this.handleEditorLoaded()}
                onDeleted={() => this.handleRemoveSnapshot()}
              />
            </div>
          </div>
        </Fade>
      </div>
    );
  }

  handleRemoveSnapshot() {
    var newCreatedSnapshotIds = this.state.createdSnapshotIds
      .filter(s => s !== this.state.liveSnapshotId)
    this.setState({
      liveSnapshotId: newCreatedSnapshotIds[0],
      createdSnapshotIds: newCreatedSnapshotIds
    });
  }

  handleEditorLoaded(){
    if(this.hasScrolledToFirstSnapshot) {
      return;
    }
    this.hasScrolledToFirstSnapshot = true;
    this.editorScrollRef.current!.scrollNow();
  }

  handleSnapshotSelect(id) {
    this.setState({liveSnapshotId: id});
  }

  verification?(verification:Verification, requestId:string, verificationId:string):void {
    this.props.action.createdVerification(verificationId, verification)
      .then(v => {
        this.setState({
          createdSnapshotIds: [...this.state.createdSnapshotIds, verificationId]
        });
      });
  }

  browserAcquired?():void {
    this.setState({takeScreenshotDisabled: false});
  }

  closed?(reason:string):void {
    this.setState({takeScreenshotDisabled: true});
  }
}

export default connect<any,any,any,any>(
  (state) => {return {verificationById: state.verificationById}},
)(withRouter(CreatePage));
