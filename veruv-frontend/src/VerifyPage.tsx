import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Action, Status } from './server/action';
import VerificationContent from './verification/VerificationContent';
import { Verification } from './server/serverApi';
import { Alert, Button, FormControl, FormGroup, InputGroup } from 'react-bootstrap';
import setTitle from './util/titleUtil';
import ReactGA from 'react-ga';

interface Props {
  action:Action;
  // Router matching
  match;
  // Store props
  verification;
}

class BrowserPage extends Component<Props> {
  snapshotPasswordRef:HTMLInputElement|undefined = undefined;

  componentDidMount() {
    setTitle('Verify');
    if (this.props.verification.status == Status.PENDING) {
      this.props.action.getVerification(this.props.match.params.verificationId);
    }
  }

  render() {
    switch(this.props.verification.status) {
      case Status.REJECTED:
        return (
          <Alert bsStyle='danger'>
            <div>
              Snapshot failed to load
            </div>
            <Button
              style={{margin: '20px auto 0px'}}
              bsStyle='danger'
              onClick={() => {
                this.props.action.getVerification(this.props.match.params.verificationId);
              }}>
              Retry
            </Button>
          </Alert>
        );
      case Status.PASSWORD:
        return (
          <Alert bsStyle='warning'>
            <div>
              Password required for access
            </div>
            <FormGroup style={{margin: '20px auto 10px', maxWidth: '200px'}}>
              <InputGroup>
                <FormControl
                  name="password"
                  placeholder="Enter password"
                  type="password"
                  inputRef={ref => this.snapshotPasswordRef = ref}
                  onKeyDown={e => {
                    if(e.keyCode === 13) this.handleRetryWithPassword();
                  }}
                />
                <InputGroup.Button>
                  <Button
                    onClick={e => this.handleRetryWithPassword()}
                  >Submit</Button>
                </InputGroup.Button>
              </InputGroup>
            </FormGroup>
          </Alert>
        );
      case Status.PENDING:
        return (
          <Alert bsStyle='info'>
            Loading snapshot...
          </Alert>
        );
      default:
      case Status.NONE:
      case Status.NOTFOUND:
        return (
          <Alert bsStyle='danger'>
            Snapshot doesn't exist, is expired or private.
          </Alert>
        );
      case Status.FULFILLED:
        ReactGA.event({
          category: 'verify',
          action: this.props.verification.isMine ? 'openMine' : 'open',
        });
        return (
          <VerificationContent
            status={this.props.verification.status}
            verification={this.props.verification as Verification} />
        );
    }
  }

  handleRetryWithPassword() {
    if(!this.snapshotPasswordRef){
      return;
    }

    this.props.action.getVerification(
      this.props.match.params.verificationId,
      this.snapshotPasswordRef.value);
  }
}

export default connect<any,any,any,any>(
  (state, ownProps) => {
    if(ownProps.match.params.verificationId) {
      return {
        verification: state.verificationById[ownProps.match.params.verificationId] || { status: Status.PENDING },
      };
    } else {
      return {
        verification: {status: Status.NONE}
      };
    }
  },
)(BrowserPage);
