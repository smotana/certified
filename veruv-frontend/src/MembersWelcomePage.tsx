import React, { Component } from 'react';
import withAuthentication, { AuthenticatorProps } from './server/auth/authenticator';
import AuthenticatedBlock from './server/auth/AuthenticatedBlock';
import setTitle from './util/titleUtil';

class MembersWelcomePage extends Component<AuthenticatorProps> {
  readonly styles = {
  }

  componentDidMount() {
    setTitle('Welcome');
  }

  render() {
    return (
      <AuthenticatedBlock inline={true}>
        Welcome to the members area!
      </AuthenticatedBlock>
    );
  }
}

export default withAuthentication(MembersWelcomePage);
