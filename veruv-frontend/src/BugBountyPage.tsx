import React, { Component } from 'react';
import {
  Grid,
  Row,
  Col,
} from 'react-bootstrap';
import Hr from './Hr';
import ScrollAnchor from './components/ScrollAnchor';
import BountyIcon from '@material-ui/icons/BugReportRounded';
import setTitle from './util/titleUtil';

class BugBountyPage extends Component {
  readonly styles = {
    content: {
      textAlign: 'left' as 'left',
      fontSize: '1em',
    },
    paragraph: {
    },
    hr: {
      marginTop: "40px",
      marginBottom: "30px",
      width: "10%",
    },
  }

  componentDidMount() {
    setTitle('Bug Bounty');
  }

  render() {
    var securityEmailContact = (<a href="mailto:security@veruv.com">security@veruv.com</a>);
    return (
      <div style={this.styles.content}>
        <ScrollAnchor scrollOnStateName='bounty' />
        <h1>
          <BountyIcon fontSize='default' />
          &nbsp;
          Bug bounty program
        </h1>
        <p style={this.styles.paragraph}>
          We take every security concern seriously. If you have any questions regarding security
          or you have uncovered a potential vulnerability, don't hesitate to contact us at {securityEmailContact}.
          We operate a bug bounty reward program to compensate the hard work it takes to find vulnerabilities.
          Please follow this guideline to receive your reward.
        </p>
        <h3>Testing</h3>
        <p style={this.styles.paragraph}>
          Before you commence testing, please give us a heads up at {securityEmailContact} in order for us not to confuse your testing with an actual attack.
          You may also ask us for additional details that may help your research.
        </p>
        <h3>Disclosure</h3>
        <p style={this.styles.paragraph}>
          Disclose any findings to our email {securityEmailContact}. 
          You may be eligible to receive compensation for your work as long as you follow our guideline.
        </p>
        <ul>
          <li>Vulnerabilities must be disclosed to us privately and giving us reasonable time to respond.
            You may publicly disclose your finding only after the vulnerabilities have been fully investigated and patched.</li>
          <li>You may not purposefully exploit the vulnerability to your advantage or cause damage to our service or company.</li>
        </ul>
        <h3>Compensation</h3>
        <p style={this.styles.paragraph}>
          We greatly appreciate your work and want to compensate you fairly. Compensation will vary at our discretion,
          but we will follow the suggested compensation table below.
          We may increase compensation based on the complexity of the attack or complexity of uncovering the vulnerability.
          Please stay clear of blatant denial of service attacks, unfortunately they will not be compensated.
        </p>
        <ul>
          <li>$500 USD: Intercept any sensitive information from another user. Includes:</li>
          <ul>
            <li>Cross-site scripting vulnerability intercepting sensitive information from another user</li>
            <li>Break out of the browser container and execute arbitrary code</li>
            <li>Break out of the web server and execute arbitrary code</li>
          </ul>
          <li>$200 USD: Break out of the Selenium driver and execute arbitrary code on the isolated container host</li>
          <li>$100 USD: Cross-site scripting vulnerability not exposing sensitive information</li>
          <li>$0 USD: Denial of service vulnerability</li>
        </ul>
      </div>
    );
  }
}

export default BugBountyPage;
