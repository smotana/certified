const SITE_NAME = 'veruv_site';
const EXTENSION_NAME = 'veruv_ext';

class Bridge {

  constructor() {
    this.connected = false
    /**
     * Key as SITE/EXTENSION target, value as listener
     */
    this.eventListener = {};
    /**
     * Key as SITE/EXTENSION target, value as object:
     *  key as handler name, value as fun(msgType,msgContent)->void
     */
    this.eventHandlersMsg = {};
    /**
     * Key as handler name, value as fun ()->void
     */
    this.eventHandlersConnected = {};
  }

  registerExtensionHandler(name, handlerConnected, handlerMsg) {
    this._registerHandler(name, handlerConnected, handlerMsg, EXTENSION_NAME);
  }

  registerSiteHandler(name, handlerConnected, handlerMsg) {
    this._registerHandler(name, handlerConnected, handlerMsg, SITE_NAME);
  }

  deregisterExtensionHandler(name) {
    this._deregisterHandler(name, EXTENSION_NAME);
  }

  deregisterSiteHandler(name) {
    this._deregisterHandler(name, SITE_NAME);
  }

  sendMessageToSite(type, content) {
    this._sendMessage(type, content, SITE_NAME);
  }

  sendMessageToExtension(type, content) {
    this._sendMessage(type, content, EXTENSION_NAME);
  }

  _registerHandler(name, handlerConnected, handlerMsg, target) {
    // Register message listener
    if(!this.eventListener[target]) {
      var self = this;
      const listener = function(event) {
        // Ensure this message is for us
        if(event.data['dst'] !== target) {
          return;
        }
        // Handle connection establishment
        if(event.data['type'] === 'syn') {
          self._sendMessage('ack', null, target === SITE_NAME ? EXTENSION_NAME : SITE_NAME);
          if(!self.connected) {
            self._sendMessage('syn', null, target === SITE_NAME ? EXTENSION_NAME : SITE_NAME);
          }
          return;
        }
        if(event.data['type'] === 'ack') {
          if(!self.connected) {
            Object.values(self.eventHandlersConnected).forEach(function(handler) {
              handler && handler();
            });
            self.connected = true;
          }
          return;
        }
        if(event.data['type'] === 'fin') {
          self.connected = false;
        }
        // Handle received message
        if(self.eventHandlersMsg[target]) {
          Object.values(self.eventHandlersMsg[target]).forEach(function(handler) {
            handler && handler(event.data['type'], event.data['content']);
          });
        }
      }

      // Register listener
      window.addEventListener('message', listener, false);
      this.eventListener[target] = listener;

      // Establish connection
      this._sendMessage('syn', null, target === SITE_NAME ? EXTENSION_NAME : SITE_NAME);
    }

    // Register message handler
    if(handlerMsg) {
      if(!this.eventHandlersMsg[target]) {
        this.eventHandlersMsg[target] = {};
      }
      this.eventHandlersMsg[target][name] = handlerMsg;
    }

    // Register connected status handler
    if(handlerConnected) {
      if(this.connected) {
        handlerConnected();
      } else {
        this.eventHandlersConnected[name] = handlerConnected;
      }
    }
  }

  _deregisterHandler(name, target) {
    if(this.eventHandlersMsg[target]
      && this.eventHandlersMsg[target][name]) {
        this.eventHandlersMsg[target][name] = undefined;
    }
    if(this.connected) {
      this._sendMessage('fin', null, EXTENSION_NAME);
      this._sendMessage('fin', null, SITE_NAME);
      this.connected = false;
    }
  }

  _sendMessage(type, content, target) {
    const msg = {
      dst: target,
      type: type,
      content: content,
    }
    window.postMessage(msg, '*');
  }
}

export default Bridge;
