
import Bridge from './bridge.js';

interface _Handler {
  handleConnected?();
  handleMessage?(type: string, data: any):void;
}

class SiteBridge {
  bridgeJs:Bridge;
  static instance:SiteBridge;

  constructor() {
    this.bridgeJs = new Bridge();
  }

  static getInstance():SiteBridge {
    return this.instance || (this.instance = new this());
  }

  registerSiteHandler(name:string, handler:Handler) {
    this.bridgeJs.registerSiteHandler(
      name,
      () => handler.handleConnected && handler.handleConnected(),
      (type: string, data: any) => handler.handleMessage && handler.handleMessage(type, data));
  }

  deregisterSiteHandler(name:string) {
    this.bridgeJs.deregisterSiteHandler(name);
  }

  sendMessageToExtension(type:string, data?:any) {
    this.bridgeJs.sendMessageToExtension(type, data);
  }
}

export type Handler = _Handler;
export default SiteBridge;
