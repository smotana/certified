import React, { Component } from 'react';
import AnimBubble from './AnimBubble';

interface Props {
  fullPage?:boolean;
}

class AnimAnchor extends Component<Props> {
  render() {
    return (
      <div style={{
        ...(this.props.fullPage ? {
          width: '100%',
          height: '100%',
          position: 'absolute',
          overflow: 'hidden',
        } : {
          position: 'relative',
        }),
        zIndex: -100,
        top: '0px',
        left: '0px',
      }}>
        {this.props.children}
      </div>
    );
  }
}

export default AnimAnchor;
