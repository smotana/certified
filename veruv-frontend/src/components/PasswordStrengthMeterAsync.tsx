import React, { Component } from 'react';
import { ProgressBar, Overlay, Popover } from 'react-bootstrap';
import withScript from '../util/withScript'

interface Props {
  style?:React.CSSProperties;
  password?:string;
  userInputs?:string[];
  forceHide?:boolean;
}

interface State {
  scriptLoaded;
}

class PasswordStrengthMeterAsync extends Component<Props, State> {
  lastWarning:string|undefined = undefined;

  render() {
    // https://github.com/dropbox/zxcvbn#usage
    const strengthResult = window['zxcvbn'](
      (this.props.password || '').substring(0,100),
      this.props.userInputs)
    var color;
    switch(strengthResult.score) {
      case 0:
        color = 'danger';
        break;
      case 1:
        color = 'danger';
        break;
      case 2:
        color = 'warning';
        break;
      case 3:
        color = 'warning';
        break;
      case 4:
        color = 'success';
        break;
      default:
        return null;
    }

    if(strengthResult.feedback && strengthResult.feedback.warning) {
      this.lastWarning = strengthResult.feedback.warning;
    }
    
    return (
      <div>
        <ProgressBar
          style={{
            ...{
              height:'3px',
              margin:'auto',
            },
            ...this.props.style}
          }
          bsStyle={color}
          now={strengthResult.guesses_log10 * 10}
        />
        <Overlay
          show={!this.props.forceHide && strengthResult.feedback && strengthResult.feedback.warning}
          target={this}
          placement='bottom'
        >
          <Popover
            bsStyle='danger'
            id="popover"
          >
            {this.lastWarning}
          </Popover>
        </Overlay>
      </div>
    );
  }
};

export default withScript('/js/zxcvbn.js', PasswordStrengthMeterAsync, true);