import React, { Component, Children } from 'react';

interface Props {
  style?:React.CSSProperties;
  children:(string|React.ReactNode)[];
  radius:string;
  width?:string;
  offsetDegrees:number;
  showTime:number;
  transitionTime:number;
  startIndex?:number;
  timingFunction?:string;
  visibleLayerCount?:number;
  stopOnHover?:boolean;
}

class AnimCycleContent extends Component<Props> {
  init:boolean = true;
  task:NodeJS.Timeout|undefined = undefined;
  currentIndex:number = 0;
  hovering:boolean = false;

  componentDidMount() {
    if(this.init) {
      this.init = false;
      this.currentIndex = this.props.startIndex || Math.floor(Math.random() * this.props.children.length);
      this.transitionNow();
      this.task = setInterval(
        () => this.transitionNow(),
        this.props.showTime + this.props.transitionTime);
    }
  }

  componentWillUnmount() {
    this.task && clearInterval(this.task);
  }

  render() {
    const layers = this.props.visibleLayerCount || 2;
    const renderedItems:React.ReactNode[] = [];
    const itemsLength = this.props.children.length;
    for (let i = 0; i < itemsLength; i++) {
      var position = 0;
      var opacity = 0;
      var hidden = true;
      const forwardDistance = Math.abs((i - this.currentIndex + itemsLength) % itemsLength);
      if(forwardDistance <= layers) {
        position = forwardDistance;
        opacity = (layers - forwardDistance) / layers;
        hidden = false;
      }
      const backwardDistance = Math.abs((this.currentIndex - i + itemsLength) % itemsLength);
      if(backwardDistance <= layers) {
        position = -backwardDistance;
        opacity = (layers - backwardDistance) / layers;
        hidden = false;
      }

      renderedItems.push(this.renderItem(
        this.props.children[i],
        position,
        opacity,
        hidden));
    }
    return (
      <div style={{
        display: 'inline-block',
        position: 'relative',
        overflow: 'visible',
        verticalAlign: 'top',
        width: this.props.width || this.props.radius,
        ...this.props.style
      }}>
        &nbsp;
        {renderedItems}
      </div>
    );
  }

  renderItem(content:string|React.ReactNode, position:number, opacity:number, hidden:boolean) {
    const timingFunction = this.props.timingFunction || 'cubic-bezier(0.2, 1.08, 1, 0.99)';
    return (
      <div
        style={{
          textAlign: 'left',
          position: 'absolute',
          top: '0px',
          left: '0px',
          visibility: hidden ? 'hidden' : 'visible',
          width: this.props.radius,
          transform: `rotate(${position * this.props.offsetDegrees}deg)`, 
          transformOrigin: 'right',
          opacity: opacity,
          transition:
            `transform ${this.props.transitionTime}ms ${timingFunction},`
            + `opacity ${this.props.transitionTime}ms ${timingFunction}`,
        }}
      >
        <span
          onMouseOver={() => {
            if(!hidden && opacity != 0) {
              this.hovering = true;
            }
          }}
          onMouseOut={() => {
            this.hovering = false;
          }}
        >
          {content}
        </span>
      </div>
    );
  }

  transitionNow() {
    if((this.hovering && this.props.stopOnHover)
      || document.hidden) {
      return;
    }
    this.currentIndex++;
    this.currentIndex %= this.props.children.length;
    this.forceUpdate();
  }
}

export default AnimCycleContent;
