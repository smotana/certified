import React, { Component } from 'react';
import {
  FormGroup,
  FormControl,
  Col,
  ControlLabel,
} from 'react-bootstrap';
import PropTypes from 'prop-types';
import EditingIcon from '@material-ui/icons/Edit';

class InputEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newValue: this.props.value,
      hasChanged: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.value !== this.props.value) {
      this.setState({
        newValue: nextProps.value,
        hasChanged: false,
      });
    }
    if(this.props.clearAfterEnabling && !nextProps.disabled && this.props.disabled) {
      this.setState({
        newValue: this.props.value,
        hasChanged: false,
      });
    }
  }

  onChangeVal(event) {
    let newValue = event.target.value;
    let hasChanged = this.props.value !== newValue

    this.props.onChange && this.props.onChange(hasChanged, newValue);

    this.setState({
      newValue: newValue,
      hasChanged: hasChanged,
    });
  }

  render() {
    let validationState = this.state.hasChanged
      ? 'warning'
      : this.props.defaultState;

    let glyph = this.state.hasChanged
      ? (
          <FormControl.Feedback>
            <EditingIcon style={{fontSize: '20px', margin: '7px'}} />
          </FormControl.Feedback>
        )
      : null;

    return (
      <FormGroup validationState={validationState}>
        <Col componentClass={ControlLabel} sm={2}>{this.props.name}</Col>
        <Col sm={10}>
          <FormControl
            name={this.props.name}
            type={this.props.type}
            value={this.state.newValue}
            onChange={this.onChangeVal.bind(this)}
            onFocus={this.props.onFocus}
            onBlur={this.props.onBlur}
            placeholder={this.props.placeholder}
            disabled={this.props.disabled}
          />
          {glyph}
          {this.props.children}
       </Col>
      </FormGroup>
    );
  }
}

InputEdit.propTypes = {
  name: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  defaultState: PropTypes.string, // Should either be null, or after a save, should either be 'success' or 'error'
  onChange: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  type: PropTypes.string,
  disabled: PropTypes.bool,
  clearAfterEnabling: PropTypes.bool // Special property for passwords to clear value after disabled is set to true
};


export default InputEdit;
