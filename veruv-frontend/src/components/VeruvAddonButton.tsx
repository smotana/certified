import React, { Component } from 'react';
import { addons } from '../util/addonUtil';
import { ClientBrowser, detectClientBrowser } from '../util/detectClientBrowser';
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';

interface Props {
  style?:React.CSSProperties;
  trackOnOpen:(clientBrowser:ClientBrowser)=>void;
}

interface State {
  clientBrowserSelection:ClientBrowser;
}

class VeruvAddonButton extends Component<Props, State> {

  constructor(props) {
    super(props);

    this.state = {
      clientBrowserSelection: detectClientBrowser(),
    }
  }

  render() {
    var addonKeys = Object.keys(addons) as ClientBrowser[];
    var availableIn = (
      <div style={{
        fontSize: '0.9em',
        opacity: 0.6,
      }}>
        Available for&nbsp;
        {addonKeys.map((clientBrowser:ClientBrowser, index) => [
          (
            <Link
              style={{textDecoration: 'underline'}}
              to=""
              onClick={() => this.setState({clientBrowserSelection: clientBrowser})}
            >{clientBrowser}</Link>
          ),
          (index < addonKeys.length - 2)
            ? ", "
            : ((index < addonKeys.length - 1)
              ? " and "
              : null
            )
        ])}
      </div>
    );

    // Find addon based on current browser, defaults to Chrome
    var addon = addons[this.state.clientBrowserSelection]
      || addons[ClientBrowser.CHROME];
    if(!addon) {
      return availableIn;
    }  

    return (
      <div style={{
        width: '250px',
        ...this.props.style,
      }}>
        <Button
          style={{
            width: '70%',
            padding: '2px 10px',
            margin: '10px',
          }}
          onClick={e => {
            window.open(addon.storeLink)

            this.props.trackOnOpen(addon.clientBrowser);
          }}
        >
          <img
            style={{width: '100%'}}
            src={addon.getAddonImg} />
        </Button>
        {availableIn}
      </div>
    );
  }
}

export default VeruvAddonButton;
