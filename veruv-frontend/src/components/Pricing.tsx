import * as React from 'react';
import {
  Panel,
  ListGroupItem,
  ListGroup,
  Button,
} from 'react-bootstrap';
import GridExt from './GridExt';
import { LinkContainer } from 'react-router-bootstrap';

type PricingContent = React.ReactNode;

type BuilderColumn = number;

class PricingBuilder {
  readonly styles = {
    panelStyle: {
      margin: '0px auto 40px auto',
    },
    headingContainer: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      minHeight: '96px',
    },
    headingText: {
      fontSize: '1.5em',
    },
    text: {
    },
    subText: {
      marginTop: '7px',
      fontSize: '0.85em',
      color: 'grey',
    },
    price: {
      fontSize: '2em',
      color: 'darkgreen',
    },
    priceDesc: {
      color: 'grey',
    },
  };
  panelStyle:React.CSSProperties|undefined = undefined;

  columns:React.ReactNode[][] = [];
  headings:{[column:number]:React.ReactNode} = {};

  createColumn(heading:string|undefined = undefined, subheading:string|undefined = undefined):BuilderColumn {
    this.columns.push([]);
    var column:BuilderColumn = this.columns.length - 1;
    if(heading || subheading) {
      this.headings[column] = (
        <div>
          {heading && <div style={{...this.styles.text, ...this.styles.headingText}}>{heading}</div>}
          {subheading && <div style={this.styles.subText}>{subheading}</div>}
        </div>
      );
    }
    return column;
  }

  addRowText(column:BuilderColumn|BuilderColumn[], text:string, subtext:string|undefined = undefined) {
    if(typeof column !== "number") {
      column.forEach(c => this.addRowText(c, text, subtext));
      return;
    }
    this.columns[column].push((
      <div>
        <div style={this.styles.text}>{text}</div>
        {subtext && <div style={this.styles.subText}>{subtext}</div>}
      </div>
    ));
  }

  addRowButton(column:BuilderColumn|BuilderColumn[], title:string, link:string) {
    if(typeof column !== "number") {
      column.forEach(c => this.addRowButton(c, title, link));
      return;
    }
    this.columns[column].push((
      <LinkContainer to={link}>
        <Button>{title}</Button>
      </LinkContainer>
    ));
  }

  addRowPrice(column:BuilderColumn|BuilderColumn[], price:number|string, perDesc:string|undefined = undefined) {
    if(typeof column !== "number") {
      column.forEach(c => this.addRowPrice(c, price, perDesc));
      return;
    }
    this.addRowContent(column, (
      <div>
        <span style={this.styles.price}>{price}</span>
        {perDesc && (
          <span style={this.styles.priceDesc}> {perDesc}</span>
        )}
      </div>
    ));
  }

  addRowContent(column:BuilderColumn|BuilderColumn[], content:React.ReactNode) {
    if(typeof column !== "number") {
      column.forEach(c => this.addRowContent(c, content));
      return;
    }
    this.columns[column].push(content);
  }

  setPanelStyle(style:React.CSSProperties) {
    this.panelStyle = style;
  }

  build(xsColCnt:number = 1):PricingContent {
    return (
      <GridExt xsColCnt={xsColCnt} smColCnt={this.columns.length}>
        {this.columns.map((column, index) => {return (
          <Panel style={{
            ...this.styles.panelStyle,
            ...this.panelStyle,
          }}>
            {this.headings[index] && (
              <Panel.Heading style={this.styles.headingContainer}>
                {this.headings[index]}
              </Panel.Heading>
            )}
            <ListGroup fill>
              {column.map(row => { return (
                <ListGroupItem>
                  {row}
                </ListGroupItem>
              )})}
            </ListGroup>
          </Panel>
        )})}
      </GridExt>
    );
  }
}

export default PricingBuilder;
