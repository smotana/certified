import React, { Component } from 'react';
import AnimBubble from './AnimBubble';

interface Props {
  src:string;
  radius:string;
  style?:React.CSSProperties;
  scale?:boolean;
}

class CircleImage extends Component<Props> {
  render() {
    return (
      <div style={{
          margin: 'auto',
          boxShadow: 'rgba(0, 9, 152, 0.1) 1px 5px 9px 4px',
          overflow: 'hidden',
          borderRadius: this.props.radius,
          ...({
            width: this.props.radius,
            height: this.props.radius,
          }),
          ...this.props.style
      }}>
        <img
          style={{
            ...(this.props.scale && {
              height: this.props.radius,
              width: 'auto',
            })
          }}
          src={this.props.src}
        />
      </div>
    );
  }
}

export default CircleImage;
