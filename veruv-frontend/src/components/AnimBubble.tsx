import React, { Component } from 'react';

interface Props {
  size:string;
  x?:string,
  y?:string,
  delay?:string,
  duration?:string,
  timingFunction?:string,
  background?:string,
}

class BubbleAnim extends Component<Props> {
  init:boolean = true;
  task:NodeJS.Timeout|undefined = undefined;

  componentDidMount() {
    if(this.init) {
      this.init = false;
      this.task = setTimeout(() => this.forceUpdate(), 1);
    }
  }

  componentWillUnmount() {
    this.task && clearTimeout(this.task);
  }

  render() {
    var size = this.init ? '0px' : this.props.size;
    var timingFunction = this.props.timingFunction || 'cubic-bezier(0.2, 1.08, 1, 0.99)';
    var duration = this.props.duration || '1s';
    return (
      <div style={{
        position: 'absolute',
        borderRadius: '50%',
        width: size,
        height: size,
        top: `calc(${this.props.y || '0px'} - ${size} / 2)`,
        left: `calc(${this.props.x || '0px'} - ${size} / 2)`,
        backgroundImage: this.props.background || 'radial-gradient(circle, rgba(0, 9, 152, 0.01) 50%, rgba(0, 9, 152, 0.1) 100%)',
        transition: `width ${duration} ${timingFunction},`
          + `height ${duration} ${timingFunction},`
          + `top ${duration} ${timingFunction},`
          + `left ${duration} ${timingFunction}`,
        transitionDelay: this.props.delay || '0s',
      }}>&nbsp;</div>
    );
  }
}

export default BubbleAnim;
