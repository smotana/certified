import React, { Component } from 'react';
import {
  Grid,
  Row,
  Col,
  Clearfix,
} from 'react-bootstrap';

// Wrapper for Grid Row and Col that automatically adds Clearfix to clear all appropriate rows.
class GridExt extends Component {
  static minMax(num, min, max) {
    return Math.max(min, Math.min(num, max));
  }

  render() {
    let defColCnt = GridExt.minMax(this.props.xsColCnt || this.props.smColCnt || this.props.mdColCnt || this.props.lgColCnt || this.props.defColCnt || 1, 1, 12);
    let xsColCnt = GridExt.minMax(this.props.xsColCnt, 1, 12) || defColCnt;
    let smColCnt = GridExt.minMax(this.props.smColCnt, 1, 12) || xsColCnt;
    let mdColCnt = GridExt.minMax(this.props.mdColCnt, 1, 12) || smColCnt;
    let lgColCnt = GridExt.minMax(this.props.lgColCnt, 1, 12) || mdColCnt;

    let cols = [];
    for(let i=0;i<this.props.children.length;i++) {
      if(i!==0 && (i%xsColCnt===0 || i%smColCnt===0 || i%mdColCnt===0 || i%lgColCnt===0)) {
        let clearfixProps = {};
        if(i%xsColCnt===0) clearfixProps.visibleXsBlock = true;
        if(i%smColCnt===0) clearfixProps.visibleSmBlock = true;
        if(i%mdColCnt===0) clearfixProps.visibleMdBlock = true;
        if(i%lgColCnt===0) clearfixProps.visibleLgBlock = true;
        cols.push(<Clearfix {...clearfixProps} />);
      }
      cols.push(
        <Col
          xs={Math.floor(12/xsColCnt)}
          xsOffset={i === 0 && Math.floor(12%xsColCnt/2)}
          sm={Math.floor(12/smColCnt)}
          smOffset={i === 0 && Math.floor(12%smColCnt/2)}
          md={Math.floor(12/mdColCnt)}
          mdOffset={i === 0 && Math.floor(12%mdColCnt/2)}
          lg={Math.floor(12/lgColCnt)}
          lgOffset={i === 0 && Math.floor(12%lgColCnt/2)}
        >
          <div style={{
            margin: 'auto',
            ...(this.props.spaceBetweenContent && {marginBottom: this.props.spaceBetweenContent}),
            ...(this.props.maxContentWidth && {maxWidth: this.props.maxContentWidth}),
            ...(this.props.contentStyle && this.props.contentStyle),
          }}>
            {this.props.children[i]}
          </div>
        </Col>);
    }
    return (<Grid fluid={true}><Row>{cols}</Row></Grid>);
  }
}

export default GridExt;
