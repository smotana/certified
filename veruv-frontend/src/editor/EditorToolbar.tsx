import * as React from 'react';
import {Tool} from './EditorScreen';
import {
  ButtonToolbar,
  ButtonGroup,
  Button,
  ToggleButtonGroup,
  ToggleButton,
  Popover,
  OverlayTrigger,
  DropdownButton,
  MenuItem,
  ControlLabel,
  FormGroup,
  Col,
  Collapse,
  InputGroup,
  FormControl,
} from 'react-bootstrap';
import { Verification, visibilityToUserString, Visibility, verificationExpiryPretty } from '../server/serverApi';
import CropIcon from '@material-ui/icons/CropRounded';
import RedactIcon from '@material-ui/icons/FormatPaint';
import UndoIcon from '@material-ui/icons/UndoOutlined';
import SaveIcon from '@material-ui/icons/SaveRounded';
import DeleteIcon from '@material-ui/icons/DeleteForever';
import ShareIcon from '@material-ui/icons/ShareRounded';
import CopyIcon from '@material-ui/icons/FileCopyOutlined';
import OpenIcon from '@material-ui/icons/OpenInNewRounded';
import copyToClipboard from '../util/copyUtil';
import ExtendExpiryButton from '../payment/ExtendExpiryButton';
import ReactGA from 'react-ga';
import PasswordStrengthMeterAsync from '../components/PasswordStrengthMeterAsync';

interface Props {
  style?:React.CSSProperties;
  snapshotId?:string;
  snapshot?:Verification;
  disabled?:boolean;
  hasMadeChanges:boolean;
  selectedTool:Tool|undefined;
  showGeneratedPassword?:string;
  handleToolSelectionChanged:(tool:Tool)=>void;
  handleUndo:()=>void;
  handleReset:()=>void;
  handleSave:()=>Promise<void>;
  handleDelete:()=>void;
  handleVisibilityChange:(visibility:Visibility, passOpt?:string)=>Promise<void>;
}

interface State {
  snapshotPasswordFocused:boolean;
  snapshotPassword?:string;
}

class EditorToolbar extends React.Component<Props, State> {
  styles = {
    toolbar: {
      verticalAlign: 'middle',
      display: 'flex',
      justifyContent: 'center',
      flexWrap: 'wrap' as 'wrap',
      padding: '0px',
      marginLeft: '0px', // Undo Bootstrap ButtonToolbar -5px margin
    },
    toolbarButton: {
      height: '38px',
    },
    shareButton: {
      height: '34px',
      fontSize: '1.4em'
    },
    center: {
      display: 'flex',
      justifyContent: 'center',
    },
    toolbarComponent: {
      display: 'flex',
      padding: '8px',
    },
    sharePopoverColumn: {
      paddingLeft: '0px',
    },
  }
  urlRef:HTMLInputElement|undefined = undefined;

  constructor(props) {
    super(props);

    this.state = {
      snapshotPasswordFocused: false,
    };
  }

  render() {
    var infoColumnSize = 3;
    var contentColumnSize = 9;
    var isLinkEnabled = this.props.snapshot && this.props.snapshot.visibility !== Visibility.PRIVATE;
    var verificationUrl = this.props.snapshotId
      ? location.protocol + '//' + location.host + '/v' + this.props.snapshotId : ''
    return (
      <ButtonToolbar style={{...this.styles.toolbar, ...this.props.style}}>
        <ToggleButtonGroup
          style={this.styles.toolbarComponent}
          type="radio"
          name="options"
          value={this.props.selectedTool}
          onChange={v => this.props.handleToolSelectionChanged(v)}
        >
          <ToggleButton
            value={Tool.CROP}
            disabled={this.props.disabled}
            style={this.styles.toolbarButton}>
            <CropIcon titleAccess='Crop tool' />
          </ToggleButton>
          <ToggleButton
            value={Tool.REDACT}
            disabled={this.props.disabled}
            style={this.styles.toolbarButton}>
            <RedactIcon titleAccess='Redaction tool' />
          </ToggleButton>
        </ToggleButtonGroup>
        <ButtonGroup style={this.styles.toolbarComponent}>
          <Button
            style={this.styles.toolbarButton}
            onClick={e => this.props.handleUndo()}
            disabled={this.props.disabled || !this.props.hasMadeChanges}>
            <UndoIcon titleAccess='Undo last change' />
          </Button>
          {/* <Button
            style={this.styles.toolbarButton}
            onClick={e => this.props.handleReset()}
            disabled={this.props.disabled || !this.props.hasMadeChanges}
            bsStyle={!this.props.hasMadeChanges ? 'default' : 'danger'}>
            <ResetIcon titleAccess='Reset changes' />
          </Button> */}
          <Button
            style={this.styles.toolbarButton}
            onClick={e => this.props.handleSave()}
            disabled={this.props.disabled || !this.props.hasMadeChanges}>
            <SaveIcon titleAccess='Save changes' />
          </Button>
          <OverlayTrigger
            trigger='click'
            placement='bottom'
            rootClose
            overlay={(
              <Popover title="Permanently delete?" id='permanently-delete'>
                <div style={this.styles.center}>
                  <Button
                    bsStyle='danger'
                    disabled={this.props.disabled}
                    onClick={e => {
                      this.props.handleDelete();
                      document.body.click() // Hide Overlay
                    }}
                  >Delete</Button>
                </div>
              </Popover>
            )}>
            <Button
              style={this.styles.toolbarButton}
              disabled={this.props.disabled}>
              <DeleteIcon titleAccess='Delete' />
            </Button>
          </OverlayTrigger>
        </ButtonGroup>
        <ButtonGroup style={this.styles.toolbarComponent}>
            <label className='btn btn-default disabled' style={{
              backgroundColor: '#fff',
              ...this.styles.toolbarButton,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'flex-start',
              paddingTop: '0px',
              paddingBottom: '0px',
              justifyContent: 'center',
              cursor: 'default',
              opacity: 1,
            }}>
              <div style={{fontSize: '9px'}}>Expires in:</div>
              <div style={{fontSize: '14px'}}>{verificationExpiryPretty(this.props.snapshot)}</div>
            </label>
            <ExtendExpiryButton
              trackSource='editor'
              snapshotId={this.props.snapshotId}
              disabled={this.props.disabled}
              buttonStyle={{
                ...this.styles.toolbarButton,
                borderTopLeftRadius: '0px',
                borderBottomLeftRadius: '0px',
                marginLeft: '-1px',
              }}
            />
        </ButtonGroup>
        <ButtonGroup style={this.styles.toolbarComponent}>
          <OverlayTrigger
            trigger='click'
            placement='bottom'
            rootClose
            overlay={(
              <Popover
                title='Share'
                id='share'
                bsStyle='success'
                style={{maxWidth: '100%'}}
              >
                <div
                  className="form-horizontal"
                  style={{
                    margin: '20px 20px 0px 20px',
                }}>
                  <FormGroup>
                    <Col
                      style={this.styles.sharePopoverColumn}
                      componentClass={ControlLabel}
                      sm={infoColumnSize}>
                      Sharing
                    </Col>
                    <Col
                      sm={contentColumnSize}>
                      <DropdownButton
                        disabled={this.props.disabled}
                        title={this.props.snapshot && visibilityToUserString[this.props.snapshot.visibility]}
                        key='info'
                        id='info'
                        onSelect={v => this.props.handleVisibilityChange(v)}
                      >
                        {Object['values'](Visibility).map(visibility => {
                          return (
                            <MenuItem
                              active={this.props.snapshot && this.props.snapshot.visibility === visibility}
                              eventKey={visibility}>
                              {visibilityToUserString[visibility]}
                            </MenuItem>
                          )
                        })}
                      </DropdownButton>
                      <Collapse in={this.props.snapshot && this.props.snapshot.visibility === Visibility.PASSWORD}>
                        <div style={{maxWidth: '163px', marginTop: '10px'}}>
                          <InputGroup>
                            <FormControl
                              disabled={this.props.disabled}
                              name="password"
                              placeholder="New pass"
                              type="password"
                              value={this.state.snapshotPassword}
                              onKeyDown={e => {
                                if(e.keyCode === 13) this.handlePasswordChange();
                              }}
                              onFocus={e => this.setState({snapshotPasswordFocused: true})}
                              onBlur={e => this.setState({snapshotPasswordFocused: false})}
                              onChange={e => this.setState({snapshotPassword: e.target['value']})}
                            />
                            <InputGroup.Button>
                              <Button
                                disabled={this.props.disabled}
                                onClick={e => this.handlePasswordChange()}
                              >Change</Button>  
                            </InputGroup.Button>
                          </InputGroup>
                          <PasswordStrengthMeterAsync
                              forceHide={!this.state.snapshotPasswordFocused}
                              style={{
                                width:'95%',
                                borderRadius: '0px 0px 4px 4px',
                              }}
                              password={this.state.snapshotPassword}
                              userInputs={['veruv', this.props.snapshotId]}
                            />
                          <Collapse in={!!this.props.showGeneratedPassword}>
                            <div>
                              Generated password: {this.props.showGeneratedPassword}
                            </div>
                          </Collapse>
                        </div>
                      </Collapse>
                    </Col>
                  </FormGroup>
                  <FormGroup>
                    <Col
                      style={this.styles.sharePopoverColumn}
                      componentClass={ControlLabel}
                      sm={infoColumnSize}>
                      Link
                    </Col>
                    <Col
                      sm={contentColumnSize}>
                      <div style={{
                        padding: '7px 21px 7px 0px',
                        fontSize: '1.1em',
                        ...(!isLinkEnabled && {opacity: 0.5}),
                      }}>
                        {verificationUrl}
                      </div>
                      <ButtonGroup style={{display: 'flex'}}>
                        <Button
                          disabled={!isLinkEnabled}
                          onClick={e => {
                            copyToClipboard(verificationUrl)

                            ReactGA.event({
                              category: 'editor',
                              action: 'share',
                              label: 'copyLink',
                            });
                          }}
                          style={{
                            height: '28px',
                            padding: '0px 12px',
                            display: 'flex',
                            alignItems: 'center',
                        }}>
                          <CopyIcon
                            titleAccess='Copy link'
                            fontSize='inherit'/>
                          <span style={{marginLeft: '10px'}}>Copy</span>
                        </Button>
                        <Button
                          disabled={!isLinkEnabled}
                          onClick={e => {
                            window.open(verificationUrl)

                            ReactGA.event({
                              category: 'editor',
                              action: 'share',
                              label: 'openLink',
                            });
                          }}
                          style={{
                            height: '28px',
                            padding: '0px 12px',
                            display: 'flex',
                            alignItems: 'center',
                        }}>
                          <OpenIcon
                            titleAccess='Open link'
                            fontSize='inherit'/>
                          <span style={{marginLeft: '10px'}}>Open</span>
                        </Button>
                      </ButtonGroup>
                    </Col>
                  </FormGroup>
                </div>
              </Popover>
            )}>
            <Button
              style={this.styles.toolbarButton}
              disabled={this.props.disabled}
              onClick={e => {
                if(this.props.hasMadeChanges) {
                  this.props.handleSave();
                }
                ReactGA.event({
                  category: 'editor',
                  action: 'share',
                  label: 'openPopup',
                });
              }}
              bsStyle='primary'>
              <ShareIcon titleAccess='Share link' />
            </Button>
          </OverlayTrigger>
        </ButtonGroup>
      </ButtonToolbar>
    );
  }

  handlePasswordChange() {
    this.props.handleVisibilityChange(Visibility.PASSWORD, this.state.snapshotPassword)
    .then(() => {
      this.setState({snapshotPassword: ''});
    });
  }
}

export default EditorToolbar;
