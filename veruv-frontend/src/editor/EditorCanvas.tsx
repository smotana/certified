import * as React from 'react';
import { Verification, SnapshotEdits, Box, Point } from '../server/serverApi';
import deepEqual from 'deep-equal';

enum MouseAction {
  Click = 'Click',
  SelectionStart = 'SelectionStart',
  SelectionEnd = 'SelectionEnd',
  Move = 'Move'
}

interface Props {
  snapshotId?:string;
  snapshot?:Verification;
  edits:SnapshotEdits;
  style?:React.CSSProperties;
  onDimensionsChange?:(dimensions:{width:number,height:number})=>void;
  onMouseAction?:(point:Point, action:MouseAction)=>void;
  onLoaded?:()=>void;
}

class EditorCanvas extends React.Component<Props> {
  readonly styles = {
    canvas: {
      display: 'block',
    },
  };
  canvasRef:React.RefObject<HTMLCanvasElement> = React.createRef();
  lastWidth?:number;
  lastHeight?:number;
  loaded:boolean = false;

  componentDidMount() {
    this.updateCanvas();
  }

  shouldComponentUpdate?(nextProps) {
    return this.props.snapshotId !== nextProps.snapshotId
      || !deepEqual(this.props.edits, nextProps.edits);
  }

  componentDidUpdate(prevProps) {
    this.updateCanvas();
  }

  render() {
    return (
      <canvas
        style={{...this.styles.canvas, ...this.props.style}}
        ref={this.canvasRef}
        onClick={e => this.handleMouseAction(e, MouseAction.Click)}
        onMouseMove={e => this.handleMouseAction(e, MouseAction.Move)}
        onTouchMove={e => this.handleMouseAction(e, MouseAction.Move)}
      />
    );
  }

  handleMouseAction(event, action:MouseAction) {
    if(!this.props.onMouseAction){
      return;
    }
    var rect = this.canvasRef.current!.getBoundingClientRect();
    var clientX, clientY;
    if(event.clientX && event.clientY) {
      clientX = event.clientX;
      clientY = event.clientY;
    } else if (event.touches && event.touches[0] && event.touches[0].clientX && event.touches[0].clientY) {
      clientX = event.touches[0].clientX;
      clientY = event.touches[0].clientY;
    } else {
      return;
    }
    var xPos = clientX - rect.left;
    var yPos = clientY - rect.top;
    if(action != MouseAction.Move) {
      console.log('mouse action', event, xPos, yPos, action, event.touches && event.touches[0], event.clientX, event.clientY);
    }
    this.props.onMouseAction({x: xPos, y: yPos}, action);
  }

  updateCanvas() {
    if(!this.canvasRef || !this.props.snapshot) {
      return;
    }

    var imageContainer = new Image();
    imageContainer.onload = () => {
      // Draw optionally cropped image
      var crop = this.props.edits.crop;
      var width = crop ? Math.abs(crop.p1.x - crop.p2.x) : imageContainer.width;
      var height = crop ? Math.abs(crop.p1.y - crop.p2.y) : imageContainer.height;

      this.canvasRef.current!.width = width;
      this.canvasRef.current!.height = height;
      if(!this.lastHeight
        || !this.lastWidth
        || this.lastHeight != height
        || this.lastWidth != width) {
        this.lastHeight = height;
        this.lastWidth = width;
        this.props.onDimensionsChange && this.props.onDimensionsChange({width: width, height: height});
      }

      var context = this.canvasRef.current!.getContext('2d')!;
      if(crop) {
        var sX = Math.min(crop.p1.x, crop.p2.x);
        var sY = Math.min(crop.p1.y, crop.p2.y);
        context.drawImage(imageContainer,
          sX, sY, width, height,
          0, 0, width, height);
      } else {
        context.drawImage(imageContainer,
          0, 0, width, height);
      }

      // Add redactions on top of image
      if(this.props.edits.redactions) {
        context.fillStyle = 'black';
        this.props.edits.redactions.forEach(redaction => {
          var xRedaction = Math.min(redaction.p1.x, redaction.p2.x);
          var yRedaction = Math.min(redaction.p1.y, redaction.p2.y);
          if(crop) {
            xRedaction -= Math.min(crop.p1.x, crop.p2.x);
            yRedaction -= Math.min(crop.p1.y, crop.p2.y);
          }
          var widthRedaction = Math.abs(redaction.p1.x - redaction.p2.x);
          var heightRedaction = Math.abs(redaction.p1.y - redaction.p2.y);

          context.fillRect(xRedaction, yRedaction, widthRedaction, heightRedaction);
        });
      }

      if(!this.loaded) {
        this.loaded = true;
        this.props.onLoaded && this.props.onLoaded();
      }
    }
    imageContainer.src = 'data:image/png;base64,' + this.props.snapshot.screenshot;
  }
}

export { MouseAction };
export default EditorCanvas;
