import * as React from 'react';
import { Verification } from '../server/serverApi';
import {
  NavItem,
  Nav,
} from 'react-bootstrap';
import ReactGA from 'react-ga';

interface Props {
  style?:React.CSSProperties;
  snapshots:{[id:string]:Verification};
  defaultSelectedSnapshotId?:string;
  onSelect?:(id:string)=>void;
}

interface State {
  liveSnapshotId?:string
}

class SnapshotSelection extends React.Component<Props, State> {
  styles = {
    tabContainer: {
      display: 'flex',
      maxWidth: '1024px',
    },
    snapshotPreviewImg: {
      maxWidth: '100%',
      maxHeight: '75px',
    },
  }

  constructor(props) {
    super(props);

    this.state = {};
  }

  /**
   * Update liveSnapshotId based on incoming additions/removals of snapshots
   */
  static getDerivedStateFromProps(nextProps, prevState) {
    var snapshotIds:string[] =  Object.keys(nextProps.snapshots);
    if(// Set liveSnapshotId if not set
      !prevState.liveSnapshotId
      // or if current liveSnapshotId was removed
      || (prevState.liveSnapshotId && snapshotIds.indexOf(prevState.liveSnapshotId) == -1)) {
      var liveSnapshotId = snapshotIds[0];
      if(liveSnapshotId) {
        nextProps.onSelect && nextProps.onSelect(liveSnapshotId);
      }
      return {liveSnapshotId: liveSnapshotId};
    }
    return null;
  }

  render() {
    var activeId = this.state.liveSnapshotId || this.props.defaultSelectedSnapshotId;
    return (
        <Nav
          className='snapshotSelectionNav' // used in index.css
          style={{...this.styles.tabContainer, ...this.props.style}}
          activeKey={activeId}
          bsStyle='tabs'
          onSelect={eventKey => this.handleSelect(eventKey)}
        >
          {Object["entries"](this.props.snapshots)
            .sort(([leftId, leftVerif], [rightId, rightVerif]) =>  leftVerif.ts - rightVerif.ts)
            .map(([id, verif])=> (
              <NavItem key={id} eventKey={id}>
                <img
                  style={{...this.styles.snapshotPreviewImg,
                    ...{opacity: id === activeId ? 1.0 : 0.5}}}
                  src={'data:image/png;base64,' + verif.screenshot}
                />
              </NavItem>
          ))}
        </Nav>
    );
  }

  handleSelect(id) {
    this.setState({liveSnapshotId: id});
    this.props.onSelect && this.props.onSelect(id);

    ReactGA.event({
      category: 'editor',
      action: 'select',
    });
  }
}

export default SnapshotSelection;
