import * as React from 'react';
import { Verification, SnapshotEdits, Box, Point } from '../server/serverApi';
import EditorCanvas, { MouseAction } from './EditorCanvas';
import { minSizedBox } from '../util/pointUtil';

enum Tool {
  CROP = 'crop',
  REDACT = 'redact',
}

interface _Selection {
  tool:Tool;
  box:Box;
}

interface Props {
  redactionMinSize:number;
  snapshotId?:string;
  snapshot?:Verification;
  edits:SnapshotEdits;
  selection?:Selection;
  style?:React.CSSProperties;
  onMouseAction?:(point:Point, action:MouseAction)=>void;
  onLoaded?:()=>void;
}

interface State {
  dimensions?:{width:number,height:number}
}

class EditorScreen extends React.Component<Props, State> {
  readonly styles = {
    outerContainer: {
      display: 'block',
    },
    container: {
      position: 'relative' as 'relative',
      margin: 'auto',
      transition: 'width 200ms ease-out, height 200ms ease-out',
      display: 'table-cell',
    },
    selection: {
      pointerEvents: 'none' as 'none',
      position: 'absolute' as 'absolute',
      opacity: '0',
      zIndex: 11,
    },
    selectionRedactTool: {
      backgroundColor: 'rgba(0, 0, 0, 0.3)',
    },
    selectionCropTool: {
      boxShadow: '0px 0px 0px 10000px rgba(0, 0, 0, 0.3)',
    },
    canvas: {
      position: 'absolute' as 'absolute',
      top: 0,
      left: 0,
      zIndex: 10,
    }
  };
  canvasRef:React.RefObject<HTMLCanvasElement> = React.createRef();

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    var selection = this.getCurrentSelection();
    return (
      <div style={{
        ...this.styles.outerContainer,
        ...this.props.style,
      }}>
        <div style={{
          ...this.styles.container,
          ...(this.state.dimensions ? {width: this.state.dimensions.width+'px', height: this.state.dimensions.height+'px'} : null),
        }}>
          <div style={{...this.styles.selection, ...selection}} />
          <EditorCanvas
            style={this.styles.canvas}
            snapshotId={this.props.snapshotId}
            snapshot={this.props.snapshot}
            edits={this.props.edits}
            onDimensionsChange={d => this.handleDimensionsChange(d)}
            onMouseAction={this.props.onMouseAction}
            onLoaded={this.props.onLoaded}
          />
        </div>
      </div>
    );
  }

  handleDimensionsChange(d) {
    this.setState({dimensions: d}); 
  }

  getCurrentSelection() {
    if(!this.props.selection) {
      return null;
    }
    var selectionBox = this.props.selection.tool == Tool.REDACT
      ? minSizedBox(this.props.redactionMinSize, this.props.selection.box)
      : this.props.selection.box;
    var positionCss = {
      opacity: '1',
      left: Math.min(selectionBox.p1.x, selectionBox.p2.x),
      top: Math.min(selectionBox.p1.y, selectionBox.p2.y),
      width: Math.abs(selectionBox.p1.x - selectionBox.p2.x),
      height: Math.abs(selectionBox.p1.y - selectionBox.p2.y),
    }
    var toolCss;
    switch (this.props.selection.tool) {
      case Tool.REDACT:
        toolCss = this.styles.selectionRedactTool;
        positionCss.width = Math.max(this.props.redactionMinSize, positionCss.width);
        positionCss.height = Math.max(this.props.redactionMinSize, positionCss.height);
        break;
      case Tool.CROP:
        toolCss = this.styles.selectionCropTool;
        break;
      default:
        throw new Error(`Unknown tool ${this.props.selection.tool}`);
    }
    return {...toolCss, ...positionCss};
  }
}

export type Selection = _Selection;
export { Tool, MouseAction };
export default EditorScreen;
