import * as React from 'react';
import { Verification, SnapshotEdits, Point, Box, Visibility } from '../server/serverApi';
import EditorScreen, { Tool, Selection, MouseAction } from './EditorScreen';
import EditorToolbar from './EditorToolbar';
import { minSizedBox } from '../util/pointUtil';
import { Action, Status } from '../server/action';
import LiveWindow from '../browser/LiveWindow';
import { FormControl } from 'react-bootstrap';
import ReactGA from 'react-ga';
import generator from 'generate-password';

interface Props {
  style?:React.CSSProperties;
  action:Action;
  snapshotId?:string;
  snapshot?:Verification;
  onDeleted?:()=>void;
  onLoaded?:()=>void;
}


interface State {
  edits:SnapshotEdits;
  selectedTool?:Tool;
  /**
   * If null, no selection is being made.
   * If set, first point p1 is starting point,
   * p2 corresponds to current mouse position.
   */
  toolSelection?:Box;
  isMakingCallToServer?:boolean;
  failureType?:'failedSave'|'failedDelete'|'failedSetVisibilityPublic'|'failedSetVisibilityPrivate';
  noteFocused?:boolean;
  generatedPassword?:string;
}

class Editor extends React.Component<Props, State> {
  static readonly MOUSE_EVENT_THROTTLE_TIME = 100;
  /** Also change on server side in SanitizerImageEdits.java */
  readonly REDACTION_MIN_SIZE = 15;
  /** Also change on server side in SanitizerNote.java */
  static readonly NOTE_MAX_LENGTH = 1024;
  styles = {
    editor: {
      border: "1px solid #ddd",
      borderTop: "0px",
      borderRadius: "4px",
      borderTopRightRadius: '0px',
      borderTopLeftRadius: '0px',
      boxShadow: 'rgba(0, 9, 152, 0.1) 1px 5px 9px 4px',
      backgroundColor: 'white',
    },
    toolbar: {
      padding: '5px',
      borderBottom: '1px solid #ddd',
    },
    noteInput: {
      resize: 'none' as 'none',
      fontSize: '1.2em',
      border: '0px',
      borderBottom: '1px solid rgb(221, 221, 221)',
      borderRadius: '0px',
      transition: 'height 200ms ease-out',
    },
    noteFocused: { height: '170px' },
    noteNotFocused: { height: '34px' },
  }
  editsHistory:SnapshotEdits[] = [];
  noteInputRef?:HTMLInputElement;
  lastMouseClickTime:number = 0;

  constructor(props) {
    super(props);

    this.state = {
      edits: {}
    };
  }

  shouldComponentUpdate?(nextProps) {
    if(this.props.snapshotId !== nextProps.snapshotId) {
      this.editsHistory = [];
      this.setState({edits: {}});
    }
    return true;
  }

  render() {
    var selection;
    if(this.state.toolSelection && this.state.selectedTool) {
      selection = {
        tool: this.state.selectedTool,
        box: this.state.toolSelection,
      };
    }

    var messageShow:boolean = false;
    var messageText:string|undefined = undefined;
    var messageRetryClick:(()=>void)|undefined = undefined;
    switch(this.state.failureType) {
      case 'failedSave':
        messageShow = true;
        messageText = 'Failed to save changes';
        messageRetryClick = () => {
          this.setState({failureType: undefined});
          this.handleSave();
        };
        break;
      case 'failedDelete':
        messageShow = true;
        messageText = 'Failed to delete';
        messageRetryClick = () => {
          this.setState({failureType: undefined});
          this.handleDelete();
        };
        break;
      case 'failedSetVisibilityPublic':
      case 'failedSetVisibilityPrivate':
        messageShow = true;
        messageText = 'Failed to set visibility';
        messageRetryClick = () => {
          this.setState({failureType: undefined});
          this.handleVisibilityChange(this.state.failureType === 'failedSetVisibilityPublic' ? Visibility.PUBLIC : Visibility.PRIVATE);
        };
        break;
    }

    var noteValue = this.state.edits.note !== undefined
      ? this.state.edits.note
      : this.props.snapshot && this.props.snapshot.note;

    return (
      <div style={{...this.styles.editor, ...this.props.style}}>
        <EditorToolbar
          style={this.styles.toolbar}
          snapshotId={this.props.snapshotId}
          snapshot={this.props.snapshot}
          disabled={this.state.isMakingCallToServer}
          hasMadeChanges={this.editsHistory.length > 0}
          selectedTool={this.state.selectedTool}
          showGeneratedPassword={this.state.generatedPassword}
          handleToolSelectionChanged={tool => this.handleToolSelectionChanged(tool)}
          handleUndo={() => this.handleUndo()}
          handleReset={() => this.handleReset()}
          handleSave={() => this.handleSave()}
          handleDelete={() => this.handleDelete()}
          handleVisibilityChange={(v,p) => this.handleVisibilityChange(v,p)}
        />
        <FormControl
          name="note"
          style={{...this.styles.noteInput,
            ...{height: this.state.noteFocused && this.noteInputRef && this.noteInputRef.scrollHeight
              ? (this.noteInputRef.scrollHeight + 2) + 'px' : '50px' }}}
          inputRef={ref => this.noteInputRef = ref}
          maxLength={Editor.NOTE_MAX_LENGTH}
          onFocus={e => this.setState({noteFocused: true})}
          onBlur={e => this.setState({noteFocused: false})}
          componentClass="textarea"
          disabled={this.state.isMakingCallToServer}
          placeholder='Optional note to display'
          value={noteValue}
          onChange={e => this.handleNoteChange(e)} />
        <LiveWindow
          showProgressBar={this.state.isMakingCallToServer}
          messageShow={messageShow}
          messageContent={messageText}
          messageStyle='danger'
          messageButtonPrimaryShow={true}
          messageButtonPrimaryTitle='Retry'
          messageButtonPrimaryClick={messageRetryClick}
          messageButtonSecondaryShow={true}
          messageButtonSecondaryTitle='Dismiss'
          messageButtonSecondaryClick={() => {
            this.setState({failureType: undefined});
          }}
        >
          <EditorScreen
            style={{cursor: (this.state.selectedTool ? 'crosshair' : 'default')}}
            redactionMinSize={this.REDACTION_MIN_SIZE}
            snapshotId={this.props.snapshotId}
            snapshot={this.props.snapshot}
            edits={this.state.edits}
            onMouseAction={(point:Point, action:MouseAction) => this.handleOnMouseAction(point, action)}
            selection={selection}
            onLoaded={this.props.onLoaded}
          />
        </LiveWindow>
      </div>
    );
  }

  handleNoteChange(event) {
    if(!this.props.snapshotId || !this.props.snapshot) {
      return;
    }
    var currentEdits = this.state.edits;
    var newEdits = {
      crop: currentEdits.crop,
      redactions: currentEdits.redactions,
      note: event.target.value
    };

    var prevEdits = this.editsHistory[this.editsHistory.length - 1] || currentEdits;
    var prevNote = prevEdits && prevEdits.note;
    var prevNoteWithDefault = this.editsHistory.length <= 1
      ? this.props.snapshot && this.props.snapshot.note
      : prevNote
    var currentNote = currentEdits.note;
    var newNote = newEdits.note;
    if(prevNote === currentNote) {
      // Only add history snapshot when first starting to change note
      // This way undo button will undo all note changes, not just last character
      this.editsHistory.push(currentEdits);

      ReactGA.event({
        category: 'editor',
        action: 'note',
        label: 'changed',
      });
    } else if(prevNoteWithDefault === newNote) {
      // If the change to note has reverted the note changes,
      // remove the no-op history snapshot
      this.editsHistory.pop();

      ReactGA.event({
        category: 'editor',
        action: 'note',
        label: 'reverted',
      });
    }

    this.setState({
      edits: newEdits
    })
  }

  handleSave() {
    if(!this.props.snapshotId || this.editsHistory.length == 0) {
      return Promise.resolve(undefined);
    }
    this.setState({
      isMakingCallToServer: true,
      toolSelection: undefined
    });

    ReactGA.event({
      category: 'editor',
      action: 'save',
    });

    return this.props.action.applyEdits(this.props.snapshotId, this.state.edits)
      .then(() => {
        this.editsHistory = [];
        this.setState({
          isMakingCallToServer: false,
          edits: {}
        });
      })
      .catch((e) => {
        this.setState({
          isMakingCallToServer: false,
          failureType: 'failedSave'
        });
        throw e; // Propagate
      });
  }

  handleDelete() {
    if(!this.props.snapshotId) {
      return;
    }
    this.setState({
      isMakingCallToServer: true,
      toolSelection: undefined
    });

    ReactGA.event({
      category: 'editor',
      action: 'delete',
    });

    this.props.action.deleteVerification(this.props.snapshotId)
      .then(() => {
        this.editsHistory = [];
        this.setState({
          isMakingCallToServer: false,
          edits: {}
        });
        this.props.onDeleted && this.props.onDeleted();
      })
      .catch((e) => {
        this.setState({
          isMakingCallToServer: false,
          failureType: 'failedSave'
        });
        throw e; // Propagate
      });
  }

  handleVisibilityChange(visibility:Visibility, password?:string):Promise<void> {
    if(!this.props.snapshotId) {
      return Promise.reject();
    }

    var generatedPassword:string|undefined = undefined
    if(visibility === Visibility.PASSWORD) {
      if(!password) {
        generatedPassword = generator.generate({
          length: 10,
          numbers: true,
          uppercase: true,
          excludeSimilarCharacters: true,
          strict: true,
        });      
        password = generatedPassword;
      }
    } else {
      password = undefined;
    }

    this.setState({
      isMakingCallToServer: true,
      generatedPassword: generatedPassword,
    });

    ReactGA.event({
      category: 'editor',
      action: 'visibilitySet',
      label: visibility,
    });

    return this.props.action.setVisibility(
      this.props.snapshotId,
      visibility,
      password)
      .then(() => {
        this.setState({
          isMakingCallToServer: false,
        });
      })
      .catch((e) => {
        this.setState({
          isMakingCallToServer: false,
          failureType: visibility === Visibility.PUBLIC ? 'failedSetVisibilityPublic' : 'failedSetVisibilityPrivate'
        });
        throw e; // Propagate
      });
  }

  handleUndo() {
    var prevEdits = this.editsHistory.pop()!;
    this.setState({edits: prevEdits});

    ReactGA.event({
      category: 'editor',
      action: 'undo',
    });
  }

  handleReset() {
    this.editsHistory = [];
    this.setState({edits: {}});

    ReactGA.event({
      category: 'editor',
      action: 'reset',
    });
  }

  handleToolSelectionChanged(selectedTool) {
    this.setState({
      selectedTool: this.state.selectedTool === selectedTool ? undefined : selectedTool,
      toolSelection: undefined,
    });

    ReactGA.event({
      category: 'editor',
      action: 'toolSelect',
      label: selectedTool,
    });
  }

  handleOnMouseAction(mousePoint:Point, action:MouseAction) {

    if(action === MouseAction.Click) {
    }

    switch(action) {
      case MouseAction.Click:
      case MouseAction.SelectionStart:
      case MouseAction.SelectionEnd:
        var currTime = new Date().getTime();
        if(currTime - this.lastMouseClickTime <= Editor.MOUSE_EVENT_THROTTLE_TIME) {
          console.log("mouse event throttled")
          break;
        }
        this.lastMouseClickTime = currTime;
        this.state.toolSelection
          ? this.handleSelectionEnd(mousePoint)
          : this.handleSelectionStart(mousePoint);
        break;
      case MouseAction.Move:
        this.handleMouseMove(mousePoint);
        break;
    }
  }

  handleMouseMove(mousePoint:Point) {
    if(!this.state.toolSelection) {
      return;
    }
    this.setState({
      toolSelection: {
        p1: this.state.toolSelection.p1,
        p2: mousePoint,
      }
    });
  }

  handleSelectionStart(mousePoint:Point) {
    if(!this.state.selectedTool) {
      return;
    }
    this.setState({
      toolSelection: {
        p1: mousePoint,
        p2: mousePoint,
      }
    });

    ReactGA.event({
      category: 'editor',
      action: 'selectionStart',
      label: this.state.selectedTool,
    });
  }

  handleSelectionEnd(mousePoint:Point) {
    if(!this.state.selectedTool || !this.state.toolSelection) {
      return;
    }
    var previousCropOffset:Point = this.state.edits.crop
      ? {
        x:Math.min(this.state.edits.crop.p1.x, this.state.edits.crop.p2.x),
        y:Math.min(this.state.edits.crop.p1.y, this.state.edits.crop.p2.y)
      } : {
        x:0,
        y:0
      };
    var pointInitial:Point = {
      x: previousCropOffset.x + this.state.toolSelection.p1.x,
      y: previousCropOffset.y + this.state.toolSelection.p1.y,
    }
    // Use current mousepoint instead of outdated toolSelection p2
    var pointCurrent:Point = {
      x: previousCropOffset.x + mousePoint.x,
      y: previousCropOffset.y + mousePoint.y,
    }
    var selectionBox:Box = {
      p1: {
        x: Math.min(pointInitial.x, pointCurrent.x),
        y: Math.min(pointInitial.y, pointCurrent.y)
      },
      p2: {
        x: Math.max(pointInitial.x, pointCurrent.x),
        y: Math.max(pointInitial.y, pointCurrent.y)
      }
    };

    var newEdits;
    switch (this.state.selectedTool) {
      case Tool.CROP:
        newEdits = {
          crop: selectionBox,
          redactions: this.state.edits.redactions,
          note: this.state.edits.note,
        };
        break;
      case Tool.REDACT:
        selectionBox = minSizedBox(this.REDACTION_MIN_SIZE, selectionBox);
        newEdits = {
          crop: this.state.edits.crop,
          redactions: this.state.edits.redactions ? this.state.edits.redactions.concat(selectionBox) : [selectionBox],
          note: this.state.edits.note,
        };
        break;
      default:
        throw new Error(`Unknown tool ${this.state.selectedTool}`);
    }

    ReactGA.event({
      category: 'editor',
      action: 'selectionEnd',
      label: this.state.selectedTool,
    });

    this.editsHistory.push(this.state.edits);
    this.setState({
      toolSelection: undefined,
      edits: newEdits
    })
  }
}

export default Editor;
