import React from 'react';
import { Status } from '../server/action';
import { Verification, verificationExpiryPretty, visibilityToUserString } from '../server/serverApi';
import Hr from '../Hr';
import ExtendExpiryButton from '../payment/ExtendExpiryButton';

interface Props {
  status:Status;
  verification:Verification;
}

class VerificationContent extends React.Component<Props> {
  readonly styles = {
    scroll: {
      overflowX: 'scroll' as 'scroll',
      WebkitOverflowScrolling: 'touch' as 'touch',
      maxWidth: '100%',
      padding: '0px',
    },
    dataContainer: {
      display: 'block',
      textAlign: 'left' as 'left',
      border: "1px solid #ccc",
      borderRadius: "4px",
      margin: '0px 20px 20px',
      padding: '20px',
      background: 'white',
      maxWidth: '100%',
      overflow: 'hidden',
    },
    noteContainer: {
      flex: '1 1 0',
      minHeight: 0,
      minWidth: '250px',
      maxWidth: 'fit-content',
    },
    dataHeader: {
      fontSize: '2em',
    },
    dataHr: {
      width: '50%',
      maxWidth: '100px',
      marginTop: '10px',
      marginBottom: '10px',
    },
    dataKey: {
      color: 'grey',
    },
    dataValue: {
      fontSize: '1.2em',
    },
    dataUrl: {
      overflowX: 'scroll' as 'scroll',
      whiteSpace: 'nowrap' as 'nowrap',
      borderBottom: '1px solid #ccc',
      backgroundColor: '#f5f5f5',
      padding: '9.5px',
    },
    dataNote: {
      overflowY: 'scroll' as 'scroll',
      maxHeight: '140px',
      whiteSpace: 'pre-wrap' as 'pre-wrap',
      color: '#333',
      padding: '9.5px',
      backgroundColor: '#f5f5f5',
      border: '1px solid #ccc',
      borderRadius: '4px',
    },
    wordBreak: {
      wordBreak: 'break-all' as 'break-all',
      wordWrap: 'break-word' as 'break-word',
    },
    urlPrimary: {
    },
    urlSecondary: {
      opacity: 0.5,
    },
  };

  render() {
    return (
      <div style={{
        display: 'flex',
        flexDirection: 'row' as 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
        alignItems: 'flex-start'
      }}>
        {this.getHeader()}
        {this.getScreenshot()}
      </div>
    );
  }

  getHeader() {
    var tsDate = new Date(this.props.verification.ts);
    return (
      <div style={this.styles.dataContainer}>
        <img
          style={{
            display: 'block',
            margin: 'auto',
            width: '100%',
            maxWidth: '120px',
          }}
          src='/img/veruv-verified.png' />
        <Hr style={this.styles.dataHr} />

        <div style={{
          display: 'flex',
          flexDirection: 'row' as 'row',
          justifyContent: 'flex-start',
          flexWrap: 'wrap',
          alignItems: 'flex-start',
        }}>
          <div style={{
            display: 'flex',
            flexDirection: 'column' as 'column',
            alignItems: 'flex-start',
          }}>
            <div style={{margin: '6px'}}>
              <div style={this.styles.dataKey}>Date:</div>
              <div style={this.styles.dataValue}>{tsDate.toLocaleDateString()}</div>
            </div>
            <div style={{margin: '6px'}}>
              <div style={this.styles.dataKey}>Time:</div>
              <div style={this.styles.dataValue}>{tsDate.toLocaleTimeString()}</div>
            </div>
            <div style={{margin: '6px'}}>
              <div style={this.styles.dataKey}>Expires in:</div>
              <div style={this.styles.dataValue}>{verificationExpiryPretty(this.props.verification)}</div>
              <ExtendExpiryButton
                trackSource='snapshot'
                snapshotId={this.props.verification.id}
                buttonCollapseOnHide={true}
              />
            </div>
            <div style={{margin: '6px'}}>
              <div style={this.styles.dataKey}>Visibility:</div>
              <div style={this.styles.dataValue}>{visibilityToUserString[this.props.verification.visibility]}</div>
            </div>
          </div>
          <div style={{margin: '6px'}}>
            {this.getNote()}
          </div>
        </div>
      </div>
    );
  }

  getNote() {
    if(!this.props.verification.note) {
      return null;
    }
    return [
      <div style={this.styles.dataKey}>Note:</div>,
      <div style={{
        ...this.styles.dataValue,
        ...this.styles.dataNote,
        ...this.styles.wordBreak,
      }}>
        {this.props.verification.note}
      </div>
    ];
  }

  getScreenshot() {
    return (
      <div style={{
        ...this.styles.dataContainer,
        padding: '0px',
        margin: '0px',
      }}>
        <div style={{
          ...this.styles.scroll,
        }}>
          {this.getUrl()}
        </div>
        <div style={{
          ...this.styles.scroll,
        }}>
          <img src={'data:image/png;base64,' + this.props.verification.screenshot} style={{display: 'block'}} />
        </div>
      </div>
    );
  }

  getUrl() {
    var url:any = this.props.verification.url;
    try {
      var urlParsed = new URL(url);
      var hostStart = url.indexOf(urlParsed.host);
      var hostEnd = hostStart + urlParsed.host.length;
      url = (
        <div style={this.styles.dataUrl}>
          <span style={this.styles.urlSecondary}>
            {url.substring(0, hostStart)}
          </span>
          <span style={this.styles.urlPrimary}>
            {url.substring(hostStart,hostEnd)}
          </span>
          <span style={this.styles.urlSecondary}>
            {url.substring(hostEnd, url.length)}
          </span>
        </div>
      );
    } catch(e) {
      console.log('Failed parsing url', this.props.verification.url, e)
    }
    return url;
  }
}

export default VerificationContent;
