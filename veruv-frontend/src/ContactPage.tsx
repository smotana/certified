import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import ContactIcon from '@material-ui/icons/PersonRounded';
import setTitle from './util/titleUtil';


class ContactPage extends Component {
  readonly styles = {
  }

  componentDidMount() {
    setTitle('Contact');
  }

  render() {
    return (
      <div>
        <h1>
          <ContactIcon fontSize='default' />
          &nbsp;
          Contact
        </h1>
        <Grid style={{maxWidth:'512px'}}>
          <Row>
            <Col xs={2}>Sales:</Col>
            <Col xs={10}><a href="mailto:sales@veruv.com">sales@veruv.com</a></Col>
          </Row>
          <Row>
            <Col xs={2}>Security:</Col>
            <Col xs={10}><a href="mailto:security@veruv.com">security@veruv.com</a></Col>
          </Row>
          <Row>
            <Col xs={2}>Privacy:</Col>
            <Col xs={10}><a href="mailto:privacy@veruv.com">privacy@veruv.com</a></Col>
          </Row>
          <Row>
            <Col xs={2}>Legal:</Col>
            <Col xs={10}><a href="mailto:legal@veruv.com">legal@veruv.com</a></Col>
          </Row>
        </Grid>

      </div>
    );
  }
}

export default ContactPage;
