import React, { Component } from 'react';
import { Heading, AddonOrDirect, UseCase, Container, Image } from './UseCaseUtil';


class IncomeVerification extends Component {
  static useCase:UseCase = {
    name: 'income-verification',
    title: 'Digital Income Verification',
    thumbnail: {
      ratio: 1,
      path: '/img/usecase/income-verification/iv_headline.png'
    },
    generate:()=>(<IncomeVerification />)
  };

  render() {
    return (
      <Container>
        <Heading
          useCase={IncomeVerification.useCase}
          subtitle='Fast and secure way to create and send Income Verification completely online'
        />
        <h2>What is Income Verification?</h2>
        <p>Income Verification is a statement proving that a person receives a certain salary for a specific job. Typically, income verification is proven by providing a pay stub or a bank statement.</p>
        <p>Alternatively, you can issue an online certified Income Verification and transmit your verification quickly and securely.</p>
        <p>Check with the requesting party to make sure a digital Income Verification is acceptable.</p>

        <h3>Trusting a <i>digital</i> Income Verification</h3>
        <p>The requesting party needs to be confident that the verification you provide is <strong>legitimate income you receive from your employer</strong> and it has not been modified while in your possesion.</p>
        <ul>
          <li>
            <p><i>Physical</i> Income Verification typically establishes legitimacy with an official company stamp on a pay stub or a bank statement signed by your financial institute.</p>
          </li>
          <li>
            <p><i>Digital</i> Income Verification created via Veruv is certified by us to be legitimate. You access your income proof using Veruv's browser and a snapshot of that website will be stored securely with us. Once you share a link with the requesting party, they will receive that snapshot directly from us.</p>
          </li>
        </ul>
        <p>As you are never in possession of the certified snapshot, the requesting party can be confident the digital statement is legitimate.</p>

        <h3>Examples of verifying digital Income Verification</h3>
        <p>
          Take a look at the following example of a digital pay stub. Notice the important parts of this proof: 
        </p>
        <ul>
          <li>The website domain name is a legitimate website known to issue pay stubs</li>
          <li>Legal name of the individual is visible at the top left corner</li>
          <li>Salary is clearly visible and noted with a date</li>
          <li>All other sensitive information has been redacted with a black box</li>
        </ul>
        <Image src='/img/usecase/income-verification/iv_example.png' width={1024} />
        <p>
          Here is another example using a bank statement. You can also use a deposited cheque as well. Again, notice the important parts of this proof: 
        </p>
        <ul>
          <li>The website domain name is a legitimate website of a known bank</li>
          <li>Legal name of the individual is visible at the top right corner</li>
          <li>Salary deposit is clearly visible with an amount and a date of deposit</li>
          <li>All other sensitive information has been redacted with a black box</li>
        </ul>
        <Image src='/img/usecase/income-verification/iv_bank_statement.png' width={1024} />

        <h2>Creating a digital Income Verification</h2>
        <AddonOrDirect
          trackSource={IncomeVerification.useCase.name}
          instructionsSignIntoToSite={(
            <div>
              <h3>Find proof of income online</h3>
              <p>
                Income can be verified either with a pay stub or a bank deposit statement.
              </p>
              <ul>
                <li>If you receive your pay stubs online, visit the website you use to access them now.</li>
                <li>If you receive your payment with a cheque, your financial institution may have access to your deposited cheques online. Visit their website now and look for deposited cheques.</li>
                <li>If you receive a deposit directly to your account, your financial institution will have a record of the deposit. Visit their website now and look for the online bank statement indicating your pay deposit.</li>
              </ul>
            </div>
          )}
          instructionsSnapshotSite={(
            <div>
              <h3>Take a snapshot of your income verification</h3>
              <p>
                Make sure that all of the following information is clearly visible on the page:
              </p>
              <ul>
                <li><strong>Your identity</strong> such as your full name</li>
                <li><strong>Deposit amount</strong> of your salary</li>
                <li><strong>Date</strong> or indication when the deposit occurred</li>
              </ul>
            </div>
          )}
        />

        <h2>That's it!</h2>
        <p>
          You have successfully created your Income Verification. If you need more help, don't hesitate to contact us with questions or concerns at <a href={'mailto:support@veruv.com?subject=Help with '+IncomeVerification.useCase.title}>support@veruv.com</a>.
        </p>
      </Container>
    );
  }
}

export default IncomeVerification;
