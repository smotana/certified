import React, { Component } from 'react';
import { Heading, AddonOrDirect, UseCase, Container, Image } from './UseCaseUtil';


/** TODO TODO TODO */
class AccountOwnership extends Component {
  static useCase:UseCase = {
    name: 'account-ownership',
    title: 'Verify account ownership',
    thumbnail: {
      ratio: 1,
      path: '/img/usecase/ownership/header.png'
    },
    generate:()=>(<AccountOwnership />)
  };

  render() {
    return (
      <Container>
        <Heading
          useCase={AccountOwnership.useCase}
          subtitle='Prove that you are in control of any online account'
        />
        <h2>What does it mean to verify your ownership?</h2>
        <p>In many scenarios, you may have to prove ownership of an account to someone else. The other party needs to be certain that who they are speaking to is the <strong>true and verified person, group, or organization</strong> in control of a particular account.</p>
        <p>There are many types of accounts you may have to prove:</p>
        <ul>
          <li>Social media accounts (Facebook, Twitter)</li>
          <li>Email accounts (Gmail, Outlook, Yahoo)</li>
          <li>Instant messaging accounts (Facebook Messenger, WhatsApp, Line, WeChat)</li>
          <li>Game characters (Minecraft, Runescape, WoW)</li>
          <li>Bank accounts</li>
        </ul>

        <h3>Trusting <i>Verified</i> Ownership</h3>
        <p>The other party needs to be confident that you are who you claim to be. The typical proofs given to verify ownership are:</p>
        <ul>
          <li>
            <p><i>Send a message</i> from the account to indicate you are the true person. This is the most trustworthy approach as long as there is a messaging system in place to do this.</p>
          </li>
          <li>
            <p><i>Screenshot</i> of you logged in to the account in question. This is the simplest option but it does not prove ownership since a screenshot can be easily faked.</p>
          </li>
          <li>
            <p><i>Screen sharing</i> or <i>video chat</i> used for showing your logged in account. However a skilled person can also create a fake version of the website to pretend they are logged in to the account.</p>
          </li>
          <li>
            <p><i>Verified Ownership</i> created via Veruv is certified by us to be legitimate. You will use Veruv's browser to take a snapshot of you logged in to the account which will be stored securely with us. Once you share our link, they will receive that snapshot directly from us.</p>
          </li>
        </ul>
        <p>As you are never in possession of the verified snapshot, others can be confident your identity is true.</p>

        <h3>Example verifying Account Ownership</h3>
        <p>
          Take a look at the following example. Notice the important parts of this proof:
        </p>
        <ul>
          <li>The website domain name is the official website</li>
          <li>The page clearly shows a user is logged in and their account name is clearly visible</li>
          <li>All other sensitive information has been redacted with a black box</li>
        </ul>
        <Image src='/img/usecase/ownership/facebook.png' width={800} />
        <Image src='/img/usecase/ownership/twitter.png' width={800} />
        <Image src='/img/usecase/ownership/gmail.png' width={800} />
        <Image src='/img/usecase/ownership/runescape.png' width={800} />
        <Image src='/img/usecase/ownership/boa.png' width={800} />

        <h2>Creating a Verified Ownership</h2>
        <AddonOrDirect
          trackSource={AccountOwnership.useCase.name}
          instructionsSignIntoToSite={(
            <div>
              <h3>Sign in to your account</h3>
              <p>
                Navigate to the website where you can sign in.
              </p>
            </div>
          )}
          instructionsSnapshotSite={(
            <div>
              <h3>Take a snapshot of your account identity</h3>
              <p>
                Navigate to a page that clearly displayes the following:
              </p>
              <ul>
                <li><strong>Account identity</strong> such as your name, username or any other identifier linking your account.</li>
                <li><strong>Signed in status</strong> indicating that the account identity is indeed signed in currently. This may simply be a member area only accessible with your account.</li>
              </ul>
            </div>
          )}
        />

        <h2>That's it!</h2>
        <p>
          You have successfully created your Verified Ownership snapshot. If you need more help, don't hesitate to contact us with questions or concerns at <a href={'mailto:support@veruv.com?subject=Help with '+AccountOwnership.useCase.title}>support@veruv.com</a>.
        </p>
      </Container>
    );
  }
}

export default AccountOwnership;
