import React, { Component } from 'react';
import { Heading, AddonOrDirect, UseCase, Container, Image } from './UseCaseUtil';


class TranscriptFacebook extends Component {
  static useCase:UseCase = {
    name: 'facebook-transcript',
    title: 'Certified Facebook Transcript',
    thumbnail: {
      ratio: 1,
      path: '/img/usecase/transcript-facebook/transcript.png'
    },
    generate:()=>(<TranscriptFacebook />)
  };

  render() {
    return (
      <Container>
        <Heading
          useCase={TranscriptFacebook.useCase}
          subtitle='Certify a Facebook message or conversation as evidence'
        />
        <h2>What is a Certified Transcript?</h2>
        <p>Certified Transcript is a statement proving that a conversation between two or more parties has occurred. Typically, for legal purposes, law enforcement can request a transcript from Facebook directly. For non-legal matters, a screenshot of facebook messages is sufficient in most cases, but it does not prevent altering or injecting fake messages.</p>
        <p>Alternatively, you can create a Certified Transcript of your Facebook messages using Veruv.</p>
        <p>Check with the requesting party to make sure a Certified Transcript is acceptable.</p>

        <h3>Trusting a Certified Facebook Transcript</h3>
        <p>The requesting party needs to be confident that the <strong>conversation is legitimate as stated</strong> and it has not been modified while in your possesion.</p>
        <ul>
          <li>
            <p><i>Screenshot</i> of your messages can be easily manipulated by you or anyone in their posssesion. Unfortunately it cannot be trusted.</p>
          </li>
          <li>
            <p><i>Certified Transcript</i> created via Veruv is certified by us to be legitimate. You will use Veruv's browser to take a snapshot of the transcript which will be stored securely with us. Once you share a link with the requesting party, they will receive that snapshot directly from us.</p>
          </li>
        </ul>
        <p>As you are never in possession of the Certified Transcript, the requesting party can be confident the messages are legitimate.</p>

        <h3>Example verifying a Certified Transcript</h3>
        <p>
          Take a look at the following example of a Certified Transcript. Notice the important parts of this proof:
        </p>
        <ul>
          <li>The website domain name is the official Facebook website</li>
          <li>The name of the individuals are visible</li>
          <li>The dated messages are clearly visible</li>
          <li>All other sensitive information has been redacted with a black box</li>
        </ul>
        <Image src='/img/usecase/transcript-facebook/transcript.png' width={700} />

        <h2>Creating a Certified Transcript</h2>
        <AddonOrDirect
          trackSource={TranscriptFacebook.useCase.name}
          instructionsSignIntoToSite={(
            <div>
              <h3>Sign into Facebook's Messages</h3>
              <p>
                Facebook messages can be accessed on either of these two official Facebook websites:
              </p>
              <ul>
                <li>https://facebook.com/messages</li>
                <li>https://www.messenger.com</li>
              </ul>
              <p>
                Search or navigate to the Facebook message you would like certified.
              </p>
            </div>
          )}
          instructionsSnapshotSite={(
            <div>
              <h3>Take a snapshot of your conversation</h3>
              <p>
                Make sure that all of the following information is clearly visible on the page:
              </p>
              <ul>
                <li><strong>Your name</strong> at the top of the page</li>
                <li><strong>Names</strong> of all participants in the conversation.</li>
                <li><strong>Important messages</strong> that you would like to include in the transcript</li>
              </ul>
            </div>
          )}
        />

        <h2>That's it!</h2>
        <p>
          You have successfully created your Certified Transcript. If you need more help, don't hesitate to contact us with questions or concerns at <a href={'mailto:support@veruv.com?subject=Help with '+TranscriptFacebook.useCase.title}>support@veruv.com</a>.
        </p>
      </Container>
    );
  }
}

export default TranscriptFacebook;
