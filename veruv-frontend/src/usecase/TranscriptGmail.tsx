import React, { Component } from 'react';
import { Heading, AddonOrDirect, UseCase, Container, Image } from './UseCaseUtil';


class TranscriptGmail extends Component {
  static useCase:UseCase = {
    name: 'gmail-email',
    title: 'Notarized email on Gmail',
    thumbnail: {
      ratio: 1,
      path: '/img/usecase/transcript-gmail/email.png'
    },
    generate:()=>(<TranscriptGmail />)
  };

  render() {
    return (
      <Container>
        <Heading
          useCase={TranscriptGmail.useCase}
          subtitle='Certify a Facebook message or conversation as evidence'
        />
        <h2>What is a Notarized Email?</h2>
        <p>Notarized Email is an email that has been witnessed and signed by a notary to verify the email has truly been received and is accurate. Typically, this is done in person at a physical notary.</p>
        <p>Alternatively, you can notarize your email using Veruv.</p>
        <p>Check with the requesting party to make sure an online notary is acceptable.</p>

        <h3>Trusting a notarized email</h3>
        <p>The requesting party needs to be confident that the <strong>notarized email is legitimate</strong> and it has not been modified while in your possesion.</p>
        <ul>
          <li>
            <p><i>Screenshot</i> of an email can be easily manipulated by you or anyone in their posssesion. Unfortunately it cannot be trusted.</p>
          </li>
          <li>
            <p><i>Notarized email</i> created via Veruv is certified by us to be legitimate. You will use Veruv's browser to take a snapshot of the email which will be stored securely with us. Once you share a link with the requesting party, they will receive that snapshot directly from us.</p>
          </li>
        </ul>
        <p>As you are never in possession of the Notarized Email, the requesting party can be confident the email is legitimate.</p>
        <p>Email by nature has issues with authenticating the sender. The receiving of an email can be certified, however it is possible in some circumstances to send an email with a spoofed sender address.</p>

        <h3>Example verifying a Notarized Email</h3>
        <p>
          Take a look at the following example of a Notarized Email. Notice the important parts of this proof:
        </p>
        <ul>
          <li>The website domain name is the official Gmail website</li>
          <li>The owner of the Gmail is visible at the top right corner</li>
          <li>The email sender is visible</li>
          <li>The email itself is visible</li>
          <li>All other sensitive information has been redacted with a black box</li>
        </ul>
        <Image src='/img/usecase/transcript-gmail/email.png' width={1024} />

        <h2>Creating a Notarized Email</h2>
        <AddonOrDirect
          trackSource={TranscriptGmail.useCase.name}
          instructionsSignIntoToSite={(
            <div>
              <h3>Sign into Gmail</h3>
              <p>
                Gmail emails can be accessed on their official website:
              </p>
              <ul>
                <li>https://gmail.com/</li>
              </ul>
              <p>
                Navigate to the email you would like notarized.
              </p>
            </div>
          )}
          instructionsSnapshotSite={(
            <div>
              <h3>Take a snapshot of your email</h3>
              <p>
                Make sure that all of the following information is clearly visible on the page:
              </p>
              <ul>
                <li><strong>Your name</strong> at the top of the page</li>
                <li><strong>Names</strong> of all participants in the conversation.</li>
                <li><strong>Important messages</strong> that you would like to include in the transcript</li>
              </ul>
            </div>
          )}
        />

        <h2>That's it!</h2>
        <p>
          You have successfully created your Certified Transcript. If you need more help, don't hesitate to contact us with questions or concerns at <a href={'mailto:support@veruv.com?subject=Help with '+TranscriptGmail.useCase.title}>support@veruv.com</a>.
        </p>
      </Container>
    );
  }
}

export default TranscriptGmail;
