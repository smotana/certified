import React, { Component } from 'react';
import { Tabs, Tab, Button } from 'react-bootstrap';
import { isClientBrowserSupported } from '../util/addonUtil';
import VeruvAddonButton from '../components/VeruvAddonButton';
import ReactGA from 'react-ga';
import OpenIcon from '@material-ui/icons/OpenInNewRounded';
import Placeholder from '../components/Placeholder';
import ScrollAnchor from '../components/ScrollAnchor';

interface _UseCase {
  name:string;
  title:string;
  thumbnail:{
    path:string;
    // valueless ratio used to calculate size of the image compared to other use cases
    ratio:number;
  }
  generate:()=>React.ReactElement;
}

interface UseCaseProps {
  useCase:UseCase;
}

interface HeadingProps {
  subtitle?:string;
}
class Heading extends Component<UseCaseProps & HeadingProps> {
  render() {
    return (
      <div style={{
        textAlign: 'center',
      }}>
        <ScrollAnchor scrollOnMount={true} />
        <h1>{this.props.useCase.title}</h1>
        {this.props.subtitle && (
          <p style={{fontSize: '20px'}}>
            {this.props.subtitle}
          </p>
        )}
      </div>
    );
  }
}

interface ImageProps {
  src?:string;
  width?:string|number;
  height?:string|number;
}
class Image extends Component<ImageProps> {
  render() {
    return (
      <div style={{
          margin: '10px 20px 20px',
          boxShadow: 'rgba(0, 9, 152, 0.1) 1px 5px 9px 4px',
          overflow: 'hidden',
          ...(this.props.width && {
            maxWidth: this.props.width,
            width: '100%',
          }),
          ...(this.props.height && {
            maxHeight: this.props.height,
            height: '100%',
          }),
      }}>
        {this.props.src ? (
          <img
            style={{
              ...(this.props.width && {
                maxWidth: this.props.width,
                width: '100%',
              }),
              ...(this.props.height && {
                maxHeight: this.props.height,
                height: '100%',
              }),
            }}
            src={this.props.src}
          />
        ) : (
          <Placeholder
            width={this.props.width}
            height={this.props.height}
          />
        )}
      </div>
    );
  }
}

interface AddonOrDirectProps {
  instructionsSignIntoToSite:React.ReactNode;
  instructionsSnapshotSite:React.ReactNode;
  trackSource:string;
}
interface AddonOrDirectState {
  isAddon:boolean;
}
class AddonOrDirect extends Component<AddonOrDirectProps, AddonOrDirectState> {
  constructor(props) {
    super(props);
    this.state = {isAddon: isClientBrowserSupported()}
  }
  render() {
    return (
      <div>
        <p>
          To get started, we recommend using the Veruv browser Addon to quickly create snapshots. If your browser is not supported or you prefer not to install an Addon, use our Veruv Secure Browser directly.
        </p>
        {this.getTabs((
          <div>
            <h4>Install Veruv Addon</h4>
            <p>
              If you haven't installed the Addon already, click below to install it for your browser.
            </p>
            <p>
              
            </p>
            <VeruvAddonButton
              trackOnOpen={clientBrowser => {
                ReactGA.event({
                  category: this.props.trackSource,
                  action: 'openExtension',
                  label: clientBrowser,
                });
              }}
            />
          </div>
        ),(
          <div>
            <h4>Open Veruv Browser</h4>
            <p>
              Open our secure Veruv Browser in a new window with the below link and follow the next instructions in this guide.
            </p>
            <Button
              onClick={e => {
                window.open('/create', '_blank')

                ReactGA.event({
                  category: 'browser',
                  action: 'open',
                  label: this.props.trackSource,
                });
              }}
              style={{
                height: '28px',
                padding: '0px 12px',
                display: 'flex',
                alignItems: 'center',
            }}>
              <OpenIcon
                titleAccess='Open browser'
                fontSize='inherit'/>
              <span style={{marginLeft: '10px'}}>Open</span>
            </Button>
          </div>
        ))}

        {this.props.instructionsSignIntoToSite}

        {this.getTabs((
          <div>
            <h4>Open page in Veruv Browser</h4>
            <p>
            Let's open your current page inside the Veruv Browser using the Addon. I assume you have already installed the Veruv Addon, if not, see above how to get started.
            </p>
            <ol>
              <li>
                Click on the Veruv Addon icon in your browser.
                <Image src='/img/usecase/addon_full.png' width={300} />
              </li>
              <li>
                Ensure 'Keep session' is checked
                <Image src='/img/usecase/addon_keep_session.png' width={150} />
              </li>
              <li>
                Click 'Open'
                <Image src='/img/usecase/addon_open.png' width={150} />
              </li>
            </ol>
            <p>
              Your page should have been automatically opened in a new tab.
            </p>
          </div>
        ),(
          <div>
            Nothing to do here.
          </div>
        ))}

        {this.props.instructionsSnapshotSite}
        <h4>Snapshot it</h4>
        <p>
          Click the 'Snapshot' button at the top right corner.
          <Image src='/img/usecase/snapshot.png' width={150} />
          The first time you click it, it will scroll your page down to the 'Snapshot Editor'.
        </p>

        <h3>Share it</h3>
        <h4>Redact sensitive information</h4>
        <p>
          Now that you have your snapshot(s), let's make sure we don't accidentally release information you do not want others to see.
        </p>
        <ol>
          <li>
            In the Snapshot Editor, select the 'Redaction tool'.
            <Image src='/img/usecase/redaction_tool.png' width={150} />
          </li>
          <li>
            Paint over sensitive information.
            <Image src='/img/usecase/redact.png' width={150} />
          </li>
        </ol>

        <h4>(Optionally) extend lifetime</h4>
        <p>
          Consider extending expiry to 90 days if necessary. Otherwise the link expires within 4 hours by default.
        </p>
        <ol>
          <li>
            Click the 'Extend' button.
            <Image src='/img/usecase/extend_button.png' width={200} />
          </li>
          {/* <li>
            Input your payment info.
            <Image src='/img/usecase/extend_pay.png' width={200} />
          </li> */}
        </ol>

        <h4>Create shareable link</h4>
        <p>
          Now we are ready to share.
        </p>
        <ol>
          <li>
            Click the 'Share' button.
            <Image src='/img/usecase/share_button.png' width={100} />
          </li>
          <li>
            Make your snapshot accessible by others.
            <Image src='/img/usecase/share_visibility.png' width={200} />
          </li>
          <li>
            Open/copy your link and share.
            <Image src='/img/usecase/share_link_open.png' width={200} />
          </li>
        </ol>
      </div>
    );
  }

  getTabs(usingAddon, usingDirect) {
    return (
      <p>
        <Tabs
          animation={false}
          activeKey={this.state.isAddon ? 1 : 2}
          onSelect={k => this.setState({isAddon: k === 1})}
        >
          <Tab eventKey={1} title='Using Addon'>
            {usingAddon}
          </Tab>
          <Tab eventKey={2} title='Using Browser'>
            {usingDirect}
          </Tab>
        </Tabs>
      </p>
    );
  }
}

const Container = (props) => (
  <div style={{
    margin: '0px auto',
    maxWidth: 1024,
    textAlign: 'left',
    padding: '15px',
  }}>
    {props.children}
  </div>
)

export type UseCase = _UseCase;
export { Heading, AddonOrDirect, Container, Image };
