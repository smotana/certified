import React, { Component } from 'react';
import { Heading, AddonOrDirect, UseCase, Container, Image } from './UseCaseUtil';


class GoogleAnalytics extends Component {
  static useCase:UseCase = {
    name: 'google-analytics',
    title: 'Certify Google Analytics Data',
    thumbnail: {
      ratio: 1,
      path: '/img/usecase/google-analytics/header.png'
    },
    generate:()=>(<GoogleAnalytics />)
  };

  render() {
    return (
      <Container>
        <Heading
          useCase={GoogleAnalytics.useCase}
          subtitle='Prove your traffic volume, sources and ads'
        />
        <h2>What is Certified Google Analytics?</h2>
        <p>In many situations including a sale of a website, it is necessary to prove to the buyer that the website's statitics are accurate and legitimate. Typically, this is done using a temporary read-only Google Analytics account, screen sharing or simple screenshots.</p>
        <p>Alternatively, you can certify your Google Analytics statistics and share them publicly with a link including your website listing.</p>

        <h3>Trusting <i>Certified</i> Google Analytics</h3>
        <p>The buyer needs to be confident that the <strong>website statistics are accurate as claimed</strong> and it has not been tampered with while in your possesion.</p>
        <ul>
          <li>
            <p><i>Read-only account</i> is the most trustworthy option, however it takes time to setup and requires sharing of a lot of information.</p>
          </li>
          <li>
            <p><i>Screen sharing</i> or <i>video chat</i> used for showing your Google Analytics page is time consuming and also cannot be fully trusted as the Google Analytics website shown by screen sharing may have been modified with untrue information.</p>
          </li>
          <li>
            <p><i>Screenshot</i> of Google Analytics pages are the simplest but have no authenticity as they can be very easily modified to indicate untrue data.</p>
          </li>
          <li>
            <p><i>Certified Google Analytics</i> created via Veruv is certified by us to be legitimate. You will use Veruv's browser to take a snapshot of the data which will be stored securely with us. Once you share a link with the buyer, they will receive that snapshot directly from us.</p>
          </li>
        </ul>
        <p>As you are never in possession of the Certified Google Analytics, the buyer can be confident the data is legitimate.</p>

        <h3>Example verifying Certified Google Analytics</h3>
        <p>
          Take a look at the following example. Notice the important parts of this proof:
        </p>
        <ul>
          <li>The website domain name is the official Google Analytics website</li>
          <li>To link this data to a particular website, we can see the Google Analytics ID shown in the second snapshot. The first and second snapshot matches up.</li>
          <li>Page view data is clearly visible</li>
          <li>All other sensitive information has been redacted with a black box</li>
        </ul>
        <Image src='/img/usecase/google-analytics/traffic.png' width={1024} />
        <Image src='/img/usecase/google-analytics/traffic2.png' width={1024} />

        <h2>Creating a Certified Google Analytics</h2>
        <AddonOrDirect
          trackSource={GoogleAnalytics.useCase.name}
          instructionsSignIntoToSite={(
            <div>
              <h3>Sign into Google Analytics</h3>
              <p>
                Google Analytics can be accessed via their official website:
              </p>
              <ul>
                <li>https://analytics.google.com/analytics/web/</li>
              </ul>
              <p>
                Then navigate to the appropriate Account > Property > View.
              </p>
            </div>
          )}
          instructionsSnapshotSite={(
            <div>
              <h3>Take a snapshot of your data</h3>
              <p>
                Make sure that all of the following information is clearly visible on the page:
              </p>
              <ul>
                <li><strong>Google Analytics ID</strong> to link your data to your website/app</li>
                <li><strong>Data</strong> you wish to release. Ensure the data is shown on the snapshot fully.</li>
              </ul>
            </div>
          )}
        />

        <h2>That's it!</h2>
        <p>
          You have successfully created your Certified Google Analytics snapshot. If you need more help, don't hesitate to contact us with questions or concerns at <a href={'mailto:support@veruv.com?subject=Help with '+GoogleAnalytics.useCase.title}>support@veruv.com</a>.
        </p>
      </Container>
    );
  }
}

export default GoogleAnalytics;
