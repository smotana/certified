import React, { Component } from 'react';
import ProofOfFunds from './ProofOfFunds';
import setTitle from '../util/titleUtil';
import Masonry from 'react-masonry-component';
import { UseCase } from './UseCaseUtil';
import IncomeVerification from './IncomeVerification';
import TranscriptFacebook from './TranscriptFacebook';
import TranscriptGmail from './TranscriptGmail';
import AccountOwnership from './AccountOwnership';
import { Link } from 'react-router-dom';
import GoogleAnalytics from './GoogleAnalytics';

/*
 * TODO list:
 * Certify Google Analytics
 * Verify Domain Ownership
 * Verify Shopify finances
 * Amazon sales (FBA)
 * Affiliate
 */

interface Props {
  // Router matching
  match;
}

class UseCasePage extends Component<Props> {
  readonly styles = {
    content: {
      textAlign: 'left' as 'left',
      maxWidth: '1680px',
      margin: 'auto',
      fontSize: '1em',
    },
    paragraph: {
    },
    hr: {
      marginTop: "40px",
      marginBottom: "30px",
      width: "10%",
    },
  }
  /** When adding new use case: also add to sitemap.xml and LandingPage.tsx */
  readonly useCases:{[k:string]:UseCase} = {
    [ProofOfFunds.useCase.name]: ProofOfFunds.useCase,
    [IncomeVerification.useCase.name]: IncomeVerification.useCase,
    [TranscriptFacebook.useCase.name]: TranscriptFacebook.useCase,
    [TranscriptGmail.useCase.name]: TranscriptGmail.useCase,
    [GoogleAnalytics.useCase.name]: GoogleAnalytics.useCase,
    [AccountOwnership.useCase.name]: AccountOwnership.useCase,
  }

  componentDidMount() {
    const useCase:UseCase|undefined = this.getCurrentUseCase();
    if(useCase) {
      setTitle(useCase.title);
    } else {
      setTitle('Use cases');
    }
  }

  render() {
    const currentUseCase:UseCase|undefined = this.getCurrentUseCase();
    if(currentUseCase) {
      return currentUseCase.generate();
    } else {
      var useCaseEls = Object.keys(this.useCases).map(key => {
        const useCase:UseCase = this.useCases[key];
        const height:string = (useCase.thumbnail.ratio * 200) + 'px';
        return (
          <Link
            to={"/usecase/" + key}
            style={{
              display: 'inline-block',
              margin: '10px',
              padding: '4px',
              cursor: 'pointer',
            }}
          >
            <div className='hover-image'>
              <div style={{position:'relative'}}>
                <div
                  className='overlay'
                  style={{
                    width: '100%',
                    height: '100%',
                    backgroundImage: 'linear-gradient(rgb(255, 255, 255, 0) 0%, rgb(255, 255, 255, 1) 50%, rgba(0, 9, 152, 0) 100%)',
                    fontSize: '1.5em',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                >
                  {useCase.title}
                </div>
                <img
                  style={{
                    height: height,
                  }}
                  alt={useCase.title}
                  src={useCase.thumbnail.path}
                />
              </div>
            </div>
          </Link>
        );
      });
      return (
        <Masonry
          style={{
            margin: '0px auto',
          }}
          options={{
            transitionDuration: 400,
            stagger: 10,
            gutter: 5,
            fitWidth: true,
          }}
          updateOnEachImageLoad={true}
          enableResizableChildren={true}
        >
          {useCaseEls}
        </Masonry>
      );
    }
  }

  getCurrentUseCase():UseCase|undefined {
    return this.props.match.params.usecase && this.useCases[this.props.match.params.usecase];
  }
}

export default UseCasePage;
