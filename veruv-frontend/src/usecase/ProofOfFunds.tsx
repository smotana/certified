import React, { Component } from 'react';
import { Heading, AddonOrDirect, UseCase, Container, Image } from './UseCaseUtil';


class ProofOfFunds extends Component {
  static useCase:UseCase = {
    name: 'proof-of-funds',
    title: 'Digital Proof of Funds',
    thumbnail: {
      ratio: 1,
      path: '/img/usecase/proof-of-funds/boa_statement.png'
    },
    generate:()=>(<ProofOfFunds />)
  };

  render() {
    return (
      <Container>
        <Heading
          useCase={ProofOfFunds.useCase}
          subtitle='Fast and secure way to create and send financial proof completely online'
        />
        <h2>What is Proof of Funds?</h2>
        <p>Proof of Funds is a verified document proving that a person or a company has financial ability to complete a transaction or has specific financial capability. Typically a Proof of Funds document is issued as an official physical letter from the financial institution.</p>
        <p>Alternatively, you can issue an online certified Proof of Funds and transmit your proof quickly and securely.</p>
        <p>Check with the requesting party to make sure a digital Proof of Funds is acceptable.</p>

        <h3>Trusting a <i>digital</i> Proof of Funds</h3>
        <p>The requesting party needs to be confident that the financial statement you provide is a <strong>legitimate statement from the financial institution</strong> you claim to have obtained it from and it has not been tampered with while in your possesion.</p>
        <ul>
          <li>
            <p><i>Physical</i> Proof of Funds typically establishes legitimacy with an official signature or a stamp from the issuing institution to attempt to prevent you from creating illegitimate copies or modifications.</p>
          </li>
          <li>
            <p><i>Digital</i> Proof of Funds created via Veruv is certified by us to be legitimate. You access your financial institution's website using Veruv's browser and a snapshot of that website will be stored securely with us. Once you share a link with the requesting party, they will receive that snapshot directly from us.</p>
          </li>
        </ul>
        <p>As you are never in possession of the certified snapshot, the requesting party can be confident the digital statement is legitimate.</p>

        <h3>Example verification of a digital Proof of Funds</h3>
        <p>
          Take a look at the following example of a digital Proof of Funds. Notice the important parts of this proof: 
        </p>
        <ul>
          <li>The website domain name is a legitimate website of Bank of America</li>
          <li>Legal name of the individual is visible at the top right corner</li>
          <li>Account balance is clearly visible and noted to be current as of today</li>
          <li>All other sensitive information has been redacted with a black box</li>
        </ul>
        <Image src='/img/usecase/proof-of-funds/boa_example.png' width={1024} />

        <h2>Creating a digital Proof of Funds</h2>
        <AddonOrDirect
          trackSource={ProofOfFunds.useCase.name}
          instructionsSignIntoToSite={(
            <div>
              <h3>Find proof of your funds online</h3>
              <p>
                To provide proof of your funds, visit your financial institution's official website.
              </p>
              <h4>Visit your financial institute's official website</h4>
              <p>
                visit your financial institution's official website and log into your account.
                <Image src='/img/usecase/proof-of-funds/boa_login.png' width={200} />
              </p>
              <h4>Find your financial statement</h4>
              <p>
                Look for information indicating your current balance or financial history as required by the requesting party.
                <Image src='/img/usecase/proof-of-funds/boa_statement.png' width={360} />
              </p>
            </div>
          )}
          instructionsSnapshotSite={(
            <div>
              <h3>Take a snapshot of your proof</h3>
              <p>
                Make sure that all of the following information is clearly visible on the page:
              </p>
              <ul>
                <li><strong>Your identity</strong> such as your full name</li>
                <li><strong>Account balance</strong> or financial statement</li>
                <li><strong>Date</strong> or indication that the balance is current</li>
              </ul>
            </div>
          )}
        />

        <h2>That's it!</h2>
        <p>
          You have successfully created your Proof of Funds document. If you need more help, don't hesitate to contact us with questions or concerns at <a href={'mailto:support@veruv.com?subject=Help with '+ProofOfFunds.useCase.title}>support@veruv.com</a>.
        </p>
      </Container>
    );
  }
}

export default ProofOfFunds;
