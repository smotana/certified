import * as React from 'react';
import {
  Button,
  Grid,
  Row,
  Col,
} from 'react-bootstrap';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { Content, styles as contentStyles, ContentLink, ContentImage } from './contentApi';
import GridExt from '../components/GridExt';

interface Props extends RouteComponentProps {
  style?:React.CSSProperties;
  contentStyle?:React.CSSProperties;
  contents:Content[];
  defColCnt?:number;
  xsColCnt?:number;
  smColCnt?:number;
  mdColCnt?:number;
  lgColCnt?:number;
  spaceBetweenContent?:number;
  maxContentWidth?:number;
  maxWidth?:number;
}

class HorizontalPanels extends React.Component<Props> {

  mapContentToComponent(content:Content) {
    let button = content.link ? (
      <p>
        <ContentLink content={content} />
      </p>
    ) : null;
    let title = content.title ? (
      <h3 style={contentStyles.contentTitle}>
        {content.title}
      </h3>
    ) : null;
    let description = content.description ? (
      <p style={contentStyles.contentDescription}>
        {content.description}
      </p>
    ) : null;
    return (
      <div style={content.style}>
        <ContentImage content={content} />
        {title}
        {description}
        {button}
      </div>
    );
  }

  render() {
    var xsColCnt = this.props.xsColCnt;
    var smColCnt = this.props.smColCnt;
    if(!this.props.defColCnt
      && !this.props.xsColCnt
      && !this.props.smColCnt
      && !this.props.mdColCnt
      && !this.props.lgColCnt) {
      xsColCnt = 1;
      smColCnt = this.props.contents.length;
    }
    var containerStyle;
    if(this.props.maxWidth) {
      containerStyle = {
        maxWidth: `${this.props.maxWidth}px`,
        marginLeft: 'auto',
        marginRight: 'auto',
      };
    }
    var index = 0;
    return (
      <div style={{...contentStyles.container, ...this.props.style, ...containerStyle}}>
        <GridExt
          defColCnt={this.props.defColCnt}
          xsColCnt={xsColCnt}
          smColCnt={smColCnt}
          mdColCnt={this.props.mdColCnt}
          lgColCnt={this.props.lgColCnt}
          spaceBetweenContent={this.props.spaceBetweenContent}
          maxContentWidth={this.props.maxContentWidth}
          contentStyle={this.props.contentStyle}
        >
          {this.props.contents.map(c => this.mapContentToComponent(c))}
        </GridExt>
      </div>
    );
  }
}

export default withRouter(HorizontalPanels);
