import * as React from 'react';
import Placeholder from '../components/Placeholder';
import { withRouter } from 'react-router';
import { Button } from 'react-bootstrap';
import ScrollAnchor from '../components/ScrollAnchor';
import { EventArgs } from 'react-ga';

interface _Content {
  style?:React.CSSProperties;
  title?:string|React.ReactNode;
  description?:string|React.ReactNode;
  image?:{
    width?:number;
    height?:number;
    // Only one of path or icon should be set.
    // If neither are set, a placeholder image is displayed
    path?:string;
    icon?:React.ReactNode
  };
  link?:{
    text:string;
    path:string;
    anchor?:string;
    isExternal?:boolean;
    trackerEvent?:EventArgs;
  };
}

const styles = {
  container: {
    marginTop: '48px',
  },
  contentTitle: {
    color: "black",
  },
  contentDescription: {
    color: "rgba(101, 101, 101)",
  },
  image: {
    display: "inline-block",
  },
}

const ContentLink = withRouter((props:any) => {
  if(!props.content.link) {
    return null;
  }
  return (
    <Button onClick={e => {
        props.content.link.isExternal
          ? window.open(props.content.link!.path, '_blank')
          : props.history.push({
            pathname: props.content.link!.path,
            state: props.content.link!.anchor
              ? ScrollAnchor.scrollToState(props.content.link!.anchor)
              : undefined,
          })}
      }>
      {props.content.link.text}
    </Button>
  );
});

const ContentImage = (props) => {
  if(!props.content.image) {
    return null;
  }
  if(props.content.image.path) {
    return (
      <img
        src={props.content.image.path}
        style={{
          ...styles.image,
          ...(props.content.image.width && { width: props.content.image.width }),
          ...(props.content.image.height && { height: props.content.image.height }),
        }}
      />
    );
  }
  if(props.content.image.icon) {
    return props.content.image.icon;
  }
  return (
    <Placeholder
      width={props.content.image.width}
      height={props.content.image.height}
    />
  );
};

export type Content = _Content;
export {
  styles,
  ContentImage,
  ContentLink
};