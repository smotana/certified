import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Grid,
  Row,
  Col,
} from 'react-bootstrap';

class SnakeList extends Component {
  render() {
    if(!this.props.components) {
      return null;
    }
    if(!Array.isArray(this.props.components)) {
      throw new Error("In SnakeList, must pass more than one element");
    }

    let count = 0;
    let rows = []
    this.props.components.forEach(component => {
      let doSwitch = count++ % 2 > 0;
      let content = component.content;
      let image = component.image;
      rows.push(
        <Row>
          <Col
            sm={12}
            style={styles.centerColumn}
            smHidden={true}
            mdHidden={true}
            lgHidden={true}
            >
            {this.props.hideImageOnXs ? null : image}
            {content}
          </Col>
          <Col
            xs={6}
            md={6}
            style={styles.leftColumn}
            xsHidden={true}
            >
            {doSwitch ? image : content}
          </Col>
          <Col
            xs={6}
            md={6}
            style={styles.rightColumn}
            xsHidden={true}
            >
            {doSwitch ? content : image}
          </Col>
        </Row>
      );

      if(this.props.spaceBetweenContent && this.props.components.length !== count) {
        rows.push(
          <Row>
            <Col sm={12}>
              <div style={{height: this.props.spaceBetweenContent}}>&nbsp;</div>
            </Col>
          </Row>
        );
      }
    });

    return (
      <Grid fluid={true} style={{maxWidth: this.props.maxWidth, verticalAlign: 'middle'}}>
        {rows}
      </Grid>
    );
  }
}

const styles = {
  grid: {
    verticalAlign: 'middle',
  },
  centerColumn: {
    textAlign: 'center',
  },
  leftColumn: {
    textAlign: 'right',
  },
  rightColumn: {
    textAlign: 'left',
  },
};

SnakeList.propTypes = {
  components: PropTypes.array,
  hideImageOnXs: PropTypes.bool,
  spaceBetweenContent: PropTypes.string,
  maxWidth: PropTypes.number,
};

export default SnakeList;
