import * as React from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { styles as contentStyles } from './contentApi';

interface Props extends RouteComponentProps {
  title:string
}

class SnakeListCreator extends React.Component<Props> {

  render() {
    return (
      <h1 style={contentStyles.container}>{this.props.title}</h1>
    );
  }
}

export default withRouter(SnakeListCreator);
