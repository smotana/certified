import * as React from 'react';
import SnakeList from './SnakeListJs';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { Content, styles as contentStyles, ContentLink, ContentImage } from './contentApi';

interface Props extends RouteComponentProps {
  contents:Content[];
  hideImageOnXs?:boolean;
  spaceBetweenContent?:number
  maxWidth?:number
}

class SnakeListCreator extends React.Component<Props> {

  render() {
    return (
      <div style={contentStyles.container}>
        <SnakeList
          hideImageOnXs={this.props.hideImageOnXs}
          spaceBetweenContent={this.props.spaceBetweenContent && this.props.spaceBetweenContent + 'px'}
          maxWidth={this.props.maxWidth}
          components={this.props.contents.map(component => {return {
            content: (
              <div style={component.style}>
                <h3 style={contentStyles.contentTitle}>{component.title}</h3>
                <p style={contentStyles.contentDescription}>{component.description}</p>
                {component.link ? (
                  <p>
                    <ContentLink content={component} />
                  </p>
                ) : null}
              </div>
            ),
            image: (<ContentImage content={component} />)
          }})}
        />
      </div>
    );
  }
}

export default withRouter(SnakeListCreator);
