import React, { Component } from 'react';
import ScrollAnchor from './components/ScrollAnchor';
import TermsIcon from '@material-ui/icons/NotesRounded';
import setTitle from './util/titleUtil';


class TermsOfServicePage extends Component {
  readonly styles = {
    content: {
      textAlign: 'left' as 'left',
      fontSize: '1em',
    },
    paragraph: {
    },
    hr: {
      marginTop: "40px",
      marginBottom: "30px",
      width: "10%",
    },
  }

  componentDidMount() {
    setTitle('Terms of Service');
  }

  render() {
    return (
      <div style={this.styles.content}>
        <ScrollAnchor scrollOnStateName='security' />
        <h1>
          <TermsIcon fontSize='default' />
          &nbsp;
          Terms of Service
        </h1>
        <h3>Content protection</h3>
        <p>
          Veruv and Smotana LLC retains all rights to copyright, trademarks, trade secrets, patents and any other proprietary rights to the logo, content, design, API with the exception of the user created snapshot described below.
        </p>
        <p>
          Users retain all rights to the snapshot created using the Secure Browser, but allows Veruv to host the snapshot and make it available as directed by the website controls for visibility or deletion.
        </p>
        <h3>Right to terminate service</h3>
        <p>
          We reserve the right to terminate your service at any time for breaking rules, disrupting our service, or breaking the law. Upon termination, your payment will be prorated to the date your service was terminated.
        </p>
        <h3>Liability</h3>
        <p>
          Users assume the risks involved using our service and as such Veruv and Smotana cannot be held liable for any losses you sustain in these events.
        </p>
        <h3>Applicable law</h3>
        <p>
          By using our service Veruv owned and operated by Smotana LLC based in Mongolia is governed by the Mongolian law and any dispute of any sort must be arbitrated by those applicable laws.
        </p>
      </div>
    );
  }
}

export default TermsOfServicePage;
