import React, { Component } from 'react';
import {
  Grid,
  Row,
  Col,
  ListGroup,
  ListGroupItem,
} from 'react-bootstrap';
import Hr from './Hr';
import ScrollAnchor from './components/ScrollAnchor';
import PrivacyIcon from '@material-ui/icons/VisibilityOffRounded';
import setTitle from './util/titleUtil';


class PrivacyPolicyPage extends Component {
  readonly styles = {
    content: {
      textAlign: 'left' as 'left',
      fontSize: '1em',
    },
    hr: {
      marginTop: "40px",
      marginBottom: "30px",
      width: "10%",
    },
  }

  componentDidMount() {
    setTitle('Privacy Policy');
  }

  render() {
    var privacyEmailContact = (<a href="mailto:privacy@veruv.com">privacy@veruv.com</a>);
    return (
      <div style={this.styles.content}>
        <ScrollAnchor scrollOnStateName='privacy' />
        <h1>
          <PrivacyIcon fontSize='default' />
          &nbsp;
          Privacy Policy
        </h1>

        <ScrollAnchor scrollOnStateName='browser' />
        <h3>Data collection using the Secure Browser</h3>
        <h4>Definitions</h4>
          <ListGroup>
            <ListGroupItem><strong>'Secure Browser'</strong> or <strong>'Browser'</strong> refers to the embedded remote browser available on our site used for accessing the internet to take verified snapshots.</ListGroupItem>
            <ListGroupItem><strong>'Browsing session'</strong> refers to a user using the secure browser from the time the browser is opened until the browser is closed or abandoned.</ListGroupItem>
            <ListGroupItem><strong>'User data'</strong> refers to the data associated with interacting in a browser session and includes browsing history, pages visited, mouse movements, keyboard input, and website data used within the browser such as cookies and local storage.</ListGroupItem>
          </ListGroup>
        <h4>Collection of data</h4>
        <p>
          We simply do not collect any user data from a browsing session when using the secure browser with the exception of storing the snapshots you request and security&spam fighting efforts described below.
        </p>
        <h4>Snapshots</h4>
        <p>
          Only when user explicitly requests a snapshot, we store the following information:
          <ListGroup>
            <ListGroupItem><strong>Creation time:</strong> the time the snapshot was created.</ListGroupItem>
            <ListGroupItem><strong>Url:</strong> a sanitized version of the URL with potentially sensitive information excluded. Excluded parts of the URL are username, password, query and fragment.</ListGroupItem>
            <ListGroupItem><strong>Website screenshot:</strong> screenshot of the website at the time the snapshot was taken.</ListGroupItem>
            <ListGroupItem><strong>Note:</strong> optional note the user wrote as part of the snapshot.</ListGroupItem>
            <ListGroupItem><strong>Owner:</strong> the user that requested the snapshot.</ListGroupItem>
          </ListGroup>
          This information is kept private unless user explicitly requests to share this information using the controls provided. Sharing can either be made public or protected by a password.
        </p>
        <h4>Security and spam</h4>
          For security and spam fighting efforts only, we collect the following information:
          <ListGroup>
            <ListGroupItem><strong>Access time</strong></ListGroupItem>
            <ListGroupItem><strong>IP address</strong></ListGroupItem>
            <ListGroupItem><strong>Hardware and software fingerprint</strong> collected by Google's ReCaptcha</ListGroupItem>
          </ListGroup>

        <ScrollAnchor scrollOnAnchorTag='extension' />
        <h3>Data collection using the browser extension</h3>
        <h4>Definitions</h4>
          <ListGroup>
            <ListGroupItem><strong>'Browser Extension'</strong> or <strong>'Extension'</strong> refers to the official browser extension created by Veruv and distributed through various official browser stores</ListGroupItem>
            <ListGroupItem><strong>'Browser Session'</strong> refers to private information stored inside your browser.</ListGroupItem>
          </ListGroup>
        <h4>Collection of data</h4>
        <p>
          The Browser Extension collects the Browser Session on a user initiated action in order to transfer it to the Secure Browser.
          The Secure Browser is only controlled by user actions and the Browser Session is used to access the requested websites.
          The Browser Session is removed immediately after the Secure Browser is terminated.
          The Browser Session is never stored nor used for anything else other than as described here.
        </p>
        <h4>Browser Session</h4>
        <p>
          The Browser Extension extracts the below information as part of the Browser Session.
          When the Browser Extension is invoked, it collects the Browser Session limited to the current page.
          <ListGroup>
            <ListGroupItem><strong>Cookies</strong></ListGroupItem>
            <ListGroupItem><strong>Local Storage</strong></ListGroupItem>
            <ListGroupItem><strong>Session Storage</strong></ListGroupItem>
            <ListGroupItem><strong>User Agent</strong></ListGroupItem>
          </ListGroup>
        </p>

        <ScrollAnchor scrollOnStateName='website' />
        <h3>Data collection using Veruv.com website</h3>
        <p>
          Outside of the secure browser, we use Google Analytics to better improve our service; however, the data is not used for any advertising features.
          We also store information for anti-spam and security purposes including IP address.
        </p>
        <h4>Analytics</h4>
        <p>
          We use Google Analytics to better improve our service. Keep in mind we do not advertise at Veruv and we do not store any information for advertising features.
          Google analytics includes but is not limited to the following information:
          <ListGroup>
            <ListGroupItem><strong>Access time</strong></ListGroupItem>
            <ListGroupItem><strong>Page accessed</strong> NOTE: this does NOT include pages inside the Secure Browser</ListGroupItem>
            <ListGroupItem><strong>Anonymized IP and GeoIP location</strong> to prioritize testing our own service from various locations</ListGroupItem>
            <ListGroupItem><strong>Language preferences</strong> to prioritize future translations of the website</ListGroupItem>
            <ListGroupItem><strong>Device and browser fingerprint</strong> mainly for ensuring our site compatibility works for most users</ListGroupItem>
            <ListGroupItem><strong>A/B testing</strong> we collect various statistics to ensure the user experience is smooth and intuitive to understand</ListGroupItem>
          </ListGroup>
        </p>
        <h4>User account</h4>
        <p>
          When you register an account with us, we ask and store the following information:
          <ListGroup>
            <ListGroupItem><strong>Registration time</strong></ListGroupItem>
            <ListGroupItem><strong>Email</strong> for account recovery and to be notified of important events such as when a snapshot was delivered to you or if your payment failed to process</ListGroupItem>
            <ListGroupItem><strong>Salted and hashed password</strong> in order to verify your identity</ListGroupItem>
          </ListGroup>
        </p>
        <h4>Payments</h4>
        <p>
          If you decide to make a payment to our service, we are very grateful. Our payment processor Stripe handles storing your sensitive information securely which may include but is not limited to:
          <ListGroup>
            <ListGroupItem><strong>Payment details</strong></ListGroupItem>
            <ListGroupItem><strong>Name</strong></ListGroupItem>
            <ListGroupItem><strong>Email</strong></ListGroupItem>
            <ListGroupItem><strong>Billing address</strong></ListGroupItem>
          </ListGroup>
        </p>
        {/* <h4>Snapshot requests</h4>
        <p>
          Requesting a snapshot from someone else involves us to store a record of what you are requesting which includes:
          <ListGroup>
            <ListGroupItem><strong>Creation time</strong></ListGroupItem>
            <ListGroupItem><strong>Optional note</strong> you wish to provide to the person you are requesting a snapshot from</ListGroupItem>
            <ListGroupItem><strong>Optional URL</strong> you wish the user to use when creating a snapshot</ListGroupItem>
          </ListGroup>
        </p> */}
        <h4>Security, fraud and spam</h4>
        <p>
          For security and spam fighting efforts only, we collect information including but not limited to:
          <ListGroup>
            <ListGroupItem><strong>Access logs</strong> (excluding Secure Browser access logs)</ListGroupItem>
            <ListGroupItem><strong>IP address</strong></ListGroupItem>
            <ListGroupItem><strong>Hardware and software fingerprint</strong></ListGroupItem>
          </ListGroup>
          In addition, Google's ReCaptcha and Stripe collects additional information for abuse and fraud prevention.
        </p>
      </div>
    );
  }
}

export default PrivacyPolicyPage;
