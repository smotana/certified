import * as React from 'react';
import { Button, OverlayTrigger, Popover, Modal, Collapse } from 'react-bootstrap';
import { PurchaseInfo, PurchaseResult } from './paymentApi';
import PurchaseForm from './PurchaseForm';
import ReactGA from 'react-ga';

interface Props {
  disabled?:boolean;
  buttonStyle?:React.CSSProperties;
  buttonHidden?:boolean;
  buttonText:string;
  buttonCollapseOnHide?:boolean;
  title:string;
  description:string;
  purchaseInfo:PurchaseInfo;
  callback?:(purchaseState:PurchaseResult)=>void;
  trackCategory:string;
  trackSource:string;
}

interface State {
  show:boolean
}

class PurchaseButton extends React.Component<Props, State> {

  constructor(props) {
    super(props);

    this.state = {
      show: false,
    };
  }

  render() {

    var purchaseForm = (
      <PurchaseForm
        trackSource={this.props.trackSource}
        trackCategory={this.props.trackCategory}
        show={this.state.show}
        onHide={() => this.setState({show: false})}
        {...this.props}
      />
    );

    var button = (
      <Button
        disabled={this.props.disabled}
        style={{
          padding: '0px 12px',
          height: '28px',
          display: 'flex',
          alignItems: 'center',
          ...this.props.buttonStyle,
        }}
        onClick={() => {
          this.setState({show: true})

          ReactGA.event({
            category: this.props.trackCategory,
            action: 'click',
            label: this.props.trackSource,
          });
        }}
      >
        {this.props.buttonText}
      </Button>
    );

    var buttonWrapped = this.props.buttonCollapseOnHide
      ? (
        <Collapse in={!this.props.buttonHidden}>
          <div>
            {button}
          </div>
        </Collapse>
      )
      : !this.props.buttonHidden && button;
    return [
      buttonWrapped,
      purchaseForm,
    ];
  }
}

export default PurchaseButton;
