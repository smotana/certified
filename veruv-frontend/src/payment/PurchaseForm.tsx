import * as React from 'react';
import {
  Button,
  Collapse,
  Alert,
  Modal
} from 'react-bootstrap';
import { PurchaseInfo, PurchaseResult } from './paymentApi';
import { Elements, injectStripe, ReactStripeElements, PaymentRequestButtonElement, CardElement } from 'react-stripe-elements';
import { actionRef } from '../server/action';
import Hr from '../Hr';
import { ErrorResponse } from '../server/serverApi';
import ReactGA from 'react-ga';

enum FormState {
  READY,
  SUBMITTING,
  PURCHASED,
}

interface Props {
  show: boolean;
  onHide: Function;
  title:string;
  description:string;
  purchaseInfo:PurchaseInfo;
  callback?:(purchaseState:PurchaseResult)=>void;
  trackCategory:string;
}

interface State {
  error?:string;
  formState:FormState;
  showPaymentRequestButton:boolean;
  paymentRequest?;
  txid?: string;
}

class PurchaseFormInternal extends React.Component<Props & ReactStripeElements.InjectedStripeProps, State> {

  constructor(props) {
    super(props);

    this.state = {
      showPaymentRequestButton: false,
      formState: FormState.READY,
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    if(nextProps.stripe
      && !nextState.paymentRequest) {
      const paymentRequest = nextProps.stripe.paymentRequest({
        currency: this.props.purchaseInfo.currency,
        total: {
          label: this.props.title,
          amount: this.props.purchaseInfo.amountInCents,
          pending: false,
        },
        country: 'CA',
        requestPayerName: true,
        requestPayerEmail: false,
        requestPayerPhone: false,
        requestShipping: false,
      });

      paymentRequest.canMakePayment().then((result) => {
        this.setState({
          showPaymentRequestButton: !!result
        });
      });

      paymentRequest.on('token', ({complete, token, ...data}) => {
        this.pay('paymentButton', token, complete, data);
      });

      this.setState({
        paymentRequest: paymentRequest,
      });
    }

    return true;
  }

  render() {
    const isFree = this.props.purchaseInfo.amountInCents === 0;
    return (
      <Modal
        bsSize='sm'
        show={this.props.show}
        onHide={(e) => this.hide()}
      >
        <Modal.Header closeButton>
          <Modal.Title>{this.props.title}</Modal.Title>
          {this.props.description}
        </Modal.Header>
        {!isFree && (
          <Collapse in={this.state.formState !== FormState.PURCHASED}>
            <Modal.Body>
              <div>
                <Collapse in={this.state.showPaymentRequestButton}>
                  <div>
                    {this.state.showPaymentRequestButton && (
                      <PaymentRequestButtonElement
                        onReady={(el) => el.focus()}
                        paymentRequest={this.state.paymentRequest}
                        className="PaymentRequestButton"
                        style={{
                          paymentRequestButton: {
                            theme: 'light-outline',
                            height: '40px',
                          },
                        }} 
                      />
                    )}
                    <Hr margins='20px' length='50%'>Or</Hr>
                  </div>
                </Collapse>
                <form onSubmit={e => {
                    e.preventDefault();
                    this.submitForm();
                }}>
                  <CardElement
                    onReady={(el) => el.focus()}
                  />
                </form>
              </div>
            </Modal.Body>
          </Collapse>
        )}
        <Modal.Footer>
          <Collapse in={this.state.formState === FormState.PURCHASED}>
            <div>
              <Alert bsStyle="success" style={{
                textAlign:'center',
                margin:'10px 0px',
                fontSize: '1.2em',
              }}>
                {isFree
                  ? 'Confirmed!'
                  : 'Thank you for your order!'}
              </Alert>
              {this.state.txid && (
                <div style={{fontSize:'0.8em'}}>
                  If you are not satisfied, please contact our&nbsp;
                  <a href={`mailto:support@veruv.com?subject=[TXID: ${this.state.txid || 'N/A'}]`}>support</a>.
                  Your reference ID is {this.state.txid}
                </div>
              )}
            </div>
          </Collapse>
          <Collapse in={this.state.formState !== FormState.PURCHASED}>
            <div>
              <Collapse in={this.state.error !== undefined}>
                <div>
                  <Alert bsStyle="danger" style={{textAlign:'center'}}>
                    {this.state.error}
                  </Alert>
                </div>
              </Collapse>
              <Button
                disabled={this.state.formState !== FormState.READY}
                onClick={(e) =>  isFree ? this.submitFormFree() : this.submitForm()}
                bsStyle={isFree ? 'primary' : 'success'}
              >
                {isFree
                  ? 'Confirm'
                  : (
                    'Pay '
                    + (this.props.purchaseInfo.amountInCents / 100).toLocaleString(
                        "en-US", {
                        style:"currency",
                        currency:"USD"})
                  )}
              </Button>
            </div>
          </Collapse>
          <Collapse in={this.state.formState === FormState.PURCHASED}>
            <div>
              <Button onClick={(e) => this.hide()}>Close</Button>
            </div>
          </Collapse>
        </Modal.Footer>
      </Modal>
    );
  }

  hide() {
    if(this.state.formState === FormState.SUBMITTING) {
      return;
    }
    this.props.onHide();
    this.setState({
      error: undefined,
      formState: FormState.READY,
    });
  }

  submitFormFree() {
    this.pay('free');
  }

  submitForm() {
    if(!this.props.stripe) {
      this.setState({
        error: 'Payment processing has not been initialized yet, please re-submit again',
      });
      return;
    }

    this.setState({
      error: undefined,
      formState: FormState.SUBMITTING,
    });

    this.props.stripe.createToken().then((result) => {
      if(result.error || !result.token) {
        this.setState({
          error: result.error && result.error.message || 'Failed to process',
          formState: FormState.READY,
        });
        return;
      }
      this.pay('cardNumber', result.token);
    });
  }

  pay(paymentType:string, token?:stripe.Token, completeCallback:((status: string)=>void)|undefined = undefined, data:any|undefined = undefined) {
    actionRef.get().pay(this.props.purchaseInfo, token)
    .then((result:PurchaseResult) => {
      this.setState({
        formState: FormState.PURCHASED,
        txid: result.referenceId,
      });
      completeCallback && completeCallback('success');

      ReactGA.event({
        category: this.props.trackCategory,
        action: 'paySuccess',
      });
    })
    .catch((err) => {
      var errorStr;
      if(err instanceof ErrorResponse) {
        errorStr = err.userFacingMessage || err.code;
      } else {
        errorStr = 'Failed processing your card';
      }
      this.setState({
        error: errorStr,
        formState: FormState.READY,
      });
      completeCallback && completeCallback('fail');

      ReactGA.event({
        category: this.props.trackCategory,
        action: 'payFailed',
        label: errorStr,
      });
    });

    ReactGA.event({
      category: this.props.trackCategory,
      action: 'pay',
      label: paymentType,
    });
  }
}

const PurchaseFormInternalInjected = injectStripe(PurchaseFormInternal);

class PurchaseForm extends React.Component<Props> {
  render() {
    return (
      <Elements>
        <PurchaseFormInternalInjected {...this.props} />
      </Elements>
    );
  }
}

export default PurchaseForm;
