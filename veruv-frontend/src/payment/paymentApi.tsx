import * as React from 'react';

enum Product {
  EXTEND_EXPIRY = 'EXTEND_EXPIRY',
}

interface _PurchaseInfo {
  product:Product;
  currency:string;
  amountInCents:number;
  metadata?:{};
}

interface _PurchaseResult {
  referenceId: string,
  [Product.EXTEND_EXPIRY]?: {
    snapshotId: string,
    expiry: number,
    canExtendExpiry: boolean,
  }
}

export type PurchaseResult = _PurchaseResult;
export type PurchaseInfo = _PurchaseInfo;
export {
  Product,
};
