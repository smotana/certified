import * as React from 'react';
import PurchaseButton from './PurchaseButton';
import { PurchaseResult, Product } from './paymentApi';
import withAuthentication, { AuthenticatorProps } from '../server/auth/authenticator';
import { connect } from 'react-redux';
import { Verification } from '../server/serverApi';

interface Props {
  disabled?:boolean;
  buttonStyle?:React.CSSProperties;
  buttonCollapseOnHide?:boolean;
  snapshotId:string;
  callback?:(purchaseState:PurchaseResult)=>void;
  trackSource:string;
  // Store props
  isOwner:boolean;
  snapshot?:Verification;
}

class ExtendExpiryButton extends React.Component<Props & AuthenticatorProps> {

  render() {
    if(!this.props.isOwner) {
      return null;
    }

    var hidden = false;
    if(!this.props.snapshot
      || !this.props.snapshot.canExtendExpiry) {
      hidden = true;
    }

    return (
      <PurchaseButton
        trackSource={this.props.trackSource}
        trackCategory='extendExpiry'
        disabled={this.props.disabled}
        buttonStyle={this.props.buttonStyle}
        buttonHidden={hidden}
        buttonText='Extend'
        buttonCollapseOnHide={this.props.buttonCollapseOnHide}
        title='Extend to 90 days'
        description='Your snapshot expiration will be extended to 90 days from now.'
        purchaseInfo={{
          product: Product.EXTEND_EXPIRY,
          currency: 'usd',
          // If changed, change in MenuImpl.java too
          amountInCents: 0,
          metadata: {
            snapshotId: this.props.snapshotId,
          },
        }}
        callback={this.props.callback}
       />
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    isOwner: state.mySnapshots.ids.includes(ownProps.snapshotId)
      || state.mySnapshots.createdIds.includes(ownProps.snapshotId),
    snapshot: state.verificationById[ownProps.snapshotId],
  };
}

export default withAuthentication(connect(mapStateToProps)(ExtendExpiryButton));
