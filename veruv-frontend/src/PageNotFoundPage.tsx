import React, { Component } from 'react';
import {
  Alert,
} from 'react-bootstrap';
import setTitle from './util/titleUtil';


class PageNotFoundPage extends Component {
  readonly styles = {
    alert: {
      fontSize: "2em",
    },
  }

  componentDidMount() {
    setTitle('Page not found');
  }

  render() {
    return (
      <div>
        <Alert bsStyle='danger' style={this.styles.alert}>
          Oops, page not found
        </Alert>
      </div>
    );
  }
}

export default PageNotFoundPage;
