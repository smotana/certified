import React, { Component, Context } from 'react';
import ReactGA from 'react-ga';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from 'react-router-dom'
import CreatePage from './CreatePage';
import ContactPage from './ContactPage';
import VerifyPage from './VerifyPage';
import LandingPage from './LandingPage';
import PricingPage from './PricingPage';
import AccountPage from './AccountPage';
import MembersWelcomePage from './MembersWelcomePage';
import PageNotFoundPage from './PageNotFoundPage';
import MinePage from './MinePage';
import { Store, createStore, compose, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import promiseMiddleware from 'redux-promise-middleware';
import { reducers, Action, actionRef } from './server/action';
import thunk from 'redux-thunk';
import AppHeader from './AppHeader';
import AnimAnchor from './components/AnimAnchor';
import AnimBubble from './components/AnimBubble';
import smoothscroll from 'smoothscroll-polyfill';
import AboutPage from './AboutPage';
import UseCasePage from './usecase/UseCasePage';
import { StripeProvider } from 'react-stripe-elements';
import { isProd } from './util/detectEnv';

interface State {
  stripe?:any;
}

class App extends Component<{}, State> {
  readonly styles = {
    app: {
      height: '100vh',
      minHeight: '768px',
      textAlign: 'center' as 'center',
      margin: "auto",
      display: 'flex',
      flexDirection: 'column' as 'column',
      alignItems: 'stretch',
    },
  };
  readonly store:Store;
  readonly action:Action;

  constructor(props){
    super(props);

    const STRIPE_KEY = isProd()
      ? 'pk_live_6HJ7aPzGuVyPwTX5ngwAw0Gh'
      : 'pk_test_M1ANiFgYLBV2UyeVB10w1Ons';
    if (window['Stripe']) {
      this.state = ({
        stripe: window['Stripe'](STRIPE_KEY),
      });
    } else {
      this.state = {
        stripe: null,
      };
      document.querySelector('#stripe-js')!.addEventListener('load', () => {
        this.setState({
          stripe: window['Stripe'](STRIPE_KEY),
        });
      });
    }

    this.store = createStore(
        reducers,
        Action.initialState(),(
          // Use Redux dev tools in development
          (isProd()
            ? compose
            : (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose)
        )(applyMiddleware(thunk, promiseMiddleware()))
    );
    this.action = new Action(this.store);
    actionRef.set(this.action);
    // Since our initial token in store may be taken from
    // local storage, refresh the token in the background
    this.action.verifyToken();

    smoothscroll.polyfill();
  }

  render() {
    if(isProd()) {
      ReactGA.initialize('UA-127162051-2', {
        gaOptions: {}
      });
      ReactGA.set({
        anonymizeIp: true,
        forceSSL: true
      });
      ReactGA.pageview(window.location.pathname + window.location.search);
    }

    return (
      <Router>
        <Provider store={this.store}>
        <StripeProvider stripe={this.state.stripe}>
          <div style={this.styles.app}>
            {isProd() && (
              <Route path="/" render={({location}) => {
                ReactGA.set({page: location.pathname + location.search});
                ReactGA.pageview(location.pathname + location.search);
                return null;
              }} />
            )}
            <AnimAnchor fullPage>
              <AnimBubble
                size='100vw' x='0vw' y='0px'
                delay='600ms' duration='1000ms'
              />
              <AnimBubble
                size='70vw' x='65vw' y='10vw'
                delay='750ms' duration='850ms'
              />
              <AnimBubble
                size='40vw' x='45vw' y='30vw'
                delay='900ms' duration='700ms'
              />
            </AnimAnchor>
            <div style={{
              width: '100%',
            }}>
              <AppHeader />
            </div>
            <div style={{
              flex: '1 1 0',
              backgroundImage: 'linear-gradient(to top, rgba(0, 9, 152, 0.1) 0px, rgba(0, 9, 152, 0) 300px)',
            }}>
              <Switch>
                <Route path="/v:verificationId?" exact render={props => (
                  <VerifyPage {...props} action={this.action} />
                )} />
                <Route path="/create" exact render={props => (
                  <CreatePage {...props} action={this.action} />
                )} />
                <Route path="/snapshots" exact render={props => (
                  <MinePage {...props} action={this.action} />
                )} />
                <Route path="/usecase/:usecase?" render={props => (
                  <UseCasePage {...props} />
                )} />
                <Route path="/about/:page?" render={props => (
                  <AboutPage {...props} />
                )} />
                <Route path="/pricing" exact render={props => (
                  <PricingPage {...props} />
                )} />
                <Route path="/account" exact render={props => (
                  <AccountPage {...props} />
                )} />
                <Route path="/members" exact render={props => (
                  <MembersWelcomePage {...props} />
                )} />
                <Route path="/" exact render={props => (
                  <LandingPage {...props} />
                )} />

                // Redirects
                <Route path="/(home|index.html|index.htm)" exact render={props => (
                  <Redirect to='/' />
                )} />
                <Route path="/(contact|legal)" exact render={props => (
                  <Redirect to='/about/contact' />
                )} />
                <Route path="/(privacy|privacy-policy)" exact render={props => (
                  <Redirect to='/about/privacy-policy' />
                )} />
                <Route path="/(terms|terms-of-service)" exact render={props => (
                  <Redirect to='/about/terms-of-service' />
                )} />
                {/* <Route path="/(bug|bug-bounty)" exact render={props => (
                  <Redirect to='/about/but-bounty' />
                )} /> */}
                <Route path="/security" exact render={props => (
                  <Redirect to='/about/security' />
                )} />

                // Not found
                <Route render={props => (
                  <PageNotFoundPage {...props} />
                )} />
              </Switch>
            </div>

            <div style={{
              paddingTop: '30px',
              paddingBottom: '30px',
              backgroundColor: 'rgba(0, 9, 152, 0.1)',
            }}>
              <small>© Veruv. Created by <a target="_blank" href='https://smotana.com'>Smotana</a>. All rights reserved.</small>
            </div>
          </div>
        </StripeProvider></Provider>
      </Router>
    );
  }
}

export default App;
