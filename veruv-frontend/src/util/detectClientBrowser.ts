
var browserCache:ClientBrowser|undefined = undefined;

enum ClientBrowser {
  CHROME = 'Chrome',
  FIREFOX = 'Firefox',
  OPERA = 'Opera',
  SAFARI = 'Safari',
  IE = 'Internet Explorer',
  EDGE = 'Edge',
  BLINK = 'Blink',
  UNKNOWN = 'Unknown',
}

function detectClientBrowser():ClientBrowser {
  if(browserCache === undefined) {
    // START Browser detection https://stackoverflow.com/a/9851769
    // Opera 8.0+
    var isOpera = (!!window['opr'] && !!['opr']['addons']) || !!window['opera'] || navigator.userAgent.indexOf(' OPR/') >= 0;

    // Firefox 1.0+
    var isFirefox = typeof window['InstallTrigger'] !== 'undefined';

    // Safari 3.0+ "[object HTMLElementConstructor]" 
    var isSafari = /constructor/i.test(window['HTMLElement']) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof window['safari'] !== 'undefined' && window['safari'].pushNotification));

    // Internet Explorer 6-11
    var isIE = /*@cc_on!@*/false || !!document["documentMode"];

    // Edge 20+
    var isEdge = !isIE && !!window["StyleMedia"];

    // Chrome 1 - 71
    var isChrome = !!window['chrome'] && (!!window['chrome'].webstore || !!window['chrome'].runtime);

    // Blink engine detection
    var isBlink = (isChrome || isOpera) && !!window['CSS'];
    // END Browser detection

    if(isOpera) {
      browserCache = ClientBrowser.OPERA;
    } else if(isFirefox) {
      browserCache = ClientBrowser.FIREFOX;
    } else if(isSafari) {
      browserCache = ClientBrowser.SAFARI;
    } else if(isIE) {
      browserCache = ClientBrowser.IE;
    } else if(isEdge) {
      browserCache = ClientBrowser.EDGE;
    } else if(isChrome) {
      browserCache = ClientBrowser.CHROME;
    } else if(isBlink) {
      browserCache = ClientBrowser.BLINK;
    } else {
      browserCache = ClientBrowser.UNKNOWN;
    }
  }
  return browserCache;
}

export { ClientBrowser, detectClientBrowser };
