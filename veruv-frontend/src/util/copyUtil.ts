

// https://stackoverflow.com/a/46118025
function copyToClipboard(text:string) {
  var input = document.createElement("input");
  document.body.appendChild(input);
  input.setAttribute('value', text);
  input.select();
  document.execCommand("copy");
  document.body.removeChild(input);
}

export default copyToClipboard;