import React, { Component } from 'react';
import { LinkContainer as ReactLinkContainer } from 'react-router-bootstrap';
import { NavLinkProps } from "react-router-dom";
import {
  MenuItem,
  NavItem,
} from 'react-bootstrap';

interface LinkContainerProps extends NavLinkProps {
  disabled?: boolean;
}

class LinkContainer extends Component<LinkContainerProps> {
  render() {
    return this.props.disabled ? this.props.children : (
      <ReactLinkContainer {...this.props}>
        {this.props.children}
      </ReactLinkContainer>
    );
  }
}

class LinkMenuItem extends Component<LinkContainerProps & MenuItem.MenuItemProps> {
  render() {
    return (
      <LinkContainer {...this.props as any}>
        <MenuItem {...this.props as any}>
          {this.props.children}
        </MenuItem>
      </LinkContainer>
    );
  }
}

class LinkNavItem extends Component<LinkContainerProps & NavItem.NavItemProps> {
  render() {
    return (
      <LinkContainer {...this.props as any}>
        <NavItem {...this.props as any}>
          {this.props.children}
        </NavItem>
      </LinkContainer>
    );
  }
}

export { LinkContainer, LinkMenuItem, LinkNavItem };
