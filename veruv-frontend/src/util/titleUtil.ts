
// If changed, change index.html title too
const defaultText = 'Certified snapshots of online information | Veruv'
const titleSuffix = ' | Veruv: Certified snapshots'

function setTitle(text?:string) {
  const title:string = (text
    ? text + titleSuffix
    : defaultText);
  document.title = title;
}

export default setTitle;
