import { ClientBrowser, detectClientBrowser } from './detectClientBrowser';

var isClientBrowserSupportedCache:boolean|undefined = undefined;

interface _Addon {
  clientBrowser:ClientBrowser;
  getAddonImg:string;
  storeLink:string;
}

const addons:{[clientBrowser:string]:Addon} = {
  [ClientBrowser.CHROME]: {
    clientBrowser: ClientBrowser.CHROME,
    getAddonImg: '/img/get-addon-chrome.png',
    storeLink: 'https://chrome.google.com/webstore/detail/veruv/pfjafdoblmknhigkglngjbjboeajhfpk',
  },
  [ClientBrowser.FIREFOX]: {
    clientBrowser: ClientBrowser.FIREFOX,
    getAddonImg: '/img/get-addon-firefox.png',
    storeLink: 'https://addons.mozilla.org/en-US/firefox/addon/veruv/'
  },
  // TODO Enable when Opera addon is approved (It seems many others are waiting months and even years, probably not gonna happen anytime soon...)
  // [ClientBrowser.OPERA]: {
  //   clientBrowser: ClientBrowser.OPERA,
  //   getAddonImg: '/img/get-addon-opera.png',
  //   storeLink: 'about:blank',
  // },
}

function isClientBrowserSupported():boolean {
  if(isClientBrowserSupportedCache === undefined) {
    isClientBrowserSupportedCache = !!addons[detectClientBrowser()];
  }
  return isClientBrowserSupportedCache;
}

export type Addon = _Addon;
export { addons, isClientBrowserSupported };
