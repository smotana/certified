import { Box } from "../server/serverApi";

/**
 * Increases a box if necessary to have at minimum given width and height.
 * Extension is added on both ends evenly.
 */
function minSizedBox(minSize:number, box:Box):Box {
  var width = Math.abs(box.p1.x - box.p2.x);
  var height = Math.abs(box.p1.y - box.p2.y);
  if(width >= minSize && height >= minSize) {
    return box;
  }
  var widthExtension = Math.max(0, (minSize - width) / 2);
  var heightExtension = Math.max(0, (minSize - height) / 2);
  return {
    p1: {
      x: Math.min(box.p1.x, box.p2.x) - Math.floor(widthExtension),
      y: Math.min(box.p1.y, box.p2.y) - Math.floor(heightExtension)
    },
    p2: {
      x: Math.max(box.p1.x, box.p2.x) + Math.ceil(widthExtension),
      y: Math.max(box.p1.y, box.p2.y) + Math.ceil(heightExtension)
    }
  };
}

export { minSizedBox };
