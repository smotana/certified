
// https://stackoverflow.com/questions/1038727/how-to-get-browser-width-using-javascript-code
function getWidth() {
  return Math.max(
    document.body.scrollWidth,
    document.documentElement && document.documentElement.scrollWidth || 0,
    document.body.offsetWidth,
    document.documentElement && document.documentElement.offsetWidth || 0,
    document.documentElement && document.documentElement.clientWidth || 0
  );
}
function getHeight() {
  return Math.max(
    document.body.scrollHeight,
    document.documentElement && document.documentElement.scrollHeight || 0,
    document.body.offsetHeight,
    document.documentElement && document.documentElement.offsetHeight || 0,
    document.documentElement && document.documentElement.clientHeight || 0
  );
}

function detectBrowserDimensions() {
  return {width: getWidth(), height: getHeight()}
}

export default detectBrowserDimensions;