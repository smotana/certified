// https://stackoverflow.com/a/53241430
import React from 'react';
import makeAsyncScriptLoader from 'react-async-script';
import { Collapse } from 'react-bootstrap';

interface State {
  scriptLoaded:boolean;
}

function withScript(url:string, WrappedComponent, fadeIn:boolean=false, LoadingComponent=false) {

  class Wrapper extends React.PureComponent<any,any> {
    constructor(props) {
      super(props);
      this.state = {
        scriptLoaded: false
      };
    }
    render() {
      const AsyncScriptLoader = makeAsyncScriptLoader(url)(LoadingComponent || (props => (<div />)));
      var cmpt = this.state.scriptLoaded ? (
        <WrappedComponent {...this.props} />
      ) : (
        <AsyncScriptLoader
          asyncScriptOnLoad={() => {
            this.setState({scriptLoaded: true});
          }}
        />
      );
      return fadeIn ? (
        <Collapse
          in={!this.props.forceHide && this.state.scriptLoaded}
        >
          <div>
            {cmpt}
          </div>
        </Collapse>
      ) : cmpt;
    }
  }
  return Wrapper;
}

export default withScript