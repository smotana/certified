import React, { Component } from 'react';
import {
  Grid,
  Row,
  Col,
} from 'react-bootstrap';
import Hr from './Hr';
import ScrollAnchor from './components/ScrollAnchor';
import SecurityIcon from '@material-ui/icons/SecurityRounded';
import PrivacyIcon from '@material-ui/icons/VisibilityOffRounded';
import BountyIcon from '@material-ui/icons/SearchRounded';
import setTitle from './util/titleUtil';


class SecurityPage extends Component {
  readonly styles = {
    content: {
      textAlign: 'left' as 'left',
      fontSize: '1em',
    },
    hr: {
      marginTop: "40px",
      marginBottom: "30px",
      width: "10%",
    },
  }

  componentDidMount() {
    setTitle('Security');
  }

  render() {
    var securityEmailContact = (<a href="mailto:security@veruv.com">security@veruv.com</a>);
    return (
      <div style={this.styles.content}>
        <ScrollAnchor scrollOnStateName='security' />
        <h1>
          <SecurityIcon fontSize='default' />
          &nbsp;
          Security at Veruv
        </h1>
        <h2>Your information is secure</h2>
        <p>
          Our users entrust us in handling their sensitive information which is why security is our top priority in all aspects of our service to maintain trust with our users.
        </p>
        <p>
          If you find any security related issues or simply have a question, don't hesitate to contact us at {securityEmailContact}.
        </p>
        <h3>Secure browser</h3>
        <p>
          The embedded browser you see on our website used for capturing snapshots is our highest security concern.
          We ensure the browser is safe and secure to use as if you were browsing on your own computer with your own browser at home.
        </p>
        <p>
          In summary, the embedded secure browser runs inside a hardened network-isolated one-time-use Docker container running Chrome with sandboxing enabled with only Selenium as input and a Squid proxy server as output.
        </p>
        <h4>Architecture</h4>
        <p>
          The embedded browser you see on our website is actually a browser running remotely on a security-isolated Docker container.
        </p>
        <p>
          All mouse and keyboard inputs by the client is sanitized and transmitted to the browser running Selenium.
        </p>
        <p>
          The browser itself is an up-to-date Chrome browser running with built-in Chrome sandboxing. The Chrome is only operated using Selenium directly connected to the web server exchanging your inputs and the browser screen.
        </p>
        <p>
          The browser runs inside a Docker container which is started just for your session and torn down immediately after use.
          The Docker container is hardened to prevent breakout to host system.
          In addition, the Docker container is network isolated only allowing a Selenium connection in and a connection to our Squid proxy server for browsing.
        </p>
        <p>
          The Squid proxy server is used to ensure only external access to the internet is allowed and control bandwidth limitations.
        </p>
        <h3>Payment information</h3>
        <p>
          Sensitive information regarding your payment details is all stored safely and securely with our payment processor. We do not have access to any of your payment data.
        </p>
        {/* <h3>Bug bounty program</h3>
        <p>
          We operate a bug bounty reward program to compensate the hard work it takes to find vulnerabilities and to incentivize bugs to be disclosed to us rather than being used maliciously. See more details under 'Bug bounty'.
        </p> */}
      </div>
    );
  }
}

export default SecurityPage;
