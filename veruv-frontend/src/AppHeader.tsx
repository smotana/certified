import React, { Component } from 'react';
import {
  Nav,
  Navbar,
  NavItem,
  NavDropdown,
  MenuItem,
} from 'react-bootstrap';
import {
  Link,
} from 'react-router-dom'
import { LinkContainer, LinkMenuItem, LinkNavItem } from './util/LinkContainer';
import withAuthentication, { AuthenticatorProps } from './server/auth/authenticator';
import { isBeta, isProd, detectEnv } from './util/detectEnv';

class AppHeader extends Component<AuthenticatorProps> {
  readonly styles = {
    navbarContainer: {
      maxWidth: '1224px',
      margin: 'auto',
    },
    navbar: {
      backgroundColor: 'transparent',
      borderColor: 'transparent',
    },
    navbarLink: {
      backgroundColor: 'transparent',
      color: '#000998',
    }
  };

  render() {
    return (
      <div style={this.styles.navbarContainer}>
        <Navbar fluid collapseOnSelect style={this.styles.navbar}>
          <Navbar.Header>
            <div
              style={{
                padding: '10px',
                height: '50px',
                lineHeight: '20px',
                float: 'left',
            }}>
              <Link to={"/"}>
                <img
                  style={{
                    maxHeight:'100%',
                  }}
                  src='/img/veruv-v.png' />
              </Link>
              <span style={{fontSize: '0.6em', color: '#000998'}}>
                {!isProd() && ' ' + detectEnv()}
                {isBeta() && ' BETA'}
              </span>
            </div>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse style={this.styles.navbarLink}>
            <Nav>
              <LinkNavItem to={"/"} activeClassName={''}>Home</LinkNavItem>
              <LinkNavItem to={"/create"} activeClassName={''}>Create</LinkNavItem>
            </Nav>
            <Nav pullRight>
              <LinkNavItem to={"/usecase"} activeClassName={''}>Use cases</LinkNavItem>
              <LinkNavItem to={"/pricing"} activeClassName={''}>Pricing</LinkNavItem>
              <NavDropdown id="navdropdown-appHeader-about" title='About'>
                <LinkMenuItem to={"/about/security"}>Security</LinkMenuItem>
                {/* <LinkMenuItem to={"/about/bug-bounty"}>Bug Bounty</LinkMenuItem> */}
                <MenuItem divider />
                <LinkMenuItem to={"/about/privacy-policy"}>Privacy Policy</LinkMenuItem>
                <LinkMenuItem to={"/about/terms-of-service"}>Terms of Service</LinkMenuItem>
                <MenuItem divider />
                <LinkMenuItem to={"/about/contact"}>Contact</LinkMenuItem>
              </NavDropdown>
              <NavDropdown id="navdropdown-appHeader-user" title={this.props.displayName as any}>
                <LinkMenuItem to={"/snapshots"}>My Snapshots</LinkMenuItem>
                <MenuItem divider />
                <LinkMenuItem to={"/account"} disabled={!this.props.isAuthenticated}>Account</LinkMenuItem>
                {this.props.isAuthenticated ? (
                  <MenuItem onSelect={e => this.props.logout()}>Logout</MenuItem>
                ) : (
                  <LinkMenuItem to={"/members"}>Login/Register</LinkMenuItem>
                )}
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}

export default withAuthentication(AppHeader);
