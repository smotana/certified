
help:
	@echo "\tmake run-dev-frontend\n\tmake build-no-test && make run-dev\n\tmake build && make publish-jar && make prod-rolling-restart"

build:
	mvn install

build-no-test:
	mvn install -DskipTests

build-server-no-test:
	cd veruv-server && mvn install -DskipTests

publish-image:
	cd veruv-container && mvn dockerfile:push

publish-jar:
	cd veruv-container && mvn exec:exec

prod-rolling-restart:
	gcloud beta compute instance-groups managed rolling-action replace --max-surge 1 --max-unavailable 0 --zone us-east1-b veruv-webserver

run-dev:
	@$(MAKE) _run-dev -j 10
_run-dev: datastore-run datastore-viewer tomcat-run-dev nginx-run

run-dev-frontend:
	cd veruv-frontend && npm start

tomcat-run-dev:
	docker run --rm --name veruv-webserver \
	-e VERUV_ENVIRONMENT=DEVELOPMENT_LOCAL \
	-p 80:8080 \
	-v `pwd`/veruv-server/target/veruv-server-0.1:/usr/local/tomcat/webapps/ROOT \
	-v `pwd`/veruv-server/src/test/resources/logback-dev.xml:/usr/local/tomcat/webapps/ROOT/WEB-INF/classes/logback.xml \
	-v /var/run/docker.sock:/var/run/docker.sock \
	us.gcr.io/com-smotana-veruv/veruv-webserver

nginx-run: .nginx/key.pem .nginx/cert.pem .nginx/nginx.conf
	docker run --rm --name veruv-webserver-ssl-reverse-proxy \
	-p 8300:8300 \
	-p 443:8443 \
	-v `pwd`/.nginx:/etc/nginx/conf.d \
	nginx

.nginx:
	mkdir -p .nginx
.nginx/%.pem: | .nginx
	openssl req -subj '/CN=localhost' -x509 -newkey rsa:4096 -nodes -keyout ./.nginx/key.pem -out ./.nginx/cert.pem -days 365
.ONESHELL:
.nginx/nginx.conf: | .nginx
	cat <<- EOF > $@
	server {
	  listen 8443;
	  ssl on;
	  ssl_certificate /etc/nginx/conf.d/cert.pem;
	  ssl_certificate_key /etc/nginx/conf.d/key.pem;
	  location / {
	     proxy_pass http://host.docker.internal:8080;
	     proxy_http_version 1.1;
	     proxy_set_header Upgrade $http_upgrade;
	     proxy_set_header Connection "upgrade";
	     proxy_read_timeout 86400;
	  }
	}
	server {
	  listen 8300;
	  ssl on;
	  ssl_certificate /etc/nginx/conf.d/cert.pem;
	  ssl_certificate_key /etc/nginx/conf.d/key.pem;
	  location / {
	     proxy_pass http://host.docker.internal:3000;
	  }
	}
	EOF

datastore-run:
	gcloud beta emulators datastore start --host-port=localhost:8141 --no-store-on-disk --project=com-smotana-veruv-dev

#install with npm i -g @streamrail/dsui
datastore-viewer:
	dsui --port 8081 --apiEndpoint localhost:8141 --projectId com-smotana-veruv-dev

.PHONY: \
	build \
	build-no-test \
	publish-jar \
	publish-image \
	prod-rolling-restart \
	run-dev \
	_run-dev \
	run-dev-frontend \
	tomcat-run-dev \
	nginx-run \
	nginx-self-sign \
	datastore-run \
	datastore-viewer
