package com.smotana.veruv.store.gcloud;

import com.google.auth.Credentials;
import com.google.cloud.NoCredentials;
import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.testing.LocalDatastoreHelper;
import com.google.cloud.storage.InMemoryStorageClient;
import com.google.inject.Inject;
import com.smotana.veruv.store.SnapshotStore.Snapshot;
import com.smotana.veruv.store.SnapshotStore.StorageType;
import com.smotana.veruv.store.SnapshotStore.Visibility;
import com.smotana.veruv.store.UserStore;
import com.smotana.veruv.store.UserStore.User;
import com.smotana.veruv.testutil.AbstractVeruvTest;
import com.smotana.veruv.util.PasswordUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.Instant;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

import static com.smotana.veruv.TestUtil.retry;
import static com.smotana.veruv.util.PasswordUtil.Type.USER;
import static org.junit.Assert.*;

@Slf4j
public class GCloudStoreTest extends AbstractVeruvTest {

    private static LocalDatastoreHelper localDatastoreHelper;

    @Inject
    private GCloudStore store;
    @Inject
    private GCloudStore.Config storeConfig;

    @Override
    protected void configure() {
        super.configure();

        bind(Credentials.class).toInstance(NoCredentials.getInstance());
        install(InMemoryStorageClient.module());
        bind(Datastore.class).toInstance((localDatastoreHelper.getOptions().getService()));
        install(GCloudStore.module());
    }

    @BeforeClass
    public static void beforeClass() throws Exception {
        localDatastoreHelper = LocalDatastoreHelper.create(1d);
        localDatastoreHelper.start();
    }

    @Before
    public void beforeTest() throws Exception {
        localDatastoreHelper.reset();
    }

    @AfterClass
    public static void afterClass() throws Exception {
        localDatastoreHelper.stop();
    }

    @Test(timeout = 60_000L)
    public void testUserStore() throws Exception {
        String email = randomEmail();
        String pass = randomPass();

        String id = store.createUser(email, pass);
        assertUserExists(new User(id, email, PasswordUtil.saltHashPassword(USER, pass, id)));

        String newEmail = randomEmail();
        store.updateUserEmail(id, newEmail);
        assertUserNotExists(email);
        assertUserExists(new User(id, newEmail, PasswordUtil.saltHashPassword(USER, pass, id)));

        String newPass = randomPass();
        store.updateUserPassHash(id, newPass);
        assertUserExists(new User(id, newEmail, PasswordUtil.saltHashPassword(USER, newPass, id)));

        store.deleteUser(id);
        assertUserNotExists(id);
        assertUserNotExists(email);
        assertUserNotExists(newEmail);
    }

    @Test(timeout = 60_000L)
    public void testUserStore_emailUniqueness() throws Exception {
        String email = randomEmail();

        store.createUser(email, randomPass());

        try {
            store.createUser(email, randomPass());
            fail();
        } catch (UserStore.EmailTakenException ex) {
            // Expected
        }

        String id = store.createUser(randomEmail(), randomPass());
        try {
            store.updateUserEmail(id, email);
            fail();
        } catch (UserStore.EmailTakenException ex) {
            // Expected
        }
    }

    @Test(timeout = 60_000L)
    public void testSnapshotStore() throws Exception {
        Snapshot snapshot = store.createSnapshot(
                StorageType.Free,
                "url",
                new byte[]{32},
                32,
                32,
                "",
                Visibility.PRIVATE,
                Optional.empty(),
                Optional.empty());
        assertSnapshotExists(snapshot);

        String newNote = "new note";
        store.editNote(snapshot.getId(), newNote);
        Snapshot actualSnapshot = store.getSnapshotById(snapshot.getId()).get();
        snapshot = new Snapshot(
                snapshot.getStorageType(),
                snapshot.getStorageId(),
                snapshot.getId(),
                snapshot.getCreated(),
                snapshot.getExpiry(),
                snapshot.getUrl(),
                snapshot.getScreenshot(),
                snapshot.getScreenshotWidth(),
                snapshot.getScreenshotHeight(),
                newNote,
                snapshot.getVisibility(),
                snapshot.getPassHashedOpt(),
                snapshot.getUserIdOpt());
        assertEquals(snapshot, actualSnapshot);
        assertSnapshotExists(snapshot);

        byte[] newScreenshot = new byte[]{16};
        int newScreenshotWidth = 14;
        int newScreenshotHeight = 13;
        store.editScreenshot(snapshot, snapshot.getStorageType(), newScreenshot, newScreenshotWidth, newScreenshotHeight);
        actualSnapshot = store.getSnapshotById(snapshot.getId()).get();
        snapshot = new Snapshot(
                snapshot.getStorageType(),
                snapshot.getStorageId(),
                snapshot.getId(),
                snapshot.getCreated(),
                snapshot.getExpiry(),
                snapshot.getUrl(),
                newScreenshot,
                newScreenshotWidth,
                newScreenshotHeight,
                snapshot.getNote(),
                snapshot.getVisibility(),
                snapshot.getPassHashedOpt(),
                snapshot.getUserIdOpt());
        assertEquals(snapshot, actualSnapshot);
        assertSnapshotExists(snapshot);

        Visibility newVisibility = Visibility.PUBLIC;
        store.setSnapshotVisibility(snapshot.getId(), newVisibility, Optional.empty());
        snapshot = new Snapshot(
                snapshot.getStorageType(),
                snapshot.getStorageId(),
                snapshot.getId(),
                snapshot.getCreated(),
                snapshot.getExpiry(),
                snapshot.getUrl(),
                snapshot.getScreenshot(),
                snapshot.getScreenshotWidth(),
                snapshot.getScreenshotHeight(),
                snapshot.getNote(),
                newVisibility,
                snapshot.getPassHashedOpt(),
                snapshot.getUserIdOpt());
        assertSnapshotExists(snapshot);

        User user = store.getUserById(store.createUser(randomEmail(), randomPass())).get();
        Optional<String> newUserId = Optional.of(user.getId());
        store.setSnapshotOwner(snapshot.getId(), newUserId);
        snapshot = new Snapshot(
                snapshot.getStorageType(),
                snapshot.getStorageId(),
                snapshot.getId(),
                snapshot.getCreated(),
                snapshot.getExpiry(),
                snapshot.getUrl(),
                snapshot.getScreenshot(),
                snapshot.getScreenshotWidth(),
                snapshot.getScreenshotHeight(),
                snapshot.getNote(),
                snapshot.getVisibility(),
                snapshot.getPassHashedOpt(),
                newUserId);
        assertSnapshotExists(snapshot);

        newUserId = Optional.empty();
        store.setSnapshotOwner(snapshot.getId(), newUserId);
        snapshot = new Snapshot(
                snapshot.getStorageType(),
                snapshot.getStorageId(),
                snapshot.getId(),
                snapshot.getCreated(),
                snapshot.getExpiry(),
                snapshot.getUrl(),
                snapshot.getScreenshot(),
                snapshot.getScreenshotWidth(),
                snapshot.getScreenshotHeight(),
                snapshot.getNote(),
                snapshot.getVisibility(),
                snapshot.getPassHashedOpt(),
                newUserId);
        assertSnapshotExists(snapshot);
        assertSnapshotNotOwnedByUser(snapshot.getId(), user.getId());

        StorageType newStorageType = StorageType.Regular;
        Instant snapshotChangedStorageType = Instant.now();
        store.setStorageType(snapshot.getId(), newStorageType);
        Instant newExpiry = store.getSnapshotById(snapshot.getId()).get().getExpiry();
        assertTrue(!snapshotChangedStorageType.plus(storeConfig.regularStorageBucketExpiry()).isAfter(newExpiry));
        assertTrue(!Instant.now().plus(storeConfig.regularStorageBucketExpiry()).isBefore(newExpiry));
        snapshot = new Snapshot(
                newStorageType,
                incrementStorageId(snapshot.getStorageId()),
                snapshot.getId(),
                snapshot.getCreated(),
                newExpiry,
                snapshot.getUrl(),
                snapshot.getScreenshot(),
                snapshot.getScreenshotWidth(),
                snapshot.getScreenshotHeight(),
                snapshot.getNote(),
                snapshot.getVisibility(),
                snapshot.getPassHashedOpt(),
                newUserId);
        assertSnapshotExists(snapshot);

        snapshotChangedStorageType = Instant.now();
        store.setStorageType(snapshot.getId(), snapshot.getStorageType());
        newExpiry = store.getSnapshotById(snapshot.getId()).get().getExpiry();
        assertTrue(!snapshotChangedStorageType.plus(storeConfig.regularStorageBucketExpiry()).isAfter(newExpiry));
        assertTrue(!Instant.now().plus(storeConfig.regularStorageBucketExpiry()).isBefore(newExpiry));
        snapshot = new Snapshot(
                newStorageType,
                incrementStorageId(snapshot.getStorageId()),
                snapshot.getId(),
                snapshot.getCreated(),
                newExpiry,
                snapshot.getUrl(),
                snapshot.getScreenshot(),
                snapshot.getScreenshotWidth(),
                snapshot.getScreenshotHeight(),
                snapshot.getNote(),
                snapshot.getVisibility(),
                snapshot.getPassHashedOpt(),
                newUserId);
        assertSnapshotExists(snapshot);

        store.deleteSnapshot(snapshot.getId());
        assertSnapshotNotExists(snapshot.getId());

        // Test snapshot created with owner already
        Snapshot snapshotCreatedWithOwner = store.createSnapshot(
                StorageType.Regular,
                "url",
                new byte[]{32},
                32,
                32,
                "",
                Visibility.PUBLIC,
                Optional.empty(),
                Optional.of(user.getId()));
        assertSnapshotExists(snapshotCreatedWithOwner);

        store.setSnapshotOwner(snapshotCreatedWithOwner.getId(), Optional.empty());
        store.deleteSnapshot(snapshotCreatedWithOwner.getId());
        assertSnapshotNotExists(snapshotCreatedWithOwner.getId());
        assertSnapshotNotOwnedByUser(snapshotCreatedWithOwner.getId(), user.getId());
    }

    private void assertSnapshotExists(Snapshot snapshot) throws Exception {
        retry(() -> {
            assertEquals(Optional.of(snapshot), store.getSnapshotById(snapshot.getId()));
            if (snapshot.getUserIdOpt().isPresent()) {
                assertEquals(Optional.of(snapshot), store.getSnapshotsByUser(snapshot.getUserIdOpt().get())
                        .stream()
                        .filter(s -> snapshot.getId().equals(s.getId()))
                        .findAny());
            }
        });
    }

    private void assertSnapshotNotExists(String snapshotId) throws Exception {
        retry(() -> {
            assertEquals(Optional.empty(), store.getSnapshotById(snapshotId));
        });
    }

    private void assertSnapshotNotOwnedByUser(String snapshotId, String userId) throws Exception {
        retry(() -> {
            assertFalse(store.getSnapshotsByUser(userId)
                    .stream()
                    .anyMatch(s -> snapshotId.equals(s.getId())));
        });
    }

    private void assertUserNotExists(String idOrEmail) throws Exception {
        retry(() -> {
            assertEquals(Optional.empty(), store.getUserById(idOrEmail));
            assertEquals(Optional.empty(), store.getUserByEmail(idOrEmail));
        });
    }

    private void assertUserExists(User expectedUser) throws Exception {
        retry(() -> {
            assertEquals(Optional.of(expectedUser), store.getUserById(expectedUser.getId()));
            assertEquals(Optional.of(expectedUser), store.getUserByEmail(expectedUser.getEmail()));
        });
    }

    private String randomEmail() {
        return ThreadLocalRandom.current().nextInt(99999) + "@veruv.com";
    }

    private String randomPass() {
        return String.valueOf(ThreadLocalRandom.current().nextInt());
    }

    private String incrementStorageId(String previousStorageId) {
        String[] split = previousStorageId.split("-v");
        long newVersion = Long.parseLong(split[1]) + 1L;
        return split[0] + "-v" + Long.toString(newVersion);
    }
}
