package com.smotana.veruv.core;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.util.concurrent.ServiceManager;
import com.google.gson.Gson;
import com.google.inject.Injector;
import com.google.inject.Stage;
import com.smotana.veruv.core.VeruvInjector.Environment;
import com.smotana.veruv.docker.DockerManager;
import com.smotana.veruv.store.SnapshotStore;
import com.smotana.veruv.store.UserStore;
import com.smotana.veruv.web.api.BrowserEndpoint;
import com.smotana.veruv.web.api.PingResource;
import com.smotana.veruv.web.api.ShutdownResource;
import com.smotana.veruv.web.api.VerificationResource;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@Slf4j
@RunWith(Parameterized.class)
public class VeruvInjectorTest {

    private final Environment env;

    public VeruvInjectorTest(Environment env) {
        this.env = env;
    }

    @Parameterized.Parameters(name = "{0}")
    public static Iterable<Object> data() {
        return ImmutableList.copyOf(Environment.values());
    }

    @Test(timeout = 5_000L)
    public void testBindings() throws Exception {
        Injector injector = VeruvInjector.INSTANCE.create(env, Stage.TOOL);

        ImmutableSet.of(
                Gson.class,
                DockerManager.class,
                PingResource.class,
                BrowserEndpoint.class,
                ServiceManager.class,
                VerificationResource.class,
                SnapshotStore.class,
// TODO disabled for now
//                SnapshotRequestStore.class,
//                SnapshotRequestResource.class,
//                CreditStore.class,
//                SubscriptionStore.class,
                UserStore.class,
                ShutdownResource.class
        ).forEach(injector::getBinding);
    }
}
