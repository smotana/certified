package com.smotana.veruv.util;

import com.google.common.util.concurrent.SettableFuture;
import com.google.inject.Inject;
import com.kik.config.ice.ConfigSystem;
import com.smotana.veruv.testutil.AbstractVeruvTest;
import lombok.Data;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Supplier;

import static com.smotana.veruv.TestUtil.retry;
import static org.junit.Assert.*;

public class StockPileTest extends AbstractVeruvTest {

    @Inject
    private StockPile.Config config;

    private ExecutorService executor;

    @Override
    protected void configure() {
        super.configure();
        install(ConfigSystem.configModuleWithOverrides(StockPile.Config.class, om -> {
            om.override(om.id().maxSize()).withValue(4L);
            om.override(om.id().stockSize()).withValue(2L);
            om.override(om.id().retryTime()).withValue(Duration.ZERO);
        }));
    }

    @Before
    public void before() {
        Item.resetCounter();
        executor = Executors.newCachedThreadPool();
    }

    @After
    public void after() {
        executor.shutdownNow();
    }


    @Test(timeout = 60_000L)
    public void test() throws Exception {
        assertEquals(0L, Item.instancesCreated());
        StockPile<Item> stockPile = new StockPile<>("Item", Item::new, config, Optional.empty());
        stockPile.startUp();
        try {
            retry(() -> assertEquals(2L, Item.instancesCreated()));

            stockPile.get();
            retry(() -> assertEquals(3L, Item.instancesCreated()));

            stockPile.get();
            retry(() -> assertEquals(4L, Item.instancesCreated()));

            stockPile.get();
            retry(() -> assertEquals(4L, Item.instancesCreated()));

            stockPile.get();
            retry(() -> assertEquals(4L, Item.instancesCreated()));

            Future<Item> f1 = getInBackground(stockPile);
            retry(() -> assertEquals(4L, Item.instancesCreated()));
            assertFalse(f1.isDone());

            Future<Item> f2 = getInBackground(stockPile);
            retry(() -> assertEquals(4L, Item.instancesCreated()));
            assertFalse(f1.isDone());
            assertFalse(f2.isDone());

            stockPile.release();
            retry(() -> assertEquals(5L, Item.instancesCreated()));
            retry(() -> assertTrue(f1.isDone()));
            assertFalse(f2.isDone());

            stockPile.release();
            retry(() -> assertEquals(6L, Item.instancesCreated()));
            retry(() -> assertTrue(f2.isDone()));

            stockPile.release();
            retry(() -> assertEquals(7L, Item.instancesCreated()));

            stockPile.release();
            retry(() -> assertEquals(8L, Item.instancesCreated()));

            stockPile.release();
            retry(() -> assertEquals(8L, Item.instancesCreated()));

            stockPile.get();
            retry(() -> assertEquals(9L, Item.instancesCreated()));
        } finally {
            stockPile.shutDown();
        }
    }

    @Test(timeout = 60_000L)
    public void testFaultyFactory() throws Exception {
        StockPile<Item> stockPile = new StockPile<>("Item", new FaultyFactory(), config, Optional.empty());
        stockPile.startUp();
        try {
            stockPile.get();
        } finally {
            stockPile.shutDown();
        }
    }

    @Test(timeout = 60_000L)
    public void testNestedStockPile() throws Exception {
        StockPile<Item> stockPileInner = new StockPile<>("ItemInner", Item::new, config, Optional.empty());
        StockPile<Item> stockPile = new StockPile<>("ItemOuter", stockPileInner::get, config, Optional.empty());
        stockPileInner.startUp();
        stockPile.startUp();
        try {
            stockPile.get();
            stockPile.get();
            stockPile.get();
            stockPile.get();
            Future<Item> f1 = getInBackground(stockPile);
            Future<Item> f2 = getInBackground(stockPile);

            retry(() -> assertEquals(4L, Item.instancesCreated()));
            assertFalse(f1.isDone());
            assertFalse(f2.isDone());

            stockPile.release();
            stockPileInner.release();
            retry(() -> assertTrue(f1.isDone()));
            assertFalse(f2.isDone());

            Future<Item> f3 = getInBackground(stockPile);
            assertFalse(f2.isDone());
            assertFalse(f3.isDone());

            stockPile.release();
            stockPileInner.release();
            retry(() -> assertTrue(f2.isDone()));
            assertFalse(f3.isDone());

            stockPile.release();
            stockPileInner.release();
            retry(() -> assertTrue(f3.isDone()));
        } finally {
            stockPileInner.shutDown();
            stockPile.shutDown();
        }
    }

    private <E> Future<E> getInBackground(StockPile<E> stockPile) {
        SettableFuture<E> future = SettableFuture.create();
        executor.execute(() -> {
            future.set(stockPile.get());
        });
        return future;
    }

    @Data
    private static class Item {
        private static long nextId = 0;
        private final long id = nextId++;

        static void resetCounter() {
            nextId = 0;
        }

        static long instancesCreated() {
            return nextId;
        }
    }

    private static class FaultyFactory implements Supplier<Item> {
        private boolean shouldFailNextTime = true;

        @Override
        public Item get() {
            boolean shouldFailNow = shouldFailNextTime;
            shouldFailNextTime = !shouldFailNextTime;
            if (shouldFailNow) {
                throw new RuntimeException("Faulty factory exception");
            } else {
                return new Item();
            }
        }
    }
}