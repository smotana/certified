package com.smotana.veruv.web.api;

import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.testing.LocalDatastoreHelper;
import com.google.cloud.storage.InMemoryStorageClient;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.util.Modules;
import com.kik.config.ice.ConfigSystem;
import com.smotana.veruv.StringableSecretKey;
import com.smotana.veruv.core.AbstractVeruvModule;
import com.smotana.veruv.docker.DockerClientProvider;
import com.smotana.veruv.image.ImageEditorImpl;
import com.smotana.veruv.store.gcloud.GCloudStore;
import com.smotana.veruv.testutil.AbstractVeruvJerseyTest;
import com.smotana.veruv.util.TimingAttackUtil;
import com.smotana.veruv.web.guard.GuardAuth;
import com.smotana.veruv.web.guard.GuardAuthImpl;
import com.smotana.veruv.web.guard.TokenManagerImpl;
import com.smotana.veruv.web.message.AccountInfo;
import com.smotana.veruv.web.message.Login;
import com.smotana.veruv.web.message.Register;
import com.smotana.veruv.web.message.VerifyToken;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.crypto.SecretKey;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import static io.jsonwebtoken.SignatureAlgorithm.HS512;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@Slf4j
public class UserResourceTest extends AbstractVeruvJerseyTest {

    @Inject
    private Provider<UserResource> userResourceProvider;
    @Inject
    private GuardAuth guardAuth;

    private static LocalDatastoreHelper localDatastoreHelper;

    @Override
    protected void configure() {
        super.configure();
        installResource(UserResource.class);
        install(InMemoryStorageClient.module());
        bind(Datastore.class).toInstance((localDatastoreHelper.getOptions().getService()));
        install(Modules.override(
                DockerClientProvider.module(),
                GCloudStore.module(),
                GuardAuthImpl.module(),
                TokenManagerImpl.module(),
                TimingAttackUtil.module(),
                ImageEditorImpl.module()
        ).with(new AbstractVeruvModule() {
            @Override
            protected void configure() {
                StringableSecretKey privKey = new StringableSecretKey(Keys.secretKeyFor(HS512));
                log.trace("Using generated priv key: {}", privKey);
                bind(SecretKey.class).toInstance(privKey);
                install(ConfigSystem.overrideModule(TokenManagerImpl.Config.class, om -> {
                    om.override(om.id().tokenSignerPrivKey()).withValue(privKey);
                }));

                install(ConfigSystem.overrideModule(DockerClientProvider.Config.class, om -> {
                    om.override(om.id().dockerServerUri()).withValue("unix:///var/run/docker.sock");
                }));
            }
        }));
    }

    @BeforeClass
    public static void beforeClass() throws Exception {
        localDatastoreHelper = LocalDatastoreHelper.create(1d);
        localDatastoreHelper.start();
    }

    @Before
    public void beforeTest() throws Exception {
        localDatastoreHelper.reset();
    }

    @AfterClass
    public static void afterClass() throws Exception {
        localDatastoreHelper.stop();
    }

    @Test(timeout = 60_000L)
    public void test() throws Exception {
        String email = "email@example.com";
        String pass = "pass";
        AccountInfo accountInfoRegister = register(email, pass);
        assertVerifyToken(accountInfoRegister);

        AccountInfo accountInfoLogin = login(email, pass);
        assertVerifyToken(accountInfoLogin);
    }

    @Test(timeout = 60_000L)
    public void testInvalidLoginEmail() throws Exception {
        register("email@example.com", "pass");
        try {
            userResourceProvider.get().login(new Login("invalid@example.com", "pass"));
        } catch (WebApplicationException ex) {
            assertEquals(403, ex.getResponse().getStatus());
            return;
        }
        fail();
    }

    @Test(timeout = 60_000L)
    public void testInvalidLoginPass() throws Exception {
        register("email@example.com", "pass");
        try {
            userResourceProvider.get().login(new Login("email@example.com", "invalid pass"));
        } catch (WebApplicationException ex) {
            assertEquals(403, ex.getResponse().getStatus());
            return;
        }
        fail();
    }

    @Test(timeout = 60_000L)
    public void testInvalidToken() throws Exception {
        try {
            userResourceProvider.get().verifyToken(new VerifyToken("Invalid token"));
        } catch (WebApplicationException ex) {
            assertEquals(403, ex.getResponse().getStatus());
            return;
        }
        fail();
    }

    private AccountInfo register(String email, String pass) throws Exception {
        Response response = userResourceProvider.get().register(new Register(email, pass));
        assertEquals(200, response.getStatus());
        AccountInfo accountInfo = (AccountInfo) response.getEntity();
        assertEquals(guardAuth.normalizeEmail(email), accountInfo.getEmail());
        return accountInfo;
    }

    private AccountInfo login(String email, String pass) throws Exception {
        Response response = userResourceProvider.get().login(new Login(email, pass));
        assertEquals(200, response.getStatus());
        AccountInfo accountInfo = (AccountInfo) response.getEntity();
        assertEquals(guardAuth.normalizeEmail(email), accountInfo.getEmail());
        return accountInfo;
    }

    private void assertVerifyToken(AccountInfo expectedAccountInfo) throws Exception {
        Response response = userResourceProvider.get().verifyToken(new VerifyToken(expectedAccountInfo.getToken()));
        assertEquals(200, response.getStatus());
        AccountInfo actualAccountInfo = (AccountInfo) response.getEntity();
        assertEquals(expectedAccountInfo, actualAccountInfo);
    }
}