package com.smotana.veruv.web;

import com.smotana.veruv.web.api.PingResource;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;

import javax.ws.rs.core.Application;

public class CoreResourceTest extends JerseyTest {

    @Override
    protected Application configure() {
        return new ResourceConfig(PingResource.class);
    }

    // TODO fix up
//    @Test
//    public void test() {
//        Response response = target("/test/ping").request()
//                .get();
//
//        assertEquals("Http Response should be 200: ", Response.Status.OK.getStatusCode(), response.getStatus());
//        assertEquals("Http Content-Type should be: ", MediaType.TEXT_PLAIN, response.getHeaderString(HttpHeaders.CONTENT_TYPE));
//
//        String content = response.readEntity(String.class);
//        assertEquals("Content of response is: ", "pong", content);
//    }
}