package com.smotana.veruv.web.guard;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.util.Modules;
import com.kik.config.ice.ConfigSystem;
import com.smotana.veruv.StringableSecretKey;
import com.smotana.veruv.core.AbstractVeruvModule;
import com.smotana.veruv.testutil.AbstractVeruvTest;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import javax.crypto.SecretKey;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.Optional;

import static com.smotana.veruv.web.guard.TokenManagerImpl.TOKEN_ALGO;
import static io.jsonwebtoken.SignatureAlgorithm.HS512;
import static org.junit.Assert.*;

@Slf4j
public class TokenManagerImplTest extends AbstractVeruvTest {

    @Inject
    private TokenManager tokenManager;

    @Override
    protected void configure() {
        super.configure();

        install(Modules.override(
                TokenManagerImpl.module()
        ).with(new AbstractVeruvModule() {
            @Override
            protected void configure() {
                StringableSecretKey privKey = new StringableSecretKey(Keys.secretKeyFor(HS512));
                log.trace("Using generated priv key: {}", privKey);
                bind(SecretKey.class).toInstance(privKey);
                install(ConfigSystem.overrideModule(TokenManagerImpl.Config.class, om -> {
                    om.override(om.id().tokenSignerPrivKey()).withValue(privKey);
                }));
            }
        }));
    }

    @Test(timeout = 60_000L)
    public void testTokenCreation() throws Exception {
        TokenManager.Token expectedToken = new TokenManager.Token(TokenManager.ResourceType.USER, "matus");
        String actualTokenStr = tokenManager.create(expectedToken);

        TokenManager.Token actualToken = tokenManager.parse(actualTokenStr);
        assertEquals(expectedToken, actualToken);

        tokenManager.authorize(actualToken, expectedToken.getType(), expectedToken.getId());
    }


    @Test(timeout = 60_000L)
    public void testParseResourceType() throws Exception {
        String tokenInvalidSignature = Jwts.builder()
                .setIssuedAt(new Date())
                .setClaims(ImmutableMap.of(
                        "type", TokenManager.ResourceType.SNAPSHOT.name(),
                        "id", "someId"
                ))
                .signWith(Keys.secretKeyFor(TOKEN_ALGO), TOKEN_ALGO)
                .compact();

        Optional<TokenManager.ResourceType> resourceType = tokenManager.parseTypeUnsafe(tokenInvalidSignature);
        assertTrue(resourceType.isPresent());
        assertEquals(TokenManager.ResourceType.SNAPSHOT, resourceType.get());

        try {
            tokenManager.parse(tokenInvalidSignature);
            fail();
        } catch (WebApplicationException ex) {
            assertEquals(Response.Status.FORBIDDEN.getStatusCode(), ex.getResponse().getStatus());
        }
    }
}
