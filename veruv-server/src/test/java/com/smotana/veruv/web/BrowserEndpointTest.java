package com.smotana.veruv.web;

import com.gargoylesoftware.htmlunit.javascript.host.event.KeyboardEvent;
import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.testing.LocalDatastoreHelper;
import com.google.cloud.storage.InMemoryStorageClient;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.util.Modules;
import com.kik.config.ice.ConfigSystem;
import com.smotana.veruv.StringableSecretKey;
import com.smotana.veruv.core.AbstractVeruvModule;
import com.smotana.veruv.docker.BrowserCapabilitiesFactory;
import com.smotana.veruv.docker.DockerClientProvider;
import com.smotana.veruv.docker.DockerManagerImpl;
import com.smotana.veruv.image.ImageEditorImpl;
import com.smotana.veruv.image.VideoEncoderImpl;
import com.smotana.veruv.resources.BrowserExpiry;
import com.smotana.veruv.resources.BrowserImpl;
import com.smotana.veruv.resources.BrowserResourceManagerImpl;
import com.smotana.veruv.resources.InMemoryResourceQueue;
import com.smotana.veruv.store.gcloud.GCloudStore;
import com.smotana.veruv.testutil.AbstractVeruvTest;
import com.smotana.veruv.util.TimingAttackUtil;
import com.smotana.veruv.web.api.BrowserEndpoint;
import com.smotana.veruv.web.guard.GuardAuthImpl;
import com.smotana.veruv.web.guard.GuardBrowserImpl;
import com.smotana.veruv.web.guard.TokenManagerImpl;
import com.smotana.veruv.web.guard.challenge.InMemoryChallenger;
import com.smotana.veruv.web.message.*;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.messages.Container;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.mockito.Mockito;

import javax.crypto.SecretKey;
import javax.websocket.CloseReason;
import javax.websocket.EndpointConfig;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.smotana.veruv.web.ClientProperties.CLIENT_PROPERTIES_KEY;
import static io.jsonwebtoken.SignatureAlgorithm.HS512;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


@Slf4j
public class BrowserEndpointTest extends AbstractVeruvTest {
    private static final String DOCKER_INSTANCE_NAME_PREFIX = "veruvTest-";

    @Inject
    private Gson gson;
    @Inject
    private Provider<BrowserEndpoint> browserEndpointProvider;
    @Inject
    private DockerClientProvider dockerClientProvider;

    private static LocalDatastoreHelper localDatastoreHelper;

    @Override
    protected void configure() {
        install(InMemoryStorageClient.module());
        bind(Datastore.class).toInstance((localDatastoreHelper.getOptions().getService()));
        install(Modules.override(
                DockerClientProvider.module(),
                TokenManagerImpl.module(),
                GCloudStore.module(),
                BrowserResourceManagerImpl.module(),
                BrowserImpl.module(),
                DockerManagerImpl.module(),
                BrowserExpiry.module(),
                InMemoryResourceQueue.module(),
                VideoEncoderImpl.module(),
                ImageEditorImpl.module(),
                BrowserCapabilitiesFactory.module(),
                GuardBrowserImpl.module(),
                InMemoryChallenger.module(),
                GuardAuthImpl.module(),
                TimingAttackUtil.module()
        ).with(new AbstractVeruvModule() {
            @Override
            protected void configure() {
                install(ConfigSystem.configModuleWithOverrides(DockerManagerImpl.Config.class, om -> {
                    om.override(om.id().instanceNamePrefix()).withValue(DOCKER_INSTANCE_NAME_PREFIX);
                    om.override(om.id().connectSelfToDockerNetwork()).withValue(false);
                }));

                install(ConfigSystem.configModuleWithOverrides(DockerClientProvider.Config.class, om -> {
                    om.override(om.id().dockerServerUri()).withValue("unix:///var/run/docker.sock");
                }));

                StringableSecretKey privKey = new StringableSecretKey(io.jsonwebtoken.security.Keys.secretKeyFor(HS512));
                log.trace("Using generated priv key: {}", privKey);
                bind(SecretKey.class).toInstance(privKey);
                install(ConfigSystem.overrideModule(TokenManagerImpl.Config.class, om -> {
                    om.override(om.id().tokenSignerPrivKey()).withValue(privKey);
                }));
            }
        }));
    }

    @BeforeClass
    public static void beforeClass() throws Exception {
        localDatastoreHelper = LocalDatastoreHelper.create(1d);
        localDatastoreHelper.start();
    }

    @Before
    public void beforeTest() throws Exception {
        localDatastoreHelper.reset();
    }

    @AfterClass
    public static void afterClass() throws Exception {
        localDatastoreHelper.stop();
    }

    @After
    public void cleanupContainers() throws Exception {
        DockerClient client = dockerClientProvider.get();
        List<Container> containers = client.listContainers(DockerClient.ListContainersParam.withStatusRunning());
        containers.stream()
                .filter(c -> c.names().stream().anyMatch(name -> name.startsWith(DOCKER_INSTANCE_NAME_PREFIX)))
                .map(Container::id)
                .forEach(dockerId -> {
                    try {
                        client.killContainer(dockerId);
                    } catch (Exception ex) {
                        log.error("Possible container leak from test, docker id: {}", dockerId, ex);
                    }
                    try {
                        client.removeContainer(dockerId);
                    } catch (Exception ex) {
                        log.error("Possible container leak from test, docker id: {}", dockerId, ex);
                    }
                });
    }

    // TODO Test not working with new docker container for now
//    @Test(timeout = 60_000L)
    public void test() throws Exception {
        MockSession mockSession = new MockSession();
        BrowserEndpoint browserEndpoint = browserEndpointProvider.get();
        browserEndpoint.onOpen(mockSession, createEndpointConfig(new ClientProperties(
                "127.0.0.1",
                "my user agent",
                "",
                ""
        )));

        sendMessage(browserEndpoint, new BrowserRequest("about:blank", 1024, 768, null, null, null, null, null));
        assertRecv(mockSession, new BrowserInQueue(10L, TimeUnit.SECONDS, 0L));
        assertRecv(mockSession, new BrowserAcquired());
        sendMessage(browserEndpoint, new ActionNavigateUrl("https://google.com"));
        sendMessage(browserEndpoint, new ActionKeyboardKeyPrint(new int[]{'h', 'i'}));
        sendMessage(browserEndpoint, new ActionKeyboardKeyPress(KeyboardEvent.DOM_VK_DELETE, ActionKeyboardKeyPress.KeyPressType.PRESS));
        sendMessage(browserEndpoint, new ActionKeyboardKeyPress(KeyboardEvent.DOM_VK_DELETE, ActionKeyboardKeyPress.KeyPressType.PRESS));
        sendMessage(browserEndpoint, new ActionMouseClick(40L, 80L, true));
        sendMessage(browserEndpoint, new ActionMouseClick(50L, 90L, true));
        sendMessage(browserEndpoint, new ActionMouseClick(60L, 100L, true));
        sendMessage(browserEndpoint, new ActionKeyboardKeyPrint(new int[]{'h', 'i'}));
        sendMessage(browserEndpoint, new ActionMouseClick(70L, 90L, true));
        sendMessage(browserEndpoint, new ActionMouseClick(80L, 80L, true));
        browserEndpoint.onClose(null, new CloseReason(CloseReason.CloseCodes.NORMAL_CLOSURE, ""));
    }

    private void sendMessage(BrowserEndpoint browserEndpoint, Message message) throws Exception {
        String messageString = gson.toJson(message);
        browserEndpoint.onMessage(messageString);
    }

    private Message assertRecv(MockSession mockSession, String msgType) throws Exception {
        Message actualMsg = recv(mockSession);
        assertEquals(msgType, actualMsg.getType());
        return actualMsg;
    }

    private Message assertRecv(MockSession mockSession, Message expectedMsg) throws Exception {
        Message actualMsg = recv(mockSession);
        assertEquals(expectedMsg, actualMsg);
        return actualMsg;
    }

    private Message recv(MockSession mockSession) throws Exception {
        log.info("Waiting for new message...");
        String messageString = mockSession.msgQueue.take();
        Message msg = gson.fromJson(messageString, Message.class);
        log.info("Received from server: {}", msg);
        return msg;
    }

    private ImmutableList<Message> recvAll(MockSession mockSession) throws Exception {
        List<String> msgsStr = Lists.newArrayList();
        mockSession.msgQueue.drainTo(msgsStr);
        ImmutableList<Message> msgs = msgsStr.stream()
                .map(msgStr -> gson.fromJson(msgStr, Message.class))
                .collect(ImmutableList.toImmutableList());
        log.info("Received from server: {}", msgs);
        return msgs;
    }

    private EndpointConfig createEndpointConfig(ClientProperties clientProperties) {
        EndpointConfig endpointConfigMock = Mockito.mock(EndpointConfig.class);
        when(endpointConfigMock.getUserProperties()).thenReturn(
                ImmutableMap.of(CLIENT_PROPERTIES_KEY, clientProperties));
        return endpointConfigMock;
    }
}