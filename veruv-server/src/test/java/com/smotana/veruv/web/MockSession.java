package com.smotana.veruv.web;

import com.google.common.collect.Queues;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang.NotImplementedException;

import javax.websocket.*;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.net.URI;
import java.nio.ByteBuffer;
import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;

@EqualsAndHashCode
public class MockSession implements Session {

    public final BlockingQueue<String> msgQueue = Queues.newLinkedBlockingQueue();

    private final String id = UUID.randomUUID().toString();

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void close() throws IOException {
    }

    @Override
    public void close(CloseReason closeReason) throws IOException {
    }

    @Override
    public RemoteEndpoint.Basic getBasicRemote() {
        return new RemoteEndpoint.Basic() {

            @Override
            public void sendText(String text) throws IOException {
                msgQueue.add(text);
            }

            // <editor-fold desc="Not implemented" defaultstate="collapsed">
            @Override
            public void setBatchingAllowed(boolean allowed) throws IOException {
                throw new NotImplementedException();
            }

            @Override
            public boolean getBatchingAllowed() {
                throw new NotImplementedException();
            }

            @Override
            public void flushBatch() throws IOException {
                throw new NotImplementedException();
            }

            @Override
            public void sendPing(ByteBuffer applicationData) throws IOException, IllegalArgumentException {
                throw new NotImplementedException();
            }

            @Override
            public void sendPong(ByteBuffer applicationData) throws IOException, IllegalArgumentException {
                throw new NotImplementedException();
            }

            @Override
            public void sendBinary(ByteBuffer data) throws IOException {
                throw new NotImplementedException();
            }

            @Override
            public void sendText(String partialMessage, boolean isLast) throws IOException {
                throw new NotImplementedException();
            }

            @Override
            public void sendBinary(ByteBuffer partialByte, boolean isLast) throws IOException {
                throw new NotImplementedException();
            }

            @Override
            public OutputStream getSendStream() throws IOException {
                throw new NotImplementedException();
            }

            @Override
            public Writer getSendWriter() throws IOException {
                throw new NotImplementedException();
            }

            @Override
            public void sendObject(Object data) throws IOException, EncodeException {
                throw new NotImplementedException();
            }
            // </editor-fold>
        };
    }

    @Override
    public boolean isOpen() {
        return true;
    }

    // <editor-fold desc="Not implemented" defaultstate="collapsed">
    @Override
    public WebSocketContainer getContainer() {
        throw new NotImplementedException();
    }

    @Override
    public void addMessageHandler(MessageHandler handler) throws IllegalStateException {
        throw new NotImplementedException();
    }

    @Override
    public <T> void addMessageHandler(Class<T> clazz, MessageHandler.Whole<T> handler) {
        throw new NotImplementedException();
    }

    @Override
    public <T> void addMessageHandler(Class<T> clazz, MessageHandler.Partial<T> handler) {
        throw new NotImplementedException();
    }

    @Override
    public Set<MessageHandler> getMessageHandlers() {
        throw new NotImplementedException();
    }

    @Override
    public void removeMessageHandler(MessageHandler handler) {
        throw new NotImplementedException();

    }

    @Override
    public String getProtocolVersion() {
        throw new NotImplementedException();
    }

    @Override
    public String getNegotiatedSubprotocol() {
        throw new NotImplementedException();
    }

    @Override
    public List<Extension> getNegotiatedExtensions() {
        throw new NotImplementedException();
    }

    @Override
    public boolean isSecure() {
        throw new NotImplementedException();
    }

    @Override
    public long getMaxIdleTimeout() {
        throw new NotImplementedException();
    }

    @Override
    public void setMaxIdleTimeout(long milliseconds) {
        throw new NotImplementedException();
    }

    @Override
    public void setMaxBinaryMessageBufferSize(int length) {
        throw new NotImplementedException();
    }

    @Override
    public int getMaxBinaryMessageBufferSize() {
        throw new NotImplementedException();
    }

    @Override
    public void setMaxTextMessageBufferSize(int length) {
        throw new NotImplementedException();
    }

    @Override
    public int getMaxTextMessageBufferSize() {
        throw new NotImplementedException();
    }

    @Override
    public RemoteEndpoint.Async getAsyncRemote() {
        throw new NotImplementedException();
    }

    @Override
    public URI getRequestURI() {
        throw new NotImplementedException();
    }

    @Override
    public Map<String, List<String>> getRequestParameterMap() {
        throw new NotImplementedException();
    }

    @Override
    public String getQueryString() {
        throw new NotImplementedException();
    }

    @Override
    public Map<String, String> getPathParameters() {
        throw new NotImplementedException();
    }

    @Override
    public Map<String, Object> getUserProperties() {
        throw new NotImplementedException();
    }

    @Override
    public Principal getUserPrincipal() {
        throw new NotImplementedException();
    }

    @Override
    public Set<Session> getOpenSessions() {
        throw new NotImplementedException();
    }
    // </editor-fold>
}
