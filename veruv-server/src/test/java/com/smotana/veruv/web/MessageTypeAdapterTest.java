package com.smotana.veruv.web;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.smotana.veruv.testutil.AbstractVeruvTest;
import com.smotana.veruv.web.message.ActionKeyboardKeyPrint;
import com.smotana.veruv.web.message.ActionMouseClick;
import com.smotana.veruv.web.message.Message;
import com.smotana.veruv.web.message.Ping;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

@Slf4j
public class MessageTypeAdapterTest extends AbstractVeruvTest {

    @Inject
    private Gson gson;

    @Test
    public void test() {
        assertGson(new Ping());
        assertGson(new ActionKeyboardKeyPrint(new int[]{'h', 'i'}));
        assertGson(new ActionMouseClick(4, 5, true));
    }

    private void assertGson(Message expectedMsg) {
        Message actualMsg = gson.fromJson(gson.toJson(expectedMsg), Message.class);
        log.info("Asserting msg serde: {}", actualMsg);
        assertEquals(expectedMsg, actualMsg);
    }
}