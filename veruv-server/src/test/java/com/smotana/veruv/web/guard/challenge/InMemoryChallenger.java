package com.smotana.veruv.web.guard.challenge;

import com.google.inject.Module;
import com.google.inject.name.Names;
import com.smotana.veruv.core.AbstractVeruvModule;

public class InMemoryChallenger implements Challenger {

    @Override
    public boolean verify(String token, String remoteAddr) {
        return true;
    }

    @Override
    public Challenge getChallenge() {
        return new Challenge("challenge_type", "challenge_key");
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                bind(Challenger.class).annotatedWith(Names.named(RecaptchaChallenger.NAME)).to(InMemoryChallenger.class).asEagerSingleton();
            }
        };
    }
}
