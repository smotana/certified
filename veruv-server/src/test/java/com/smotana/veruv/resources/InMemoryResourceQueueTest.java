package com.smotana.veruv.resources;

import com.google.inject.Inject;
import com.smotana.veruv.core.Global;
import com.smotana.veruv.monitor.gcloud.ConsoleMetricsClient;
import com.smotana.veruv.monitor.gcloud.GCloudMetrics;
import com.smotana.veruv.resources.ResourceQueue.Priority;
import com.smotana.veruv.resources.ResourceQueue.QueueTicket;
import com.smotana.veruv.testutil.AbstractVeruvTest;
import com.smotana.veruv.util.TimeUtil;
import lombok.Data;
import org.junit.Test;

import java.util.Optional;
import java.util.OptionalLong;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class InMemoryResourceQueueTest extends AbstractVeruvTest {

    @Inject
    private Global.Config configGlobal;
    @Inject
    private InMemoryResourceQueue queue;

    @Override
    protected void configure() {
        super.configure();
        install(InMemoryResourceQueue.module());
        install(GCloudMetrics.module());
        install(ConsoleMetricsClient.module());
    }

    @Test(timeout = 60_000L)
    public void test() throws Exception {
        assertUserResourceRatio(0);

        DebugEventHandler h1 = new DebugEventHandler(Priority.NORMAL, "1.2.3.1");
        QueueTicket t1 = queue.addToQueue(h1);
        assertAcquired(h1);
        assertUserResourceRatio(1);

        DebugEventHandler h2 = new DebugEventHandler(Priority.NORMAL, "1.2.3.2");
        QueueTicket t2 = queue.addToQueue(h2);
        assertAcquired(h2);
        assertUserResourceRatio(2);

        DebugEventHandler h3 = new DebugEventHandler(Priority.NORMAL, "1.2.3.3");
        QueueTicket t3 = queue.addToQueue(h3);
        assertInQueue(h3, 1);
        assertUserResourceRatio(3);

        DebugEventHandler h4 = new DebugEventHandler(Priority.NORMAL, "1.2.3.4");
        QueueTicket t4 = queue.addToQueue(h4);
        assertInQueue(h3, 1);
        assertInQueue(h4, 2);
        assertUserResourceRatio(4);

        t1.release();
        assertAcquired(h3);
        assertInQueue(h4, 1);
        assertUserResourceRatio(3);

        DebugEventHandler h5 = new DebugEventHandler(Priority.NORMAL, "1.2.3.5");
        QueueTicket t5 = queue.addToQueue(h5);
        assertInQueue(h4, 1);
        assertInQueue(h5, 2);
        assertUserResourceRatio(4);

        configSet(Global.Config.class, "maxBrowserSessionsPerHost", "1");
        assertInQueue(h4, 2);
        assertInQueue(h5, 3);
        assertUserResourceRatio(4);

        DebugEventHandler h6 = new DebugEventHandler(Priority.NORMAL, "1.2.3.6");
        QueueTicket t6 = queue.addToQueue(h6);
        assertInQueue(h4, 2);
        assertInQueue(h5, 3);
        assertInQueue(h6, 4);
        assertUserResourceRatio(5);

        configSet(Global.Config.class, "maxBrowserSessionsPerHost", "3");
        assertAcquired(h4);
        assertInQueue(h5, 1);
        assertInQueue(h6, 2);
        assertUserResourceRatio(5);

        t3.release();
        assertAcquired(h4);
        assertUserResourceRatio(4);
    }

    void assertUserResourceRatio(double expectedUserCount) {
        double expectedLoad = expectedUserCount / (double) configGlobal.maxBrowserSessionsPerHost();
        assertEquals(expectedLoad, queue.calculateLoad(), 0.00001);
    }

    void assertAcquired(DebugEventHandler handler) {
        assertTrue(handler.isReadyCalled());
        assertEstimate(handler, 0L);
    }

    void assertInQueue(DebugEventHandler handler, long usersAhead) {
        assertFalse(handler.isReadyCalled());
        assertEstimate(handler, usersAhead);
    }

    void assertEstimate(DebugEventHandler handler, long usersAhead) {
        TimeUtil.SingleUnitDuration timeEstimate = queue.getEstimate(usersAhead);
        assertEquals(OptionalLong.of(timeEstimate.getDuration()), handler.getInQueueEstimateTimeOpt());
        assertEquals(Optional.of(timeEstimate.getUnit()), handler.getUnitOpt());
        assertEquals(OptionalLong.of(usersAhead), handler.getUsersInQueueOpt());
    }

    @Data
    private static class DebugEventHandler implements ResourceQueue.EventHandler {
        private final Priority priority;
        private final String remoteAddr;
        private boolean isValid = true;
        private OptionalLong inQueueEstimateTimeOpt;
        private Optional<TimeUnit> unitOpt;
        private OptionalLong usersInQueueOpt;
        private boolean readyCalled = false;

        @Override
        public void onInQueue(long inQueueEstimateTime, TimeUnit unit, long usersInQueue) {
            this.inQueueEstimateTimeOpt = OptionalLong.of(inQueueEstimateTime);
            this.unitOpt = Optional.of(unit);
            this.usersInQueueOpt = OptionalLong.of(usersInQueue);
        }

        @Override
        public void onReady() {
            readyCalled = true;
        }
    }
}