package com.smotana.veruv.payment;

import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.Singleton;
import com.kik.config.ice.ConfigSystem;
import com.kik.config.ice.annotations.DefaultValue;
import com.smotana.veruv.core.AbstractVeruvModule;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;

@Slf4j
@Singleton
public class MenuImpl implements Menu {

    public interface Config {
        @DefaultValue("usd")
        String currency();

        // TODO Should push to client side, currently you have to change it in ExtendExpiryButton.tsx if changed here
        @DefaultValue("0")
        long extendExpiryPrice();
    }

    @Inject
    private Config config;

    @Override
    public Optional<Price> getProductPrice(Product product) {
        switch (product) {
            case EXTEND_EXPIRY:
                return Optional.of(new Price(product, config.currency(), config.extendExpiryPrice()));
            default:
                return Optional.empty();
        }
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                bind(Menu.class).to(MenuImpl.class).asEagerSingleton();
                install(ConfigSystem.configModule(Config.class));
            }
        };
    }
}
