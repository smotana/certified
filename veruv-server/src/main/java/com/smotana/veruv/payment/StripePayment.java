package com.smotana.veruv.payment;

import com.google.common.collect.ImmutableMap;
import com.smotana.veruv.web.message.Payment;

import javax.ws.rs.WebApplicationException;
import java.util.Optional;

public interface StripePayment {
    /**
     * @param referenceId Reference ID to be sent to client in case of problems
     * @param payment Payment object, assumed to be sanitized already
     * @param stripeDescription description to record into Stripe's charge
     * @param stripeMetadata extra metadata to record into Stripe's charge
     * @param statementDescription Will show up on customer's credit card, truncated to 22 characters
     * @param receiptEmailOpt Optionally supply email for Stripe to send a receipt to
     * @return Capture object to finalize payment or refund if needed
     * @throws WebApplicationException ready to be sent to client
     */
    Capturer createCharge(String referenceId,
                          Payment payment,
                          String stripeDescription,
                          ImmutableMap<String, String> stripeMetadata,
                          String statementDescription,
                          Optional<String> receiptEmailOpt) throws WebApplicationException;

    interface Capturer {
        /**
         * Captures and finalizes a charge
         *
         * @throws WebApplicationException ready to be sent to client
         */
        void capture() throws WebApplicationException;

        /**
         * Refunds a charge.
         *
         * @throws WebApplicationException ready to be sent to client
         */
        void refund() throws WebApplicationException;

        /**
         * @return Id to be shown to client in case of disputes. Is added into Stripe metadata
         */
        String getReferenceId();
    }
}
