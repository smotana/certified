package com.smotana.veruv.payment;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.Singleton;
import com.kik.config.ice.ConfigSystem;
import com.kik.config.ice.annotations.DefaultValue;
import com.kik.config.ice.annotations.NoDefaultValue;
import com.smotana.veruv.core.AbstractVeruvModule;
import com.smotana.veruv.util.LogUtil;
import com.smotana.veruv.web.message.Error;
import com.smotana.veruv.web.message.Payment;
import com.stripe.Stripe;
import com.stripe.exception.*;
import com.stripe.model.Charge;
import com.stripe.model.Refund;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import rx.Observable;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.util.Optional;

@Slf4j
@Singleton
public class StripePaymentImpl implements StripePayment {

    public interface Config {
        @DefaultValue("true")
        boolean enabled();

        @NoDefaultValue
        String apiKeySecret();

        @NoDefaultValue
        Observable<String> apiKeySecretObservable();
    }

    @Inject
    private Config config;

    @Inject
    private void setup() {
        config.apiKeySecretObservable().subscribe(s -> Stripe.apiKey = s);
        Stripe.apiKey = config.apiKeySecret();
    }

    @Override
    public Capturer createCharge(String referenceId,
                                 Payment payment,
                                 String stripeDescription,
                                 ImmutableMap<String, String> stripeMetadata,
                                 String statementDescription,
                                 Optional<String> receiptEmailOpt) {
        if (!config.enabled()) {
            if (LogUtil.rateLimitAllowLog("not-enabled")) {
                log.info("Not enabled, not processing createCharge");
            }
            throw new WebApplicationException(Response.status(Response.Status.SERVICE_UNAVAILABLE)
                    .entity(new Error("payment-disabled", "Payments have been disabled, please retry later.")).build());
        }

        log.info("Charge started: token {} referenceId {}", payment.getPaymentToken(), referenceId);
        ImmutableMap.Builder<String, Object> chargeOptsBuilder = ImmutableMap.<String, Object>builder()
                .put("source", payment.getPaymentToken())
                .put("amount", payment.getAmountInCents())
                .put("currency", payment.getCurrency())
                .put("description", stripeDescription)
                .put("metadata", ImmutableMap.<String, String>builder()
                        .putAll(stripeMetadata)
                        .put("referenceId", referenceId)
                        .build())
                .put("statement_descriptor", StringUtils.left(statementDescription, 22))
                .put("capture", false);
        receiptEmailOpt.ifPresent(receiptEmail -> chargeOptsBuilder.put("receipt_email", receiptEmail));
        Charge charge = exceptionWrapper(referenceId, () -> Charge.create(chargeOptsBuilder.build()));
        log.info("Charge {} completed: token {} referenceId {}", charge.getId(), payment.getPaymentToken(), referenceId);
        return new CapturerImpl(referenceId, charge);
    }

    private void capture(String referenceId, Charge charge) {
        if (!config.enabled()) {
            if (LogUtil.rateLimitAllowLog("not-enabled")) {
                log.info("Not enabled, not processing capture");
            }
            throw new WebApplicationException(Response.status(Response.Status.SERVICE_UNAVAILABLE)
                    .entity(new Error("payment-disabled", "Payments have been disabled, please retry later.")).build());
        }

        log.info("Capture {} started: referenceId {}", charge.getId(), referenceId);
        exceptionWrapper(referenceId, () -> charge.capture(ImmutableMap.<String, Object>builder().build()));
        log.info("Capture {} completed: referenceId {}", charge.getId(), referenceId);
    }

    private void refund(String referenceId, Charge charge) {
        if (!config.enabled()) {
            if (LogUtil.rateLimitAllowLog("not-enabled")) {
                log.info("Not enabled, not processing refund");
            }
            throw new WebApplicationException(Response.status(Response.Status.SERVICE_UNAVAILABLE)
                    .entity(new Error("payment-disabled", "Payments have been disabled, please retry later.")).build());
        }

        log.info("Refund {} started: referenceId {}", charge.getId(), referenceId);
        Refund refund = exceptionWrapper(referenceId, () -> Refund.create(ImmutableMap.<String, Object>builder()
                .put("charge", charge.getId())
                .build()));
        log.info("Refund {} completed: refund {} referenceId {}", charge.getId(), refund.getId(), referenceId);
    }

    private <T> T exceptionWrapper(String referenceId, StripeCall<T> stripeCall) throws WebApplicationException {
        try {
            return stripeCall.call();
        } catch (CardException ex) {
            log.error("ReferenceId {}: Card declined code {} msg {} declCode {} charge {} param {}",
                    referenceId, ex.getCode(), ex.getMessage(), ex.getDeclineCode(), ex.getCharge(), ex.getParam(), ex);
            throw new WebApplicationException(Response.status(Response.Status.PAYMENT_REQUIRED)
                    .entity(new Error("card-declined", "Your payment was declined. Please review your information or contact your financial institute.")).build());
        } catch (RateLimitException ex) {
            if (LogUtil.rateLimitAllowLog("stripe-rate-limit")) {
                log.error("ReferenceId {}: Rate limited by Stripe", referenceId, ex);
            }
            throw new WebApplicationException(Response.status(Response.Status.TOO_MANY_REQUESTS)
                    .entity(new Error("internal-server-error", "There are too many requests at the moment, please retry later.")).build());
        } catch (InvalidRequestException ex) {
            if (LogUtil.rateLimitAllowLog("stripe-invalid-request")) {
                log.error("ReferenceId {}: Invalid request sent to Stripe", referenceId, ex);
            }
            throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new Error("internal-server-error", "Our payment processor is having issues at the moment, please retry later.")).build());
        } catch (AuthenticationException ex) {
            if (LogUtil.rateLimitAllowLog("stripe-auth-failed")) {
                log.error("ReferenceId {}: Authentication with Stripe's API failed", referenceId, ex);
            }
            throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new Error("internal-server-error", "Our payment processor is having issues at the moment, please retry later.")).build());
        } catch (ApiConnectionException ex) {
            if (LogUtil.rateLimitAllowLog("stripe-issues")) {
                log.warn("ReferenceId {}: Stripe is having issues", referenceId, ex);
            }
            throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new Error("internal-server-error", "Our payment processor is having issues at the moment, please retry again...")).build());
        } catch (StripeException ex) {
            if (LogUtil.rateLimitAllowLog("stripe-generic-stripe-exc")) {
                log.error("ReferenceId {}: Stripe is having issues", referenceId, ex);
            }
            throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new Error("internal-server-error", "Our payment processor is having issues at the moment, please retry again...")).build());
        } catch (Exception ex) {
            if (LogUtil.rateLimitAllowLog("stripe-generic-exc")) {
                log.error("ReferenceId {}: Failed processing stripe call", referenceId, ex);
            }
            throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new Error("internal-server-error", "We are having issues processing your request, please retry again...")).build());
        }
    }

    private interface StripeCall<T> {
        T call() throws StripeException;
    }

    private class CapturerImpl implements Capturer {
        private final String referenceId;
        private final Charge charge;

        private CapturerImpl(String referenceId, Charge charge) {
            this.referenceId = referenceId;
            this.charge = charge;
        }

        @Override
        public void capture() throws WebApplicationException {
            StripePaymentImpl.this.capture(referenceId, charge);
        }

        @Override
        public void refund() {
            StripePaymentImpl.this.refund(referenceId, charge);
        }

        @Override
        public String getReferenceId() {
            return referenceId;
        }
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                bind(StripePayment.class).to(StripePaymentImpl.class).asEagerSingleton();
                install(ConfigSystem.configModule(Config.class));
            }
        };
    }
}
