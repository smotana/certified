package com.smotana.veruv.payment;

import lombok.Data;

import java.util.Optional;

public interface Menu {

    Optional<Price> getProductPrice(Product product);

    enum Product {
        EXTEND_EXPIRY
    }

    @Data
    class Price {
        private final Product product;
        private final String currency;
        private final long amountInCents;

        public boolean isFree() {
            return amountInCents == 0;
        }
    }
}
