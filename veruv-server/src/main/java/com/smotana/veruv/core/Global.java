package com.smotana.veruv.core;

import com.google.inject.Module;
import com.kik.config.ice.ConfigSystem;
import com.kik.config.ice.annotations.DefaultValue;
import rx.Observable;

import java.time.Duration;

public class Global {

    public interface Config {
        @DefaultValue("PT60S")
        Duration gracefulShutdownLimit();

        @DefaultValue("2")
        long maxBrowserSessionsPerHost();

        Observable<Long> maxBrowserSessionsPerHostObservable();

        @DefaultValue("PT1H")
        Duration anonymousSnapshotExpiry();

        @DefaultValue("PT1H")
        Duration anonymousRequestExpiry();

        @DefaultValue("false")
        boolean enableBrowserDebug();

        /**
         * First page to load. The setup extension depends on this in manifest.json
         */
        @DefaultValue("https://veruv.com/start")
        String setupUrl();
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                install(ConfigSystem.configModule(Config.class));
            }
        };
    }
}
