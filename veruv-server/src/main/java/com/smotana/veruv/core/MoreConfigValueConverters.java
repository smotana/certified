package com.smotana.veruv.core;

import com.google.inject.AbstractModule;
import com.google.inject.Module;
import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.MapBinder;
import com.google.inject.util.Types;
import com.kik.config.ice.convert.ConfigValueConverter;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;
import java.util.Optional;

import static com.smotana.veruv.web.guard.TokenManagerImpl.TOKEN_ALGO;

@Slf4j
public class MoreConfigValueConverters {

    public static Module module() {
        return new AbstractModule() {
            @Override
            protected void configure() {
                TypeLiteral<TypeLiteral<?>> typeType = new TypeLiteral<TypeLiteral<?>>() {
                };
                TypeLiteral<ConfigValueConverter<?>> converterType = new TypeLiteral<ConfigValueConverter<?>>() {
                };
                MapBinder<TypeLiteral<?>, ConfigValueConverter<?>> mapBinder = MapBinder.newMapBinder(binder(), typeType, converterType);

                { // SecretKey
                    ConfigValueConverter<SecretKey> converterInstance = str -> {
                        if (str == null) {
                            return null;
                        }
                        byte[] keyBytes = Base64.getDecoder().decode(str);
                        return new SecretKeySpec(keyBytes, 0, keyBytes.length, TOKEN_ALGO.getJcaName());
                    };
                    bindConverter(SecretKey.class, mapBinder, converterInstance);
                    bindOptionalConverter(new TypeLiteral<Optional<SecretKey>>() {
                    }, SecretKey.class, mapBinder, v -> Optional.ofNullable(converterInstance.apply(v)));
                }
                { // SignatureAlgorithm
                    ConfigValueConverter<SignatureAlgorithm> converterInstance = str -> {
                        if (str == null) {
                            return null;
                        }
                        return SignatureAlgorithm.forName(str);
                    };
                    bindConverter(SignatureAlgorithm.class, mapBinder, converterInstance);
                    bindOptionalConverter(new TypeLiteral<Optional<SignatureAlgorithm>>() {
                    }, SignatureAlgorithm.class, mapBinder, v -> Optional.ofNullable(converterInstance.apply(v)));
                }
            }

            /** From ConfigValueConverters.java */
            private <T> void bindConverter(
                    Class<T> convertToType,
                    MapBinder<TypeLiteral<?>, ConfigValueConverter<?>> mapBinder,
                    ConfigValueConverter<T> converterInstance) {
                // Bind into map binder
                mapBinder.addBinding(TypeLiteral.get(convertToType)).toInstance(converterInstance);

                // Bind for individual injection
                bind((TypeLiteral<ConfigValueConverter<T>>) TypeLiteral.get(Types.newParameterizedType(ConfigValueConverter.class, convertToType))).toInstance(converterInstance);
            }

            /** From ConfigValueConverters.java */
            private <T, I> void bindOptionalConverter(
                    TypeLiteral<T> convertToType,
                    Class<I> innerConverterType,
                    MapBinder<TypeLiteral<?>, ConfigValueConverter<?>> mapBinder,
                    ConfigValueConverter<T> converterInstance) {
                // Bind into map binder
                mapBinder.addBinding(convertToType).toInstance(converterInstance);

                // Bind for individual injection
                bind((TypeLiteral<ConfigValueConverter<T>>) TypeLiteral.get(
                        Types.newParameterizedType(ConfigValueConverter.class,
                                Types.newParameterizedType(Optional.class, innerConverterType)))).toInstance(converterInstance);
            }

            /** From ConfigValueConverters.java */
            private <T> void bindConverter(
                    TypeLiteral<T> convertToType,
                    MapBinder<TypeLiteral<?>, ConfigValueConverter<?>> mapBinder,
                    ConfigValueConverter<T> converterInstance) {
                // Bind into map binder
                mapBinder.addBinding(convertToType).toInstance(converterInstance);

                // Bind for individual injection
                bind((TypeLiteral<ConfigValueConverter<T>>) TypeLiteral.get(Types.newParameterizedType(ConfigValueConverter.class, convertToType.getType()))).toInstance(converterInstance);
            }
        };
    }
}
