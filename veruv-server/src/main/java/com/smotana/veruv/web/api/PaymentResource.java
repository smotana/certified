package com.smotana.veruv.web.api;

import com.google.common.collect.ImmutableMap;
import com.smotana.veruv.payment.Menu;
import com.smotana.veruv.payment.StripePayment;
import com.smotana.veruv.store.SnapshotStore;
import com.smotana.veruv.store.UserStore;
import com.smotana.veruv.web.guard.GuardAuth;
import com.smotana.veruv.web.guard.GuardPayment;
import com.smotana.veruv.web.guard.TokenManager;
import com.smotana.veruv.web.message.Payment;
import com.smotana.veruv.web.message.PaymentResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.text.RandomStringGenerator;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.time.Instant;
import java.util.Optional;

@Slf4j
@Singleton
@Path("payment")
public class PaymentResource extends AbstractVeruvResource {

    @Context
    private HttpServletRequest request;
    @Inject
    private GuardAuth guardAuth;
    @Inject
    private GuardPayment guardPayment;
    @Inject
    private StripePayment stripePayment;
    @Inject
    private TokenManager tokenManager;
    @Inject
    private SnapshotStore snapshotStore;
    @Inject
    private UserStore userStore;

    private static final RandomStringGenerator referenceIdGen = new RandomStringGenerator.Builder()
            .withinRange('0', 'z')
            .filteredBy(c -> (Character.isLetter(c) || Character.isDigit(c)))
            .build();

    @POST
    @Path("pay")
    public Response pay(Payment msg) throws Exception {
        Menu.Price price = guardPayment.verifyPrice(msg);

        switch (price.getProduct()) {
            case EXTEND_EXPIRY:
                String snapshotId = getMetadataValue(msg, "snapshotId");

                TokenManager.Token token = tokenManager.parse(msg.getToken());
                guardAuth.authorize(request.getRemoteAddr(), GuardAuth.Action.PAYMENT_PAY);

                SnapshotStore.Snapshot snapshot = snapshotStore.getSnapshotById(snapshotId)
                        .orElseThrow(() -> new WebApplicationException(Responses.NOT_FOUND.getResponse()));

                guardAuth.authorize(
                        request.getRemoteAddr(),
                        GuardAuth.Action.VERIFICATION_UPDATE_STORAGE,
                        Optional.of(token),
                        Optional.of(snapshotId),
                        Optional.of(snapshot));

                Optional<StripePayment.Capturer> capturerOpt = Optional.empty();
                if (!price.isFree()) {
                    Optional<String> receiptEmailOpt = Optional.empty();
                    if (token.getType() == TokenManager.ResourceType.USER) {
                        UserStore.User user = userStore.getUserById(token.getId())
                                .orElseThrow(() -> new WebApplicationException(Responses.NOT_FOUND.getResponse()));
                        receiptEmailOpt = Optional.of(user.getEmail());
                    }

                    String referenceId = referenceIdGen.generate(8);
                    capturerOpt = Optional.of(stripePayment.createCharge(
                            referenceId,
                            msg,
                            "Extend snapshot expiry " + price.getAmountInCents() + price.getCurrency(),
                            ImmutableMap.of(
                                    "userId", token.getType() == TokenManager.ResourceType.USER
                                            ? token.getId()
                                            : "anonymous",
                                    "snapshotId", snapshotId,
                                    "ip", request.getRemoteAddr()),
                            "Veruv refId:" + referenceId,
                            receiptEmailOpt));
                }
                Instant newExpiry;
                try {
                    newExpiry = snapshotStore.setStorageType(snapshot.getId(), SnapshotStore.StorageType.Regular);
                    capturerOpt.ifPresent(StripePayment.Capturer::capture);
                } catch (IOException ex) {
                    log.error("Failed to process request, refunding payment {}", msg, ex);
                    capturerOpt.ifPresent(StripePayment.Capturer::refund);
                    throw new WebApplicationException(Responses.INTERNAL_SERVER_ERROR.getResponse());
                }
                return Response.ok(new PaymentResult(
                        capturerOpt.map(StripePayment.Capturer::getReferenceId).orElse(null),
                        new PaymentResult.ExtendExpiry(
                                snapshotId,
                                newExpiry.toEpochMilli(),
                                false))).build();
            default:
                log.error("Unknown product: {}", price.getProduct());
                throw new WebApplicationException(Responses.INTERNAL_SERVER_ERROR.getResponse());
        }
    }

    private String getMetadataValue(Payment paymentMsg, String key) {
        return getMetadataValueOpt(paymentMsg, key)
                .orElseThrow(() -> {
                    log.error("Missing required field {} in payment metadata", key, paymentMsg);
                    return new WebApplicationException(Responses.BAD_REQUEST.getResponse());
                });
    }

    private Optional<String> getMetadataValueOpt(Payment paymentMsg, String key) {
        if (paymentMsg.getMetadata() == null) {
            log.error("Missing metadata in payment {}", paymentMsg);
            throw new WebApplicationException(Responses.BAD_REQUEST.getResponse());
        }
        return Optional.ofNullable(paymentMsg.getMetadata().get(key));
    }
}
