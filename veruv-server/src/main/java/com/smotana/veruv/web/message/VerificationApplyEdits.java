package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * Client updating verification visibility.
 *
 * Either one of userToken or verificationToken must be set.
 *
 * Origin: Client
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class VerificationApplyEdits extends Message implements AuthorizedMessage {
    /** token for either user owning resource or for resource itself */
    @ToString.Exclude
    @SerializedName("token")
    private final String token;
    @SerializedName("verificationId")
    private final String verificationId;
    @SerializedName("crop")
    private final Rectangle crop;
    @SerializedName("redactions")
    private final List<Rectangle> redactions;
    @ToString.Exclude
    @SerializedName("note")
    private final String note;

    @Data
    public class Point {
        @SerializedName("x")
        private final int x;
        @SerializedName("y")
        private final int y;
    }

    @Data
    public class Rectangle {
        @SerializedName("p1")
        private final Point p1;
        @SerializedName("p2")
        private final Point p2;
    }
}
