package com.smotana.veruv.web.guard;

import com.gargoylesoftware.htmlunit.javascript.host.event.KeyboardEvent;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.Keys;
import org.openqa.selenium.htmlunit.HtmlUnitKeyboardMapping;

import java.lang.reflect.Field;
import java.nio.CharBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

@Slf4j
@Singleton
public class SanitizerKeys {
    private static final Set<Integer> ALLOWED_KEY_CODES = ImmutableSet.of(
            13, // Enter
            16, // Shift
            18, // Alt
            17, // Ctrl
            91, // Command
            8, // Backspace
            46, // Delete
            9, // Tab
            112, // F1
            113, // F2
            114, // F3
            115, // F4
            116, // F5
            117, // F6
            118, // F7
            119, // F8
            120, // F9
            121, // F10
            122, // F11
            123, // F12
            27, // Esc
            38, // Arrow Up
            40, // Arrow Down
            37, // Arrow Left
            39, // Arrow Right
            33, // Page up
            34, // Page down
            36, // Home
            35 // End
    );
    private final ImmutableMap<Integer, Keys> keysMappings;

    @Inject
    public SanitizerKeys() {
        keysMappings = populateKeysMapping();
    }

    /**
     * Input is unsafe input from user which will be sent to the Selenium browser.
     *
     * Concern:
     * - Characters that may break out of the Selenium browser
     * - Ensure characters are "printable" and not special characters
     */
    public CharSequence sanitizeCharCodes(int[] charCodes) {
        return CharBuffer.wrap(IntStream.of(charCodes)
                .mapToObj(c -> String.valueOf((char) c))
                .collect(Collectors.joining()));
    }

    /**
     * Input is unsafe input from user attempting to press a special non-printable key into the Selenium browser such as
     * Shift.
     *
     * Concern:
     * - Keys that may break out of the Selenium browser
     */
    public CharSequence sanitizeKeyCode(int keyCode) {
        checkArgument(ALLOWED_KEY_CODES.contains(keyCode), "Keycode not allowed: " + keyCode);
        Keys key = keysMappings.get(keyCode);
        if (key != null) {
            return key;
        } else {
            // Fall back to charCode
            return CharBuffer.wrap(Character.toChars(keyCode));
        }
    }

    /**
     * Uses {@link org.openqa.selenium.htmlunit.HtmlUnitKeyboardMapping} to map Browser event charCodes to Keys
     */
    private ImmutableMap<Integer, Keys> populateKeysMapping() {
        HashMap<Integer, Keys> mapper = Maps.newHashMap();

        try {
            mapper.putAll(getMappingsFromHtmlUnitKeyboardMapping());
        } catch (Exception ex) {
            log.warn("org.openqa.selenium.htmlunit.HtmlUnitKeyboardMapping changed, cannot extract keyboard event charcode mappings to Keys", ex);
        }

        mapper.put(KeyboardEvent.DOM_VK_CANCEL, Keys.CANCEL);
        mapper.put(KeyboardEvent.DOM_VK_HELP, Keys.HELP);
        mapper.put(KeyboardEvent.DOM_VK_BACK_SPACE, Keys.BACK_SPACE);
        mapper.put(KeyboardEvent.DOM_VK_TAB, Keys.TAB);
        mapper.put(KeyboardEvent.DOM_VK_CLEAR, Keys.CLEAR);
        mapper.put(KeyboardEvent.DOM_VK_PAUSE, Keys.PAUSE);
        mapper.put(KeyboardEvent.DOM_VK_ESCAPE, Keys.ESCAPE);
        mapper.put(KeyboardEvent.DOM_VK_SPACE, Keys.SPACE);
        mapper.put(KeyboardEvent.DOM_VK_PAGE_UP, Keys.PAGE_UP);
        mapper.put(KeyboardEvent.DOM_VK_PAGE_DOWN, Keys.PAGE_DOWN);
        mapper.put(KeyboardEvent.DOM_VK_END, Keys.END);
        mapper.put(KeyboardEvent.DOM_VK_HOME, Keys.HOME);
        mapper.put(KeyboardEvent.DOM_VK_CLEAR, Keys.DOWN);
        mapper.put(KeyboardEvent.DOM_VK_INSERT, Keys.INSERT);
        mapper.put(KeyboardEvent.DOM_VK_DELETE, Keys.DELETE);
        mapper.put(KeyboardEvent.DOM_VK_SEMICOLON, Keys.SEMICOLON);
        mapper.put(KeyboardEvent.DOM_VK_EQUALS, Keys.EQUALS);
        mapper.put(KeyboardEvent.DOM_VK_NUMPAD0, Keys.NUMPAD0);
        mapper.put(KeyboardEvent.DOM_VK_NUMPAD1, Keys.NUMPAD1);
        mapper.put(KeyboardEvent.DOM_VK_NUMPAD2, Keys.NUMPAD2);
        mapper.put(KeyboardEvent.DOM_VK_NUMPAD3, Keys.NUMPAD3);
        mapper.put(KeyboardEvent.DOM_VK_NUMPAD4, Keys.NUMPAD4);
        mapper.put(KeyboardEvent.DOM_VK_NUMPAD5, Keys.NUMPAD5);
        mapper.put(KeyboardEvent.DOM_VK_NUMPAD6, Keys.NUMPAD6);
        mapper.put(KeyboardEvent.DOM_VK_NUMPAD7, Keys.NUMPAD7);
        mapper.put(KeyboardEvent.DOM_VK_NUMPAD8, Keys.NUMPAD8);
        mapper.put(KeyboardEvent.DOM_VK_NUMPAD9, Keys.NUMPAD9);
        mapper.put(KeyboardEvent.DOM_VK_MULTIPLY, Keys.MULTIPLY);
        mapper.put(KeyboardEvent.DOM_VK_ADD, Keys.ADD);
        mapper.put(KeyboardEvent.DOM_VK_SEPARATOR, Keys.SEPARATOR);
        mapper.put(KeyboardEvent.DOM_VK_SUBTRACT, Keys.SUBTRACT);
        mapper.put(KeyboardEvent.DOM_VK_DECIMAL, Keys.DECIMAL);
        mapper.put(KeyboardEvent.DOM_VK_DIVIDE, Keys.DIVIDE);
        mapper.put(KeyboardEvent.DOM_VK_F1, Keys.F1);
        mapper.put(KeyboardEvent.DOM_VK_F2, Keys.F2);
        mapper.put(KeyboardEvent.DOM_VK_F3, Keys.F3);
        mapper.put(KeyboardEvent.DOM_VK_F4, Keys.F4);
        mapper.put(KeyboardEvent.DOM_VK_F5, Keys.F5);
        mapper.put(KeyboardEvent.DOM_VK_F6, Keys.F6);
        mapper.put(KeyboardEvent.DOM_VK_F7, Keys.F7);
        mapper.put(KeyboardEvent.DOM_VK_F8, Keys.F8);
        mapper.put(KeyboardEvent.DOM_VK_F9, Keys.F9);
        mapper.put(KeyboardEvent.DOM_VK_F10, Keys.F10);
        mapper.put(KeyboardEvent.DOM_VK_F11, Keys.F11);
        mapper.put(KeyboardEvent.DOM_VK_F12, Keys.F12);
        mapper.put(KeyboardEvent.DOM_VK_META, Keys.META);

        // Mappings where multiple values can be used:
        mapper.put(KeyboardEvent.DOM_VK_ALT, Keys.LEFT_ALT);
        mapper.put(KeyboardEvent.DOM_VK_SHIFT, Keys.LEFT_SHIFT);
        mapper.put(KeyboardEvent.DOM_VK_CONTROL, Keys.LEFT_CONTROL);
        mapper.put(KeyboardEvent.DOM_VK_LEFT, Keys.ARROW_LEFT);
        mapper.put(KeyboardEvent.DOM_VK_UP, Keys.ARROW_UP);
        mapper.put(KeyboardEvent.DOM_VK_DOWN, Keys.ARROW_DOWN);
        mapper.put(KeyboardEvent.DOM_VK_RIGHT, Keys.ARROW_RIGHT);
        mapper.put(KeyboardEvent.DOM_VK_RETURN, Keys.RETURN);

        // Custom modifier for windows key and command key on osx
        mapper.put(KeyboardEvent.DOM_VK_WIN, Keys.COMMAND);
        mapper.put(KeyboardEvent.DOM_VK_CONTEXT_MENU, Keys.COMMAND);

        return ImmutableMap.copyOf(mapper);
    }

    @VisibleForTesting
    static ImmutableMap<Integer, Keys> getMappingsFromHtmlUnitKeyboardMapping() throws NoSuchFieldException, IllegalAccessException, ClassCastException, NullPointerException {
        HashMap<Integer, Keys> mapper = Maps.newHashMap();
        Field specialKeysMapField = HtmlUnitKeyboardMapping.class.getDeclaredField("specialKeysMap");
        specialKeysMapField.setAccessible(true);
        Object specialKeysMapObj = specialKeysMapField.get(null);
        @SuppressWarnings("unchecked")
        Map<Character, Integer> specialKeysMap = (Map<Character, Integer>) specialKeysMapObj;
        specialKeysMap.forEach((character, keyboardEvent) -> {
            Keys keyFromUnicode = checkNotNull(Keys.getKeyFromUnicode(character));
            mapper.put(keyboardEvent, keyFromUnicode);
        });
        return ImmutableMap.copyOf(mapper);
    }
}
