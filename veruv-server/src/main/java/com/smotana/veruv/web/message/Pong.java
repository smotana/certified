package com.smotana.veruv.web.message;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Response for ping
 *
 * Origin: Server, Client
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Pong extends Message {
}
