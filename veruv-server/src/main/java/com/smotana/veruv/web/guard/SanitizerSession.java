package com.smotana.veruv.web.guard;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.Singleton;
import com.kik.config.ice.ConfigSystem;
import com.kik.config.ice.annotations.DefaultValue;
import com.smotana.veruv.core.AbstractVeruvModule;
import com.smotana.veruv.web.message.BrowserRequest;
import com.smotana.veruv.web.message.BrowserRequest.Cookie;
import com.smotana.veruv.web.message.BrowserRequest.LocalStorage;
import lombok.extern.slf4j.Slf4j;
import okhttp3.HttpUrl;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Singleton
public class SanitizerSession {

    public interface Config {
        @DefaultValue("true")
        boolean allowCrossOrigin();
    }

    @Inject
    private Config config;

    /** If changed, also change in extractLocalStorage.js and open.js in veruv-browser-extension */
    public static final long MAX_LOCAL_STORAGE_SIZE = 1024 * 1024;
    public static final long MAX_LOCAL_STORAGE_ITEM_SIZE = 1024;
    public static final long MAX_COOKIE_COUNT = 256;
    public static final long MAX_COOKIE_ITEM_SIZE = 4096;

    public Optional<List<Cookie>> sanitizeCookies(BrowserRequest msgBrowserRequest) {
        if (msgBrowserRequest.getCookies() == null
                || msgBrowserRequest.getCookies().isEmpty()) {
            return Optional.empty();
        }

        Optional<String> starUrlTopPrivateDomainOpt = Optional.empty();
        if (!config.allowCrossOrigin()) {
            HttpUrl startUrlParsed = HttpUrl.parse(msgBrowserRequest.getStartUrl());
            if (startUrlParsed == null) {
                log.warn("Discarded all cookies because of invalid start url: {}", msgBrowserRequest.getStartUrl());
                return Optional.empty();
            }
            starUrlTopPrivateDomainOpt = Optional.ofNullable(startUrlParsed.topPrivateDomain());
            if (!starUrlTopPrivateDomainOpt.isPresent()) {
                log.warn("Discarded all cookies because of invalid top private domain: {}", msgBrowserRequest.getStartUrl());
                return Optional.empty();
            }
        }

        boolean discardedMaxCount = false;
        long discardedMaxItemSize = 0;
        long counter = 0;
        ImmutableList.Builder<Cookie> cookieBuilder = ImmutableList.builder();
        for (Cookie cookie : msgBrowserRequest.getCookies()) {
            long cookieSize = Strings.nullToEmpty(cookie.getDomain()).length()
                    + Strings.nullToEmpty(cookie.getName()).length()
                    + Strings.nullToEmpty(cookie.getValue()).length()
                    + Strings.nullToEmpty(cookie.getPath()).length();
            if (!config.allowCrossOrigin() && starUrlTopPrivateDomainOpt.isPresent()) {
                String cookieDomain = cookie.getDomain();
                if (cookieDomain.startsWith(".")) {
                    cookieDomain = cookieDomain.substring(1);
                }
                if (cookieDomain.endsWith(".")) {
                    cookieDomain = cookieDomain.substring(0, cookieDomain.length() - 1);
                }
                if (!cookieDomain.contains(starUrlTopPrivateDomainOpt.get())) {
                    log.info("Discarded cookie due to different domain than startUrl {} cookie domain {}",
                            msgBrowserRequest.getStartUrl(), cookieDomain);
                    continue;
                }
            }

            if (cookieSize > MAX_COOKIE_ITEM_SIZE) {
                discardedMaxItemSize++;
                continue;
            }
            counter++;
            if (counter > MAX_COOKIE_COUNT) {
                discardedMaxCount = true;
                break;
            }
            cookieBuilder.add(cookie);
        }
        if (discardedMaxItemSize > 0 || discardedMaxCount) {
            log.warn("Discarded local storage items {} for max size {} keys for individual max size",
                    discardedMaxCount, discardedMaxItemSize);
        }
        ImmutableList<Cookie> cookiesSanitized = cookieBuilder.build();
        return cookiesSanitized.isEmpty() ? Optional.empty() : Optional.of(cookiesSanitized);
    }

    public Optional<List<LocalStorage>> sanitizeLocalStorage(BrowserRequest msgBrowserRequest) {
        if (msgBrowserRequest.getLocalStorage() == null
                || msgBrowserRequest.getLocalStorage().isEmpty()) {
            return Optional.empty();
        }

        Optional<HttpUrl> startUrlParsedOpt = Optional.ofNullable(HttpUrl.parse(msgBrowserRequest.getStartUrl()));
        if (!config.allowCrossOrigin() && !startUrlParsedOpt.isPresent()) {
            log.warn("Discarded all local storage because of invalid start url: {}", msgBrowserRequest.getStartUrl());
            return Optional.empty();
        }

        boolean discardedMaxSize = false;
        long discardedMaxItemSize = 0;
        long sizeCounter = 0;
        ImmutableList.Builder<LocalStorage> localStorageBuilder = ImmutableList.builder();
        for (LocalStorage localStorage : msgBrowserRequest.getLocalStorage()) {
            if (!config.allowCrossOrigin() && startUrlParsedOpt.isPresent()) {
                HttpUrl urlParsed = HttpUrl.parse(localStorage.getUrl());
                if (urlParsed == null) {
                    log.warn("Discarded local storage with invalid url: {}", localStorage.getUrl());
                    continue;
                }
                if (!urlParsed.scheme().equals(startUrlParsedOpt.get().scheme())
                        || !urlParsed.host().equals(startUrlParsedOpt.get().host())
                        || urlParsed.port() != startUrlParsedOpt.get().port()) {
                    log.info("Discarded local storage due to same origin policy, startUrl {} localStorageUrl {}",
                            msgBrowserRequest.getStartUrl(), localStorage.getUrl());
                    continue;
                }
            }

            ImmutableMap.Builder<String, String> localContentBuilder = ImmutableMap.builder();
            for (Map.Entry<String, String> entry : localStorage.getLocalContent().entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                int itemSize = key.length() + value.length();
                if (itemSize > MAX_LOCAL_STORAGE_ITEM_SIZE) {
                    discardedMaxItemSize++;
                    continue;
                }
                sizeCounter += itemSize;
                if (sizeCounter > MAX_LOCAL_STORAGE_SIZE) {
                    discardedMaxSize = true;
                    break;
                }
                localContentBuilder.put(key, value);
            }
            ImmutableMap.Builder<String, String> sessionContentBuilder = ImmutableMap.builder();
            for (Map.Entry<String, String> entry : localStorage.getSessionContent().entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                int itemSize = key.length() + value.length();
                if (itemSize > MAX_LOCAL_STORAGE_ITEM_SIZE) {
                    discardedMaxItemSize++;
                    continue;
                }
                sizeCounter += itemSize;
                if (sizeCounter > MAX_LOCAL_STORAGE_SIZE) {
                    discardedMaxSize = true;
                    break;
                }
                sessionContentBuilder.put(key, value);
            }
            localStorageBuilder.add(new LocalStorage(
                    localStorage.getUrl(),
                    localContentBuilder.build(),
                    sessionContentBuilder.build()));
        }
        if (discardedMaxItemSize > 0 || discardedMaxSize) {
            log.warn("Discarded local storage items {} for max size {} keys for individual max size",
                    discardedMaxSize, discardedMaxItemSize);
        }
        ImmutableList<LocalStorage> localStorageSanitized = localStorageBuilder.build();
        return localStorageSanitized.isEmpty() ? Optional.empty() : Optional.of(localStorageSanitized);
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                bind(SanitizerSession.class).asEagerSingleton();
                install(ConfigSystem.configModule(Config.class));
            }
        };
    }
}
