package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;
import java.util.Map;

/**
 * Request a browser resource.
 *
 * Origin: Client
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class BrowserRequest extends Message {
    @SerializedName("startUrl")
    private final String startUrl;
    @SerializedName("screenWidth")
    private final int screenWidth;
    @SerializedName("screenHeight")
    private final int screenHeight;
    @SerializedName("captchaType")
    private final String captchaType;
    @SerializedName("captchaResult")
    private final String captchaResult;

    // Following is optional and used when opening specific page with existing session
    @SerializedName("userAgent")
    private final String userAgent;
    @SerializedName("cookies")
    private final List<Cookie> cookies;
    @SerializedName("localStorage")
    private final List<LocalStorage> localStorage;

    @Data
    @ToString(callSuper = true)
    @EqualsAndHashCode(callSuper = true)
    public static class Cookie extends Message {
        @SerializedName("i")
        private final boolean hostOnly;
        @SerializedName("d")
        private final String domain;
        @SerializedName("n")
        private final String name;
        @SerializedName("v")
        private final String value;
        @SerializedName("p")
        private final String path;
        @SerializedName("s")
        private final boolean isSession;
        @SerializedName("e")
        private final long expiry;
        @SerializedName("c")
        private final boolean secure;
        @SerializedName("h")
        private final boolean httpOnly;
    }

    @Data
    @ToString(callSuper = true)
    @EqualsAndHashCode(callSuper = true)
    public static class LocalStorage extends Message {
        @SerializedName("url")
        private final String url;
        @SerializedName("localContent")
        private final Map<String, String> localContent;
        @SerializedName("sessionContent")
        private final Map<String, String> sessionContent;
    }
}
