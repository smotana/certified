package com.smotana.veruv.web.guard.challenge;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.SerializedName;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import com.kik.config.ice.ConfigSystem;
import com.kik.config.ice.annotations.DefaultValue;
import com.kik.config.ice.annotations.NoDefaultValue;
import com.smotana.veruv.core.AbstractVeruvModule;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import rx.Observable;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Slf4j
@Singleton
public class RecaptchaChallenger implements Challenger {
    public static final String NAME = "RECAPTCHA_V2";

    public interface Config {
        @DefaultValue("true")
        boolean enabled();

        @DefaultValue("true")
        boolean allowOnUnavailable();

        @DefaultValue("https://www.recaptcha.net/recaptcha/api/siteverify")
        String recaptchaUrl();

        @NoDefaultValue
        String siteKey();

        Observable<String> siteKeyObservable();

        @NoDefaultValue
        String secretKey();
    }

    @Data
    private class RecaptchaResponse {
        @SerializedName("success")
        private final boolean success;
        /** timestamp of the challenge load (ISO format yyyy-MM-dd'T'HH:mm:ssZZ) */
        @SerializedName("challenge_ts")
        private final String challengeTs;
        /** the hostname of the site where the reCAPTCHA was solved */
        @SerializedName("hostname")
        private final String hostname;
        /** Optional error codes */
        @SerializedName("error-codes")
        private final ImmutableList<String> errorCodes;

    }

    @Inject
    private Config config;
    @Inject
    private Gson gson;

    private Challenge challenge;

    @Inject
    protected void setup() {
        config.siteKeyObservable().subscribe(newSiteKey -> challenge = new Challenge(NAME, newSiteKey));
        challenge = new Challenge(NAME, config.siteKey());
    }

    @Override
    public boolean verify(String token, String remoteAddr) {
        if (!config.enabled()) {
            return true;
        }

        Response response = ClientBuilder.newClient()
                .target(config.recaptchaUrl())
                .queryParam("secret", config.secretKey())
                .queryParam("response", token)
                .queryParam("remoteip", remoteAddr)
                .request(MediaType.APPLICATION_JSON)
                .accept("application/ld+json")
                .get();

        if (response.getStatus() != 200) {
            log.error("Recaptcha verification service returned error: {}", response);
            return config.allowOnUnavailable();
        }

        String responseStr = response.readEntity(String.class);
        RecaptchaResponse recaptchaResponse;
        try {
            recaptchaResponse = gson.fromJson(responseStr, RecaptchaResponse.class);
        } catch (JsonSyntaxException ex) {
            log.error("Recaptcha failed to parse response: {}", responseStr);
            return config.allowOnUnavailable();
        }

        if (recaptchaResponse.getErrorCodes() != null
                && (recaptchaResponse.getErrorCodes().contains("missing-input-secret")
                || recaptchaResponse.getErrorCodes().contains("invalid-input-secret")
                || recaptchaResponse.getErrorCodes().contains("bad-request"))) {
            log.error("Failed to verify recaptcha with token {} remoteAddress {} response {}",
                    token, remoteAddr, recaptchaResponse);
            return config.allowOnUnavailable();
        }

        log.trace("Ip {} recaptcha result {}", remoteAddr, recaptchaResponse);

        return recaptchaResponse.isSuccess();
    }

    @Override
    public Challenge getChallenge() {
        return challenge;
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                install(ConfigSystem.configModule(Config.class));
                bind(Challenger.class).annotatedWith(Names.named(NAME)).to(RecaptchaChallenger.class).asEagerSingleton();
            }
        };
    }
}
