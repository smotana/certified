package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Client is asking to get snapshot request.
 *
 * Origin: Client
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class SnapshotRequestGet extends Message {
    @SerializedName("snapshotRequestId")
    private final String snapshotRequestId;
    /** optional password if required */
    @ToString.Exclude
    @SerializedName("password")
    private final String password;
}
