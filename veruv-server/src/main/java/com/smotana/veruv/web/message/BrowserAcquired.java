package com.smotana.veruv.web.message;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Browser is ready. Client is allowed to start sending commands.
 *
 * Origin: Server
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class BrowserAcquired extends Message {
}
