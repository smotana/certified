package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import com.smotana.veruv.store.SnapshotStore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Client updating verification visibility.
 *
 * Origin: Client
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class VerificationUpdateVisibility extends Message implements AuthorizedMessage {
    /** token for either user owning resource or for resource itself */
    @ToString.Exclude
    @SerializedName("token")
    private final String token;
    @SerializedName("verificationId")
    private final String verificationId;
    @SerializedName("visibility")
    private final SnapshotStore.Visibility visibility;
    /** Use only if visibility is set to PASSWORD */
    @ToString.Exclude
    @SerializedName("pass")
    private final String pass;
}
