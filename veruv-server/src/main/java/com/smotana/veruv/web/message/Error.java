package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Error occured.
 *
 * Origin: Server
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Error extends Message {
    @SerializedName("code")
    private final String code;
    /** Optional */
    @SerializedName("userFacingMessage")
    private final String userFacingMessage;
}
