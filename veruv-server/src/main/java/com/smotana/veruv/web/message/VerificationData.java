package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import com.smotana.veruv.store.SnapshotStore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Base64;

/**
 * Server returning verification data.
 *
 * Origin: Server
 */
@Data
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class VerificationData extends Message {
    @SerializedName("id")
    private final String id;
    @SerializedName("ts")
    private final long ts;
    @SerializedName("expiry")
    private final long expiry;
    @SerializedName("url")
    private final String url;
    @ToString.Exclude
    @SerializedName("screenshot")
    private final String screenshot;
    @ToString.Exclude
    @SerializedName("note")
    private final String note;
    @SerializedName("visibility")
    private final SnapshotStore.Visibility visibility;
    @SerializedName("isMine")
    private final boolean isMine;
    @SerializedName("canExtendExpiry")
    private final boolean canExtendExpiry;

    public VerificationData(SnapshotStore.Snapshot snapshot, boolean isMine) {
        this(snapshot.getId(),
                snapshot.getCreated().toEpochMilli(),
                snapshot.getExpiry().toEpochMilli(),
                snapshot.getUrl(),
                Base64.getEncoder().encodeToString(snapshot.getScreenshot()),
                snapshot.getNote(),
                snapshot.getVisibility(),
                isMine,
                true);
    }
}
