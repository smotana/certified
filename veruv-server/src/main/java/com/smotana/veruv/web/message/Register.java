package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Register request.
 *
 * Origin: Client
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Register extends Message {
    @SerializedName("email")
    private final String email;
    /**
     * Client already salted and hashed password hence the serialized name.
     * However server salts and hashes again hence the variable name
     */
    @ToString.Exclude
    @SerializedName("passwordSaltedHashed")
    private final String passUnhashed;
}
