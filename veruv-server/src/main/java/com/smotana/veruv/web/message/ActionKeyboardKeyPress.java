package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Client is asking to press a key on they keyboard.
 * NOTE: this is a keyCode not a charCode
 *
 * Origin: Client
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ActionKeyboardKeyPress extends Message {
    @ToString.Exclude
    @SerializedName("k")
    private final int keyCode;
    @ToString.Exclude
    @SerializedName("t")
    private final KeyPressType pressKeyType;

    public enum KeyPressType {
        UP,
        DOWN,
        PRESS
    }
}
