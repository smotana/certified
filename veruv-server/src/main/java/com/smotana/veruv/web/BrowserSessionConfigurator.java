package com.smotana.veruv.web;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.smotana.veruv.core.VeruvInjector;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpSession;
import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;
import java.util.List;

import static com.google.common.base.Preconditions.checkState;
import static com.smotana.veruv.web.ClientProperties.CLIENT_PROPERTIES_KEY;
import static com.smotana.veruv.web.VeruvRequestListener.REMOTE_ADDR_ATTR;

@Slf4j
public class BrowserSessionConfigurator extends ServerEndpointConfig.Configurator {

    @Inject
    private Injector injector;

    public BrowserSessionConfigurator() {
        VeruvInjector.INSTANCE.get().injectMembers(this);
    }

    @Override
    public <T> T getEndpointInstance(Class<T> clazz) {
        return injector.getInstance(clazz);
    }

    @Override
    public void modifyHandshake(ServerEndpointConfig config, HandshakeRequest request, HandshakeResponse response) {
        HttpSession httpSession = (HttpSession) request.getHttpSession();
        Object remoteAddrObj = httpSession.getAttribute(REMOTE_ADDR_ATTR);
        checkState(remoteAddrObj != null, "Failed to retrieve remote address, is null");
        checkState(remoteAddrObj instanceof String, "Remote address is not a string");
        String remoteAddr = (String) remoteAddrObj;
        checkState(!Strings.isNullOrEmpty(remoteAddr), "Failed to retrieve remote address, empty or null string");

        /* For reference, here are captured headers coming through the GCloud load balancer:
         * host=veruv.com
         * pragma=no-cache
         * cache-control=no-cache
         * upgrade-insecure-requests=1
         * user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36
         * dnt=1
         * accept=text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,* / *;q=0.8
         * accept-encoding=gzip, deflate, br
         * accept-language=en-CA,en;q=0.9,en-US;q=0.8,sk;q=0.7,cs;q=0.6
         * cookie=JSESSIONID=1D39E735900B4C4F7FD64132F19823CB
         * x-cloud-trace-context=5151fcfc6e41fd38a0981783d4ecfc68/2629417572006233544
         * via=1.1 google
         * connection=Keep-Alive
         * x-forwarded-proto=https
         */

        String userAgent = "";
        List<String> userAgentList = request.getHeaders().get("user-agent");
        if (userAgentList != null && userAgentList.size() > 0) {
            userAgent = userAgentList.get(0);
        }

        String xForwardedFor = String.join(", ", request.getHeaders().getOrDefault("x-forwarded-for", ImmutableList.of()));

        String forwarded = String.join(", ", request.getHeaders().getOrDefault("forwarded", ImmutableList.of()));

        config.getUserProperties().put(CLIENT_PROPERTIES_KEY, new ClientProperties(remoteAddr, userAgent, xForwardedFor, forwarded));
    }
}
