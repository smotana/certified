package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * Server responding to SnapshotRequestGetAll
 *
 * Origin: Server
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class SnapshotRequestDataAll extends Message {
    @SerializedName("requests")
    private final List<SnapshotRequestData> requests;
}
