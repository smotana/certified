package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Client is asking to get all snapshot requests owned.
 *
 * Origin: Client
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class SnapshotRequestCreate extends Message implements AuthorizedMessage {
    /** Optionally include user to own the request */
    @ToString.Exclude
    @SerializedName("token")
    private final String token;
    @SerializedName("url")
    private final String url;
    @ToString.Exclude
    @SerializedName("passwordSaltedHashedOpt")
    private final String passUnhashedOpt;
    @SerializedName("noteOpt")
    private final String noteOpt;
}
