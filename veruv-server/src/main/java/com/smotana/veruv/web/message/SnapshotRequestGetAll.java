package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Client is asking to get all snapshot requests owned.
 *
 * Origin: Client
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class SnapshotRequestGetAll extends Message implements AuthorizedMessage {
    @ToString.Exclude
    @SerializedName("token")
    private final String token;
}
