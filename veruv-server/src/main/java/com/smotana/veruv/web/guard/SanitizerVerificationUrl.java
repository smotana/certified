package com.smotana.veruv.web.guard;

import com.google.common.base.Strings;
import com.google.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import okhttp3.HttpUrl;

import static com.google.common.base.Preconditions.checkArgument;

@Slf4j
@Singleton
public class SanitizerVerificationUrl {

    /**
     * Sanitizes real URL taken from Selenium browser and outputs a URL which will be visible to the other party.
     *
     * Concerns:
     * - Cannot expose private information from the url such as username, password, query or fragment
     * - Must prevent user attempting to fake url such as punycode unicode characters
     */
    public String sanitizeUrl(String url) {
        if ("about:blank".equals(url)) {
            return url;
        }
        checkArgument(!Strings.isNullOrEmpty(url), "Cannot guard null or empty url");
        HttpUrl urlParsed = HttpUrl.parse(url);
        checkArgument(urlParsed != null, "Cannot guard invalid url: " + url);

        StringBuilder safeUrlBuilder = new StringBuilder();
        safeUrlBuilder.append(urlParsed.scheme())
                .append("://")
                .append(urlParsed.host());
        if ((urlParsed.scheme() == "https" && urlParsed.port() != 443) || (urlParsed.scheme() == "http" && urlParsed.port() != 80)) {
            safeUrlBuilder.append(":").append(urlParsed.port());
        }
        safeUrlBuilder.append(urlParsed.encodedPath());

        String urlSanitized = safeUrlBuilder.toString();
        log.trace("Sanitized url '{}' to '{}'", url, urlSanitized);
        return urlSanitized;
    }
}
