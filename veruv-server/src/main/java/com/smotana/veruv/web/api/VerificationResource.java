package com.smotana.veruv.web.api;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.smotana.veruv.image.ImageEditor;
import com.smotana.veruv.store.SnapshotStore;
import com.smotana.veruv.store.SnapshotStore.Snapshot;
import com.smotana.veruv.web.guard.GuardAuth;
import com.smotana.veruv.web.guard.GuardAuth.Action;
import com.smotana.veruv.web.guard.SanitizerImageEdits;
import com.smotana.veruv.web.guard.SanitizerNote;
import com.smotana.veruv.web.guard.TokenManager;
import com.smotana.veruv.web.guard.TokenManager.Token;
import com.smotana.veruv.web.message.*;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Optional;

// TODO input sanitation
@Slf4j
@Singleton
@Path("verification")
public class VerificationResource extends AbstractVeruvResource {

    @Context
    private HttpServletRequest request;
    @Inject
    private Gson gson;
    @Inject
    private SnapshotStore snapshotStore;
    @Inject
    private SanitizerImageEdits sanitizerImageEdits;
    @Inject
    private SanitizerNote sanitizerNote;
    @Inject
    private GuardAuth guardAuth;
    @Inject
    private TokenManager tokenManager;
    @Inject
    private ImageEditor imageEditor;

    @POST
    @Path("get/")
    public Response get(VerificationGet msg) throws IOException {
        log.trace("Verification get {} from {}", msg, request.getRemoteAddr());
        if (msg == null) {
            return Responses.BAD_REQUEST.getResponse();
        }

        Snapshot snapshot = snapshotStore.getSnapshotById(msg.getVerificationId())
                .orElseThrow(() -> new WebApplicationException(Responses.NOT_FOUND.getResponse()));

        Optional<Token> tokenOpt = Optional.ofNullable(Strings.emptyToNull(msg.getToken())).map(tokenManager::parse);
        if (tokenOpt.isPresent()) {
            try {
                guardAuth.authorize(
                        request.getRemoteAddr(),
                        Action.VERIFICATION_GET_AS_OWNER,
                        tokenOpt,
                        Optional.of(msg.getVerificationId()),
                        Optional.of(snapshot));
                return Response.ok(gson.toJson(new VerificationData(snapshot, true))).build();
            } catch (WebApplicationException ex) {
                if (ex.getResponse().getStatus() != 403) {
                    throw ex;
                }
            }
        }

        Optional<String> passOpt = Optional.ofNullable(msg.getPassOpt());
        if (passOpt.isPresent()) {
            guardAuth.authorize(
                    request.getRemoteAddr(),
                    Action.VERIFICATION_GET_WITH_PASSWORD,
                    tokenOpt,
                    Optional.of(msg.getVerificationId()),
                    Optional.of(snapshot),
                    passOpt);
        } else {
            guardAuth.authorize(
                    request.getRemoteAddr(),
                    Action.VERIFICATION_GET,
                    tokenOpt,
                    Optional.of(msg.getVerificationId()),
                    Optional.of(snapshot));
        }
        return Response.ok(gson.toJson(new VerificationData(snapshot, false))).build();
    }

    @POST
    @Path("delete/")
    public Response delete(VerificationDelete msg) throws IOException {
        log.trace("Verification delete {} from {}", msg, request.getRemoteAddr());

        if (msg == null || Strings.isNullOrEmpty(msg.getVerificationId())) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        Token token = tokenManager.parse(msg.getToken());

        Snapshot snapshot = snapshotStore.getSnapshotById(msg.getVerificationId())
                .orElseThrow(() -> new WebApplicationException(Responses.NOT_FOUND.getResponse()));

        guardAuth.authorize(
                request.getRemoteAddr(),
                Action.VERIFICATION_DELETE,
                Optional.of(token),
                Optional.of(msg.getVerificationId()),
                Optional.of(snapshot));

        try {
            snapshotStore.deleteSnapshot(msg.getVerificationId());
        } catch (IOException ex) {
            log.warn("Failed to delete verification with id {}", msg.getVerificationId(), ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        return Response.ok().build();
    }

    @POST
    @Path("setVisibility/")
    public Response setVisibility(VerificationUpdateVisibility msg) {
        log.trace("Verification setVisibility {} from {}", msg, request.getRemoteAddr());

        if (msg == null || msg.getVisibility() == null || Strings.isNullOrEmpty(msg.getVerificationId())
                || (msg.getVisibility() == SnapshotStore.Visibility.PASSWORD ^ !Strings.isNullOrEmpty(msg.getPass()))) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        guardAuth.authorize(
                request.getRemoteAddr(),
                Action.VERIFICATION_UPDATE_VISIBILITY,
                Optional.of(tokenManager.parse(msg.getToken())),
                Optional.of(msg.getVerificationId()));

        try {
            snapshotStore.setSnapshotVisibility(msg.getVerificationId(), msg.getVisibility(), Optional.ofNullable(Strings.emptyToNull(msg.getPass())));
        } catch (IOException ex) {
            log.warn("Failed to set visibility {} to image {}", msg.getVerificationId(), msg.getVerificationId(), ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        return Response.ok().build();
    }

    @POST
    @Path("applyEdits/")
    public Response applyEdits(VerificationApplyEdits msg) throws IOException {
        log.trace("Verification applyEdits {} from {}", msg, request.getRemoteAddr());

        if (msg == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        guardAuth.authorize(
                request.getRemoteAddr(),
                Action.VERIFICATION_APPLY_EDITS,
                Optional.of(tokenManager.parse(msg.getToken())),
                Optional.of(msg.getVerificationId()));

        Snapshot snapshot = snapshotStore.getSnapshotById(msg.getVerificationId())
                .orElseThrow(() -> new WebApplicationException(Responses.NOT_FOUND.getResponse()));

        Optional<ImageEditor.Edits> edits = sanitizerImageEdits.sanitizeImageEdits(msg, snapshot.getScreenshotWidth(), snapshot.getScreenshotHeight());
        Optional<String> noteOpt = sanitizerNote.sanitizeNote(msg.getNote());
        if (!edits.isPresent() && !noteOpt.isPresent()) {
            throw new WebApplicationException(Responses.BAD_REQUEST.getResponse());
        }

        if (noteOpt.isPresent()) {
            snapshotStore.editNote(
                    snapshot.getId(),
                    noteOpt.get());
            snapshot = new Snapshot(
                    snapshot.getStorageType(),
                    snapshot.getStorageId(),
                    snapshot.getId(),
                    snapshot.getCreated(),
                    snapshot.getExpiry(),
                    snapshot.getUrl(),
                    snapshot.getScreenshot(),
                    snapshot.getScreenshotWidth(),
                    snapshot.getScreenshotHeight(),
                    noteOpt.get(),
                    snapshot.getVisibility(),
                    snapshot.getPassHashedOpt(),
                    snapshot.getUserIdOpt());
        }

        if (edits.isPresent()) {
            BufferedImage newImage = imageEditor.edit(imageEditor.toImage(snapshot.getScreenshot()), edits.get());
            byte[] screenshot = imageEditor.toBytes(newImage);
            snapshotStore.editScreenshot(
                    snapshot,
                    snapshot.getStorageType(),
                    screenshot,
                    newImage.getWidth(),
                    newImage.getHeight());
            snapshot = new Snapshot(
                    snapshot.getStorageType(),
                    snapshot.getStorageId(),
                    snapshot.getId(),
                    snapshot.getCreated(),
                    snapshot.getExpiry(),
                    snapshot.getUrl(),
                    screenshot,
                    newImage.getWidth(),
                    newImage.getHeight(),
                    snapshot.getNote(),
                    snapshot.getVisibility(),
                    snapshot.getPassHashedOpt(),
                    snapshot.getUserIdOpt());
        }

        // TODO optimization: don't send back screenshot if only note was edited
        VerificationData msgVerificationData = new VerificationData(snapshot, true);
        return Response.ok(gson.toJson(msgVerificationData)).build();
    }

    @POST
    @Path("getMySnapshots/")
    public Response getMySnapshots(VerificationGetMine msg) {
        log.trace("Verification getMySnapshots {} from {}", msg, request.getRemoteAddr());

        if (msg == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        ImmutableList<Snapshot> snapshots = ImmutableList.of();
        ImmutableList.Builder<String> removeTokensBuilder = ImmutableList.builder();
        if (!Strings.isNullOrEmpty(msg.getToken())) {
            // User is logged in, authorize
            Token token = tokenManager.parse(msg.getToken());
            guardAuth.authorize(
                    request.getRemoteAddr(),
                    Action.VERIFICATION_GET_MINE_AUTH_USER,
                    Optional.of(token),
                    Optional.of(token.getId()));

            // Take ownership of all anonymous ids
            if (msg.getSnapshotTokens() != null && !msg.getSnapshotTokens().isEmpty()) {
                for (String snapshotTokenStr : msg.getSnapshotTokens()) {
                    Token snapshotToken;
                    try {
                        snapshotToken = tokenManager.parse(snapshotTokenStr);
                        guardAuth.authorize(
                                request.getRemoteAddr(),
                                Action.VERIFICATION_GET_MINE_CHANGE_OWNERSHIP,
                                Optional.of(snapshotToken),
                                Optional.of(snapshotToken.getId()));
                    } catch (WebApplicationException ex) {
                        removeTokensBuilder.add(snapshotTokenStr);
                        continue;
                    }
                    try {
                        if (!snapshotStore.getSnapshotById(snapshotToken.getId()).isPresent()) {
                            removeTokensBuilder.add(snapshotTokenStr);
                            continue;
                        }
                        snapshotStore.setSnapshotOwner(snapshotToken.getId(), Optional.of(token.getId()));
                        removeTokensBuilder.add(snapshotTokenStr);
                    } catch (IOException ex) {
                        log.warn("Failed to change ownership of snapshot {} to user {}", snapshotToken.getId(), token.getId(), ex);
                        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
                    }
                }
            }

            // Fetch all snapshots owned by user
            try {
                snapshots = snapshotStore.getSnapshotsByUser(token.getId());
            } catch (IOException ex) {
                log.warn("Failed to get snapshots by user {} ", token.getId(), ex);
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        } else {
            // We have an anonymous user, fetch snapshots by tokens
            if (msg.getSnapshotTokens() != null && !msg.getSnapshotTokens().isEmpty()) {
                ImmutableList.Builder<Snapshot> snapshotBuilder = ImmutableList.builder();
                for (String snapshotTokenStr : msg.getSnapshotTokens()) {
                    Token snapshotToken;
                    try {
                        snapshotToken = tokenManager.parse(snapshotTokenStr);
                        guardAuth.authorize(
                                request.getRemoteAddr(),
                                Action.VERIFICATION_GET_MINE_ANONYMOUS,
                                Optional.of(snapshotToken),
                                Optional.of(snapshotToken.getId()));
                    } catch (WebApplicationException ex) {
                        removeTokensBuilder.add(snapshotTokenStr);
                        continue;
                    }
                    try {
                        snapshotStore.getSnapshotById(snapshotToken.getId())
                                .ifPresent(snapshotBuilder::add);
                    } catch (IOException ex) {
                        log.warn("Failed to fetch snapshot {} for anonymous user {}", snapshotToken.getId(), request.getRemoteAddr(), ex);
                        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
                    }
                }
                snapshots = snapshotBuilder.build();
            }
        }

        log.trace("Verification getMySnapshots found {} for {}", snapshots, request.getRemoteAddr());
        return Response.ok(gson.toJson(new VerificationGetMineResponse(
                snapshots.stream()
                        .map(s -> new VerificationData(s, true))
                        .collect(ImmutableList.toImmutableList()),
                removeTokensBuilder.build()))).build();
    }
}
