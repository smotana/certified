package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Request a browser resource.
 *
 * Origin: Client
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class PaymentResult extends Message {
    /** Null for free products */
    @SerializedName("referenceId")
    private final String referenceId;

    /** Optional results */
    @SerializedName("EXTEND_EXPIRY")
    private final ExtendExpiry extendExpiry;

    @Data
    @ToString(callSuper = true)
    @EqualsAndHashCode(callSuper = true)
    public static class ExtendExpiry extends Message {
        @SerializedName("snapshotId")
        private final String snapshotId;
        @SerializedName("expiry")
        private final long expiry;
        @SerializedName("canExtendExpiry")
        private final boolean canExtendExpiry;
    }
}
