package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * Server responding to retrieving own snapshots.
 *
 * Origin: Server
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class VerificationGetMineResponse extends Message {
    @SerializedName("snapshots")
    private final List<VerificationData> snapshots;
    @SerializedName("removeTokens")
    private final List<String> removeTokens;
}
