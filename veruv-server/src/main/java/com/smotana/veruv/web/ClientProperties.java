package com.smotana.veruv.web;

import lombok.Data;

import java.io.Serializable;

@Data
public class ClientProperties implements Serializable {
    public static final String CLIENT_PROPERTIES_KEY = "_clientProperties_";
    private final String remoteAddress;
    private final String userAgent;
    private final String xForwardedFor;
    private final String forwarded;
}
