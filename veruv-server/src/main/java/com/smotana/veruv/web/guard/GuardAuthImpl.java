package com.smotana.veruv.web.guard;

import com.google.common.base.Strings;
import com.google.common.base.Suppliers;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableMap;
import com.google.common.util.concurrent.RateLimiter;
import com.google.common.util.concurrent.RateLimiters;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.kik.config.ice.ConfigSystem;
import com.kik.config.ice.annotations.DefaultValue;
import com.smotana.veruv.core.AbstractVeruvModule;
import com.smotana.veruv.store.SnapshotStore;
import com.smotana.veruv.store.SnapshotStore.Snapshot;
import com.smotana.veruv.store.UserStore;
import com.smotana.veruv.store.UserStore.User;
import com.smotana.veruv.util.PasswordUtil;
import com.smotana.veruv.util.TimingAttackUtil;
import com.smotana.veruv.web.api.Responses;
import com.smotana.veruv.web.guard.TokenManager.ResourceType;
import com.smotana.veruv.web.guard.TokenManager.Token;
import com.smotana.veruv.web.message.AccountInfo;
import com.smotana.veruv.web.message.Login;
import com.smotana.veruv.web.message.Register;
import lombok.extern.slf4j.Slf4j;
import rx.Observable;

import javax.inject.Singleton;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.lang.reflect.Type;
import java.time.Duration;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static com.smotana.veruv.util.PasswordUtil.Type.SNAPSHOT;
import static com.smotana.veruv.util.PasswordUtil.Type.USER;

@Slf4j
@Singleton
public class GuardAuthImpl implements GuardAuth {

    public interface Config {
        @DefaultValue("true")
        boolean rateLimiterEnabled();

        /** By default, a single permit corresponds to one minute of waiting */
        @DefaultValue("720")
        double rateLimiterAttempts();

        Observable<Double> rateLimiterAttemptsObservable();

        @DefaultValue("PT12H")
        Duration rateLimiterPeriod();

        Observable<Duration> rateLimiterPeriodObservable();

        @DefaultValue("PT12H")
        Duration rateLimiterBurst();

        Observable<Duration> rateLimiterBurstObservable();

        @DefaultValue("{" +
                "REGISTER: 1," +
                "LOGIN: 1," +
                "LOGIN_FAILED: 15," +
                "TOKEN_CHECK: 1," +
                "UPDATE_EMAIL: 6," +
                "UPDATE_PASS: 6," +
                "VERIFICATION_GET: 1," +
                "VERIFICATION_GET_WITH_PASSWORD: 15," +
                "VERIFICATION_GET_AS_OWNER: 1," +
                "VERIFICATION_UPDATE_STORAGE: 15," +
                "VERIFICATION_CREATE: 1," +
                "VERIFICATION_DELETE: 1," +
                "VERIFICATION_APPLY_EDITS: 1," +
                "VERIFICATION_UPDATE_VISIBILITY: 1," +
                "VERIFICATION_GET_MINE_AUTH_USER: 1," +
                "VERIFICATION_GET_MINE_CHANGE_OWNERSHIP: 1," +
                "VERIFICATION_GET_MINE_ANONYMOUS: 1," +
                "PAYMENT_PAY: 15," +
                "SNAPSHOT_REQUEST_CREATE: 1," +
                "SNAPSHOT_REQUEST_GET: 1," +
                "SNAPSHOT_REQUEST_GET_ALL: 1" +
                "}")
        String permitsForActions();

        Observable<String> permitsForActionsObservable();
    }

    @Inject
    private Config config;
    @Inject
    private Gson gson;
    @Inject
    private UserStore userStore;
    @Inject
    private SnapshotStore snapshotStore;
    @Inject
    private TimingAttackUtil timingAttackUtil;
    @Inject
    private TokenManager tokenManager;

    private volatile LoadingCache<String, RateLimiter> rateLimiters;
    private volatile ImmutableMap<Action, Integer> permitsForAction;

    @Inject
    protected void setup() {
        config.permitsForActionsObservable().subscribe(this::buildPermitsForAction);
        buildPermitsForAction(config.permitsForActions());

        Stream.of(
                config.rateLimiterAttemptsObservable(),
                config.rateLimiterPeriodObservable(),
                config.rateLimiterBurstObservable())
                .forEach(o -> o.subscribe(v -> createCache()));
        createCache();
    }

    @Override
    public AccountInfo authenticateRegister(String remoteAddress, Register registerMsg) {
        rateLimit(remoteAddress, Action.REGISTER);

        if (registerMsg == null
                || Strings.isNullOrEmpty(registerMsg.getEmail())
                || Strings.isNullOrEmpty(registerMsg.getPassUnhashed())) {
            log.warn("Client sent bad register request {} {}", registerMsg.getEmail(), remoteAddress);
            throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST).build());
        }

        String emailNormalized = normalizeEmail(registerMsg.getEmail());
        String id;
        try {
            id = userStore.createUser(emailNormalized, registerMsg.getPassUnhashed());
        } catch (UserStore.EmailTakenException ex) {
            throw new WebApplicationException(ex, Responses.EMAIL_TAKEN.getResponse());
        } catch (IOException ex) {
            throw new WebApplicationException(ex, Responses.INTERNAL_SERVER_ERROR.getResponse());
        }

        String token = tokenManager.create(ResourceType.USER, id);

        return new AccountInfo(token, emailNormalized);
    }

    @Override
    public AccountInfo authenticateLogin(String remoteAddress, Login loginMsg) {
        rateLimit(remoteAddress, Action.LOGIN);

        if (loginMsg == null
                || Strings.isNullOrEmpty(loginMsg.getEmail())
                || Strings.isNullOrEmpty(loginMsg.getPassUnhashed())) {
            log.warn("Client sent bad login request {} {}", loginMsg.getEmail(), remoteAddress);
            throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST).build());
        }

        TimingAttackUtil.CriticalSection criticalSection = timingAttackUtil.start();
        try {
            Optional<User> userOpt;
            try {
                userOpt = userStore.getUserByEmail(normalizeEmail(loginMsg.getEmail()));
            } catch (IOException ex) {
                log.warn("Failed loging in user, fetching by email {}", loginMsg.getEmail(), ex);
                throw new WebApplicationException(ex, Responses.INTERNAL_SERVER_ERROR.getResponse());
            }
            if (!userOpt.isPresent()) {
                log.info("login failed invalid email {} from {}", loginMsg.getEmail(), remoteAddress);
                rateLimit(remoteAddress, Action.LOGIN_FAILED);
                throw new WebApplicationException(Responses.AUTH_FAILED_RESPONSE.getResponse());
            }

            String clientPass = PasswordUtil.saltHashPassword(USER, loginMsg.getPassUnhashed(), userOpt.get().getId());
            if (!userOpt.get().getPassHash().equals(clientPass)) {
                log.info("login failed invalid pass for email {} from {}", loginMsg.getEmail(), remoteAddress);
                rateLimit(remoteAddress, Action.LOGIN_FAILED);
                throw new WebApplicationException(Responses.AUTH_FAILED_RESPONSE.getResponse());
            }

            String token = tokenManager.create(ResourceType.USER, userOpt.get().getId());

            return new AccountInfo(token, userOpt.get().getEmail());
        } catch (Exception ex) {
            criticalSection.end();
            throw ex;
        }
    }

    @Override
    public void authorize(String remoteAddress,
                          Action action) {
        authorize(remoteAddress, action, Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
    }

    @Override
    public void authorize(String remoteAddress,
                          Action action,
                          Optional<Token> tokenOpt,
                          Optional<String> resourceIdOpt) {
        authorize(remoteAddress, action, tokenOpt, resourceIdOpt, Optional.empty(), Optional.empty());
    }

    @Override
    public void authorize(String remoteAddress,
                          Action action,
                          Optional<Token> tokenOpt,
                          Optional<String> resourceIdOpt,
                          Optional<Snapshot> snapshotOpt) {
        authorize(remoteAddress, action, tokenOpt, resourceIdOpt, Optional.empty(), Optional.empty());
    }

    @Override
    public void authorize(String remoteAddress,
                          Action action,
                          Optional<Token> tokenOpt,
                          Optional<String> resourceIdOpt,
                          Optional<Snapshot> snapshotOpt,
                          Optional<String> passwordOpt) {
        rateLimit(remoteAddress, action);

        Supplier<Token> token = Suppliers.memoize(() -> tokenOpt.orElseThrow(() -> new WebApplicationException(Responses.UNAUTHORIZED.getResponse())));
        Supplier<String> resourceId = Suppliers.memoize(() -> resourceIdOpt.orElseThrow(() -> new WebApplicationException(Responses.UNAUTHORIZED.getResponse())));
        Supplier<Snapshot> snapshot = Suppliers.memoize(() -> snapshotOpt.orElseGet(() -> {
            try {
                return snapshotStore.getSnapshotById(resourceId.get()).orElseThrow(() -> new WebApplicationException(Responses.UNAUTHORIZED.getResponse()));
            } catch (IOException ex) {
                throw new WebApplicationException(ex, Responses.INTERNAL_SERVER_ERROR.getResponse());
            }
        }));

        TimingAttackUtil.CriticalSection criticalSection = timingAttackUtil.start();
        try {
            switch (action) {
                case REGISTER:
                case LOGIN:
                    throw new WebApplicationException(Responses.BAD_REQUEST.getResponse());
                case TOKEN_CHECK:
                case UPDATE_EMAIL:
                case UPDATE_PASS:
                case VERIFICATION_GET_MINE_AUTH_USER:
                    tokenManager.authorize(token.get(), ResourceType.USER, resourceId.get());
                    break;
                case VERIFICATION_GET:
                case VERIFICATION_GET_WITH_PASSWORD:
                    if (snapshot.get().getVisibility() == SnapshotStore.Visibility.PUBLIC) {
                        break;
                    } else if (snapshot.get().getVisibility() == SnapshotStore.Visibility.PASSWORD) {
                        if (passwordOpt.isPresent()) {
                            String suppliedPassHashed = PasswordUtil.saltHashPassword(SNAPSHOT, passwordOpt.get(), snapshot.get().getId());
                            if (snapshot.get().getPassHashedOpt().isPresent() && snapshot.get().getPassHashedOpt().get().equals(suppliedPassHashed)) {
                                break;
                            }
                        }
                        throw new WebApplicationException(Responses.UNAUTHORIZED_PASSWORD_REQUIRED.getResponse());
                    }
                    throw new WebApplicationException(Responses.NOT_FOUND.getResponse());
                case VERIFICATION_GET_AS_OWNER:
                case VERIFICATION_UPDATE_STORAGE:
                case VERIFICATION_DELETE:
                case VERIFICATION_APPLY_EDITS:
                case VERIFICATION_UPDATE_VISIBILITY:
                    switch (token.get().getType()) {
                        case USER:
                            String userId = snapshot.get().getUserIdOpt().orElseThrow(() -> {
                                log.info("Failed to verify action {}, provided user token but resource {} has anonymous user", action, resourceId.get());
                                return new WebApplicationException(Responses.UNAUTHORIZED.getResponse());
                            });
                            tokenManager.authorize(token.get(), ResourceType.USER, userId);
                            break;
                        case SNAPSHOT:
                            tokenManager.authorize(token.get(), ResourceType.SNAPSHOT, resourceId.get());
                            break;
                        default:
                            log.trace("Failed to verify action {}, token has invalid type {}", action, token.get().getType());
                            throw new WebApplicationException(Responses.UNAUTHORIZED.getResponse());
                    }
                    break;
                case VERIFICATION_GET_MINE_CHANGE_OWNERSHIP:
                case VERIFICATION_GET_MINE_ANONYMOUS:
                    tokenManager.authorize(token.get(), ResourceType.SNAPSHOT, resourceId.get());
                    break;
                case VERIFICATION_CREATE:
                    // Snapshot creation can be done anonymously or attached to a user
                    // Authorize user if supplied here
                    if (tokenOpt.isPresent()) {
                        tokenManager.authorize(token.get(), ResourceType.USER, resourceId.get());
                    }
                    break;
                case PAYMENT_PAY:
                    // Nothing to do, we just need to rate limit
                    break;
                default:
                    throw new WebApplicationException(Responses.UNAUTHORIZED.getResponse());
            }
        } catch (Exception ex) {
            criticalSection.end();
            throw ex;
        }
    }

    private void rateLimit(String remoteAddress, Action action) {
        if (!config.rateLimiterEnabled()) {
            return;
        }
        final int requiredPermits;
        requiredPermits = permitsForAction.getOrDefault(action, 0);
        if (requiredPermits > 0) {
            if (!rateLimiters.getUnchecked(remoteAddress).tryAcquire(requiredPermits)) {
                log.info("Denying action {} for ip {}", action, remoteAddress);
                throw new WebApplicationException(Responses.RATE_LIMITED.getResponse());
            }
        }
    }

    private void buildPermitsForAction(String permitsForActionStr) {
        if (Strings.isNullOrEmpty(permitsForActionStr)) {
            log.info("No permits set");
            permitsForAction = ImmutableMap.of();
            return;
        }
        Type type = new TypeToken<Map<Action, Integer>>() {
        }.getType();
        Map<Action, Integer> permitsForActionNew;
        try {
            permitsForActionNew = gson.fromJson(permitsForActionStr, type);
        } catch (JsonSyntaxException ex) {
            throw new RuntimeException("Failed to parse config value permitsForAction", ex);
        }
        log.info("Setting permits {}", permitsForActionNew);
        permitsForAction = ImmutableMap.copyOf(permitsForActionNew);
    }

    private void createCache() {
        final long expireAfter = config.rateLimiterPeriod().toMillis();
        final double permitsPerSecond = config.rateLimiterAttempts() / (config.rateLimiterPeriod().toMillis() / 1000d);
        final double maxBurstSeconds = config.rateLimiterBurst().toMillis() / 1000d;
        log.info("Created new RL: {}qps {}sec burst, expire after {}ms", permitsPerSecond, maxBurstSeconds, expireAfter);
        rateLimiters = CacheBuilder.newBuilder()
                .expireAfterAccess(expireAfter, TimeUnit.MILLISECONDS)
                .build(new CacheLoader<>() {
                    @Override
                    public RateLimiter load(String key) throws Exception {
                        return RateLimiters.createFull(permitsPerSecond, maxBurstSeconds);
                    }
                });
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                install(ConfigSystem.configModule(Config.class));
                bind(GuardAuth.class).to(GuardAuthImpl.class).asEagerSingleton();
            }
        };
    }
}
