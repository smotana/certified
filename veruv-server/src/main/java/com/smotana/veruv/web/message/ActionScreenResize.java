package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Navigate browser forward, back, or refresh.
 *
 * Origin: Client
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ActionScreenResize extends Message {
    @SerializedName("screenWidth")
    private final int screenWidth;
    @SerializedName("screenHeight")
    private final int screenHeight;
}
