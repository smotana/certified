package com.smotana.veruv.web;

import com.smotana.veruv.resources.Browser;
import com.smotana.veruv.resources.BrowserResourceManager;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.websocket.CloseReason;
import javax.websocket.Session;
import java.io.IOException;
import java.util.Optional;

@Data
@Slf4j
public class UserSession {
    private final ClientProperties props;
    private Optional<Session> websocketOpt = Optional.empty();
    private Optional<BrowserResourceManager.Resource> resourceOpt = Optional.empty();

    public String getId() {
        return getWebsocketOpt().map(Session::getId).orElse("NONE");
    }

    public Session getWebsocket() {
        return getWebsocketOpt().get();
    }

    public BrowserResourceManager.Resource getResource() {
        return getResourceOpt().get();
    }

    public Optional<Browser> getBrowserOpt() {
        return getResourceOpt().map(BrowserResourceManager.Resource::getBrowserOpt).orElse(Optional.empty());
    }

    public Browser getBrowser() {
        return getBrowserOpt().get();
    }

    public boolean isReady() {
        return getBrowserOpt().map(Browser::isConnected).orElse(Boolean.FALSE);
    }

    public void close() {
        try {
            getWebsocket().close();
        } catch (IOException ex) {
            log.debug("Failed to close socket", ex);
        }
    }

    public void close(CloseReason closeReason) {
        try {
            getWebsocket().close(closeReason);
        } catch (IOException ex) {
            log.debug("Failed to close socket", ex);
        }
    }

    public void closeCommunicationError() {
        close(new CloseReason(CloseReason.CloseCodes.PROTOCOL_ERROR, "Communication error"));
    }
}
