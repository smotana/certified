package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.concurrent.TimeUnit;

/**
 * Update as to the estimated time in queue, may be sent multiple times. usersInQueue of zero means we have the
 * resource, but the resource is starting up.
 *
 * Origin: Server
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class BrowserInQueue extends Message {
    @SerializedName("timeInQueueEstimate")
    private final long timeInQueueEstimate;
    @SerializedName("timeInQueueEstimateUnit")
    private final TimeUnit timeInQueueEstimateUnit;
    @SerializedName("usersInQueue")
    private final long usersInQueue;
}
