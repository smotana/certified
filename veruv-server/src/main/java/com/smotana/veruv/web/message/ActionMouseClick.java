package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Client is asking to (double) click in a particular location on the screen.
 *
 * Origin: Client
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ActionMouseClick extends Message {
    @ToString.Exclude
    @SerializedName("x")
    private final long x;
    @ToString.Exclude
    @SerializedName("y")
    private final long y;
    @ToString.Exclude
    @SerializedName("doubleClick")
    private final boolean doubleClick;
}
