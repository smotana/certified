package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * Client retrieving snapshots they own.
 * Optionally merging orphaned snapshots created anonymously.
 *
 * Origin: Client
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class VerificationGetMine extends Message implements AuthorizedMessage {
    @ToString.Exclude
    @SerializedName("token")
    private final String token;
    @ToString.Exclude
    @SerializedName("snapshotTokens")
    private final List<String> snapshotTokens;
}
