package com.smotana.veruv.web.message;

import lombok.Data;


@Data
public class Message {

    protected Message() {
    }

    public String getType() {
        return this.getClass().getSimpleName();
    }
}