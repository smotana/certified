package com.smotana.veruv.web.guard;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.Singleton;
import com.kik.config.ice.ConfigSystem;
import com.kik.config.ice.annotations.DefaultValue;
import com.kik.config.ice.annotations.NoDefaultValue;
import com.smotana.veruv.core.AbstractVeruvModule;
import com.smotana.veruv.core.Global;
import com.smotana.veruv.web.api.Responses;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.SignatureException;
import lombok.extern.slf4j.Slf4j;

import javax.crypto.SecretKey;
import javax.ws.rs.WebApplicationException;
import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import static io.jsonwebtoken.SignatureAlgorithm.HS512;

@Slf4j
@Singleton
public class TokenManagerImpl implements TokenManager {
    public static final SignatureAlgorithm TOKEN_ALGO = HS512;

    public interface Config {
        @NoDefaultValue
        SecretKey tokenSignerPrivKey();

        @DefaultValue("PT720H")
        Duration userTokenValidity();
    }

    @Inject
    private Config config;
    @Inject
    private Global.Config globalConfig;

    @Override
    public String create(Token token) {
        return create(token.getType(), token.getId());
    }

    @Override
    public String create(ResourceType type, String id) {
        return Jwts.builder()
                .setIssuedAt(new Date())
                .addClaims(ImmutableMap.of(
                        "type", type.name(),
                        "id", id
                ))
                .signWith(config.tokenSignerPrivKey(), TOKEN_ALGO)
                .compact();
    }

    @Override
    public Optional<ResourceType> parseTypeUnsafe(String tokenStr) {
        AtomicReference<Claims> claimsRef = new AtomicReference();
        try {
            Jwts.parser().setSigningKeyResolver(new SigningKeyResolver() {
                @Override
                public Key resolveSigningKey(JwsHeader header, Claims claims) {
                    claimsRef.set(claims);
                    return null;
                }

                @Override
                public Key resolveSigningKey(JwsHeader header, String plaintext) {
                    throw new IllegalStateException("Unexpected plaintext jwt");
                }
            }).parseClaimsJws(tokenStr);
        } catch (Exception ex) {
            // Expected
        }
        try {
            return Optional.ofNullable(claimsRef.get())
                    .map(this::tokenFromClaims)
                    .map(Token::getType);
        } catch (Exception ex) {
            return Optional.empty();
        }
    }


    @Override
    public Token parse(String tokenStr) throws WebApplicationException {
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(config.tokenSignerPrivKey())
                    .parseClaimsJws(tokenStr)
                    .getBody();
            if (claims.getIssuedAt() == null) {
                log.trace("Attempted to use token without issuedAt {}", tokenStr);
                throw new WebApplicationException(Responses.AUTH_TOKEN_EXPIRED.getResponse());
            }
            Token token = tokenFromClaims(claims);
            Duration validityDuration;
            switch (token.getType()) {
                case USER:
                    validityDuration = config.userTokenValidity();
                    break;
                case SNAPSHOT:
                    validityDuration = globalConfig.anonymousSnapshotExpiry();
                    break;
                case SNAPSHOT_REQUEST:
                    validityDuration = globalConfig.anonymousRequestExpiry();
                    break;
                default:
                    throw new WebApplicationException(Responses.INTERNAL_SERVER_ERROR.getResponse());
            }
            if (claims.getIssuedAt().toInstant().plus(validityDuration).isBefore(Instant.now())) {
                log.trace("Attempted to use expired token issued at {}, token {}", claims.getIssuedAt(), tokenStr);
                throw new WebApplicationException(Responses.AUTH_TOKEN_EXPIRED.getResponse());
            }
            return token;
        } catch (ExpiredJwtException ex) {
            // Exception is only thrown when using JWT expiration, but we are using issuedAt so this should never be called
            log.trace("Attempted to use expired token {}", tokenStr);
            throw new WebApplicationException(Responses.AUTH_TOKEN_EXPIRED.getResponse());
        } catch (UnsupportedJwtException | MalformedJwtException | SignatureException | IllegalArgumentException ex) {
            log.warn("Failure to verify token '{}'", tokenStr, ex);
            throw new WebApplicationException(Responses.UNAUTHORIZED.getResponse());
        }
    }


    @Override
    public Token authorize(Token token, ResourceType expectedType, String expectedId) throws WebApplicationException {
        if (!expectedType.equals(token.getType())) {
            log.info("Token resource type {} different from expected {}", token.getType(), expectedType);
            throw new WebApplicationException(Responses.UNAUTHORIZED.getResponse());
        }
        if (!expectedId.equals(token.getId())) {
            log.info("Token id {} different from expected {} for resource type {}", token.getType(), expectedType, expectedType);
            throw new WebApplicationException(Responses.UNAUTHORIZED.getResponse());
        }

        return token;
    }

    private Token tokenFromClaims(Claims claims) {
        ResourceType type = ResourceType.valueOf((String) claims.get("type"));
        String id = (String) claims.get("id");
        return new Token(type, id);
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                install(ConfigSystem.configModule(Config.class));
                bind(TokenManager.class).to(TokenManagerImpl.class).asEagerSingleton();
            }
        };
    }
}
