package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Client is asking to print keys from keyboard. Maximum number of characters is 100.
 * NOTE: this is a charCode not a keyCode
 *
 * Origin: Client
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ActionKeyboardKeyPrint extends Message {
    @ToString.Exclude
    @SerializedName("c")
    private final int[] charCode;
}
