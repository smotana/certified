package com.smotana.veruv.web.guard;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.util.concurrent.RateLimiter;
import com.google.common.util.concurrent.RateLimiters;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.kik.config.ice.ConfigSystem;
import com.kik.config.ice.annotations.DefaultValue;
import com.smotana.veruv.core.AbstractVeruvModule;
import com.smotana.veruv.web.guard.challenge.Challenger;
import com.smotana.veruv.web.guard.challenge.RecaptchaChallenger;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import rx.Observable;

import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

@Slf4j
@Singleton
public class GuardBrowserImpl implements GuardBrowser {

    public interface Config {
        @DefaultValue("true")
        boolean enabled();

        @DefaultValue("12")
        double blockingRateLimiterAttempts();

        Observable<Double> blockingRateLimiterAttemptsObservable();

        @DefaultValue("PT12H")
        Duration blockingRateLimiterPeriod();

        Observable<Duration> blockingRateLimiterPeriodObservable();

        @DefaultValue("PT12H")
        Duration blockingRateLimiterBurst();

        Observable<Duration> blockingRateLimiterBurstObservable();

        @DefaultValue("1")
        double challengeRateLimiterAttempts();

        Observable<Double> challengeRateLimiterAttemptsObservable();

        @DefaultValue("PT1M")
        Duration challengeRateLimiterPeriod();

        Observable<Duration> challengeRateLimiterPeriodObservable();

        @DefaultValue("PT2M")
        Duration challengeRateLimiterBurst();

        Observable<Duration> challengeRateLimiterBurstObservable();
    }

    @Data
    private class Limiter {
        private final RateLimiter blockingRateLimiter;
        private final RateLimiter challengeRateLimiter;
    }

    @Inject
    private Config config;
    @Inject
    @Named(RecaptchaChallenger.NAME)
    private Challenger recaptchaChallenger;

    private volatile LoadingCache<String, Limiter> rateLimiters;

    @Inject
    protected void setup() {
        Stream.of(
                config.blockingRateLimiterAttemptsObservable(),
                config.blockingRateLimiterPeriodObservable(),
                config.blockingRateLimiterBurstObservable(),
                config.challengeRateLimiterAttemptsObservable(),
                config.challengeRateLimiterPeriodObservable(),
                config.challengeRateLimiterBurstObservable())
                .forEach(o -> o.subscribe(v -> createCache()));
        createCache();
    }

    private void createCache() {
        final long expireAfter = Math.max(config.blockingRateLimiterPeriod().toMillis(), config.challengeRateLimiterPeriod().toMillis());
        final double blockingPermitsPerSecond = config.blockingRateLimiterAttempts() / (config.blockingRateLimiterPeriod().toMillis() / 1000d);
        final double blockingMaxBurstSeconds = config.blockingRateLimiterBurst().toMillis() / 1000d;
        final double challengePermitsPerSecond = config.challengeRateLimiterAttempts() / (config.challengeRateLimiterPeriod().toMillis() / 1000d);
        final double challengeMaxBurstSeconds = config.challengeRateLimiterBurst().toMillis() / 1000d;
        log.info("Created new cache: blocking RL {}qps {}sec burst, challenge RL {}qps {}sec burst, expire after {}ms",
                blockingPermitsPerSecond, blockingMaxBurstSeconds, challengePermitsPerSecond, challengeMaxBurstSeconds, expireAfter);
        rateLimiters = CacheBuilder.newBuilder()
                .expireAfterAccess(expireAfter, TimeUnit.MILLISECONDS)
                .build(new CacheLoader<>() {
                    @Override
                    public Limiter load(String key) throws Exception {
                        return new Limiter(
                                RateLimiters.createFull(blockingPermitsPerSecond, blockingMaxBurstSeconds),
                                RateLimiters.createFull(challengePermitsPerSecond, challengeMaxBurstSeconds));
                    }
                });
    }

    @Override
    public Result acquireConnection(String remoteAddr, Optional<String> challengeType, Optional<String> challengeToken) {
        if (!config.enabled()) {
            return Result.ALLOWED;
        }

        Limiter limiter = rateLimiters.getUnchecked(remoteAddr);

        // Acquire and update all rate limiters before continuing
        boolean blockingAcquired = limiter.getBlockingRateLimiter().tryAcquire();
        boolean challengeAcquired = limiter.getChallengeRateLimiter().tryAcquire();

        if (!blockingAcquired) {
            log.trace("Blocking user {}", remoteAddr);
            return Result.BLOCK;
        } else if (!challengeAcquired) {
            if (challengeType.isPresent()
                    && RecaptchaChallenger.NAME.equals(challengeType.get())
                    && challengeToken.isPresent()) {
                if (recaptchaChallenger.verify(challengeToken.get(), remoteAddr)) {
                    log.trace("Allowing user passing challenge {}", remoteAddr);
                    return Result.ALLOWED;
                } else {
                    log.trace("Challenging user failing challenge {}", remoteAddr);
                    return Result.CHALLENGE;
                }
            } else {
                log.trace("Challenging user {}", remoteAddr);
                return Result.CHALLENGE;
            }
        } else {
            log.trace("Allowing user {}", remoteAddr);
            return Result.ALLOWED;
        }
    }

    @Override
    public Challenger.Challenge getChallenge() {
        return recaptchaChallenger.getChallenge();
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                install(ConfigSystem.configModule(Config.class));
                bind(GuardBrowser.class).to(GuardBrowserImpl.class).asEagerSingleton();
            }
        };
    }
}
