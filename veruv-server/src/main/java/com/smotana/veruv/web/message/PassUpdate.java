package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Update password on account.
 *
 * Origin: Client
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class PassUpdate extends Message implements AuthorizedMessage {
    @ToString.Exclude
    @SerializedName("token")
    private final String token;
    @ToString.Exclude
    @SerializedName("newPass")
    private final String newPass;
}
