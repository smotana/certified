package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Server is sending an update to the screen.
 *
 * Origin: Server
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Frame extends Message {
    @SerializedName("x")
    private final long xPos;
    @SerializedName("y")
    private final long yPos;
    @SerializedName("w")
    private final long width;
    @SerializedName("h")
    private final long height;
    @ToString.Exclude
    @SerializedName("d")
    private final String image;
    /** Only used if screen dimensions changed */
    @SerializedName("sw")
    private final Long screenWidth;
    @SerializedName("sh")
    private final Long screenHeight;
}
