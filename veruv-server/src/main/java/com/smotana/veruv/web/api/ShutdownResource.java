package com.smotana.veruv.web.api;

import com.google.common.net.InetAddresses;
import com.google.inject.Module;
import com.kik.config.ice.ConfigSystem;
import com.kik.config.ice.annotations.NoDefaultValue;
import com.smotana.veruv.core.AbstractVeruvModule;
import com.smotana.veruv.resources.GracefulShutdownHook;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;

@Slf4j
@Singleton
@Path("/")
public class ShutdownResource {

    public interface Config {
        @NoDefaultValue
        String shutdownSecret();
    }

    @Context
    private HttpServletRequest request;
    @Inject
    private Config config;
    @Inject
    private GracefulShutdownHook gracefulShutdownHook;

    /** Prevent Method not allowed */
    @GET
    @Path("shutdown")
    public String shutdown() {
        throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND).build());
    }

    @POST
    @Path("shutdown")
    public String shutdown(String secretActual) {
        if (request.getMethod().equals("POST")
                && InetAddresses.forString(request.getRemoteAddr()).isLoopbackAddress()
                && config.shutdownSecret().equals(secretActual)) {
            log.info("Shutting down gracefully");
            gracefulShutdownHook.gracefulShutdown();
            log.info("Graceful shut down finished, shutting down all");
            try {
                Socket socket = new Socket("127.0.0.1", 8015);
                PrintStream os = new PrintStream(socket.getOutputStream());
                os.println("shutdown");
                socket.close();
                System.out.println("Shutting down server ...");
            } catch (IOException e) {
                e.printStackTrace();
            }

            return "OK";
        }
        log.warn("Shut down attempted from {} with data {}", request.getRemoteAddr(), secretActual);
        throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND).build());
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                install(ConfigSystem.configModule(Config.class));
            }
        };
    }
}
