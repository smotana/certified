package com.smotana.veruv.web.guard;

import lombok.Data;

import javax.ws.rs.WebApplicationException;
import java.util.Optional;

public interface TokenManager {

    enum ResourceType {
        USER,
        SNAPSHOT,
        SNAPSHOT_REQUEST
    }

    String create(Token token);

    String create(ResourceType type, String id);

    /** Extracts resource type without checking signature */
    Optional<ResourceType> parseTypeUnsafe(String tokenStr);

    Token parse(String tokenStr) throws WebApplicationException;

    Token authorize(Token token, ResourceType expectedType, String expectedId) throws WebApplicationException;

    @Data
    class Token {
        private final ResourceType type;
        private final String id;
    }
}
