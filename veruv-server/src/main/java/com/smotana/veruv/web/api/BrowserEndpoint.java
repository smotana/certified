package com.smotana.veruv.web.api;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.kik.config.ice.ConfigSystem;
import com.kik.config.ice.annotations.DefaultValue;
import com.smotana.veruv.core.AbstractVeruvModule;
import com.smotana.veruv.resources.Browser;
import com.smotana.veruv.resources.BrowserResourceManager;
import com.smotana.veruv.resources.BrowserResourceManager.Resource;
import com.smotana.veruv.store.SnapshotStore.Snapshot;
import com.smotana.veruv.util.LogUtil;
import com.smotana.veruv.web.BrowserSessionConfigurator;
import com.smotana.veruv.web.ClientProperties;
import com.smotana.veruv.web.UserSession;
import com.smotana.veruv.web.guard.*;
import com.smotana.veruv.web.guard.TokenManager.Token;
import com.smotana.veruv.web.guard.challenge.Challenger;
import com.smotana.veruv.web.message.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static com.smotana.veruv.web.ClientProperties.CLIENT_PROPERTIES_KEY;
import static javax.websocket.CloseReason.CloseCodes.TOO_BIG;

@Slf4j
@ServerEndpoint(value = "/api/browser", configurator = BrowserSessionConfigurator.class)
public class BrowserEndpoint implements BrowserResourceManager.ResourceEventHandler {

    public interface Config {
        @DefaultValue("5242880")
        int maxTextMessageSize();
    }

    @Inject
    private Config config;
    @Inject
    private GuardBrowser guardBrowser;
    @Inject
    private Gson gson;
    @Inject
    private BrowserResourceManager browserResourceManager;
    @Inject
    private SanitizerNavigateUrl sanitizerNavigateUrl;
    @Inject
    private SanitizerScreenDimensions sanitizerScreenDimensions;
    @Inject
    private SanitizerSession sanitizerSession;
    @Inject
    private GuardAuth guardAuth;
    @Inject
    private TokenManager tokenManager;

    private UserSession userSession = null;

    @OnOpen
    public void onOpen(Session session, EndpointConfig endpointConfig) {
        session.setMaxTextMessageBufferSize(config.maxTextMessageSize());
        ClientProperties props = (ClientProperties) endpointConfig.getUserProperties().get(CLIENT_PROPERTIES_KEY);
        userSession = new UserSession(props);
        userSession.setWebsocketOpt(Optional.of(session));
        log.debug("Session open {}", userSession.getId());
    }

    @OnMessage
    public void onMessage(String messageString) throws IOException {
        Message message = parseMessage(messageString);

        log.trace("RECV: Client {} msg {}", userSession.getId(), message);

        handleMessage(message);
    }

    @OnClose
    public void onClose(Session session, CloseReason closeReason) {
        log.debug("Session closed {} due to {}", userSession.getId(), closeReason);
        if (closeReason.getCloseCode() == TOO_BIG) {
            if (LogUtil.rateLimitAllowLog("websocketMessageTooBig")) {
                log.warn("Message too big, consider increasing MaxTextMessageBufferSize");
            }
        }
        if (userSession != null) {
            userSession.getResourceOpt().ifPresent(resource -> browserResourceManager.releaseResource(resource));
        }
    }

    @OnError
    public void onError(Session session, Throwable th) {
        log.trace("Session error", th);
    }


    @Override
    public void onInQueue(long timeInQueueEstimate, TimeUnit unit, long othersInQueue) {
        sendMessage(new BrowserInQueue(timeInQueueEstimate, unit, othersInQueue));
    }

    @Override
    public void onBrowserReady(Browser browser) {
        sendMessage(new BrowserAcquired());
    }

    @Override
    public void onBrowserReadyStateChanged(boolean isComplete) {
        sendMessage(new BrowserReadyState(isComplete));
    }

    @Override
    public void onBrowserNavigated(String url) {
        sendMessage(new BrowserNavigated(url));
    }

    @Override
    public void onFrameRefresh(long xPos, long yPos, long width, long height, byte[] img, Optional<Long> newScreenWidth, Optional<Long> newScreenHeight) {
        String imgBase64 = Base64.getEncoder().encodeToString(img);
        sendMessage(new Frame(xPos, yPos, width, height, imgBase64, newScreenWidth.orElse(null), newScreenHeight.orElse(null)));
    }


    @Override
    public void onUserNotify(BrowserResourceManager.NotifyType type, String message) {
        log.trace("Notifying user {} with {} msg '{}'", userSession.getId(), type, message);
        sendMessage(new BrowserUserNotify(type.getType(), message));
    }

    @Override
    public void onClose(CloseReason.CloseCode closeCode, String reasonPhrase) {
        log.trace("Closing session {} with {} reason '{}'", userSession.getId(), closeCode, reasonPhrase);
        userSession.close(new CloseReason(closeCode, reasonPhrase));
    }

    private Message parseMessage(String messageString) throws IOException {
        try {
            return gson.fromJson(messageString, Message.class);
        } catch (JsonSyntaxException ex) {
            throw new IOException("Failed to parse JSON: " + StringUtils.abbreviate(messageString, 100), ex);
        }
    }

    private void handleMessage(Message msg) throws IOException {
        switch (msg.getType()) {
            case "Ping":
                log.trace("Client {} pinged", userSession.getId());
                userSession.getResourceOpt().ifPresent(Resource::updateActivity);
                sendMessage(new Pong());
                break;
            case "Pong":
                log.trace("Client {} ponged", userSession.getId());
                break;
            case "BrowserRequest":
                BrowserRequest msgBrowserRequest = (BrowserRequest) msg;

                if (userSession.getResourceOpt().isPresent()) {
                    userSession.closeCommunicationError();
                    return;
                }

                GuardBrowser.Result guardResult = guardBrowser.acquireConnection(
                        userSession.getProps().getRemoteAddress(),
                        Optional.ofNullable(Strings.emptyToNull(msgBrowserRequest.getCaptchaType())),
                        Optional.ofNullable(Strings.emptyToNull(msgBrowserRequest.getCaptchaResult())));
                switch (guardResult) {
                    case ALLOWED:
                        break;
                    case CHALLENGE:
                        Challenger.Challenge challenge = guardBrowser.getChallenge();
                        sendMessage(new RequestChallenge(challenge.getChallengeType(), challenge.getChallengeKey()));
                        return;
                    case BLOCK:
                        userSession.close(new CloseReason(CloseReason.CloseCodes.TRY_AGAIN_LATER, "You tried connecting too frequently, please try again later"));
                        return;
                    default:
                        throw new RuntimeException("Unknown result: " + guardResult);
                }
                String startUrlNormalized;
                startUrlNormalized = sanitizerNavigateUrl.sanitizeUrl(msgBrowserRequest.getStartUrl());
                SanitizerScreenDimensions.ScreenDimensions screenDimensions = sanitizerScreenDimensions.sanitizeScreenDimensions(msgBrowserRequest.getScreenWidth(), msgBrowserRequest.getScreenHeight());
                Optional<List<BrowserRequest.Cookie>> cookiesSanitized = sanitizerSession.sanitizeCookies(msgBrowserRequest);
                Optional<List<BrowserRequest.LocalStorage>> localStorageSanitized = sanitizerSession.sanitizeLocalStorage(msgBrowserRequest);
                log.trace("Client {} requesting browser with start url {} dimensions {}", userSession.getId(), startUrlNormalized, screenDimensions);
                Resource resource = browserResourceManager.requestResource(this,
                        startUrlNormalized,
                        screenDimensions,
                        userSession.getProps().getRemoteAddress(),
                        userSession.getProps().getXForwardedFor(),
                        userSession.getProps().getForwarded(),
                        Optional.ofNullable(Strings.emptyToNull(msgBrowserRequest.getUserAgent())),
                        cookiesSanitized,
                        localStorageSanitized);
                userSession.setResourceOpt(Optional.of(resource));
                break;
            default:
                if (!userSession.isReady()) {
                    log.warn("Client {} performing action {} without browser", userSession.getId(), msg.getType());
                    userSession.closeCommunicationError();
                    break;
                }
                userSession.getResourceOpt().ifPresent(Resource::updateActivity);
                handleBrowserMessage(msg);
        }
    }

    private void handleBrowserMessage(Message msg) throws IOException {
        switch (msg.getType()) {
            case "ActionNavigate":
                ActionNavigate msgActionNavigate = (ActionNavigate) msg;
                log.trace("Client {} navigating {}", userSession.getId(), msgActionNavigate.getNavigateType());
                switch (msgActionNavigate.getNavigateType()) {
                    case FORWARD:
                        userSession.getBrowser().navigateForward();
                        break;
                    case BACK:
                        userSession.getBrowser().navigateBack();
                        break;
                    case REFRESH:
                        userSession.getBrowser().refresh();
                        break;
                    default:
                        log.error("Client {} performing unknown navigation action {}", userSession.getId(), msgActionNavigate.getNavigateType());
                        userSession.closeCommunicationError();
                }
                break;
            case "ActionNavigateUrl":
                ActionNavigateUrl msgActionNavigateUrl = (ActionNavigateUrl) msg;
                String urlNormalized = sanitizerNavigateUrl.sanitizeUrl(msgActionNavigateUrl.getUrl());
                log.trace("Client {} navigating to {}", userSession.getId(), urlNormalized);
                userSession.getBrowser().navigate(urlNormalized);
                break;
            case "ActionMouseMove":
                ActionMouseMove msgActionMouseMove = (ActionMouseMove) msg;
                // TODO sanity check x y coordinates
                log.trace("Client {} mouse move to x {} y {}", userSession.getId(), msgActionMouseMove.getX(), msgActionMouseMove.getY());
                userSession.getBrowser().moveMouse(msgActionMouseMove.getX(), msgActionMouseMove.getY());
                break;
            case "ActionMouseClick":
                ActionMouseClick msgActionMouseClick = (ActionMouseClick) msg;
                // TODO sanity check x y coordinates
                log.trace("Client {} {} clicking x {} y {}", userSession.getId(), msgActionMouseClick.isDoubleClick() ? "double" : "single", msgActionMouseClick.getX(), msgActionMouseClick.getY());
                userSession.getBrowser().click(msgActionMouseClick.getX(), msgActionMouseClick.getY(), msgActionMouseClick.isDoubleClick());
                break;
            case "ActionKeyboardKeyPrint":
                ActionKeyboardKeyPrint msgActionKeyboardKeyPrint = (ActionKeyboardKeyPrint) msg;
                if (msgActionKeyboardKeyPrint.getCharCode() == null
                        || msgActionKeyboardKeyPrint.getCharCode().length < 1
                        || msgActionKeyboardKeyPrint.getCharCode().length > 100) {
                    log.warn("Client {} sent incorrect keys", userSession.getId());
                    userSession.closeCommunicationError();
                    break;
                }
                log.trace("Client {} keyboard input starting with '{}'", userSession.getId(), msgActionKeyboardKeyPrint.getCharCode()[0]);
                userSession.getBrowser().type(msgActionKeyboardKeyPrint.getCharCode());
                break;
            case "ActionKeyboardKeyPress":
                ActionKeyboardKeyPress msgActionKeyboardKeyPress = (ActionKeyboardKeyPress) msg;
                log.trace("Client {} keyboard input '{}' type {}", userSession.getId(), msgActionKeyboardKeyPress.getKeyCode(), msgActionKeyboardKeyPress.getPressKeyType());
                userSession.getBrowser().keyPress(msgActionKeyboardKeyPress.getKeyCode(), msgActionKeyboardKeyPress.getPressKeyType());
                break;
            case "ActionScreenResize":
                ActionScreenResize msgActionScreenResize = (ActionScreenResize) msg;
                log.trace("Client {} screen resize {}", userSession.getId(), msgActionScreenResize);
                SanitizerScreenDimensions.ScreenDimensions screenDimensions = sanitizerScreenDimensions.sanitizeScreenDimensions(msgActionScreenResize.getScreenWidth(), msgActionScreenResize.getScreenHeight());
                userSession.getBrowser().setScreenDimensions(screenDimensions);
                break;
            case "VerificationRequest":
                VerificationRequest msgVerificationRequest = (VerificationRequest) msg;
                log.trace("Client {} requesting verification with request {}", userSession.getId(), msgVerificationRequest.getRequestId());

                Optional<Token> userToken = Optional.ofNullable(Strings.emptyToNull(msgVerificationRequest.getToken())).map(tokenManager::parse);
                Optional<String> userIdOpt = userToken.map(Token::getId);
                guardAuth.authorize(
                        userSession.getProps().getRemoteAddress(),
                        GuardAuth.Action.VERIFICATION_CREATE,
                        userToken,
                        userIdOpt);

                Snapshot snapshot = userSession.getBrowser().getSnapshot(userIdOpt);
                String resourceToken = tokenManager.create(TokenManager.ResourceType.SNAPSHOT, snapshot.getId());
                log.trace("Client {} created verification {} verification {}", userSession.getId(), snapshot);
                sendMessage(new VerificationCreated(
                        resourceToken,
                        snapshot.getId(),
                        new VerificationData(snapshot, true)));
                break;
            default:
                log.warn("Client {} sent an unknown message: {}", userSession.getId(), msg.getType());
                userSession.closeCommunicationError();
                break;
        }
    }

    private void sendMessage(Message message) {
        synchronized (userSession.getWebsocket()) {
            if (!userSession.getWebsocket().isOpen()) {
                log.trace("Client {} Not sending message on closed session: {}", userSession.getId(), message);
                return;
            }
            log.trace("SEND: Client {} msg {}", userSession.getId(), message);
            try {
                userSession.getWebsocket().getBasicRemote().sendText(gson.toJson(message));
            } catch (IOException ex) {
                log.debug("Failed to send message {}, closing session", message.getType());
                userSession.close();
            }
        }
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                bind(BrowserEndpoint.class);
                install(ConfigSystem.configModule(Config.class));
            }
        };
    }
}
