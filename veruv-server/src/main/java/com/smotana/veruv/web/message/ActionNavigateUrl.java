package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Navigate browser to a url.
 *
 * Origin: Client
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ActionNavigateUrl extends Message {
    @ToString.Exclude
    @SerializedName("url")
    private final String url;
}
