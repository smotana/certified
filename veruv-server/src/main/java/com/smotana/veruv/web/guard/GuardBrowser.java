package com.smotana.veruv.web.guard;

import com.smotana.veruv.web.guard.challenge.Challenger;

import java.util.Optional;

public interface GuardBrowser {

    enum Result {
        ALLOWED,
        CHALLENGE,
        BLOCK
    }

    Result acquireConnection(String remoteAddr, Optional<String> challengeType, Optional<String> challengeToken);

    Challenger.Challenge getChallenge();
}
