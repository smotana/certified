package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Server responding to SnapshotRequestGet
 *
 * Origin: Server
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class SnapshotRequestData extends Message {
    /** Token specific to this snapshot */
    @ToString.Exclude
    @SerializedName("token")
    private final String token;
    @SerializedName("id")
    private final String id;
    @SerializedName("created")
    private final long created;
    @SerializedName("expiry")
    private final long expiry;
    @SerializedName("url")
    private final String url;
    @SerializedName("note")
    private final String note;
}
