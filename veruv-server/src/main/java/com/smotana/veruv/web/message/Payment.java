package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Map;

/**
 * Request a browser resource.
 *
 * Origin: Client
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Payment extends Message implements AuthorizedMessage {
    @SerializedName("product")
    private final String product;
    /** Only present when payment is not free */
    @SerializedName("paymentToken")
    private final String paymentToken;
    @SerializedName("amountInCents")
    private final long amountInCents;
    @SerializedName("currency")
    private final String currency;
    @SerializedName("token")
    private final String token;
    @SerializedName("metadata")
    private final Map<String, String> metadata;
}
