package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Client is asking to delete verification.
 *
 * Origin: Client
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class VerificationDelete extends Message implements AuthorizedMessage {
    /** token for either user owning resource or for resource itself */
    @ToString.Exclude
    @SerializedName("token")
    private final String token;
    @SerializedName("verificationId")
    private final String verificationId;
}
