package com.smotana.veruv.web.guard;

import com.google.common.base.Strings;
import com.google.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import okhttp3.HttpUrl;

@Slf4j
@Singleton
public class SanitizerNavigateUrl {
    public static final String BLANK_URL = "about:blank";

    /**
     * Sanitizes url to be navigated to in browser.
     */
    public String sanitizeUrl(String url) throws IllegalArgumentException {
        if (Strings.isNullOrEmpty(url) || BLANK_URL.equals(url)) {
            return BLANK_URL;
        }

        // Ensure url is valid format
        HttpUrl urlParsed = HttpUrl.parse(url);
        if (urlParsed == null) {
            return BLANK_URL;
        }

        return url;
    }
}
