package com.smotana.veruv.web.guard;

import com.google.inject.Inject;
import com.google.inject.Module;
import com.smotana.veruv.core.AbstractVeruvModule;
import com.smotana.veruv.payment.Menu;
import com.smotana.veruv.payment.Menu.Price;
import com.smotana.veruv.payment.Menu.Product;
import com.smotana.veruv.web.api.Responses;
import com.smotana.veruv.web.message.Error;
import com.smotana.veruv.web.message.Payment;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

@Slf4j
public class GuardPaymentImpl implements GuardPayment {

    @Inject
    private Menu menu;

    @Override
    public Price verifyPrice(Payment payment) throws WebApplicationException {
        Product product;
        try {
            product = Product.valueOf(payment.getProduct());
        } catch (IllegalArgumentException ex) {
            log.warn("Unknown product {} for payment {}", payment.getProduct(), payment);
            throw new WebApplicationException(Responses.BAD_REQUEST.getResponse());
        }

        Price productPrice = menu.getProductPrice(product).orElseThrow(() -> {
            log.warn("Unknown product {} for payment {}", payment.getProduct(), payment);
            return new WebApplicationException(Responses.BAD_REQUEST.getResponse());
        });

        if (payment.getAmountInCents() < productPrice.getAmountInCents()) {
            log.warn("Requested product of price {} has actual price of {} for payment request {}",
                    payment.getAmountInCents(), productPrice.getAmountInCents(), payment);
            throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
                    .entity(new Error("bad-request", "The price has changed, please refresh the page.")).build());
        }

        if (!payment.getCurrency().equals(productPrice.getCurrency())) {
            log.warn("Requested product of currency {} has actual currency of {} for payment request {}",
                    payment.getCurrency(), productPrice.getCurrency(), payment);
            throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
                    .entity(new Error("bad-request", "The price has changed, please refresh the page.")).build());
        }

        if (payment.getAmountInCents() > productPrice.getAmountInCents()) {
            log.warn("Requested product of price {} has actual higher price of {} for payment request {}, continuing anyway",
                    payment.getAmountInCents(), productPrice.getAmountInCents(), payment);
        }

        return productPrice;
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                bind(GuardPayment.class).to(GuardPaymentImpl.class);
            }
        };
    }
}
