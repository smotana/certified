package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Server requesting a challenge.
 *
 * Origin: Server
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class RequestChallenge extends Message {
    @SerializedName("type")
    private final String type;
    @SerializedName("key")
    private final String key;
}
