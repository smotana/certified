package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Server is sending back a verification confirmation.
 *
 * Origin: Server
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class VerificationCreated extends Message {
    /** Token specific to this snapshot */
    @ToString.Exclude
    @SerializedName("token")
    private final String token;
    @SerializedName("verificationId")
    private final String verificationId;
    @ToString.Exclude
    @SerializedName("verification")
    private final VerificationData verification;
}
