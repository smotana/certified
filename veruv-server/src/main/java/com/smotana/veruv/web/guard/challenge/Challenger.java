package com.smotana.veruv.web.guard.challenge;

import lombok.Data;

public interface Challenger {

    @Data
    class Challenge {
        private final String challengeType;
        private final String challengeKey;
    }

    boolean verify(String token, String remoteAddr);

    Challenger.Challenge getChallenge();
}
