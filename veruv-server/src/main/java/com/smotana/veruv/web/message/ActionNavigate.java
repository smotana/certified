package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Navigate browser forward, back, or refresh.
 *
 * Origin: Client
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ActionNavigate extends Message {
    @SerializedName("navigateType")
    private final NavigateType navigateType;

    public enum NavigateType {
        FORWARD,
        BACK,
        REFRESH
    }
}
