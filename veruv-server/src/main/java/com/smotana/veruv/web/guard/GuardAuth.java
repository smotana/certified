package com.smotana.veruv.web.guard;

import com.smotana.veruv.store.SnapshotStore;
import com.smotana.veruv.web.guard.TokenManager.Token;
import com.smotana.veruv.web.message.AccountInfo;
import com.smotana.veruv.web.message.Login;
import com.smotana.veruv.web.message.Register;

import javax.ws.rs.WebApplicationException;
import java.util.Optional;

public interface GuardAuth {

    enum Action {
        REGISTER,
        LOGIN,
        LOGIN_FAILED,
        TOKEN_CHECK,
        UPDATE_EMAIL,
        UPDATE_PASS,
        VERIFICATION_GET,
        VERIFICATION_GET_WITH_PASSWORD,
        VERIFICATION_GET_AS_OWNER,
        VERIFICATION_CREATE,
        VERIFICATION_DELETE,
        VERIFICATION_APPLY_EDITS,
        VERIFICATION_UPDATE_VISIBILITY,
        VERIFICATION_UPDATE_STORAGE,
        VERIFICATION_GET_MINE_AUTH_USER,
        VERIFICATION_GET_MINE_CHANGE_OWNERSHIP,
        VERIFICATION_GET_MINE_ANONYMOUS,
        PAYMENT_PAY,
        SNAPSHOT_REQUEST_CREATE,
        SNAPSHOT_REQUEST_GET,
        SNAPSHOT_REQUEST_GET_ALL
    }

    AccountInfo authenticateRegister(String remoteAddress, Register registerMsg) throws WebApplicationException;

    AccountInfo authenticateLogin(String remoteAddress, Login loginMsg) throws WebApplicationException;

    void authorize(String remoteAddress,
                   Action action) throws WebApplicationException;

    void authorize(String remoteAddress,
                   Action action,
                   Optional<Token> token,
                   Optional<String> resourceId) throws WebApplicationException;

    void authorize(String remoteAddress,
                   Action action,
                   Optional<Token> tokenOpt,
                   Optional<String> resourceIdOpt,
                   Optional<SnapshotStore.Snapshot> snapshotOpt);

    void authorize(String remoteAddress,
                   Action action,
                   Optional<Token> tokenOpt,
                   Optional<String> resourceIdOpt,
                   Optional<SnapshotStore.Snapshot> snapshotOpt,
                   Optional<String> password);

    default String normalizeEmail(String email) {
        return email.toLowerCase();
    }
}
