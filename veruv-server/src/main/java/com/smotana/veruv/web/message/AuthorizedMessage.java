package com.smotana.veruv.web.message;

public interface AuthorizedMessage {
    String getToken();
}
