package com.smotana.veruv.web.guard;

import com.smotana.veruv.payment.Menu.Price;
import com.smotana.veruv.web.message.Payment;

import javax.ws.rs.WebApplicationException;

public interface GuardPayment {

    Price verifyPrice(Payment payment) throws WebApplicationException;
}
