package com.smotana.veruv.web.guard;

import com.google.inject.Singleton;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Singleton
public class SanitizerScreenDimensions {
    /** Also change on client side in browserApi.ts */
    public static final int BROWSER_WIDTH_MAX = 1680;
    public static final int BROWSER_WIDTH_MIN = 1024;
    public static final int BROWSER_HEIGHT_MAX = 1200;
    public static final int BROWSER_HEIGHT_MIN = 768;
    public static final float BROWSER_HEIGHT_RATIO_DIFF_MAX = 2f;
    public static final int RESOLUTION_DEPTH = 24;

    /**
     * Sanitizes dimensions based on min/max of width and max ratio difference.
     */
    public ScreenDimensions sanitizeScreenDimensions(int width, int height) {
        // Limit max and min
        int newWidth = Math.max(Math.min(width, BROWSER_WIDTH_MAX), BROWSER_WIDTH_MIN);
        int newHeight = Math.max(Math.min(height, BROWSER_HEIGHT_MAX), BROWSER_HEIGHT_MIN);
        // Limit width height ratio
        newHeight = Math.max(Math.min(newHeight, (int) Math.floor(newWidth * BROWSER_HEIGHT_RATIO_DIFF_MAX)), (int) Math.ceil(newWidth / BROWSER_HEIGHT_RATIO_DIFF_MAX));
        log.debug("Sanitized screen dimensions from {}x{} to {}x{}", width, height, newWidth, newHeight);
        // Compensate height due to browser top bar
        return new ScreenDimensions(newWidth, newHeight);
    }

    @Data
    public static class ScreenDimensions {
        private final int width;
        private final int height;
    }
}
