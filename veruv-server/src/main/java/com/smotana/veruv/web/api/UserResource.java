package com.smotana.veruv.web.api;

import com.smotana.veruv.store.UserStore;
import com.smotana.veruv.store.UserStore.User;
import com.smotana.veruv.web.guard.GuardAuth;
import com.smotana.veruv.web.guard.GuardAuth.Action;
import com.smotana.veruv.web.guard.TokenManager;
import com.smotana.veruv.web.guard.TokenManager.Token;
import com.smotana.veruv.web.message.*;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Optional;

@Slf4j
@Singleton
@Path("auth")
public class UserResource extends AbstractVeruvResource {

    @Context
    private HttpServletRequest request;
    @Inject
    private GuardAuth guardAuth;
    @Inject
    private UserStore userStore;
    @Inject
    private TokenManager tokenManager;

    @POST
    @Path("register")
    public Response register(Register msg) {
        return Response.ok(guardAuth.authenticateRegister(request.getRemoteAddr(), msg)).build();
    }

    @POST
    @Path("login")
    public Response login(Login msg) {
        return Response.ok(guardAuth.authenticateLogin(request.getRemoteAddr(), msg)).build();
    }

    @POST
    @Path("token")
    public Response verifyToken(VerifyToken msg) throws IOException {
        Token token = tokenManager.parse(msg.getToken());
        guardAuth.authorize(
                request.getRemoteAddr(),
                Action.TOKEN_CHECK,
                Optional.of(token),
                Optional.of(token.getId()));

        User user = userStore.getUserById(token.getId())
                .orElseThrow(() -> new WebApplicationException(Responses.UNAUTHORIZED.getResponse()));

        return Response.ok(new AccountInfo(msg.getToken(), user.getEmail())).build();
    }

    @POST
    @Path("email/update")
    public Response changeEmail(EmailUpdate msg) throws IOException {
        Token token = tokenManager.parse(msg.getToken());
        guardAuth.authorize(
                request.getRemoteAddr(),
                Action.UPDATE_EMAIL,
                Optional.of(token),
                Optional.of(token.getId()));

        try {
            userStore.updateUserEmail(token.getId(), guardAuth.normalizeEmail(msg.getNewEmail()));
        } catch (UserStore.EmailTakenException ex) {
            throw new WebApplicationException(ex, Responses.EMAIL_TAKEN.getResponse());
        }

        return Responses.OK.getResponse();
    }

    @POST
    @Path("pass/update")
    public Response changePass(PassUpdate msg) throws IOException {
        Token token = tokenManager.parse(msg.getToken());
        guardAuth.authorize(
                request.getRemoteAddr(),
                Action.UPDATE_PASS,
                Optional.of(token),
                Optional.of(token.getId()));

        userStore.updateUserPassHash(token.getId(), msg.getNewPass());

        return Responses.OK.getResponse();
    }
}
