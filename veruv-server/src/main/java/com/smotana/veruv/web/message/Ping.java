package com.smotana.veruv.web.message;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Simple ping, other side must respond with pong immediately.
 *
 * Origin: Server, Client
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Ping extends Message {
}
