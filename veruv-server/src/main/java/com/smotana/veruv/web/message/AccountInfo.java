package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Account information after successful authentication.
 *
 * Origin: Server
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class AccountInfo extends Message {
    @ToString.Exclude
    @SerializedName("token")
    private final String token;
    @SerializedName("email")
    private final String email;
}
