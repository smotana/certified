package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Update email on account.
 *
 * Origin: Client
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class EmailUpdate extends Message implements AuthorizedMessage {
    @ToString.Exclude
    @SerializedName("token")
    private final String token;
    @SerializedName("newEmail")
    private final String newEmail;
}
