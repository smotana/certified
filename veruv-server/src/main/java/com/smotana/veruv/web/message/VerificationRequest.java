package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Client is asking to take a screenshot of current screen as verification.
 *
 * Origin: Client
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class VerificationRequest extends Message implements AuthorizedMessage {
    @ToString.Exclude
    @SerializedName("token")
    private final String token;
    @SerializedName("requestId")
    private final String requestId;
}
