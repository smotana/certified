package com.smotana.veruv.web.api;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.smotana.veruv.core.Global;
import com.smotana.veruv.store.SnapshotRequestStore;
import com.smotana.veruv.store.SnapshotRequestStore.SnapshotRequest;
import com.smotana.veruv.web.guard.GuardAuth;
import com.smotana.veruv.web.guard.GuardAuth.Action;
import com.smotana.veruv.web.guard.SanitizerNavigateUrl;
import com.smotana.veruv.web.guard.SanitizerNote;
import com.smotana.veruv.web.guard.TokenManager;
import com.smotana.veruv.web.guard.TokenManager.Token;
import com.smotana.veruv.web.message.Error;
import com.smotana.veruv.web.message.*;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Optional;

// TODO input sanitation
@Slf4j
@Singleton
// TODO Disabled for now
//@Path("snapshot/request/")
public class SnapshotRequestResource extends AbstractVeruvResource {

    @Context
    private HttpServletRequest request;
    @Inject
    private Global.Config globalConfig;
    @Inject
    private Gson gson;
    @Inject
    private SnapshotRequestStore snapshotRequestStore;
    @Inject
    private GuardAuth guardAuth;
    @Inject
    private TokenManager tokenManager;
    @Inject
    private SanitizerNavigateUrl sanitizerNavigateUrl;
    @Inject
    private SanitizerNote sanitizerNote;

    @POST
    @Path("user/get/")
    public Response getAll(SnapshotRequestGetAll msg) throws IOException {
        log.trace("snapshot request all for user get {} from {}", msg, request.getRemoteAddr());
        if (msg == null) {
            return Responses.BAD_REQUEST.getResponse();
        }

        Token token = tokenManager.parse(msg.getToken());
        guardAuth.authorize(
                request.getRemoteAddr(),
                Action.SNAPSHOT_REQUEST_GET_ALL,
                Optional.of(token),
                Optional.of(token.getId()));

        ImmutableList<SnapshotRequestData> requests = snapshotRequestStore.getRequestsByUser(token.getId())
                .stream()
                .map(this::mapSnapshotRequest)
                .collect(ImmutableList.toImmutableList());

        SnapshotRequestDataAll requestsAll = new SnapshotRequestDataAll(requests);

        return Response.ok(gson.toJson(requestsAll)).build();
    }

    @POST
    @Path("get/")
    public Response get(SnapshotRequestGet msg) throws IOException {
        log.trace("snapshot request get {} from {}", msg, request.getRemoteAddr());
        if (msg == null) {
            return Responses.BAD_REQUEST.getResponse();
        }

        guardAuth.authorize(
                request.getRemoteAddr(),
                Action.SNAPSHOT_REQUEST_GET,
                Optional.empty(),
                Optional.empty());

        SnapshotRequestData snapshotRequestData = snapshotRequestStore.getRequestById(msg.getSnapshotRequestId())
                .map(this::mapSnapshotRequest)
                .orElseThrow(() -> new WebApplicationException(Responses.NOT_FOUND.getResponse()));

        return Response.ok(gson.toJson(snapshotRequestData)).build();
    }

    @POST
    @Path("create/")
    public Response get(SnapshotRequestCreate msg) throws IOException {
        log.trace("snapshot request create {} from {}", msg, request.getRemoteAddr());
        if (msg == null) {
            return Responses.BAD_REQUEST.getResponse();
        }

        Optional<Token> tokenOpt = Optional.ofNullable(Strings.emptyToNull(msg.getToken())).map(tokenManager::parse);
        Optional<String> userIdOpt = tokenOpt.map(Token::getId);
        guardAuth.authorize(
                request.getRemoteAddr(),
                Action.SNAPSHOT_REQUEST_CREATE,
                tokenOpt,
                userIdOpt);

        String sanitizedUrl = sanitizerNavigateUrl.sanitizeUrl(msg.getUrl());
        if (SanitizerNavigateUrl.BLANK_URL.equals(sanitizedUrl)) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(new Error("bad-url", "Given url is not valid")).build();
        }
        Optional<String> passUnhashedOpt = Optional.ofNullable(Strings.emptyToNull(msg.getPassUnhashedOpt()));
        Optional<String> noteOpt = sanitizerNote.sanitizeNote(msg.getNoteOpt());

        // TODO authorize setting password only for paying subscribers

        SnapshotRequest request = snapshotRequestStore.createRequest(
                sanitizedUrl,
                passUnhashedOpt,
                userIdOpt,
                noteOpt);

        return Response.ok(gson.toJson(mapSnapshotRequest(request))).build();
    }


    private SnapshotRequestData mapSnapshotRequest(SnapshotRequest request) {
        String token = tokenManager.create(TokenManager.ResourceType.SNAPSHOT_REQUEST, request.getId());
        return new SnapshotRequestData(
                token,
                request.getId(),
                request.getCreated().toEpochMilli(),
                request.getUserIdOpt().isPresent() ? 0L : request.getCreated().plus(globalConfig.anonymousRequestExpiry()).toEpochMilli(),
                request.getUrl(),
                request.getNote().orElse(null));
    }
}
