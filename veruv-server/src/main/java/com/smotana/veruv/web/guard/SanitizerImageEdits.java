package com.smotana.veruv.web.guard;

import com.google.common.collect.ImmutableList;
import com.google.inject.Singleton;
import com.smotana.veruv.image.ImageEditor;
import com.smotana.veruv.web.message.VerificationApplyEdits;
import lombok.extern.slf4j.Slf4j;

import java.awt.*;
import java.util.Collection;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkArgument;

@Slf4j
@Singleton
public class SanitizerImageEdits {
    /** Also change on client side in Editor.tsx */
    private static final int REDACTION_MIN_SIZE = 15;

    public Optional<ImageEditor.Edits> sanitizeImageEdits(VerificationApplyEdits msg, int imgWidth, int imgHeight) {
        Optional<Rectangle> cropOpt = Optional.ofNullable(msg.getCrop())
                .map(this::convertToRectangle);
        cropOpt.ifPresent(crop -> {
            checkArgument(crop.x + crop.width <= imgWidth, "Crop width outside of image region");
            checkArgument(crop.y + crop.height <= imgHeight, "Crop height outside of image region");
        });

        ImmutableList<Rectangle> redactions = Optional.ofNullable(msg.getRedactions())
                .stream()
                .flatMap(Collection::stream)
                .map(this::convertToRectangle)
                .collect(ImmutableList.toImmutableList());
        redactions.forEach(redaction -> {
            checkArgument(redaction.x + redaction.width <= imgWidth, "Redaction width outside of image region");
            checkArgument(redaction.y + redaction.height <= imgHeight, "Redaction height outside of image region");
            checkArgument(redaction.width >= REDACTION_MIN_SIZE, "Redaction width min size is not satisfied");
            checkArgument(redaction.height >= REDACTION_MIN_SIZE, "Redaction height min size is not satisfied");
        });

        if (cropOpt.isPresent() || !redactions.isEmpty()) {
            return Optional.of(new ImageEditor.Edits(cropOpt, redactions));
        } else {
            return Optional.empty();
        }
    }

    private Rectangle convertToRectangle(VerificationApplyEdits.Rectangle rectangleInput) {
        return new Rectangle(
                Math.min(rectangleInput.getP1().getX(), rectangleInput.getP2().getX()),
                Math.min(rectangleInput.getP1().getY(), rectangleInput.getP2().getY()),
                Math.abs(rectangleInput.getP1().getX() - rectangleInput.getP2().getX()),
                Math.abs(rectangleInput.getP1().getY() - rectangleInput.getP2().getY()));
    }
}
