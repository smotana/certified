package com.smotana.veruv.web;


import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.gson.*;
import com.google.gson.internal.Streams;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.smotana.veruv.web.message.Message;
import lombok.extern.slf4j.Slf4j;
import org.reflections.Reflections;

import java.io.IOException;


@Slf4j
public class MessageTypeAdapter extends TypeAdapter<Message> {
    private static final Gson GSON = new GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .disableHtmlEscaping()
            .create();
    private static final String TYPE_MEMBER_NAME = "__TYPE__";
    private final ImmutableMap<String, Class<? extends Message>> mapper;

    public MessageTypeAdapter() {
        mapper = new Reflections(Message.class)
                .getSubTypesOf(Message.class)
                .stream()
                .collect(ImmutableMap.toImmutableMap(c -> c.getSimpleName(), c -> c));
        log.info("Found Message classes: {}", mapper.keySet());
    }

    @Override
    public Message read(JsonReader in) throws IOException {
        JsonElement jsonElement = Streams.parse(in);
        JsonElement typeJsonElement = jsonElement.getAsJsonObject().remove(TYPE_MEMBER_NAME);
        if (typeJsonElement == null) {
            throw new IOException("Message is missing type: " + TYPE_MEMBER_NAME);
        }

        String messageType = typeJsonElement.getAsString();
        Class<? extends Message> messageClazz = mapper.get(messageType);
        if (messageClazz == null) {
            throw new IOException("Unknown message type: " + messageType);
        }

        return GSON.fromJson(jsonElement, messageClazz);
    }

    @Override
    public void write(JsonWriter out, Message message) throws IOException {
        String messageType = message.getClass().getSimpleName();
        if (Strings.isNullOrEmpty(messageType)) {
            throw new IOException("Anonymous message class type: " + message.getClass());
        }

        JsonObject jsonObj = GSON.toJsonTree(message).getAsJsonObject();
        if (jsonObj.has(TYPE_MEMBER_NAME)) {
            throw new IOException("Message type " + messageType + " already has clashing member " + TYPE_MEMBER_NAME);
        }
        jsonObj.add(TYPE_MEMBER_NAME, new JsonPrimitive(messageType));

        Streams.write(jsonObj, out);
    }
}
