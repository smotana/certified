package com.smotana.veruv.web.guard;

import com.google.common.base.Strings;
import com.google.inject.Singleton;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;

@Slf4j
@Singleton
public class SanitizerNote {
    /** Also change on client side in Editor.tsx */
    private static final int NOTE_MAX_LENGTH = 1024;

    public Optional<String> sanitizeNote(String note) {
        String sanitizedNote = Strings.emptyToNull(note);
        if (sanitizedNote == null) {
            return Optional.empty();
        }
        sanitizedNote = sanitizedNote.trim();
        if (sanitizedNote.length() > NOTE_MAX_LENGTH) {
            sanitizedNote = sanitizedNote.substring(0, NOTE_MAX_LENGTH);
        }
        return Optional.of(sanitizedNote);
    }
}
