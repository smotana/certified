package com.smotana.veruv.web.message;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Client is asking to move mouse to coordinates.
 *
 * Origin: Client
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ActionMouseMove extends Message {
    @ToString.Exclude
    @SerializedName("x")
    private final long x;
    @ToString.Exclude
    @SerializedName("y")
    private final long y;
}
