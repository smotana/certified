package com.smotana.veruv.resources;

import com.google.common.util.concurrent.RateLimiter;
import com.google.common.util.concurrent.RateLimiters;
import com.kik.config.ice.annotations.DefaultValue;
import com.smotana.veruv.core.Global;
import com.smotana.veruv.image.VideoEncoder;
import lombok.extern.slf4j.Slf4j;
import rx.Observable;
import rx.Subscription;

import javax.websocket.CloseReason;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static com.google.common.base.Preconditions.checkState;

@Slf4j
public class BrowserTask implements Runnable {

    public interface Config {
        @DefaultValue("131072")
        double maxScreenThroughputRateInBytesPerSecond();

        Observable<Double> maxScreenThroughputRateInBytesPerSecondObservable();

        @DefaultValue("PT0.5S")
        Duration screenRefreshRate();

        @DefaultValue("PT1S")
        Duration stateCheckRate();
    }

    private final Config config;
    private Global.Config configGlobal;
    private final BrowserResourceManager.Resource resource;
    private final BrowserResourceManager.ResourceEventHandler resourceEventHandler;
    private final VideoEncoder videoEncoder;

    private String lastUrl = "";
    private boolean lastReadyStateComplete = false;
    private boolean anyPageLoaded = false;
    private long lastScreenWidth = -1L;
    private long lastScreenHeight = -1L;
    private Instant nextScreenRefresh = Instant.MIN;
    private Instant nextStateCheck = Instant.MIN;

    BrowserTask(Global.Config configGlobal,
                Config config,
                BrowserResourceManager.Resource resource,
                BrowserResourceManager.ResourceEventHandler resourceEventHandler,
                VideoEncoder videoEncoder) {
        this.configGlobal = configGlobal;
        this.config = config;
        this.resource = resource;
        this.resourceEventHandler = resourceEventHandler;
        this.videoEncoder = videoEncoder;
    }

    @Override
    public void run() {
        RateLimiter screenThroughputRateLimiter = RateLimiters.createFull(config.maxScreenThroughputRateInBytesPerSecond(), 10);
        Subscription subscription = config.maxScreenThroughputRateInBytesPerSecondObservable().subscribe(screenThroughputRateLimiter::setRate);
        try {
            // Amount of bytes sent in screen refresh that needs to be counter in rate limiter
            int networkDebt = 0;
            while (resource.getIsConnected().get()) {
                Instant now = Instant.now();

                if (nextStateCheck.isBefore(now)) {
                    nextStateCheck = now.plus(config.stateCheckRate());
                    boolean urlChanged = refreshUrl();
                    refreshReadyState(urlChanged);
                }

                if (networkDebt == 0 && nextScreenRefresh.isBefore(now)) {
                    nextScreenRefresh = now.plus(config.screenRefreshRate());
                    networkDebt = refreshScreen();
                }

                now = Instant.now();
                long stateCheckSleepTime = now.until(nextStateCheck, ChronoUnit.MILLIS);
                if (networkDebt != 0) {
                    if (screenThroughputRateLimiter.tryAcquire(networkDebt, stateCheckSleepTime, TimeUnit.MILLISECONDS)) {
                        networkDebt = 0;
                    }
                }

                now = Instant.now();
                stateCheckSleepTime = now.until(nextStateCheck, ChronoUnit.MILLIS);
                long sleepTime;
                if (networkDebt != 0) {
                    sleepTime = stateCheckSleepTime;
                } else {
                    long screenRefreshSleepTime = now.until(nextScreenRefresh, ChronoUnit.MILLIS);
                    sleepTime = Math.min(stateCheckSleepTime, screenRefreshSleepTime);
                }
                if (sleepTime > 0) {
                    Thread.sleep(sleepTime);
                }
            }
            log.trace("Stopping due to resource not running");
        } catch (InterruptedException ex) {
            log.trace("Stopping due to interrupted");
        } catch (Exception ex) {
            log.error("Stopping due to failure", ex);
            resourceEventHandler.onClose(CloseReason.CloseCodes.UNEXPECTED_CONDITION, "Failed processing request");
        } finally {
            subscription.unsubscribe();
        }
    }

    public void refreshReadyState(boolean urlChanged) {
        // Initially keep the ready state incomplete until first page is loaded
        if (!anyPageLoaded) {
            if (urlChanged) {
                anyPageLoaded = true;
            } else {
                return;
            }
        }

        String newReadyState = resource.getBrowser().getDocumentReadyState();
        boolean newReadyStateComplete = "complete".equals(newReadyState);
        // No update if no change, but if url changed, also send an update since client preemptively
        // changes its status to incomplete when navigating
        if (!urlChanged && newReadyStateComplete == lastReadyStateComplete) {
            return;
        }
        lastReadyStateComplete = newReadyStateComplete;
        resourceEventHandler.onBrowserReadyStateChanged(newReadyStateComplete);
    }

    public boolean refreshUrl() {
        String newUrl = resource.getBrowser().getUrl();
        // Rewrite blank page to blank url
        if (newUrl.equals("about:blank")
                || newUrl.equals(configGlobal.setupUrl())) {
            newUrl = "";
        }
        if (newUrl.equals(lastUrl)) {
            return false;
        }
        checkState(!newUrl.startsWith("chrome://"), "Client navigated to chrome internal page! " + newUrl);
        lastUrl = newUrl;
        resourceEventHandler.onBrowserNavigated(newUrl);
        return true;
    }

    public int refreshScreen() throws IOException {
        byte[] screenBytes = resource.getBrowser().screenshot();

        Optional<VideoEncoder.Frame> frameOpt = videoEncoder.calcFrame(screenBytes);

        if (!frameOpt.isPresent()) {
            return 0;
        }
        VideoEncoder.Frame frame = frameOpt.get();

        Optional<Long> screenWidth = Optional.empty();
        Optional<Long> screenHeight = Optional.empty();
        if (lastScreenWidth != frame.getScreenWidth() || lastScreenHeight != frame.getScreenHeight()) {
            lastScreenWidth = frame.getScreenWidth();
            lastScreenHeight = frame.getScreenHeight();
            screenWidth = Optional.of(frame.getScreenWidth());
            screenHeight = Optional.of(frame.getScreenHeight());
        }

        resourceEventHandler.onFrameRefresh(frame.getX(), frame.getY(), frame.getWidth(), frame.getHeight(), frame.getImg(), screenWidth, screenHeight);

        return frame.getImg().length;
    }
}
