package com.smotana.veruv.resources;

import com.google.common.collect.Maps;
import com.google.common.util.concurrent.AbstractIdleService;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.Singleton;
import com.kik.config.ice.ConfigSystem;
import com.kik.config.ice.annotations.DefaultValue;
import com.smotana.veruv.core.AbstractVeruvModule;
import com.smotana.veruv.resources.BrowserResourceManager.Resource;
import com.smotana.veruv.resources.BrowserResourceManager.ResourceEventHandler;
import com.smotana.veruv.util.Instants;
import lombok.extern.slf4j.Slf4j;
import rx.Observable;

import javax.websocket.CloseReason;
import java.time.Duration;
import java.time.Instant;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.stream.Stream;

@Slf4j
@Singleton
public class BrowserExpiry extends AbstractIdleService {

    public interface Config {
        @DefaultValue("PT1M")
        Duration startupLimit();

        Observable startupLimitObservable();

        @DefaultValue("PT10M")
        Duration runningLimit();

        Observable runningLimitObservable();

        @DefaultValue("PT2M")
        Duration inactivityLimit();

        Observable inactivityLimitObservable();

        @DefaultValue("PT10S")
        Duration limitWarning();

        Observable limitWarningObservable();
    }

    @Inject
    private Config config;

    @Override
    protected void startUp() throws Exception {
        executor = Executors.newSingleThreadScheduledExecutor(new ThreadFactoryBuilder()
                .setNameFormat("BrowserExpiry-worker").build());
        Stream.of(
                config.startupLimitObservable(),
                config.runningLimitObservable(),
                config.inactivityLimitObservable(),
                config.limitWarningObservable())
                .forEach(o -> o.subscribe(v -> executor.execute(this::check)));
    }

    @Override
    protected void shutDown() throws Exception {
        executor.shutdownNow();
        executor.awaitTermination(1, TimeUnit.MINUTES);
    }

    private final ConcurrentMap<Resource, ResourceEventHandler> watching = Maps.newConcurrentMap();
    private volatile Optional<ScheduledFuture> scheduledCheck = Optional.empty();
    private ScheduledExecutorService executor;

    /** Call if state of a resource changes */
    public void runCheck() {
        executor.execute(this::check);
    }

    public void watch(Resource resource, ResourceEventHandler handler) {
        if (watching.putIfAbsent(resource, handler) == null) {
            executor.execute(this::check);
        }
    }

    public void unwatch(Resource resource) {
        watching.remove(resource);
    }

    private void check() {
        try {
            log.trace("Running check");
            Instant nextCheck = null;
            Instant now = Instant.now();
            for (Iterator<Map.Entry<Resource, ResourceEventHandler>> iterator = watching.entrySet().iterator(); iterator.hasNext(); ) {
                Map.Entry<Resource, ResourceEventHandler> entry = iterator.next();
                Resource resource = entry.getKey();
                ResourceEventHandler handler = entry.getValue();

                try {
                    if (!resource.getIsConnected().get()) {
                        iterator.remove();
                        continue;
                    }

                    if (!resource.getStarted().isPresent()) {
                        if (!resource.getCreated().isPresent()) {
                            log.warn("Creation time should be already set for resource {}", resource);
                            continue;
                        }
                        Instant startupTimeout = resource.getCreated().get().plus(config.startupLimit());
                        if (now.isAfter(startupTimeout)) {
                            resource.getBrowserOpt().ifPresent(Browser::printLogsIfPossible);
                            handler.onClose(CloseReason.CloseCodes.NORMAL_CLOSURE, "Failed to start in reasonable time, please try again later");
                            iterator.remove();
                            continue;
                        } else {
                            nextCheck = Instants.min(nextCheck, startupTimeout);
                            continue;
                        }
                    }

                    Instant runningTimeout = resource.getStarted().get().plus(config.runningLimit());
                    if (now.isAfter(runningTimeout)) {
                        handler.onClose(CloseReason.CloseCodes.NORMAL_CLOSURE, "Oops your session ran out of time");
                        iterator.remove();
                        continue;
                    } else {
                        nextCheck = Instants.min(nextCheck, runningTimeout);
                    }

                    Instant inactivityTimeout = resource.getLastActivity().plus(config.inactivityLimit());
                    if (now.isAfter(inactivityTimeout)) {
                        handler.onClose(CloseReason.CloseCodes.NORMAL_CLOSURE, "Closed due to inactivity");
                        iterator.remove();
                        continue;
                    } else {
                        nextCheck = Instants.min(nextCheck, inactivityTimeout);
                    }

                    Instant inactivityTimeoutWarning = inactivityTimeout.minus(config.limitWarning());
                    Instant runningTimeoutWarning = runningTimeout.minus(config.limitWarning());
                    if (inactivityTimeoutWarning.isBefore(runningTimeoutWarning)) {
                        if (now.isAfter(inactivityTimeoutWarning)) {
                            handler.onUserNotify(BrowserResourceManager.NotifyType.WARNING, "Your session will soon close due to inactivity");
                        } else {
                            nextCheck = Instants.min(nextCheck, inactivityTimeoutWarning);
                        }
                    } else {
                        if (now.isAfter(runningTimeoutWarning)) {
                            handler.onUserNotify(BrowserResourceManager.NotifyType.WARNING, "Your session will be closed soon");
                        } else {
                            nextCheck = Instants.min(nextCheck, runningTimeoutWarning);
                        }
                    }
                } catch (Exception ex) {
                    log.error("Failed to run watch check on resource {}", resource, ex);
                }
            }

            scheduledCheck.ifPresent(c -> c.cancel(false));
            scheduledCheck = Optional.empty();
            if (nextCheck != null) {
                long sleepUntilNextCheck = nextCheck.toEpochMilli() - System.currentTimeMillis() + 1;
                if (sleepUntilNextCheck > 0) {
                    log.trace("Next check scheduled in {}ms", sleepUntilNextCheck);
                    scheduledCheck = Optional.of(executor.schedule(this::check, sleepUntilNextCheck, TimeUnit.MILLISECONDS));
                } else {
                    log.trace("Next check scheduled now");
                    executor.execute(this::check);
                }
            } else {
                log.trace("No next check scheduled");
            }
        } catch (Exception ex) {
            log.error("Failure checking, retrying soon", ex);
            executor.schedule(this::check, 30, TimeUnit.SECONDS);
        }
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                install(ConfigSystem.configModule(Config.class));
                bind(BrowserExpiry.class).asEagerSingleton();
                addService(BrowserExpiry.class);
            }
        };
    }
}
