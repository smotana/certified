package com.smotana.veruv.resources;

public interface GracefulShutdownHook {
    void gracefulShutdown();
}
