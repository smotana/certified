package com.smotana.veruv.resources;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Predicates;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.util.concurrent.AbstractScheduledService;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.Singleton;
import com.google.monitoring.v3.TypedValue;
import com.kik.config.ice.ConfigSystem;
import com.kik.config.ice.annotations.DefaultValue;
import com.smotana.veruv.core.AbstractVeruvModule;
import com.smotana.veruv.core.Global;
import com.smotana.veruv.monitor.Metrics;
import com.smotana.veruv.util.TimeUtil;
import com.smotana.veruv.util.TimeUtil.SingleUnitDuration;
import lombok.extern.slf4j.Slf4j;
import rx.functions.Action1;

import java.time.Duration;
import java.time.Instant;
import java.util.Iterator;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

@Slf4j
@Singleton
public class InMemoryResourceQueue extends AbstractScheduledService implements ResourceQueue {

    public interface Config {
        @DefaultValue("PT1H")
        Duration cleanupAfter();

        @DefaultValue("PT1M")
        Duration sessionLengthTimeEstimate();

        @DefaultValue("PT5S")
        Duration resourceStartupTimeEstimate();
    }

    @Inject
    private Config config;

    private final ReentrantLock locker = new ReentrantLock();
    private final Set<QueueTicketImpl> activeTickets = Sets.newConcurrentHashSet();
    private final Queue<QueueTicketImpl> priorityQueue = Lists.newLinkedList();
    private final Queue<QueueTicketImpl> normalQueue = Lists.newLinkedList();
    private final AtomicLong totalResources = new AtomicLong(0);
    private final AtomicLong resourcesAvailable = new AtomicLong(0);

    @Inject
    private void setup(Global.Config configGlobal, Metrics metrics) {
        Action1<Long> updateTotalResources = t -> {
            long lastTotal = -1;
            long newTotal = -1;
            locker.lock();
            try {
                // Ignore supplied argument value to prevent race condition, instead fetch new value insider locker here
                newTotal = configGlobal.maxBrowserSessionsPerHost();
                lastTotal = totalResources.getAndSet(newTotal);
                long diff = newTotal - lastTotal;
                if (diff > 0) {
                    releaseResource(diff);
                } else if (diff < 0) {
                    reserveForced(-diff);
                }
                log.info("Update resource queue from {} to {}", lastTotal, newTotal);
            } catch (Exception ex) {
                log.error("Failed to update total resources, failed to update from {} to {}", lastTotal, newTotal, ex);
            } finally {
                locker.unlock();
            }
        };
        configGlobal.maxBrowserSessionsPerHostObservable().subscribe(updateTotalResources);
        updateTotalResources.call(configGlobal.maxBrowserSessionsPerHost());

        metrics.addGauge("/resource/queue/load", "Ratio of users to browser resources. <1 capacity available, =1 at capacity, >1 users queuing",
                () -> TypedValue.newBuilder().setDoubleValue(calculateLoad()).build());
    }

    @Override
    protected Scheduler scheduler() {
        return Scheduler.newFixedDelaySchedule(1, 1, TimeUnit.MINUTES);
    }

    @Override
    protected void runOneIteration() throws Exception {
        try {
            Instant now = Instant.now();
            Duration cleanupAfter = config.cleanupAfter();
            activeTickets.stream()
                    .filter(Predicates.or(
                            t -> t.getHandler().isValid(),
                            t -> t.getCreated().plus(cleanupAfter).isAfter(now)))
                    .forEach(t -> {
                        log.error("Cleanup expired resource created {} isValid {} from {}",
                                t.getCreated(), t.getHandler().isValid(), t.getHandler().getRemoteAddr());
                        t.release();
                    });
        } catch (Exception ex) {
            log.error("Cleanup thread failure", ex);
        }
    }

    @Override
    public QueueTicket addToQueue(EventHandler handler) {
        QueueTicketImpl ticket = new QueueTicketImpl(handler);
        locker.lock();
        try {
            if (resourcesAvailable.get() > 0) {
                resourcesAvailable.decrementAndGet();
                ticket.setReady();
            } else {
                long itemsAhead = -resourcesAvailable.get()
                        + 1L
                        + (ticket.getHandler().getPriority() == Priority.HIGH
                        ? priorityQueue.size()
                        : priorityQueue.size() + normalQueue.size());
                (ticket.getHandler().getPriority() == Priority.HIGH
                        ? priorityQueue
                        : normalQueue)
                        .offer(ticket);
                ticket.notifyWaitingTime(itemsAhead);
            }
        } finally {
            locker.unlock();
        }
        return ticket;
    }

    /**
     * Reserve resources regardless if they are available and notify queued users.
     * If they are not available, num of resources will be negative and users will have to wait for others to release
     * first.
     */
    private void reserveForced(long numResourcesToReserve) {
        checkArgument(numResourcesToReserve > 0);
        locker.lock();
        try {
            resourcesAvailable.addAndGet(-numResourcesToReserve);

            notifyAllWaitingTime();
        } finally {
            locker.unlock();
        }
    }

    /**
     * Release resources, assign to waiting users, and notify remaining queued users.
     */
    private void releaseResource() {
        releaseResource(1L);
    }

    private void releaseResource(final long numResourcesToRelease) {
        checkArgument(numResourcesToRelease > 0);
        locker.lock();
        try {

            this.resourcesAvailable.addAndGet(numResourcesToRelease);

            while (resourcesAvailable.get() > 0) {
                QueueTicketImpl ticket = priorityQueue.poll();
                if (ticket != null) {
                    resourcesAvailable.decrementAndGet();
                    ticket.setReady();
                    continue;
                }

                ticket = normalQueue.poll();
                if (ticket != null) {
                    resourcesAvailable.decrementAndGet();
                    ticket.setReady();
                    continue;
                }

                break;
            }

            notifyAllWaitingTime();
        } finally {
            locker.unlock();
        }
    }

    private void notifyAllWaitingTime() {
        locker.lock();
        try {
            if (resourcesAvailable.get() > 0) {
                checkState(priorityQueue.isEmpty());
                checkState(normalQueue.isEmpty());
                return;
            }

            long usersAhead = -resourcesAvailable.get() + 1L;
            for (QueueTicketImpl ticket : priorityQueue) {
                ticket.notifyWaitingTime(usersAhead);
                usersAhead++;
            }
            for (QueueTicketImpl ticket : normalQueue) {
                ticket.notifyWaitingTime(usersAhead);
                usersAhead++;
            }
        } finally {
            locker.unlock();
        }
    }

    private void removeFromQueue(QueueTicketImpl ticketToRemove) {
        locker.lock();
        try {
            boolean hasBeenRemoved = false;
            long usersAhead = -resourcesAvailable.get() + 1L;

            if (ticketToRemove.getHandler().getPriority() == Priority.HIGH) {
                for (Iterator<QueueTicketImpl> iterator = priorityQueue.iterator(); iterator.hasNext(); ) {
                    QueueTicketImpl ticket = iterator.next();
                    if (hasBeenRemoved) {
                        ticket.notifyWaitingTime(usersAhead);
                        usersAhead++;
                    } else if (ticketToRemove == ticket) {
                        iterator.remove();
                        hasBeenRemoved = true;
                    } else {
                        usersAhead++;
                    }
                }
            } else {
                usersAhead += priorityQueue.size();
            }

            for (Iterator<QueueTicketImpl> iterator = normalQueue.iterator(); iterator.hasNext(); ) {
                QueueTicketImpl ticket = iterator.next();
                if (hasBeenRemoved) {
                    ticket.notifyWaitingTime(usersAhead);
                    usersAhead++;
                } else if (ticketToRemove == ticket) {
                    iterator.remove();
                    hasBeenRemoved = true;
                } else {
                    usersAhead++;
                }
            }
        } finally {
            locker.unlock();
        }
    }

    @VisibleForTesting
    SingleUnitDuration getEstimate(long usersAhead) {
        Duration inQueueEstimateTime = config.resourceStartupTimeEstimate().plus(config.sessionLengthTimeEstimate().multipliedBy(usersAhead));
        return TimeUtil.largestUnitFloor(inQueueEstimateTime);
    }

    @VisibleForTesting
    double calculateLoad() {
        double usersWaiting = priorityQueue.size() + normalQueue.size();
        double total = totalResources.get();
        double available = resourcesAvailable.doubleValue();
        return (total - available + usersWaiting) / total;
    }

    private class QueueTicketImpl implements QueueTicket {
        private final Instant created = Instant.now();
        private final EventHandler handler;
        private volatile boolean isReleased = false;
        private volatile boolean hasResource = false;

        private QueueTicketImpl(EventHandler handler) {
            this.handler = handler;
            activeTickets.add(this);
        }

        EventHandler getHandler() {
            return handler;
        }

        public Instant getCreated() {
            return created;
        }

        @Override
        public void release() {
            if (isReleased) {
                return;
            }

            locker.lock();
            try {
                if (isReleased) {
                    return;
                }
                if (hasResource) {
                    releaseResource();
                } else {
                    removeFromQueue(this);
                }
                isReleased = true;
                activeTickets.remove(this);
            } finally {
                locker.unlock();
            }
        }

        private void setReady() {
            notifyWaitingTime(0);
            log.trace("Notifying {} ready", handler.getRemoteAddr());
            this.hasResource = true;
            try {
                handler.onReady();
            } catch (Exception ex) {
                log.error("onReady failed on ticket from {}", handler.getRemoteAddr(), ex);
            }
        }

        private void notifyWaitingTime(long usersAhead) {
            log.trace("Notifying {} usersAhead {}", handler.getRemoteAddr(), usersAhead);
            SingleUnitDuration timeEstimate = getEstimate(usersAhead);
            try {
                handler.onInQueue(timeEstimate.getDuration(), timeEstimate.getUnit(), usersAhead);
            } catch (Exception ex) {
                log.error("onInQueue failed on ticket from {}", handler.getRemoteAddr(), ex);
            }
        }
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                bind(ResourceQueue.class).to(InMemoryResourceQueue.class).asEagerSingleton();
                install(ConfigSystem.configModule(Config.class));
            }
        };
    }
}
