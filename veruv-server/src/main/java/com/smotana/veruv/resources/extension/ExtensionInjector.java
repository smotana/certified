package com.smotana.veruv.resources.extension;

import ch.simschla.minify.css.CssMin;
import ch.simschla.minify.js.JsMin;
import com.google.common.base.Charsets;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Base64;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Slf4j
@Singleton
public class ExtensionInjector {

    @Inject
    private Gson gson;

    public ChromeOptions optionsWithExtension(String extensionPath) {
        try {
            String zipBase64 = minifyZipExtensionFolder(Paths.get(extensionPath));
            return new ChromeOptions().addEncodedExtensions(zipBase64);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    private String minifyZipExtensionFolder(Path extensionPath) throws IOException {
        byte[] extensionBytes;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            try (ZipOutputStream zos = new ZipOutputStream(baos, Charsets.UTF_8)) {
                Files.walkFileTree(extensionPath, new SimpleFileVisitor<>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                            throws IOException {
                        Objects.requireNonNull(file);
                        Objects.requireNonNull(attrs);

                        if (!attrs.isRegularFile()) {
                            return FileVisitResult.CONTINUE;
                        }

                        String relativePath = extensionPath.relativize(file).toString();
                        zos.putNextEntry(new ZipEntry(relativePath));
                        if (file.toString().endsWith(".js")) {
                            byte[] jsBytes;
                            try (FileInputStream fis = new FileInputStream(file.toFile());
                                 ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                                JsMin.builder()
                                        .inputStream(fis)
                                        .outputStream(baos)
                                        .charset(Charsets.UTF_8)
                                        .build()
                                        .minify();
                                jsBytes = baos.toByteArray();
                            }
                            if (log.isTraceEnabled()) {
                                log.trace("Zipping minified js {}: {}", relativePath, new String(jsBytes, Charsets.UTF_8));
                            }
                            zos.write(jsBytes);
                        } else if (file.toString().endsWith(".css")) {
                            byte[] cssBytes;
                            try (FileInputStream fis = new FileInputStream(file.toFile());
                                 ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                                CssMin.builder()
                                        .inputStream(fis)
                                        .outputStream(baos)
                                        .charset(Charsets.UTF_8)
                                        .build()
                                        .minify();
                                cssBytes = baos.toByteArray();
                            }
                            if (log.isTraceEnabled()) {
                                log.trace("Zipping minified css {}: {}", relativePath, new String(cssBytes, Charsets.UTF_8));
                            }
                            zos.write(cssBytes);
                            // TODO gson turns 2 into 2.0 and chrome complains manifest_version must be integer
//                        } else if (file.toString().endsWith(".json")) {
//                            String compactJson;
//                            try (FileReader fr = new FileReader(file.toString());
//                                 JsonReader jr = new JsonReader(fr)) {
//                                Object obj = gson.fromJson(jr, Object.class);
//                                compactJson = gson.toJson(obj);
//                            }
//                            log.trace("Zipping minified json: {}", compactJson);
//                            zos.write(compactJson.getBytes(Charsets.UTF_8));
                        } else {
                            byte[] fileBytes;
                            try (FileInputStream fis = new FileInputStream(file.toFile())) {
                                fileBytes = fis.readAllBytes();
                            }
                            log.trace("Zipping minified file {}", relativePath);
                            zos.write(fileBytes);
                        }
                        zos.closeEntry();

                        return FileVisitResult.CONTINUE;
                    }
                });
            }
            extensionBytes = baos.toByteArray();
        }
        // TODO remove
//        Files.write(Paths.get("/tmp/ext.zip"), extensionBytes);
        return Base64.getEncoder().encodeToString(extensionBytes);
    }
}
