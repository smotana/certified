package com.smotana.veruv.resources.extension;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.smotana.veruv.web.message.BrowserRequest;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.List;
import java.util.Optional;

@Slf4j
@Singleton
public class InjectHeaderExtension {

    private enum MessageType {
        PING("ping"),
        SETUP("setup");

        private final String id;

        MessageType(String type) {
            this.id = type;
        }

        String getId() {
            return id;
        }
    }

    @Inject
    private Gson gson;
    @Inject
    private ExtensionInjector extensionInjector;

    public ChromeOptions optionsWithExtension() {
        String extensionFolderPath = getClass().getClassLoader().getResource("inject-header-ext").getPath();
        return extensionInjector.optionsWithExtension(extensionFolderPath);
    }

    /**
     * After successful ping, a 'pong' response is expected.
     */
    public String getPingJs() {
        return sendMessageJs(MessageType.PING, "'ping'");
    }

    /**
     * After successful setup, a 'ok' response is expected. The extension halts further communication afterwards.
     */
    public String getSetterJs(String clientHeaderXForwardedFor,
                              String clientHeaderForwarded,
                              String clientIp,
                              Optional<String> userAgentOpt,
                              Optional<List<BrowserRequest.Cookie>> cookiesOpt,
                              Optional<List<BrowserRequest.LocalStorage>> localStorageOpt) {
        ImmutableMap.Builder<String, Object> contentBuilder = ImmutableMap.builder();

        // Headers
        ImmutableMap.Builder<String, String> headersBuilder = ImmutableMap.builder();
        headersBuilder.put("x-forwarded-for", combine(clientHeaderXForwardedFor, clientIp));
        headersBuilder.put("forwarded", combine(clientHeaderForwarded, wrapForwardedHeader(clientIp)));
        headersBuilder.put("via", "1.1 Veruv");
        userAgentOpt.ifPresent(userAgent -> headersBuilder.put("user-agent", userAgent));
        contentBuilder.put("h", headersBuilder.build());

        // Local storage
        localStorageOpt.ifPresent(localStorage -> contentBuilder.put("l", localStorage));

        // Cookies
        cookiesOpt.ifPresent(cookies -> contentBuilder.put("c", cookies));

        String contentJson = gson.toJson(contentBuilder.build());

        return sendMessageJs(MessageType.SETUP, contentJson);
    }

    private String sendMessageJs(MessageType type, String msg) {
        return "var c=arguments[arguments.length-1];" +
                "var l=function(e){" +
                "if(e.data && e.data.veruvRecv){" +
                "window.removeEventListener('message',l);" +
                "c(e.data.msg);" +
                "}};" +
                "window.addEventListener('message',l,false);" +
                "window.postMessage({veruvSend:true,type:'" + type.getId() + "',msg:" + msg + "},'*')";
    }

    private String combine(String forwardedHeader, String ip) {
        if (Strings.isNullOrEmpty(forwardedHeader.trim())) {
            return ip;
        }
        return forwardedHeader + ", " + ip;
    }

    private String wrapForwardedHeader(String clientIp) {
        return "for=" + (clientIp.contains(":") ? "\"[" + clientIp + "]\"" : clientIp);
    }
}
