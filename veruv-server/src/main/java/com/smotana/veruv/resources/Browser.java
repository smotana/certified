package com.smotana.veruv.resources;

import com.smotana.veruv.store.SnapshotStore.Snapshot;
import com.smotana.veruv.web.guard.SanitizerScreenDimensions;
import com.smotana.veruv.web.message.BrowserRequest;
import org.openqa.selenium.logging.LogEntries;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.smotana.veruv.web.message.ActionKeyboardKeyPress.KeyPressType;

public interface Browser {

    void release();

    void setup(String startUrl,
               String clientHeaderXForwardedFor,
               String clientHeaderForwarded,
               String clientIp,
               Optional<String> userAgentOpt,
               Optional<List<BrowserRequest.Cookie>> cookiesOpt,
               Optional<List<BrowserRequest.LocalStorage>> localStorageOpt);

    void setScreenDimensions(SanitizerScreenDimensions.ScreenDimensions screenDimensions);

    boolean isConnected();

    void navigate(String url);

    void moveMouse(long x, long y);

    void click(long x, long y, boolean doubleClick);

    void keyPress(int keyCode, KeyPressType keyDown);

    void type(int[] charCodes);

    /** NOTE: the value is returned from JS, treat it as user input */
    String getDocumentReadyState();

    String getUrl();

    byte[] screenshot();

    void navigateForward();

    void navigateBack();

    void refresh();

    Snapshot getSnapshot(Optional<String> ownerUserId) throws IOException;

    Map<String, LogEntries> getLogs() throws IOException;

    void printLogsIfPossible();
}
