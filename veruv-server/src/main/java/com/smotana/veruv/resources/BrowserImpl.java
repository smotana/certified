package com.smotana.veruv.resources;

import com.github.rholder.retry.*;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.kik.config.ice.ConfigSystem;
import com.kik.config.ice.annotations.DefaultValue;
import com.smotana.veruv.core.AbstractVeruvModule;
import com.smotana.veruv.core.Global;
import com.smotana.veruv.docker.BrowserCapabilitiesFactory;
import com.smotana.veruv.docker.DockerInstance;
import com.smotana.veruv.docker.DockerManager;
import com.smotana.veruv.image.ImageEditor;
import com.smotana.veruv.resources.extension.InjectHeaderExtension;
import com.smotana.veruv.store.SnapshotStore;
import com.smotana.veruv.web.guard.SanitizerKeys;
import com.smotana.veruv.web.guard.SanitizerScreenDimensions;
import com.smotana.veruv.web.guard.SanitizerVerificationUrl;
import com.smotana.veruv.web.message.ActionKeyboardKeyPress;
import com.smotana.veruv.web.message.BrowserRequest;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.Logs;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

@Slf4j
public class BrowserImpl implements Browser {

    public interface Config {
        @DefaultValue("PT1M")
        Duration connectTimeout();

        @DefaultValue("PT30S")
        Duration defaultScriptTimeout();

        @DefaultValue("PT2S")
        Duration setupPingTimeout();

        @DefaultValue("PT10S")
        Duration setupScriptTimeout();

        @DefaultValue("false")
        boolean enableVerboseCrashLogging();
    }

    /**
     * Taken from {@link org.openqa.selenium.interactions.internal.SingleKeyAction#MODIFIER_KEYS}
     */
    private static final ImmutableSet<Keys> MODIFIER_KEYS = ImmutableSet.of(Keys.SHIFT, Keys.CONTROL, Keys.ALT, Keys.META,
            Keys.COMMAND, Keys.LEFT_ALT, Keys.LEFT_CONTROL, Keys.LEFT_SHIFT);

    @Inject
    private Config config;
    @Inject
    private Global.Config configGlobal;
    @Inject
    private SanitizerKeys sanitizerKeys;
    @Inject
    private ImageEditor imageEditor;
    @Inject
    private SanitizerVerificationUrl sanitizerVerificationUrl;
    @Inject
    private SnapshotStore snapshotStore;
    @Inject
    private InjectHeaderExtension injectHeaderExtension;
    @Inject
    private DockerManager dockerManager;
    @Inject
    private BrowserCapabilitiesFactory browserCapabilitiesFactory;

    private final Lock driverLock = new ReentrantLock();

    private DockerInstance dockerInstance;
    private RemoteWebDriver driver;
    private volatile boolean isConnected = false;
    private volatile long mouseX = 0L;
    private volatile long mouseY = 0L;

    @Inject
    private void startup() throws IOException {
        try {
            this.dockerInstance = dockerManager.getContainer();

            RemoteWebDriver driver;
            Capabilities capabilities = browserCapabilitiesFactory.getCapabilities();
            try {
                log.info("Attempting to connect to selenium on {}:{}", dockerInstance.getHost(), dockerInstance.getSeleniumPort());
                Retryer<RemoteWebDriver> startupRetryer = RetryerBuilder.<RemoteWebDriver>newBuilder()
                        .retryIfExceptionOfType(UnreachableBrowserException.class)
                        .withStopStrategy(StopStrategies.stopAfterDelay(config.connectTimeout().toMillis(), TimeUnit.MILLISECONDS))
                        .withWaitStrategy(WaitStrategies.exponentialWait(50, 1, TimeUnit.SECONDS))
                        .build();
                driver = startupRetryer.call(() -> new RemoteWebDriver(
                        new URL("http://" + dockerInstance.getHost() + ":" + dockerInstance.getSeleniumPort() + "/wd/hub"),
                        capabilities));
                log.info("Successfully connected to {}:{}", dockerInstance.getHost(), dockerInstance.getSeleniumPort());
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
            driver = (RemoteWebDriver) new Augmenter().augment(driver);
            driver.manage().window().setPosition(new Point(0, 0));

            driver.get(configGlobal.setupUrl());

            this.driver = driver;
            this.isConnected = true;
        } catch (Exception ex) {
            printLogsIfPossible();
            release();
            throw ex;
        }
    }

    @Override
    public void release() {
        this.isConnected = false;
        dockerManager.killContainer(dockerInstance);
    }

    @Override
    public void setup(String startUrl,
                      String clientHeaderXForwardedFor,
                      String clientHeaderForwarded,
                      String clientIp,
                      Optional<String> userAgentOpt,
                      Optional<List<BrowserRequest.Cookie>> cookiesOpt,
                      Optional<List<BrowserRequest.LocalStorage>> localStorageOpt) {
        try {
            String pingJs = injectHeaderExtension.getPingJs();
            log.debug("Attempting to ping extension");
            Retryer<Object> setupRetryer = RetryerBuilder.newBuilder()
                    .retryIfExceptionOfType(ScriptTimeoutException.class)
                    .retryIfExceptionOfType(JavascriptException.class)
                    .retryIfResult(result -> {
                        if ("pong".equals(result)) {
                            return false;
                        } else {
                            log.info("Failed to ping browser extension, didn't get a 'pong' response from extension: '{}'", result);
                            return true;
                        }
                    })
                    .withStopStrategy(StopStrategies.stopAfterDelay(config.connectTimeout().toMillis(), TimeUnit.MILLISECONDS))
                    .withWaitStrategy(WaitStrategies.exponentialWait(50, 1, TimeUnit.SECONDS))
                    .build();
            execute(() -> {
                driver.manage().timeouts().setScriptTimeout(config.setupPingTimeout().toMillis(), TimeUnit.MILLISECONDS);
                try {
                    setupRetryer.call(() -> driver.executeAsyncScript(pingJs));
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
                driver.manage().timeouts().setScriptTimeout(config.defaultScriptTimeout().toMillis(), TimeUnit.MILLISECONDS);
            });
            log.debug("Received successful reply from extension");

            String setterJs = injectHeaderExtension.getSetterJs(
                    clientHeaderXForwardedFor,
                    clientHeaderForwarded,
                    clientIp,
                    userAgentOpt,
                    cookiesOpt,
                    localStorageOpt);
            log.trace("SetterJs {}", setterJs);
            Object response = executeAndReturn(() -> {
                driver.manage().timeouts().setScriptTimeout(config.setupScriptTimeout().toMillis(), TimeUnit.MILLISECONDS);
                Object result = driver.executeAsyncScript(setterJs);
                driver.manage().timeouts().setScriptTimeout(config.defaultScriptTimeout().toMillis(), TimeUnit.MILLISECONDS);
                return result;
            });
            if (!"ok".equals(response)) {
                printLogsIfPossible();
                throw new RuntimeException("Failed to start browser, didn't get an 'ok' response from extension: "
                        + (response == null ? "" : response.toString()));
            }

            execute(() -> driver.get(startUrl));
        } catch (Exception ex) {
            printLogsIfPossible();
            throw ex;
        }
    }

    @Override
    public void setScreenDimensions(SanitizerScreenDimensions.ScreenDimensions screenDimensions) {
        execute(() -> driver.manage().window().setSize(new Dimension(screenDimensions.getWidth(), screenDimensions.getHeight())));
    }

    @Override
    public boolean isConnected() {
        return this.isConnected;
    }

    @Override
    public void navigate(String url) {
        checkState(this.isConnected, "Not connected");
        execute(() -> driver.navigate().to(url));
    }

    @Override
    public void moveMouse(long x, long y) {
        checkState(this.isConnected, "Not connected");
        execute(() -> {
            long offsetX = x - mouseX;
            long offsetY = y - mouseY;
            Action action = new Actions(driver).moveByOffset((int) offsetX, (int) offsetY).build();
            action.perform();
            mouseX = x;
            mouseY = y;
        });
    }

    @Override
    public void click(long x, long y, boolean doubleClick) {
        checkState(this.isConnected, "Not connected");
        moveMouse(x, y);
        Actions actions = new Actions(driver);
        if (doubleClick) {
            actions = actions.doubleClick();
        } else {
            actions = actions.click();
        }
        Action action = actions.build();
        execute(action::perform);
    }

    @Override
    public void keyPress(int keyCode, ActionKeyboardKeyPress.KeyPressType keyPressType) {
        checkState(this.isConnected, "Not connected");

        final CharSequence key = sanitizerKeys.sanitizeKeyCode(keyCode);
        checkArgument(keyPressType == ActionKeyboardKeyPress.KeyPressType.PRESS || MODIFIER_KEYS.contains(key),
                "Only modifier keys are allowed key up and down: " + key);

        Actions actions = new Actions(driver);
        switch (keyPressType) {
            case UP:
                actions = actions.keyUp(key);
                break;
            case DOWN:
                actions = actions.keyDown(key);
                break;
            case PRESS:
                actions = actions.sendKeys(key);
                break;
        }
        Action action = actions.build();
        execute(action::perform);
    }

    @Override
    public void type(int[] charCodes) {
        checkState(this.isConnected, "Not connected");
        Action action = new Actions(driver).sendKeys(sanitizerKeys.sanitizeCharCodes(charCodes)).build();
        execute(action::perform);
    }

    @Override
    public String getUrl() {
        return executeAndReturn(() -> driver.getCurrentUrl());
    }

    @Override
    public String getDocumentReadyState() {
        return executeAndReturn(() -> (String) driver.executeScript("return document.readyState"));
    }

    @Override
    public byte[] screenshot() {
        checkState(this.isConnected, "Not connected");
        return executeAndReturn(() -> driver.getScreenshotAs(OutputType.BYTES));
    }

    @Override
    public void navigateForward() {
        checkState(this.isConnected, "Not connected");
        execute(() -> driver.navigate().forward());
    }

    @Override
    public void navigateBack() {
        checkState(this.isConnected, "Not connected");
        execute(() -> driver.navigate().back());
    }

    @Override
    public void refresh() {
        checkState(this.isConnected, "Not connected");
        execute(() -> driver.navigate().refresh());
    }

    @Override
    public SnapshotStore.Snapshot getSnapshot(Optional<String> ownerUserId) {
        return executeAndReturn(() -> {
            String sanitizedUrl = sanitizerVerificationUrl.sanitizeUrl(getUrl());
            byte[] screenshot = screenshot();
            BufferedImage screenshotImage;
            try {
                screenshotImage = imageEditor.toImage(screenshot);
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
            return snapshotStore.createSnapshot(
                    SnapshotStore.StorageType.Free,
                    sanitizedUrl,
                    screenshot,
                    screenshotImage.getWidth(),
                    screenshotImage.getHeight(),
                    "",
                    SnapshotStore.Visibility.PRIVATE,
                    Optional.empty(),
                    ownerUserId);
        });
    }

    @Override
    public Map<String, LogEntries> getLogs() throws IOException {
        return executeAndReturn(() -> {
            Logs logs = driver.manage().logs();
            return logs.getAvailableLogTypes().stream()
                    .collect(ImmutableMap.toImmutableMap(t -> t, logs::get));
        });
    }

    @Override
    public void printLogsIfPossible() {
        if (config.enableVerboseCrashLogging()) {
            try {
                log.info("Collected logs:\n{}", getLogs().entrySet().stream()
                        .map(e -> e.getKey() + ":\n" + e.getValue().getAll().stream()
                                .map(LogEntry::toString)
                                .map(logLine -> "    " + logLine)
                                .collect(Collectors.joining("\n")))
                        .collect(Collectors.joining("\n")));
            } catch (Exception ex) {
                log.debug("Failed to print logs", ex);
            }
        }
    }

    private void execute(Runnable execution) {
        executeAndReturn(() -> {
            execution.run();
            return null;
        });
    }

    private final Retryer executionRetryer = RetryerBuilder.newBuilder()
            .retryIfExceptionOfType(UnhandledAlertException.class)
            .withStopStrategy(StopStrategies.stopAfterAttempt(3))
            .withRetryListener(new RetryListener() {
                @Override
                public <V> void onRetry(Attempt<V> attempt) {
                    if (attempt.hasException()) {
                        if (attempt.getExceptionCause() instanceof UnhandledAlertException) {
                            log.trace("Dismissing alert popup");
                            try {
                                driver.switchTo().alert().dismiss();
                            } catch (NoAlertPresentException ex) {
                                log.debug("Alert popup not present");
                            }
                        }
                    }
                }
            })
            .build();

    private <T> T executeAndReturn(Callable<T> execution) {
        driverLock.lock();
        try {
            try {
                return (T) executionRetryer.call(execution);
            } catch (ExecutionException | RetryException ex) {
                throw new RuntimeException(ex);
            }
        } finally {
            driverLock.unlock();
        }
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                install(ConfigSystem.configModule(Config.class));
                bind(Browser.class).to(BrowserImpl.class);
            }
        };
    }
}
