package com.smotana.veruv.resources;

import com.smotana.veruv.web.guard.SanitizerScreenDimensions;
import com.smotana.veruv.web.message.BrowserRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.websocket.CloseReason;
import java.io.IOException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public interface BrowserResourceManager {

    Resource requestResource(ResourceEventHandler handler,
                             String startUrl,
                             SanitizerScreenDimensions.ScreenDimensions screenDimensions,
                             String remoteAddr,
                             String clientHeaderXForwardedFor,
                             String clientHeaderForwarded,
                             Optional<String> userAgentOpt,
                             Optional<List<BrowserRequest.Cookie>> cookiesOpt,
                             Optional<List<BrowserRequest.LocalStorage>> localStorageOpt) throws IOException;

    void releaseResource(Resource resource);

    @Data
    @EqualsAndHashCode(of = {"id"})
    class Resource {
        private Instant lastActivity = Instant.now();
        private Optional<Instant> created = Optional.empty();
        private Optional<Instant> started = Optional.empty();
        private final AtomicBoolean isConnected = new AtomicBoolean(false);
        private final ResourceEventHandler handler;
        private final UUID id;
        private Optional<Browser> browserOpt = Optional.empty();
        private Optional<Future> browserTaskFuture = Optional.empty();
        private Optional<ResourceQueue.QueueTicket> queueTicketOpt = Optional.empty();

        public Browser getBrowser() {
            return browserOpt.get();
        }

        public void updateActivity() {
            lastActivity = Instant.now();
        }
    }

    enum NotifyType {
        SUCCESS("success"),
        INFO("info"),
        WARNING("warning"),
        ERROR("danger");
        /** Corresponds to bootstrap's bsStyle */
        private final String type;

        NotifyType(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }

    interface ResourceEventHandler {

        void onInQueue(long inQueueEstimateTime, TimeUnit unit, long usersInQueue);

        void onBrowserReady(Browser browser);

        void onBrowserReadyStateChanged(boolean isLoading);

        void onBrowserNavigated(String url);

        void onFrameRefresh(long xPos, long yPos, long width, long height, byte[] img, Optional<Long> newScreenWidth, Optional<Long> newScreenHeight);

        void onUserNotify(NotifyType type, String message);

        void onClose(CloseReason.CloseCode closeCode, String reasonPhrase);
    }
}
