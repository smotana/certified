package com.smotana.veruv.resources;

import java.util.concurrent.TimeUnit;

public interface ResourceQueue {

    QueueTicket addToQueue(EventHandler eventHandler);

    interface EventHandler {

        Priority getPriority();

        String getRemoteAddr();

        boolean isValid();

        void onInQueue(long inQueueEstimateTime, TimeUnit unit, long usersInQueue);

        void onReady();
    }

    enum Priority {
        HIGH,
        NORMAL,
    }

    interface QueueTicket {
        void release();
    }
}
