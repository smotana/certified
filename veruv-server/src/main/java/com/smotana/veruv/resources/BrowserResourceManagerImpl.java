package com.smotana.veruv.resources;

import com.google.common.collect.Maps;
import com.google.common.util.concurrent.*;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import com.kik.config.ice.ConfigSystem;
import com.smotana.veruv.core.AbstractVeruvModule;
import com.smotana.veruv.core.Global;
import com.smotana.veruv.docker.DockerManager;
import com.smotana.veruv.image.VideoEncoder;
import com.smotana.veruv.util.StockPile;
import com.smotana.veruv.util.TimeUtil;
import com.smotana.veruv.web.guard.SanitizerScreenDimensions;
import com.smotana.veruv.web.message.BrowserRequest;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.websocket.CloseReason;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.Future;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Slf4j
@Singleton
public class BrowserResourceManagerImpl extends AbstractIdleService implements BrowserResourceManager, GracefulShutdownHook {

    @Inject
    private Global.Config configGlobal;
    @Inject
    @Named("Browser")
    private StockPile.Config configBrowserStockPile;
    @Inject
    private BrowserTask.Config configBrowserTask;
    @Inject
    private Provider<Browser> browserFactory;
    @Inject
    private Provider<VideoEncoder> videoEncoderProvider;
    @Inject
    private ResourceQueue resourceQueue;
    @Inject
    private BrowserExpiry browserExpiry;
    @Inject
    private Provider<ServiceManager> serviceManagerProvider;

    private final Map<Resource, ResourceEventHandler> resources = Maps.newConcurrentMap();
    private final ListeningExecutorService executor = MoreExecutors.listeningDecorator(new ThreadPoolExecutor(
            2, Integer.MAX_VALUE, 60L, TimeUnit.SECONDS, new SynchronousQueue<>(),
            new ThreadFactoryBuilder().setNameFormat("BrowserResourceManagerImpl-worker-%d").build()));

    private StockPile<Browser> browserStockPile;

    @Override
    protected void startUp() throws Exception {
        browserStockPile = new StockPile<>("Browser", browserFactory::get, configBrowserStockPile, Optional.of(configGlobal::maxBrowserSessionsPerHost));
        serviceManagerProvider.get().addListener(new ServiceManager.Listener() {
            @Override
            public void healthy() {
                browserStockPile.startUp();
            }
        });
    }

    public void gracefulShutdown() {
        if (resources.isEmpty()) {
            return;
        }
        Duration gracefulShutdownLimit = configGlobal.gracefulShutdownLimit();
        TimeUtil.SingleUnitDuration shutdownLimitNotification = TimeUtil.largestUnitFloor(gracefulShutdownLimit);
        resources.forEach((resource, handler) -> handler.onUserNotify(NotifyType.WARNING,
                "Server is migrating, your browser session will have to close in "
                        + shutdownLimitNotification.getDuration() + " " + shutdownLimitNotification.getUnit()));
        long secondsSlept = 0;
        while (!resources.isEmpty() && secondsSlept < gracefulShutdownLimit.toSeconds()) {
            Uninterruptibles.sleepUninterruptibly(1, TimeUnit.SECONDS);
        }
    }

    @Override
    protected void shutDown() throws Exception {
        resources.forEach((resource, handler) -> handler.onClose(CloseReason.CloseCodes.GOING_AWAY, "Server shutting down"));
        resources.keySet().forEach(this::releaseResource);
        browserStockPile.shutDown();
        browserStockPile.drain().forEach(Browser::release);
        executor.shutdownNow();
        executor.awaitTermination(30, TimeUnit.SECONDS);
    }

    @Override
    public Resource requestResource(ResourceEventHandler handler,
                                    String startUrl,
                                    SanitizerScreenDimensions.ScreenDimensions screenDimensions,
                                    String remoteAddr,
                                    String clientHeaderXForwardedFor,
                                    String clientHeaderForwarded,
                                    Optional<String> userAgentOpt,
                                    Optional<List<BrowserRequest.Cookie>> cookiesOpt,
                                    Optional<List<BrowserRequest.LocalStorage>> localStorageOpt) throws IOException {
        // Register resource
        final Resource resource = new Resource(handler, UUID.randomUUID());
        resources.put(resource, handler);
        resource.getIsConnected().set(true);

        // Queue resource
        Runnable startResourceTask = startResourceBackgroundTask(
                resource,
                handler,
                startUrl,
                screenDimensions,
                remoteAddr,
                clientHeaderXForwardedFor,
                clientHeaderForwarded,
                userAgentOpt,
                cookiesOpt,
                localStorageOpt);
        // TODO set priority to high or normal based on paying customer or not
        QueueEventHandlerImpl queueItem = new QueueEventHandlerImpl(remoteAddr, ResourceQueue.Priority.NORMAL, resource, handler, startResourceTask);
        ResourceQueue.QueueTicket queueTicket = resourceQueue.addToQueue(queueItem);
        resource.setQueueTicketOpt(Optional.of(queueTicket));

        return resource;
    }

    private Runnable startResourceBackgroundTask(Resource resource,
                                                 ResourceEventHandler handler,
                                                 String startUrl,
                                                 SanitizerScreenDimensions.ScreenDimensions screenDimensions,
                                                 String remoteAddr,
                                                 String clientHeaderXForwardedFor,
                                                 String clientHeaderForwarded,
                                                 Optional<String> userAgentOpt,
                                                 Optional<List<BrowserRequest.Cookie>> cookiesOpt,
                                                 Optional<List<BrowserRequest.LocalStorage>> localStorageOpt) {
        return () -> {
            try {
                // Setup timer to kill resource on timeout
                resource.setCreated(Optional.of(Instant.now()));
                browserExpiry.watch(resource, handler);

                // Get and setup browser
                Browser browser = browserStockPile.get();
                resource.setBrowserOpt(Optional.of(browser));
                browser.setScreenDimensions(screenDimensions);
                browser.setup(
                        startUrl,
                        clientHeaderXForwardedFor,
                        clientHeaderForwarded,
                        remoteAddr,
                        userAgentOpt,
                        cookiesOpt,
                        localStorageOpt);

                // Update timer
                resource.updateActivity();
                resource.setStarted(Optional.of(Instant.now()));
                browserExpiry.runCheck();

                // Notify browser is ready
                handler.onBrowserReady(browser);

                // Start updates
                scheduleRefresh(resource, handler);
            } catch (Exception ex) {
                log.error("Failed to start browser", ex);
                resource.getBrowserOpt().ifPresent(Browser::printLogsIfPossible);
                handler.onClose(CloseReason.CloseCodes.TRY_AGAIN_LATER, "Failed to start browser");
                releaseResource(resource);
            }
        };
    }

    @Override
    public void releaseResource(Resource resource) {
        if (resources.remove(resource) != null) {
            browserExpiry.unwatch(resource);
            resource.getIsConnected().set(false);
            resource.getBrowserTaskFuture().ifPresent(f -> f.cancel(true));
            resource.getQueueTicketOpt().ifPresent(ResourceQueue.QueueTicket::release);
            resource.getBrowserOpt().ifPresent(b -> {
                b.release();
                browserStockPile.release();
            });
        }
    }

    private void scheduleRefresh(Resource resource, ResourceEventHandler handler) {
        Future<?> browserTaskFuture = executor.submit(new BrowserTask(
                configGlobal,
                configBrowserTask,
                resource,
                handler,
                videoEncoderProvider.get()));
        resource.setBrowserTaskFuture(Optional.of(browserTaskFuture));
    }

    @Data
    private class QueueEventHandlerImpl implements ResourceQueue.EventHandler {
        private final String remoteAddr;
        private final ResourceQueue.Priority priority;
        private final Resource resource;
        private final ResourceEventHandler handler;
        private final Runnable startResourceBackgroundTask;

        @Override
        public boolean isValid() {
            return resource.getIsConnected().get();
        }

        @Override
        public void onInQueue(long inQueueEstimateTime, TimeUnit unit, long usersInQueue) {
            handler.onInQueue(inQueueEstimateTime, unit, usersInQueue);
        }

        @Override
        public void onReady() {
            executor.execute(startResourceBackgroundTask);
        }
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                bind(GracefulShutdownHook.class).to(BrowserResourceManagerImpl.class).asEagerSingleton();
                install(ConfigSystem.configModule(BrowserTask.Config.class));
                install(ConfigSystem.configModule(StockPile.Config.class, Names.named("Browser")));
                bind(BrowserResourceManager.class).to(BrowserResourceManagerImpl.class).asEagerSingleton();
                addService(BrowserResourceManagerImpl.class);
                // To ensure services start in order, add dependency
                requireBinding(DockerManager.class);
            }
        };
    }
}
