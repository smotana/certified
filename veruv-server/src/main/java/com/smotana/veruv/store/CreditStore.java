package com.smotana.veruv.store;

import java.io.IOException;

public interface CreditStore {

    long getCreditsByUserId(String userId) throws IOException;

    void incrementCreditsForUserId(String userId, long creditsIncrement);

    /** @return false if insufficient credits */
    boolean attemptDecrementCreditsForUserId(String userId, long creditsDecrement);
}
