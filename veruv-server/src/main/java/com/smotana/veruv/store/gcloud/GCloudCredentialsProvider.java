package com.smotana.veruv.store.gcloud;

import com.google.auth.Credentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.common.base.Charsets;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.kik.config.ice.ConfigSystem;
import com.kik.config.ice.annotations.NoDefaultValue;
import com.smotana.veruv.core.AbstractVeruvModule;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;

import static com.google.common.base.Preconditions.checkArgument;

public class GCloudCredentialsProvider implements Provider<ServiceAccountCredentials> {

    public interface Config {
        @NoDefaultValue
        String credentialsJson();
    }

    @Inject
    private Config config;

    @Override
    @Singleton
    public ServiceAccountCredentials get() {
        String credentialsJson = config.credentialsJson();
        checkArgument(!Strings.isNullOrEmpty(credentialsJson), "Credentials JSON property not set in configuration");
        try (InputStream is = IOUtils.toInputStream(credentialsJson, Charsets.UTF_8)) {
            return ServiceAccountCredentials.fromStream(is);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                bind(ServiceAccountCredentials.class).toProvider(GCloudCredentialsProvider.class).asEagerSingleton();
                bind(Credentials.class).toProvider(GCloudCredentialsProvider.class).asEagerSingleton();
                install(ConfigSystem.configModule(Config.class));
            }
        };
    }
}
