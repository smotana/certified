package com.smotana.veruv.store;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.google.common.util.concurrent.AtomicLongMap;
import com.google.inject.Module;
import com.google.inject.Singleton;
import com.smotana.veruv.core.AbstractVeruvModule;
import com.smotana.veruv.util.PasswordUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.time.Instant;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static com.smotana.veruv.util.PasswordUtil.Type.USER;

/**
 * TODO Do not use! Use GCloudStore instead. For testing use with InMemoryStorageClient and Datastore emulator.
 */
@Slf4j
@Singleton
@Deprecated
public class InMemoryStore implements SnapshotRequestStore, CreditStore, SubscriptionStore {

    private final Map<String, SnapshotRequest> snapshotRequestsById = Maps.newConcurrentMap();
    private final AtomicLongMap<String> creditsByUserId = AtomicLongMap.create();
    private final Map<String, Subscription> subscriptionsById = Maps.newConcurrentMap();

    /*
     * SnapshotRequestStore
     */

    @Override
    public Optional<SnapshotRequest> getRequestById(String requestId) throws IOException {
        return Optional.ofNullable(snapshotRequestsById.get(requestId));
    }

    @Override
    public ImmutableList<SnapshotRequest> getRequestsByUser(String userId) throws IOException {
        return snapshotRequestsById.values().stream()
                .filter(sr -> sr.getUserIdOpt().isPresent() && sr.getUserIdOpt().get().equals(userId))
                .collect(ImmutableList.toImmutableList());
    }

    @Override
    public SnapshotRequest createRequest(String url,
                                         Optional<String> passUnhashedOpt,
                                         Optional<String> userIdOpt,
                                         Optional<String> noteOpt) throws IOException {
        synchronized (snapshotRequestsById) {
            SnapshotRequest snapshotRequest;
            boolean idCollision;
            do {
                String id = UUID.randomUUID().toString();
                Optional<String> passHashed = passUnhashedOpt.map(passUnhashed -> PasswordUtil.saltHashPassword(USER, passUnhashed, id));
                snapshotRequest = new SnapshotRequest(
                        id,
                        Instant.now(),
                        url,
                        passHashed,
                        userIdOpt,
                        noteOpt);
                idCollision = snapshotRequestsById.putIfAbsent(snapshotRequest.getId(), snapshotRequest) != null;
            } while (idCollision);
            return snapshotRequest;
        }
    }

    /*
     * CreditStore
     */

    @Override
    public long getCreditsByUserId(String userId) throws IOException {
        return creditsByUserId.get(userId);
    }

    @Override
    public void incrementCreditsForUserId(String userId, long creditsIncrement) {
        creditsByUserId.addAndGet(userId, creditsIncrement);
    }

    @Override
    public boolean attemptDecrementCreditsForUserId(String userId, long creditsDecrement) {
        long initialCredits = creditsByUserId.getAndUpdate(
                userId,
                credits -> credits >= creditsDecrement
                        ? credits - creditsDecrement
                        : credits);
        return initialCredits >= creditsDecrement;
    }

    /*
     * SubscriptionStore
     */

    @Override
    public Optional<Subscription> getSubscriptionById(String subscriptionId) throws IOException {
        return Optional.ofNullable(subscriptionsById.get(subscriptionId));
    }

    @Override
    public ImmutableList<Subscription> getSubscriptionsByUserId(String userId) throws IOException {
        return subscriptionsById.values().stream()
                .filter(s -> s.getUserId().equals(userId))
                .collect(ImmutableList.toImmutableList());
    }

    @Override
    public Subscription createSubscription(
            String userId,
            Instant created,
            Instant paidUntil,
            long allowedSessions,
            long price) throws IOException {
        synchronized (subscriptionsById) {
            Subscription subscription;
            boolean idCollision;
            do {
                subscription = new Subscription(
                        UUID.randomUUID().toString(),
                        userId,
                        true,
                        created,
                        paidUntil,
                        allowedSessions,
                        price);
                idCollision = subscriptionsById.putIfAbsent(subscription.getId(), subscription) != null;
            } while (idCollision);
            return subscription;
        }
    }

    @Override
    public void updatePaidUntilSubscription(String subscriptionId, Instant paidUntil) throws IOException {
        synchronized (subscriptionsById) {
            Subscription subscription = getSubscriptionById(subscriptionId).orElseThrow(() -> new IOException("Cannot find subscription by id " + subscriptionId));
            subscriptionsById.put(subscriptionId, new Subscription(
                    subscription.getId(),
                    subscription.getUserId(),
                    subscription.isAutoRenew(),
                    subscription.getCreated(),
                    paidUntil,
                    subscription.getAllowedSessions(),
                    subscription.getPrice()));
        }
    }

    @Override
    public void updateRenewalSubscription(String subscriptionId, boolean autoRenew) throws IOException {
        synchronized (subscriptionsById) {
            Subscription subscription = getSubscriptionById(subscriptionId).orElseThrow(() -> new IOException("Cannot find subscription by id " + subscriptionId));
            subscriptionsById.put(subscriptionId, new Subscription(
                    subscription.getId(),
                    subscription.getUserId(),
                    autoRenew,
                    subscription.getCreated(),
                    subscription.getPaidUntil(),
                    subscription.getAllowedSessions(),
                    subscription.getPrice()));
        }
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                bind(SnapshotRequestStore.class).to(InMemoryStore.class).asEagerSingleton();
                bind(CreditStore.class).to(InMemoryStore.class).asEagerSingleton();
                bind(SubscriptionStore.class).to(InMemoryStore.class).asEagerSingleton();
            }
        };
    }
}
