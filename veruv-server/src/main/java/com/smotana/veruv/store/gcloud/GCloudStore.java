package com.smotana.veruv.store.gcloud;

import com.github.rholder.retry.RetryException;
import com.github.rholder.retry.RetryerBuilder;
import com.github.rholder.retry.StopStrategies;
import com.github.rholder.retry.WaitStrategies;
import com.google.cloud.Timestamp;
import com.google.cloud.datastore.*;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.*;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterators;
import com.google.common.primitives.Longs;
import com.google.common.util.concurrent.AbstractIdleService;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.Singleton;
import com.kik.config.ice.ConfigSystem;
import com.kik.config.ice.annotations.DefaultValue;
import com.smotana.veruv.core.AbstractVeruvModule;
import com.smotana.veruv.store.SnapshotStore;
import com.smotana.veruv.store.UserStore;
import com.smotana.veruv.util.PasswordUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.text.RandomStringGenerator;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import static com.google.common.base.Preconditions.checkArgument;
import static com.smotana.veruv.store.gcloud.GCloudStore.KeyName.*;
import static com.smotana.veruv.util.PasswordUtil.Type.SNAPSHOT;
import static com.smotana.veruv.util.PasswordUtil.Type.USER;

// TODO complete this and add to injector modules
@Slf4j
@Singleton
public class GCloudStore extends AbstractIdleService implements SnapshotStore, UserStore {
    private static final String USER_KIND = "user";
    private static final String USER_EMAIL_KIND = "email";
    private static final String SNAPSHOT_KIND = "snap";

    public enum KeyName {
        // SNAPSHOT_KIND
        SNAPSHOT_CREATED("crt"),
        SNAPSHOT_EXPIRY("expt"),
        SNAPSHOT_URL("url"),
        SNAPSHOT_NOTE("note"),
        SNAPSHOT_VISIBILITY("vis"),
        SNAPSHOT_PASSWORD("spass"),
        SNAPSHOT_USER_ID("uid"),
        SNAPSHOT_STORAGE_TYPE("styp"),
        SNAPSHOT_STORAGE_ID("stid"),
        // SNAPSHOT Storage metadata
        SNAPSHOT_WIDTH("wdth"),
        SNAPSHOT_HEIGHT("hght"),
        // USER_KIND
        USER_EMAIL("email"),
        USER_PASS("pass");

        private final String key;

        KeyName(String key) {
            this.key = key;
        }

        public String getKey() {
            return this.key;
        }
    }

    public interface Config {
        @DefaultValue("6")
        int snapshotIdLengthMin();

        @DefaultValue("6")
        int snapshotIdLengthMax();

        @DefaultValue("com-smotana-veruv-snapshot-screenshot-free")
        String freeStorageBucketName();

        @DefaultValue("PT4H")
        Duration freeStorageBucketExpiry();

        @DefaultValue("com-smotana-veruv-snapshot-screenshot-regular")
        String regularStorageBucketName();

        @DefaultValue("P90D")
        Duration regularStorageBucketExpiry();
    }

    @Inject
    private Config config;
    @Inject
    private Datastore datastoreClient;
    @Inject
    private Storage storageClient;

    private final RandomStringGenerator shortIdGen = new RandomStringGenerator.Builder()
            .withinRange('0', 'z')
            .filteredBy(c -> (Character.isLetter(c) || Character.isDigit(c))
                    && c != 'l'
                    && c != 'I'
                    && c != 'O'
                    && c != '0')
            .build();
    private KeyFactory keyFactorySnapshot;
    private KeyFactory keyFactoryUser;
    private KeyFactory keyFactoryUserEmail;

    @Override
    protected void startUp() throws Exception {
        keyFactorySnapshot = datastoreClient.newKeyFactory().setKind(SNAPSHOT_KIND);
        keyFactoryUser = datastoreClient.newKeyFactory().setKind(USER_KIND);
        keyFactoryUserEmail = datastoreClient.newKeyFactory().setKind(USER_EMAIL_KIND);
    }

    @Override
    protected void shutDown() throws Exception {
    }

    // ** ** ** ** ** ** **
    // ** SnapshotStore **
    // ** ** ** ** ** **

    @Override
    public Optional<Snapshot> getSnapshotById(String snapshotId) throws IOException {
        Entity snapshotEntity = datastoreClient.get(keyFactorySnapshot.newKey(snapshotId));
        if (snapshotEntity == null) {
            return Optional.empty();
        }

        return getSnapshotByEntity(snapshotEntity);
    }

    @Override
    public ImmutableList<Snapshot> getSnapshotsByUser(String userId) throws IOException {
        try {
            QueryResults<Entity> snapshotEntities = datastoreClient.run(Query.newEntityQueryBuilder()
                    .setKind(SNAPSHOT_KIND)
                    .setFilter(StructuredQuery.PropertyFilter.eq(SNAPSHOT_USER_ID.getKey(), userId))
                    .build());
            ImmutableList.Builder<Snapshot> snapshotBuilder = ImmutableList.builder();
            while (snapshotEntities.hasNext()) {
                Entity snapshotEntity = snapshotEntities.next();

                getSnapshotByEntity(snapshotEntity).ifPresent(snapshotBuilder::add);
            }
            return snapshotBuilder.build();
        } catch (StorageException | DatastoreException ex) {
            throw new IOException(ex);
        }
    }

    @Override
    public void deleteSnapshot(String snapshotId) throws IOException {
        try {
            Entity snapshotEntity = datastoreClient.get(keyFactorySnapshot.newKey(snapshotId));
            if (snapshotEntity == null) {
                log.debug("Not deleting snapshot {}, doesn't exist", snapshotId);
                return;
            }

            StorageType storageType = StorageType.byId.get((int) snapshotEntity.getLong(SNAPSHOT_STORAGE_TYPE.getKey()));
            String storageId = getSnapshotStorageId(snapshotEntity);
            deleteSnapshot(snapshotId, storageType, storageId);
        } catch (StorageException | DatastoreException ex) {
            throw new IOException(ex);
        }
    }

    @Override
    public void deleteSnapshot(Snapshot snapshot) throws IOException {
        deleteSnapshot(snapshot.getId(), snapshot.getStorageType(), snapshot.getStorageId());
    }

    private void deleteSnapshot(String snapshotId, StorageType storageType, String storageId) throws IOException {
        try {
            log.debug("Deleting snapshot {} {} {}", snapshotId, storageType, storageId);
            datastoreClient.delete(keyFactorySnapshot.newKey(snapshotId));
            storageClient.delete(BlobId.of(getSnapshotBucketName(storageType), storageId));
        } catch (StorageException | DatastoreException ex) {
            throw new IOException(ex);
        }
    }

    @Data
    private class SnapshotIds {
        private final String storageId;
        private final String id;
    }

    @Override
    public Snapshot createSnapshot(StorageType type,
                                   String url,
                                   byte[] screenshot,
                                   int screenshotWidth,
                                   int screenshotHeight,
                                   String note,
                                   Visibility visibility,
                                   Optional<String> snapshotPassUnhashedOpt,
                                   Optional<String> userIdOpt) throws IOException {
        try {
            Instant created = Instant.now();
            Instant expiry = created.plus(getSnapshotBucketExpiry(type));

            SnapshotIds snapshotIds = RetryerBuilder.<SnapshotIds>newBuilder()
                    .retryIfException(ex -> ex instanceof DatastoreException
                            && "ALREADY_EXISTS".equals(((DatastoreException) ex).getReason()))
                    .withStopStrategy(StopStrategies.stopAfterAttempt(10))
                    .withWaitStrategy(WaitStrategies.noWait())
                    .build()
                    .call(() -> {
                        String sid = generateShortId();
                        String storageId = generateSnapshotStorageId(sid, created, Optional.empty());
                        Entity.Builder entityBuilder = Entity.newBuilder(keyFactorySnapshot.newKey(sid))
                                .set(SNAPSHOT_STORAGE_TYPE.getKey(), LongValue.newBuilder(type.getId()).setExcludeFromIndexes(true).build())
                                .set(SNAPSHOT_STORAGE_ID.getKey(), StringValue.newBuilder(storageId).setExcludeFromIndexes(false).build())
                                .set(SNAPSHOT_CREATED.getKey(), TimestampValue.newBuilder(Timestamp
                                        .ofTimeSecondsAndNanos(created.getEpochSecond(), created.getNano())).setExcludeFromIndexes(false).build())
                                .set(SNAPSHOT_EXPIRY.getKey(), TimestampValue.newBuilder(Timestamp
                                        .ofTimeSecondsAndNanos(expiry.getEpochSecond(), expiry.getNano())).setExcludeFromIndexes(true).build())
                                .set(SNAPSHOT_URL.getKey(), StringValue.newBuilder(url).setExcludeFromIndexes(true).build())
                                .set(SNAPSHOT_NOTE.getKey(), StringValue.newBuilder(note).setExcludeFromIndexes(true).build())
                                .set(SNAPSHOT_VISIBILITY.getKey(), LongValue.newBuilder(visibility.getId()).setExcludeFromIndexes(true).build());
                        snapshotPassUnhashedOpt.ifPresent(snapshotPassUnhashed -> {
                            String passHashed = PasswordUtil.saltHashPassword(SNAPSHOT, snapshotPassUnhashed, sid);
                            entityBuilder.set(SNAPSHOT_PASSWORD.getKey(), StringValue.newBuilder(passHashed).setExcludeFromIndexes(true).build());
                        });
                        userIdOpt.ifPresent(userId -> {
                            entityBuilder.set(SNAPSHOT_USER_ID.getKey(), StringValue.newBuilder(userId).setExcludeFromIndexes(false).build());
                        });
                        datastoreClient.add(entityBuilder.build());
                        return new SnapshotIds(storageId, sid);
                    });

            Optional<String> snapshotPassHashedOpt = snapshotPassUnhashedOpt.map(snapshotPassUnhashed -> PasswordUtil.saltHashPassword(SNAPSHOT, snapshotPassUnhashed, snapshotIds.getId()));
            storageClient.create(BlobInfo.newBuilder(BlobId.of(getSnapshotBucketName(type), snapshotIds.getStorageId()))
                            .setContentType("image/png")
                            .setMetadata(ImmutableMap.<String, String>builder()
                                    .put(SNAPSHOT_WIDTH.getKey(), String.valueOf(screenshotWidth))
                                    .put(SNAPSHOT_HEIGHT.getKey(), String.valueOf(screenshotHeight))
                                    .build())
                            .build(),
                    screenshot,
                    Storage.BlobTargetOption.doesNotExist());

            Snapshot snapshot = new Snapshot(
                    type,
                    snapshotIds.getStorageId(),
                    snapshotIds.getId(),
                    created,
                    expiry,
                    url,
                    screenshot,
                    screenshotWidth,
                    screenshotHeight,
                    note,
                    visibility,
                    snapshotPassHashedOpt,
                    userIdOpt);
            log.debug("Created snapshot {}", snapshot);
            return snapshot;
        } catch (StorageException | DatastoreException | ExecutionException | RetryException ex) {
            throw new IOException(ex);
        }
    }

    @Override
    public void editNote(String snapshotId, String note) throws IOException {
        try {
            Transaction transaction = datastoreClient.newTransaction();
            try {
                Entity snapshotEntity = transaction.get(keyFactorySnapshot.newKey(snapshotId));
                transaction.put(Entity.newBuilder(snapshotEntity)
                        .set(SNAPSHOT_NOTE.getKey(), StringValue.newBuilder(note).setExcludeFromIndexes(true).build())
                        .build());
                transaction.commit();
            } finally {
                if (transaction.isActive()) {
                    transaction.rollback();
                }
            }
        } catch (StorageException | DatastoreException ex) {
            throw new IOException(ex);
        }
    }

    @Override
    public void editScreenshot(Snapshot snapshot, StorageType storageType, byte[] screenshot, int screenshotWidth, int screenshotHeight) throws IOException {
        try {
            storageClient.create(BlobInfo.newBuilder(BlobId.of(getSnapshotBucketName(storageType), snapshot.getStorageId()))
                            .setContentType("image/png")
                            .setMetadata(ImmutableMap.<String, String>builder()
                                    .put(SNAPSHOT_WIDTH.getKey(), String.valueOf(screenshotWidth))
                                    .put(SNAPSHOT_HEIGHT.getKey(), String.valueOf(screenshotHeight))
                                    .build())
                            .build(),
                    screenshot);
        } catch (StorageException | DatastoreException ex) {
            throw new IOException(ex);
        }
    }

    @Override
    public void setSnapshotVisibility(String snapshotId, Visibility visibility, Optional<String> snapshotPassUnhashedOpt) throws IOException {
        checkArgument(visibility == Visibility.PASSWORD ^ !snapshotPassUnhashedOpt.isPresent());
        try {
            Transaction transaction = datastoreClient.newTransaction();
            try {
                Entity snapshotEntity = transaction.get(keyFactorySnapshot.newKey(snapshotId));

                Visibility prevVisibility = Visibility.byId.get((int) snapshotEntity.getLong(SNAPSHOT_VISIBILITY.getKey()));
                Entity.Builder entityBuilder = Entity.newBuilder(snapshotEntity)
                        .set(SNAPSHOT_VISIBILITY.getKey(), LongValue.newBuilder(visibility.getId()).setExcludeFromIndexes(true).build());
                if (visibility == Visibility.PASSWORD) {
                    String passHashed = PasswordUtil.saltHashPassword(SNAPSHOT, snapshotPassUnhashedOpt.get(), snapshotId);
                    entityBuilder.set(SNAPSHOT_PASSWORD.getKey(), StringValue.newBuilder(passHashed).setExcludeFromIndexes(true).build());
                } else if (prevVisibility == Visibility.PASSWORD) {
                    entityBuilder.remove(SNAPSHOT_PASSWORD.getKey());
                }

                transaction.put(entityBuilder.build());
                transaction.commit();
            } finally {
                if (transaction.isActive()) {
                    transaction.rollback();
                }
            }
        } catch (StorageException | DatastoreException ex) {
            throw new IOException(ex);
        }
    }

    @Override
    public void setSnapshotOwner(String snapshotId, Optional<String> userIdOpt) throws IOException {
        try {
            Transaction transaction = datastoreClient.newTransaction();
            try {
                Entity snapshotEntity = transaction.get(keyFactorySnapshot.newKey(snapshotId));
                Entity.Builder entityBuilder = Entity.newBuilder(snapshotEntity);
                if (userIdOpt.isPresent()) {
                    entityBuilder.set(SNAPSHOT_USER_ID.getKey(), StringValue.newBuilder(userIdOpt.get()).setExcludeFromIndexes(false).build());
                } else {
                    entityBuilder.remove(SNAPSHOT_USER_ID.getKey());
                }
                transaction.put(entityBuilder.build());
                transaction.commit();
            } finally {
                if (transaction.isActive()) {
                    transaction.rollback();
                }
            }
        } catch (StorageException | DatastoreException ex) {
            throw new IOException(ex);
        }
    }

    @Override
    public Instant setStorageType(String snapshotId, StorageType storageType) throws IOException {
        try {
            Transaction transaction = datastoreClient.newTransaction();
            try {
                Entity snapshotEntity = transaction.get(keyFactorySnapshot.newKey(snapshotId));

                StorageType prevStorageType = StorageType.byId.get((int) snapshotEntity.getLong(SNAPSHOT_STORAGE_TYPE.getKey()));
                Timestamp createdTs = snapshotEntity.getTimestamp(SNAPSHOT_CREATED.getKey());
                Instant created = Instant.ofEpochSecond(createdTs.getSeconds(), createdTs.getNanos());
                String prevStorageId = getSnapshotStorageId(snapshotEntity);
                String newStorageId = generateSnapshotStorageId(snapshotId, created, Optional.of(prevStorageId));

                Instant expiry = Instant.now().plus(getSnapshotBucketExpiry(storageType));
                storageClient.copy(Storage.CopyRequest.newBuilder()
                        .setSource(BlobId.of(getSnapshotBucketName(prevStorageType), prevStorageId))
                        .setTarget(BlobId.of(getSnapshotBucketName(storageType), newStorageId))
                        .build())
                        .getResult();

                transaction.put(Entity.newBuilder(snapshotEntity)
                        .set(SNAPSHOT_STORAGE_TYPE.getKey(), LongValue.newBuilder(storageType.getId()).setExcludeFromIndexes(true).build())
                        .set(SNAPSHOT_STORAGE_ID.getKey(), StringValue.newBuilder(newStorageId).setExcludeFromIndexes(false).build())
                        .set(SNAPSHOT_EXPIRY.getKey(), TimestampValue.newBuilder(Timestamp
                                .ofTimeSecondsAndNanos(expiry.getEpochSecond(), expiry.getNano())).setExcludeFromIndexes(true).build())
                        .build());
                transaction.commit();

                storageClient.delete(BlobId.of(getSnapshotBucketName(prevStorageType), snapshotId));

                return expiry;
            } finally {
                if (transaction.isActive()) {
                    transaction.rollback();
                }
            }
        } catch (StorageException | DatastoreException ex) {
            throw new IOException(ex);
        }
    }

    // ** ** ** ** **
    // ** UserStore **
    // ** ** ** ** ** **

    @Override
    public Optional<User> getUserById(String id) throws IOException {
        try {
            return Optional.ofNullable(datastoreClient.get(keyFactoryUser.newKey(id)))
                    .map(this::toUser);
        } catch (StorageException | DatastoreException ex) {
            throw new IOException(ex);
        }
    }

    @Override
    public Optional<User> getUserByEmail(String email) throws IOException {
        try {
            QueryResults<Entity> entities = datastoreClient.run(Query.newEntityQueryBuilder()
                    .setKind(USER_KIND)
                    .setFilter(StructuredQuery.PropertyFilter.eq(USER_EMAIL.getKey(), email))
                    .build());
            if (!entities.hasNext()) {
                return Optional.empty();
            }
            User user = toUser(entities.next());
            if (entities.hasNext()) {
                log.error("Found multiple users with same email {}: {} {}", email, user, Iterators.toString(entities));
            }
            return Optional.of(user);
        } catch (StorageException | DatastoreException ex) {
            throw new IOException(ex);
        }
    }

    @Override
    public String createUser(String email, String passUnhashed) throws IOException, EmailTakenException {
        try {
            String userId = UserStore.generateId();
            String passHashed = PasswordUtil.saltHashPassword(USER, passUnhashed, userId);

            Transaction transaction = datastoreClient.newTransaction();
            try {
                transaction.add(Entity.newBuilder(keyFactoryUserEmail.newKey(email)).build());
                transaction.add(Entity.newBuilder(keyFactoryUser.newKey(userId))
                        .set(USER_EMAIL.getKey(), email)
                        .set(USER_PASS.getKey(), StringValue.newBuilder(passHashed).setExcludeFromIndexes(true).build())
                        .build());
                transaction.commit();
            } finally {
                if (transaction.isActive()) {
                    transaction.rollback();
                }
            }
            return userId;
        } catch (StorageException | DatastoreException ex) {
            if ("ALREADY_EXISTS".equals(ex.getReason())) {
                throw new EmailTakenException(ex);
            } else {
                throw new IOException(ex);
            }
        }
    }

    @Override
    public void deleteUser(String id) throws IOException {
        try {
            datastoreClient.delete(keyFactoryUser.newKey(id));
        } catch (StorageException | DatastoreException ex) {
            throw new IOException(ex);
        }
    }

    @Override
    public void updateUserEmail(String id, String email) throws IOException, EmailTakenException {
        try {
            Transaction transaction = datastoreClient.newTransaction();
            try {
                Entity userEntity = transaction.get(keyFactoryUser.newKey(id));
                transaction.add(Entity.newBuilder(keyFactoryUserEmail.newKey(email)).build());
                transaction.delete(keyFactoryUserEmail.newKey(userEntity.getString(USER_EMAIL.getKey())));
                transaction.put(Entity.newBuilder(userEntity)
                        .set(USER_EMAIL.getKey(), email)
                        .build());
                transaction.commit();
            } finally {
                if (transaction.isActive()) {
                    transaction.rollback();
                }
            }
        } catch (StorageException | DatastoreException ex) {
            if ("ALREADY_EXISTS".equals(ex.getReason())) {
                throw new EmailTakenException(ex);
            } else {
                throw new IOException(ex);
            }
        }
    }

    @Override
    public void updateUserPassHash(String id, String passUnhashed) throws IOException {
        try {
            String passHashed = PasswordUtil.saltHashPassword(USER, passUnhashed, id);

            Transaction transaction = datastoreClient.newTransaction();
            try {
                Entity userEntity = transaction.get(keyFactoryUser.newKey(id));
                transaction.put(Entity.newBuilder(userEntity)
                        .set(USER_PASS.getKey(), StringValue.newBuilder(passHashed).setExcludeFromIndexes(true).build())
                        .build());
                transaction.commit();
            } finally {
                if (transaction.isActive()) {
                    transaction.rollback();
                }
            }
        } catch (StorageException | DatastoreException ex) {
            throw new IOException(ex);
        }
    }

    // ** ** ** ** ** ** ** **
    // **  Private methods  **
    // ** ** ** ** ** ** ** **

    private Optional<Snapshot> getSnapshotByEntity(Entity snapshotEntity) throws IOException {
        try {
            String snapshotId = snapshotEntity.getKey().getName();
            String storageId = getSnapshotStorageId(snapshotEntity);
            StorageType storageType = StorageType.byId.get((int) snapshotEntity.getLong(SNAPSHOT_STORAGE_TYPE.getKey()));
            Timestamp expiryTs = snapshotEntity.getTimestamp(SNAPSHOT_EXPIRY.getKey());
            Instant expiry = Instant.ofEpochSecond(expiryTs.getSeconds(), expiryTs.getNanos());
            BlobId blobId = BlobId.of(getSnapshotBucketName(storageType), storageId);

            if (Instant.now().isAfter(expiry)) {
                storageClient.delete(blobId);
                datastoreClient.delete(keyFactorySnapshot.newKey(snapshotId));
                log.debug("Deleting expired snapshot {}", snapshotId);
                return Optional.empty();
            }

            Blob snapshotBlob = storageClient.get(blobId);
            if (snapshotBlob == null) {
                // TODO This can happen if storage type is being changed and an old value from Datastore links to the now deleted Storage path
                log.error("Storage snapshot object missing before expiry, sid {} type {}", snapshotId, storageType);
                return Optional.empty();
            }

            return Optional.of(toSnapshot(snapshotId, snapshotEntity, snapshotBlob));
        } catch (StorageException | DatastoreException ex) {
            throw new IOException(ex);
        }
    }

    private String getSnapshotBucketName(StorageType type) {
        switch (type) {
            case Free:
                return config.freeStorageBucketName();
            case Regular:
                return config.regularStorageBucketName();
            default:
                throw new RuntimeException("Cannot determine bucket name, unknown StorageType " + type);
        }
    }

    public Duration getSnapshotBucketExpiry(StorageType type) {
        switch (type) {
            case Free:
                return config.freeStorageBucketExpiry();
            case Regular:
                return config.regularStorageBucketExpiry();
            default:
                throw new RuntimeException("Cannot determine bucket expiry, unknown StorageType " + type);
        }
    }

    private Snapshot toSnapshot(String snapshotId, Entity datastoreEntity, Blob storageBlob) {
        Timestamp createdTs = datastoreEntity.getTimestamp(SNAPSHOT_CREATED.getKey());
        Instant created = Instant.ofEpochSecond(createdTs.getSeconds(), createdTs.getNanos());
        Timestamp expiryTs = datastoreEntity.getTimestamp(SNAPSHOT_EXPIRY.getKey());
        Instant expiry = Instant.ofEpochSecond(expiryTs.getSeconds(), expiryTs.getNanos());
        return new Snapshot(
                StorageType.byId.get((int) datastoreEntity.getLong(SNAPSHOT_STORAGE_TYPE.getKey())),
                getSnapshotStorageId(datastoreEntity),
                snapshotId,
                created,
                expiry,
                datastoreEntity.getString(SNAPSHOT_URL.getKey()),
                storageBlob.getContent(),
                Integer.parseInt(storageBlob.getMetadata().get(SNAPSHOT_WIDTH.getKey())),
                Integer.parseInt(storageBlob.getMetadata().get(SNAPSHOT_HEIGHT.getKey())),
                datastoreEntity.getString(SNAPSHOT_NOTE.getKey()),
                Visibility.byId.get((int) datastoreEntity.getLong(SNAPSHOT_VISIBILITY.getKey())),
                datastoreEntity.contains(SNAPSHOT_PASSWORD.getKey())
                        ? Optional.of(datastoreEntity.getString(SNAPSHOT_PASSWORD.getKey()))
                        : Optional.empty(),
                datastoreEntity.contains(SNAPSHOT_USER_ID.getKey())
                        ? Optional.of(datastoreEntity.getString(SNAPSHOT_USER_ID.getKey()))
                        : Optional.empty());
    }

    private User toUser(Entity entity) {
        return new User(entity.getKey().getNameOrId().toString(),
                entity.getString(USER_EMAIL.getKey()),
                entity.getString(USER_PASS.getKey()));
    }

    private String generateShortId() {
        return shortIdGen.generate(config.snapshotIdLengthMin(), config.snapshotIdLengthMax());
    }

    private DateTimeFormatter snapshotStorageIdDatetimeFormatter = DateTimeFormatter
            .ofPattern("yyyyMMddHHmmss")
            .withZone(ZoneOffset.UTC);

    private String generateSnapshotStorageId(String sid, Instant created, Optional<String> previousStorageId) {
        if (!previousStorageId.isPresent()) {
            return snapshotStorageIdDatetimeFormatter.format(created) + "-" + sid + "-v1";
        }
        String[] split = previousStorageId.get().split("-v");
        Long version = null;
        if (split.length == 2) {
            version = Longs.tryParse(split[1]);
        }
        if (version == null) {
            version = 1L;
        }
        String newStorageId;
        do {
            newStorageId = snapshotStorageIdDatetimeFormatter.format(created) + "-" + sid + "-v" + version;
            version++;
        } while (newStorageId.equals(previousStorageId.get()));
        return newStorageId;
    }

    String getSnapshotStorageId(Entity snapshotEntity) {
        try {
            return snapshotEntity.getString(SNAPSHOT_STORAGE_ID.getKey());
        } catch (DatastoreException ex) {
            if (ex.getCode() != 0
                    || !"FAILED_PRECONDITION".equals(ex.getReason())
                    || !ex.getMessage().startsWith("No such property")) {
                throw ex;
            }
        }

        // TODO this is only for transition period to fallback if SNAPSHOT_STORAGE_ID is not present. Should be good to remove 90 days after this line is committed.
        String snapshotId = snapshotEntity.getKey().getName();
        return snapshotId;
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                install(ConfigSystem.configModule(Config.class));
                bind(SnapshotStore.class).to(GCloudStore.class).asEagerSingleton();
                bind(UserStore.class).to(GCloudStore.class).asEagerSingleton();
                addService(GCloudStore.class);
            }
        };
    }
}
