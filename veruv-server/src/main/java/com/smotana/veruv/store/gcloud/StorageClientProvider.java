package com.smotana.veruv.store.gcloud;

import com.google.auth.Credentials;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.kik.config.ice.ConfigSystem;
import com.kik.config.ice.annotations.NoDefaultValue;
import com.smotana.veruv.core.AbstractVeruvModule;
import com.smotana.veruv.gcloud.CachedMetadataConfig;

import static com.smotana.veruv.gcloud.GCloudGlobal.RETRY_SETTINGS;

public class StorageClientProvider implements Provider<Storage> {

    public interface Config {
        @NoDefaultValue
        String host();
    }

    @Inject
    private Config config;
    @Inject
    private CachedMetadataConfig metadataConfig;
    @Inject
    private Credentials credentials;

    @Override
    @Singleton
    public Storage get() {
        StorageOptions.Builder builder = StorageOptions.newBuilder();
        builder.setCredentials(credentials);
        builder.setRetrySettings(RETRY_SETTINGS);
        if (!Strings.isNullOrEmpty(config.host())) {
            builder.setHost(config.host());
        }
        if (!Strings.isNullOrEmpty(metadataConfig.getProjectId())) {
            builder.setProjectId(metadataConfig.getProjectId());
        }
        return builder.build().getService();
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                bind(Storage.class).toProvider(StorageClientProvider.class).asEagerSingleton();
                install(ConfigSystem.configModule(Config.class));
            }
        };
    }
}
