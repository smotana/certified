package com.smotana.veruv.store;

import lombok.Data;
import lombok.ToString;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

public interface UserStore {
    String USER_PASS_SALT = ":salt:B611425D-8542-419D-A48A-50D50CB66659:salt:";
    String SNAPSHOT_REQUEST_PASS_SALT = ":salt:98ABE3B7-21A2-4DA1-9E9B-B93E0993FD7B:salt:";

    Optional<User> getUserById(String id) throws IOException;

    Optional<User> getUserByEmail(String email) throws IOException;

    /** Returns new user id */
    String createUser(String email, String passUnhashed) throws IOException, EmailTakenException;

    void deleteUser(String id) throws IOException;

    void updateUserEmail(String id, String email) throws IOException, EmailTakenException;

    void updateUserPassHash(String id, String passUnhashed) throws IOException;

    static String generateId() {
        byte[] randomBytes = new byte[16];
        ThreadLocalRandom.current().nextBytes(randomBytes);
        return java.util.Base64.getUrlEncoder().withoutPadding().encodeToString(randomBytes);
    }

    // TODO add registration IP
    @Data
    class User {
        private final String id;
        private final String email;
        @ToString.Exclude
        private final String passHash;
    }

    class EmailTakenException extends Exception {

        public EmailTakenException() {
            super();
        }

        public EmailTakenException(Throwable cause) {
            super(cause);
        }
    }
}
