package com.smotana.veruv.store;

import com.google.common.collect.ImmutableList;
import lombok.Data;

import java.io.IOException;
import java.time.Instant;
import java.util.Optional;

public interface SnapshotRequestStore {

    Optional<SnapshotRequest> getRequestById(String requestId) throws IOException;

    ImmutableList<SnapshotRequest> getRequestsByUser(String userId) throws IOException;

    SnapshotRequest createRequest(String url,
                                  Optional<String> passUnhashedOpt,
                                  Optional<String> userIdOpt,
                                  Optional<String> noteOpt) throws IOException;

    @Data
    class SnapshotRequest {
        private final String id;
        private final Instant created;
        private final String url;
        private final Optional<String> passHashOpt;
        private final Optional<String> userIdOpt;
        private final Optional<String> note;
    }
}
