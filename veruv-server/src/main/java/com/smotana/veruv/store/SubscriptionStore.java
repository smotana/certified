package com.smotana.veruv.store;

import com.google.common.collect.ImmutableList;
import lombok.Data;

import java.io.IOException;
import java.time.Instant;
import java.util.Optional;

public interface SubscriptionStore {

    Optional<Subscription> getSubscriptionById(String subscriptionId) throws IOException;

    ImmutableList<Subscription> getSubscriptionsByUserId(String userId) throws IOException;

    Subscription createSubscription(
            String userId,
            Instant created,
            Instant paidUntil,
            long allowedSessions,
            long price) throws IOException;

    void updatePaidUntilSubscription(String subscriptionId, Instant paidUntil) throws IOException;

    void updateRenewalSubscription(String subscriptionId, boolean autoRenew) throws IOException;

    @Data
    class Subscription {
        private final String id;
        private final String userId;
        private final boolean autoRenew;
        private final Instant created;
        private final Instant paidUntil;
        private final long allowedSessions;
        private final long price;
    }
}
