package com.smotana.veruv.store;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.smotana.veruv.util.SerializableEnum;
import lombok.Data;
import lombok.ToString;

import java.io.IOException;
import java.time.Instant;
import java.util.Optional;

public interface SnapshotStore {

    Optional<Snapshot> getSnapshotById(String snapshotId) throws IOException;

    ImmutableList<Snapshot> getSnapshotsByUser(String userId) throws IOException;

    /**
     * Prefer to use deleteSnapshot(Snapshot) method if you already have a Snapshot instance to prevent an additional
     * read.
     */
    void deleteSnapshot(String snapshotId) throws IOException;

    void deleteSnapshot(Snapshot snapshot) throws IOException;

    Snapshot createSnapshot(
            StorageType type,
            String url,
            byte[] screenshot,
            int screenshotWidth,
            int screenshotHeight,
            String note,
            Visibility visibility,
            Optional<String> snapshotPassUnhashedOpt,
            Optional<String> userIdOpt) throws IOException;

    void editNote(String snapshotId, String note) throws IOException;

    void editScreenshot(Snapshot snapshot, StorageType storageType, byte[] screenshot, int screenshotWidth, int screenshotHeight) throws IOException;

    void setSnapshotVisibility(String snapshotId, Visibility visibility, Optional<String> snapshotPassUnhashedOpt) throws IOException;

    void setSnapshotOwner(String snapshotId, Optional<String> userIdOpt) throws IOException;

    /**
     * If current storage type differs, moves the storage type to the supplied type, resetting expiry.
     * If current storage type is the same, refreshes the expiry.
     *
     * @return new Expiry time
     */
    Instant setStorageType(String snapshotId, StorageType type) throws IOException;

    enum StorageType implements SerializableEnum {
        Free(1),
        Regular(2);

        public static final ImmutableMap<Integer, StorageType> byId = SerializableEnum.getMapper(StorageType.class);
        private final int id;

        StorageType(int id) {
            this.id = id;
        }

        @Override
        public int getId() {
            return id;
        }
    }

    enum Visibility implements SerializableEnum {
        PUBLIC(1),
        PRIVATE(2),
        PASSWORD(3);

        public static final ImmutableMap<Integer, Visibility> byId = SerializableEnum.getMapper(Visibility.class);
        private final int id;

        Visibility(int id) {
            this.id = id;
        }

        @Override
        public int getId() {
            return id;
        }
    }

    // TODO add IP
    @Data
    class Snapshot {
        private final StorageType storageType;
        private final String storageId;
        private final String id;
        private final Instant created;
        private final Instant expiry;
        private final String url;
        @ToString.Exclude
        private final byte[] screenshot;
        private final int screenshotWidth;
        private final int screenshotHeight;
        private final String note;
        private final Visibility visibility;
        private final Optional<String> passHashedOpt;
        private final Optional<String> userIdOpt;
    }
}
