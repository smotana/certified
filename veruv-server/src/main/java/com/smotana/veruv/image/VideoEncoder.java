package com.smotana.veruv.image;

import lombok.Data;

import java.io.IOException;
import java.util.Optional;

public interface VideoEncoder {
    Optional<Frame> calcFrame(byte[] nextImage) throws IOException;

    @Data
    class Frame {
        private final long x;
        private final long y;
        private final long width;
        private final long height;
        private final byte[] img;
        private final long screenWidth;
        private final long screenHeight;
    }
}
