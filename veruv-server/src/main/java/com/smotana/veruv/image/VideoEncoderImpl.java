package com.smotana.veruv.image;

import com.google.inject.Inject;
import com.google.inject.Module;
import com.smotana.veruv.core.AbstractVeruvModule;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Optional;

public class VideoEncoderImpl implements VideoEncoder {
    private static final int TRANSPARENT_RGBA = new Color(0xFF, 0xFF, 0xFF, 0).getRGB();
    @Inject
    private ImageEditor imageEditor;

    private BufferedImage prevImage = null;

    @Override
    public Optional<Frame> calcFrame(byte[] nextImgBytes) throws IOException {
        BufferedImage nextImg = imageEditor.toImage(nextImgBytes);

        if (prevImage == null
                || prevImage.getHeight() != nextImg.getHeight()
                || prevImage.getWidth() != nextImg.getWidth()) {
            prevImage = nextImg;
            return Optional.of(new Frame(
                    0, 0,
                    nextImg.getWidth(), nextImg.getHeight(),
                    nextImgBytes,
                    nextImg.getWidth(),
                    nextImg.getHeight()));
        } else {
            Optional<Frame> frame = diff(prevImage, nextImg);
            prevImage = nextImg;
            return frame;
        }
    }

    private Optional<Frame> diff(BufferedImage prev, BufferedImage next) throws IOException {
        BufferedImage dest = new BufferedImage(next.getWidth(), next.getHeight(), next.getType());
        boolean changed = false;
        int minX = Integer.MAX_VALUE;
        int minY = Integer.MAX_VALUE;
        int maxX = Integer.MIN_VALUE;
        int maxY = Integer.MIN_VALUE;
        for (int x = 0; x < next.getWidth(); x++) {
            for (int y = 0; y < next.getHeight(); y++) {
                int nextPixelRgb = next.getRGB(x, y);
                if (prev.getWidth() <= x || prev.getHeight() <= y
                        || prev.getRGB(x, y) != nextPixelRgb) {
                    dest.setRGB(x, y, nextPixelRgb);
                    changed = true;
                    minX = Math.min(minX, x);
                    minY = Math.min(minY, y);
                    maxX = Math.max(maxX, x);
                    maxY = Math.max(maxY, y);
                } else {
                    dest.setRGB(x, y, TRANSPARENT_RGBA);
                }
            }
        }
        if (!changed) {
            return Optional.empty();
        }
        int x = minX;
        int y = minY;
        int width = maxX - minX + 1;
        int height = maxY - minY + 1;

        BufferedImage croppedDest = dest.getSubimage(minX, minY, width, height);
        return Optional.of(new Frame(
                x, y,
                croppedDest.getWidth(), croppedDest.getHeight(),
                imageEditor.toBytes(croppedDest),
                next.getWidth(),
                next.getHeight()));
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                bind(VideoEncoder.class).to(VideoEncoderImpl.class);
            }
        };
    }
}
