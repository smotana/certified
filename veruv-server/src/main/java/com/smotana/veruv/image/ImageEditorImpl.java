package com.smotana.veruv.image;

import com.google.inject.Module;
import com.smotana.veruv.core.AbstractVeruvModule;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ImageEditorImpl implements ImageEditor {

    @Override
    public BufferedImage toImage(byte[] imgBytes) throws IOException {
        try (ByteArrayInputStream bais = new ByteArrayInputStream(imgBytes)) {
            return ImageIO.read(bais);
        }
    }

    @Override
    public byte[] toBytes(BufferedImage img) throws IOException {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ImageIO.write(img, "png", baos);
            return baos.toByteArray();
        }
    }

    @Override
    public BufferedImage edit(BufferedImage image, Edits edits) throws IOException {
        if (!edits.getRedactions().isEmpty()) {
            Graphics2D graph = image.createGraphics();
            graph.setColor(Color.BLACK);
            for (Rectangle redaction : edits.getRedactions()) {
                graph.fill(new Rectangle(
                        redaction.x,
                        redaction.y,
                        redaction.width,
                        redaction.height));
            }
            graph.dispose();
        }

        if (edits.getCrop().isPresent()) {
            Rectangle crop = edits.getCrop().get();
            image = image.getSubimage(crop.x, crop.y, crop.width, crop.height);
        }

        return image;
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                bind(ImageEditor.class).to(ImageEditorImpl.class);
            }
        };
    }
}
