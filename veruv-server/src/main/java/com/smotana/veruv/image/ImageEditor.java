package com.smotana.veruv.image;

import com.google.common.collect.ImmutableList;
import lombok.Data;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Optional;

public interface ImageEditor {

    BufferedImage toImage(byte[] imgBytes) throws IOException;

    byte[] toBytes(BufferedImage img) throws IOException;

    BufferedImage edit(BufferedImage imageBytes, Edits edits) throws IOException;

    @Data
    class Edits {
        private final Optional<Rectangle> crop;
        private final ImmutableList<Rectangle> redactions;
    }
}
