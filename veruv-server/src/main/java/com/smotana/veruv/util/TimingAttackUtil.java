package com.smotana.veruv.util;

import com.google.common.util.concurrent.Uninterruptibles;
import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.kik.config.ice.ConfigSystem;
import com.kik.config.ice.annotations.DefaultValue;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

@Slf4j
@Singleton
public class TimingAttackUtil {

    public interface CriticalSection {
        void end();
    }

    public interface Config {
        /**
         * To thwart timing attacks, each critical auth operation will take this amount of time.
         */
        @DefaultValue("PT0.1S")
        Duration authActionConstantTime();
    }

    @Inject
    private Config config;


    public CriticalSection start() {
        final Instant start = Instant.now();
        return () -> end(start);
    }

    private void end(Instant start) {
        long sleepTime = Duration.between(Instant.now(), start.plus(config.authActionConstantTime())).toMillis();
        if (sleepTime > 0) {
            Uninterruptibles.sleepUninterruptibly(sleepTime, TimeUnit.MILLISECONDS);
        } else {
            if (LogUtil.rateLimitAllowLog("criticalSectionOvershot")) {
                log.error("Critical section overshot by {}ms, increase authActionConstantTime", sleepTime, new Exception());
            }
        }
    }


    public static Module module() {
        return new AbstractModule() {
            @Override
            protected void configure() {
                bind(TimingAttackUtil.class).asEagerSingleton();
                install(ConfigSystem.configModule(Config.class));
            }
        };
    }
}
