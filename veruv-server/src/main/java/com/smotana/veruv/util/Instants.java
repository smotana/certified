package com.smotana.veruv.util;

import java.time.Instant;

public class Instants {

    private Instants() {
        // Disallow ctor
    }

    public static Instant min(Instant left, Instant right) {
        if (left == null) {
            return right;
        } else if (right == null) {
            return left;
        }
        return left.isBefore(right) ? left : right;
    }

    public static Instant max(Instant left, Instant right) {
        if (left == null) {
            return right;
        } else if (right == null) {
            return left;
        }
        return left.isAfter(right) ? left : right;
    }
}
