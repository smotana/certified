package com.smotana.veruv.util;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PasswordUtil {

    /**
     * Warning: changing the salt will result in everyone's passwords to be invalid
     */
    public enum Type {
        USER(":salt:B611425D-8542-419D-A48A-50D50CB66659:salt:"),
        SNAPSHOT(":salt:065CF1C1-2E43-41FC-B7EB-AC28AC5FDD79:salt:"),
        SNAPSHOT_REQUEST(":salt:98ABE3B7-21A2-4DA1-9E9B-B93E0993FD7B:salt:");

        private String salt;

        Type(String salt) {
            this.salt = salt;
        }

        private String getSalt() {
            return salt;
        }
    }

    private PasswordUtil() {
        // Disallow ctor
    }

    /**
     * Warning: changing this method will result in everyone's passwords to be invalid
     */
    public static String saltHashPassword(Type type, String pass, String id) {
        return Hashing.sha512().hashString(id + type.getSalt() + pass, Charsets.UTF_8).toString();
    }
}
