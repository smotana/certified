package com.smotana.veruv.util;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Queues;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.google.common.util.concurrent.Uninterruptibles;
import com.kik.config.ice.annotations.DefaultValue;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Supplier;

@Slf4j
public class StockPile<E> {

    public interface Config {
        /** Max resources includes released starting and stocked products. 0 = unlimited */
        @DefaultValue("0")
        long maxSize();

        /** Stock size to keep on hand. */
        @DefaultValue("0")
        long stockSize();

        @DefaultValue("PT30S")
        Duration retryTime();
    }

    private final Config config;
    private final ReentrantLock locker = new ReentrantLock();
    private final String name;
    private final Supplier<E> factory;
    private final AtomicLong runningResources = new AtomicLong(0L);
    private final LinkedBlockingQueue<E> stock = Queues.newLinkedBlockingQueue();
    private final ExecutorService executor;
    private final Supplier<Long> maxSizeSupplier;
    private AtomicLong starting = new AtomicLong(0);

    public StockPile(String name, Supplier<E> factory, Config config, Optional<Supplier<Long>> maxSizeOverrideOpt) {
        this.name = name;
        this.factory = factory;
        this.config = config;
        this.executor = Executors.newCachedThreadPool(new ThreadFactoryBuilder()
                .setNameFormat("StockPile-" + name + "-filler").build());
        this.maxSizeSupplier = maxSizeOverrideOpt.orElse(config::maxSize);
    }

    public void startUp() {
        checkStock(false);
    }

    public void shutDown() throws Exception {
        executor.shutdownNow();
        executor.awaitTermination(1, TimeUnit.MINUTES);
    }

    public ImmutableList<E> drain() {
        ArrayList<E> bucket = Lists.newArrayList();
        stock.drainTo(bucket);
        return ImmutableList.copyOf(bucket);
    }

    public E get() {
        log.trace("({}) Requesting product", name);
        E product = stock.poll();
        if (product == null) {
            executor.execute(() -> checkStock(true));
            product = Uninterruptibles.takeUninterruptibly(stock);
        }
        runningResources.incrementAndGet();
        executor.execute(() -> checkStock(false));
        return product;
    }

    public void release() {
        log.trace("({}) Releasing product", name);
        runningResources.decrementAndGet();
        executor.execute(() -> checkStock(false));
    }

    private void checkStock(boolean needOneRightNow) {
        final long createStockCount;
        locker.lock();
        try {
            final long desiredSize = config.stockSize();
            final long maxRunningSize = maxSizeSupplier.get();
            final long stockSize = stock.size();
            final long startingCount = starting.get();
            final long currentRunningSize = stockSize + runningResources.get() + startingCount;

            createStockCount = Math.min(
                    maxRunningSize <= 0 ? Long.MAX_VALUE : (maxRunningSize - currentRunningSize),
                    (needOneRightNow ? 1 : 0) + desiredSize - stockSize);

            log.trace("({}) Stock filler creating {} products, running {}/{} stockSize {}/{} needOneRightNow {}",
                    name, createStockCount, currentRunningSize, maxRunningSize, stockSize, desiredSize, needOneRightNow);
            starting.addAndGet(createStockCount);
        } finally {
            locker.unlock();
        }
        for (int i = 0; i < createStockCount; i++) {
            executor.execute(this::addOne);
        }
    }

    private void addOne() {
        try {
            E product = factory.get();
            locker.lock();
            try {
                stock.put(product);
                starting.decrementAndGet();
            } finally {
                locker.unlock();
            }
            log.trace("({}) Finished adding single stock", name);
        } catch (Exception ex) {
            Duration retryTime = config.retryTime();
            log.warn("({}) Failed to create product, retrying again in {}", name, retryTime, ex);
            Uninterruptibles.sleepUninterruptibly(retryTime.toMillis(), TimeUnit.MILLISECONDS);
            executor.execute(this::addOne);
        }
    }
}
