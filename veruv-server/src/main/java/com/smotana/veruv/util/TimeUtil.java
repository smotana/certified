package com.smotana.veruv.util;

import com.google.common.collect.ImmutableList;
import lombok.Data;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class TimeUtil {
    private static final ImmutableList<TimeUnit> TIMEUNITS_DECR_ORDER = ImmutableList.of(
            TimeUnit.DAYS,
            TimeUnit.HOURS,
            TimeUnit.MINUTES,
            TimeUnit.SECONDS,
            TimeUnit.MILLISECONDS,
            TimeUnit.MICROSECONDS,
            TimeUnit.NANOSECONDS
    );

    private TimeUtil() {
        // Disallow ctor
    }

    /**
     * Gets the largest truncated timeunit possible from a duration.
     * Example:
     * PT1M4S -> 1 TimeUnit.MINUTES
     * P4DT3M4S -> 4 TimeUnit.DAYS
     */
    public static SingleUnitDuration largestUnitFloor(Duration duration) {
        long part;

        // DAYS
        part = duration.toDaysPart();
        if (part > 0) {
            return new SingleUnitDuration(part, TimeUnit.DAYS);
        }

        // HOURS
        part = duration.toHoursPart();
        if (part > 0) {
            return new SingleUnitDuration(part, TimeUnit.HOURS);
        }

        // MINUTES
        part = duration.toMinutesPart();
        if (part > 0) {
            return new SingleUnitDuration(part, TimeUnit.MINUTES);
        }

        // SECONDS
        part = duration.toSecondsPart();
        if (part > 0) {
            return new SingleUnitDuration(part, TimeUnit.SECONDS);
        }

        // MILLISECONDS
        part = duration.toMillisPart();
        if (part > 0) {
            return new SingleUnitDuration(part, TimeUnit.MILLISECONDS);
        }

        // NANOSECONDS
        part = duration.toNanosPart();
        if (part > 0) {
            return new SingleUnitDuration(part, TimeUnit.NANOSECONDS);
        }

        return new SingleUnitDuration(0, TimeUnit.SECONDS);
    }

    public static SingleUnitDuration largestUnitCeil(Duration duration) {
        SingleUnitDuration floorLargestUnit = largestUnitFloor(duration);
        if (duration.compareTo(Duration.of(floorLargestUnit.getDuration(), floorLargestUnit.getUnit().toChronoUnit())) != 0) {
            return new SingleUnitDuration(floorLargestUnit.duration + 1, floorLargestUnit.unit);
        } else {
            return floorLargestUnit;
        }
    }

    @Data
    public static class SingleUnitDuration {
        private final long duration;
        private final TimeUnit unit;
    }
}
