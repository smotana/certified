package com.smotana.veruv.gcloud;

import com.google.cloud.MetadataConfig;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.Singleton;
import com.smotana.veruv.core.AbstractVeruvModule;
import com.smotana.veruv.core.VeruvInjector.Environment;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

@Slf4j
@Singleton
public class CachedMetadataConfig {

    @Inject
    private Environment env;

    private final Cache<String, String> cache = CacheBuilder.<String, String>newBuilder().build();

    public String getProjectId() {
        return get("getProjectId", MetadataConfig::getProjectId, "com-smotana-veruv-dev");
    }

    public String getZone() {
        return get("getZone", MetadataConfig::getZone, "local");
    }

    public String getInstanceId() {
        return get("getInstanceId", MetadataConfig::getInstanceId, "1");
    }

    public String getClusterName() {
        return get("getClusterName", MetadataConfig::getClusterName, "clustername");
    }

    public String getContainerName() {
        return get("getContainerName", MetadataConfig::getContainerName, "containername");
    }

    public String getNamespaceId() {
        return get("getNamespaceId", MetadataConfig::getNamespaceId, "namespace");
    }

    private String get(String identifier, Callable<String> loader, String devValue) {
        if (!env.isProduction()) {
            return devValue;
        }
        try {
            return cache.get(identifier, loader);
        } catch (ExecutionException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                bind(CachedMetadataConfig.class).asEagerSingleton();
            }
        };
    }
}
