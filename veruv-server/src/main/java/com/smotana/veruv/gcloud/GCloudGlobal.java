package com.smotana.veruv.gcloud;

import com.google.api.gax.retrying.RetrySettings;
import org.threeten.bp.Duration;

public class GCloudGlobal {
    public static final RetrySettings RETRY_SETTINGS = RetrySettings.newBuilder()
            .setMaxAttempts(6)
            .setTotalTimeout(Duration.ofMillis(30_000L))
            .setJittered(true)
            .setInitialRetryDelay(Duration.ofMillis(100L))
            .setMaxRetryDelay(Duration.ofMillis(3_000L))
            .setRetryDelayMultiplier(2.0)
            .setInitialRpcTimeout(Duration.ofMillis(50_000L))
            .setRpcTimeoutMultiplier(1.0)
            .setMaxRpcTimeout(Duration.ofMillis(50_000L))
            .build();
}
