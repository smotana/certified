package com.smotana.veruv.monitor.gcloud;

import com.google.api.MetricDescriptor;
import com.google.api.core.ApiFuture;
import com.google.api.core.SettableApiFuture;
import com.google.api.gax.rpc.ApiCallContext;
import com.google.api.gax.rpc.UnaryCallable;
import com.google.cloud.monitoring.v3.MetricServiceClient;
import com.google.cloud.monitoring.v3.stub.MetricServiceStub;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.Singleton;
import com.google.monitoring.v3.CreateMetricDescriptorRequest;
import com.google.monitoring.v3.CreateTimeSeriesRequest;
import com.google.protobuf.Empty;
import com.smotana.veruv.core.AbstractVeruvModule;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Slf4j
@Singleton
public class ConsoleMetricsClient extends MetricServiceClient {

    @Inject
    public ConsoleMetricsClient() {
        super(new MetricServiceStub() {

            public UnaryCallable<CreateTimeSeriesRequest, Empty> createTimeSeriesCallable() {
                return new UnaryCallable<>() {
                    @Override
                    public ApiFuture<Empty> futureCall(CreateTimeSeriesRequest request, ApiCallContext context) {
                        log.info("Emitted metrics:\n{}", request.getTimeSeriesList().stream()
                                .map(series -> series.getMetric().getType() + "=" + series.getPoints(0).getValue().getStringValue())
                                .collect(Collectors.joining("\n")));
                        SettableApiFuture<Empty> f = SettableApiFuture.create();
                        f.set(Empty.getDefaultInstance());
                        return f;
                    }
                };
            }

            public UnaryCallable<CreateMetricDescriptorRequest, MetricDescriptor> createMetricDescriptorCallable() {
                return new UnaryCallable<>() {
                    @Override
                    public ApiFuture<MetricDescriptor> futureCall(CreateMetricDescriptorRequest request, ApiCallContext context) {
                        log.info("Registered metric: {}", request.getMetricDescriptor().getType());
                        SettableApiFuture<MetricDescriptor> f = SettableApiFuture.create();
                        f.set(request.getMetricDescriptor());
                        return f;
                    }
                };
            }

            @Override
            public void shutdown() {
                throw new UnsupportedOperationException();
            }

            @Override
            public boolean isShutdown() {
                throw new UnsupportedOperationException();
            }

            @Override
            public boolean isTerminated() {
                throw new UnsupportedOperationException();
            }

            @Override
            public void shutdownNow() {
                throw new UnsupportedOperationException();
            }

            @Override
            public boolean awaitTermination(long duration, TimeUnit unit) throws InterruptedException {
                throw new UnsupportedOperationException();
            }

            @Override
            public void close() {
                throw new UnsupportedOperationException();
            }
        });
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                bind(MetricServiceClient.class).to(ConsoleMetricsClient.class).asEagerSingleton();
            }
        };
    }
}
