package com.smotana.veruv.monitor.gcloud;

import com.google.auth.Credentials;
import com.google.cloud.monitoring.v3.MetricServiceClient;
import com.google.cloud.monitoring.v3.MetricServiceSettings;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.smotana.veruv.core.AbstractVeruvModule;

import java.io.IOException;

public class GCloudMetricsClientProvider implements Provider<MetricServiceClient> {

    @Inject
    private Provider<Credentials> credentialsProvider;

    @Override
    @Singleton
    public MetricServiceClient get() {
        try {
            return MetricServiceClient.create(MetricServiceSettings.newBuilder()
                    .setCredentialsProvider(credentialsProvider::get)
                    .build());
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                bind(MetricServiceClient.class).toProvider(GCloudMetricsClientProvider.class).asEagerSingleton();
            }
        };
    }
}
