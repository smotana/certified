package com.smotana.veruv.monitor.gcloud;

import com.google.api.Metric;
import com.google.api.MetricDescriptor;
import com.google.api.MonitoredResource;
import com.google.cloud.monitoring.v3.MetricServiceClient;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.util.concurrent.AbstractScheduledService;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.Singleton;
import com.google.monitoring.v3.*;
import com.google.protobuf.util.Timestamps;
import com.smotana.veruv.core.AbstractVeruvModule;
import com.smotana.veruv.gcloud.CachedMetadataConfig;
import com.smotana.veruv.monitor.Metrics;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import static com.google.common.base.Preconditions.checkArgument;

@Slf4j
@Singleton
public class GCloudMetrics extends AbstractScheduledService implements Metrics {

    @Inject
    private CachedMetadataConfig metadataConfig;
    @Inject
    private MetricServiceClient client;

    private final MonitoredResource monitoredResource;
    private Map<String, Supplier<TypedValue>> metrics = Maps.newConcurrentMap();

    @Inject
    private GCloudMetrics(CachedMetadataConfig metadataConfig) {
        monitoredResource = MonitoredResource.newBuilder()
                .setType("gce_instance")
                .putAllLabels(ImmutableMap.of(
                        "instance_id", metadataConfig.getInstanceId(),
                        "zone", metadataConfig.getZone()))
                .build();
    }

    @Override
    protected Scheduler scheduler() {
        return Scheduler.newFixedRateSchedule(0, 1, TimeUnit.MINUTES);
    }

    @Override
    protected void runOneIteration() throws Exception {
        try {
            log.trace("Submitting metrics {}", metrics.keySet());

            ImmutableList.Builder<TimeSeries> timeSeriesBuilder = ImmutableList.builder();
            metrics.forEach((type, valueSupplier) -> {
                long ts = System.currentTimeMillis();
                TypedValue val = valueSupplier.get();
                timeSeriesBuilder.add(TimeSeries.newBuilder()
                        .setResource(monitoredResource)
                        .setMetric(Metric.newBuilder()
                                .setType(type)
                                .build())
                        .addAllPoints(ImmutableList.of(Point.newBuilder()
                                .setInterval(TimeInterval.newBuilder()
                                        .setEndTime(Timestamps.fromMillis(ts))
                                        .build())
                                .setValue(val)
                                .build()))
                        .build());
            });

            client.createTimeSeries(CreateTimeSeriesRequest.newBuilder()
                    .setName(ProjectName.of(metadataConfig.getProjectId()).toString())
                    .addAllTimeSeries(timeSeriesBuilder.build())
                    .build());
        } catch (Exception ex) {
            log.error("Metrics scheduled thread failure", ex);
        }
    }

    @Override
    public void addGauge(String path, String description, Supplier<TypedValue> valueSupplier) {
        checkArgument(path.startsWith("/"), "Metric path must start with forward slash");
        checkArgument(!path.startsWith("custom.googleapis.com/"), "Metric path already includes prefix custom.googleapis.com");
        log.debug("Adding metric '{}' description '{}'", path, description);
        CreateMetricDescriptorRequest request = CreateMetricDescriptorRequest.newBuilder()
                .setName(ProjectName.of(metadataConfig.getProjectId()).toString())
                .setMetricDescriptor(MetricDescriptor.newBuilder()
                        .setType("custom.googleapis.com/com/smotana/veruv" + path)
                        .setDescription(description)
                        .setMetricKind(MetricDescriptor.MetricKind.GAUGE)
                        .setValueType(MetricDescriptor.ValueType.DOUBLE)
                        .build())
                .build();
        client.createMetricDescriptor(request);
        metrics.put(request.getMetricDescriptor().getType(), valueSupplier);
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                bind(Metrics.class).to(GCloudMetrics.class).asEagerSingleton();
                addService(GCloudMetrics.class);
            }
        };
    }
}
