package com.smotana.veruv.monitor;

import com.google.monitoring.v3.TypedValue;

import java.util.function.Supplier;

// TODO finish implementation
public interface Metrics {
    void addGauge(String path, String description, Supplier<TypedValue> valueSupplier);
}
