package com.smotana.veruv.docker;

import com.google.common.base.Predicates;
import com.google.common.collect.*;
import com.google.common.util.concurrent.AbstractIdleService;
import com.google.common.util.concurrent.ServiceManager;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import com.kik.config.ice.ConfigSystem;
import com.kik.config.ice.annotations.DefaultValue;
import com.smotana.veruv.core.AbstractVeruvModule;
import com.smotana.veruv.core.Global;
import com.smotana.veruv.util.NetworkUtil;
import com.smotana.veruv.util.StockPile;
import com.smotana.veruv.web.guard.SanitizerScreenDimensions;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.exceptions.ContainerNotFoundException;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.*;
import lombok.extern.slf4j.Slf4j;
import rx.Observable;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
@Singleton
public class DockerManagerImpl extends AbstractIdleService implements DockerManager {
    /** Used by iptables */
    private static final String DOCKER_GATEWAY = "172.28.0.1";
    /** Used by iptables */
    private static final String DOCKER_BROWSER_RANGE = "172.28.48.0/24";
    /** Used by iptables */
    private static final String DOCKER_RANGE = "172.28.0.0/16";
    /** Used by iptables */
    private static final String DOCKER_WEBSERVER_IP = "172.28.48.0";
    private static final String NETWORK_NAME = "veruv_net";

    public interface Config {
        @DefaultValue("1073741824")
        long dockerMemorySize();

        @DefaultValue("1073741824")
        long dockerSharedMemorySize();

        @DefaultValue("veruv-browser-")
        String instanceNamePrefix();

        @DefaultValue("us.gcr.io/com-smotana-veruv/veruv-browser")
        String imageName();

        @DefaultValue("us.gcr.io/com-smotana-veruv/veruv-browser-debug")
        String imageNameDebug();

        @DefaultValue("true")
        boolean enableCleaner();

        Observable<Boolean> enableCleanerObservable();

        @DefaultValue("false")
        boolean enableSeleniumDebugLogging();

        @DefaultValue("true")
        boolean connectSelfToDockerNetwork();
    }

    @Inject
    private Config config;
    @Inject
    private Global.Config configGlobal;
    @Inject
    @Named("DockerInstance")
    private StockPile.Config configDockerInstanceStockPile;
    @Inject
    private DockerClient client;
    @Inject
    private Gson gson;
    @Inject
    private Provider<ServiceManager> serviceManagerProvider;

    private StockPile<DockerInstance> dockerStockPile;

    private final AtomicReference<Optional<ScheduledExecutorService>> executorRef = new AtomicReference<>(Optional.empty());
    private String seccompJson;
    private String networkId;
    private Set<DockerInstance> instances = Sets.newConcurrentHashSet();

    @Override
    protected void startUp() throws Exception {
        client.ping();

        config.enableCleanerObservable().subscribe(this::startCleaner);
        if (config.enableCleaner()) {
            clean();
            startCleaner(true);
        }

        String seccompJsonFilePath = getClass().getClassLoader().getResource("seccomp-chrome.json").getPath();
        try (FileReader fr = new FileReader(seccompJsonFilePath);
             JsonReader jr = new JsonReader(fr)) {
            Object jsonObj = gson.fromJson(jr, Object.class);
            seccompJson = gson.toJson(jsonObj);
        }

        Optional<Network> prevNetwork = client.listNetworks().stream().filter(n -> n.name().equals(NETWORK_NAME))
                .findAny();
        if (prevNetwork.isPresent()) {
            if (config.connectSelfToDockerNetwork() && prevNetwork.get().containers().containsKey(getSelfContainerId())) {
                client.disconnectFromNetwork(getSelfContainerId(), prevNetwork.get().id(), true);
            }
            client.removeNetwork(prevNetwork.get().id());
        }
        NetworkCreation network = client.createNetwork(NetworkConfig.builder()
                .name(NETWORK_NAME)
                .driver("bridge")
                .options(ImmutableMap.of(
                        "com.docker.network.bridge.host_binding_ipv4", "127.0.0.1",
                        "com.docker.network.bridge.enable_icc", "true",
                        "com.docker.network.bridge.enable_ip_masquerade", "true"))
                .internal(false)
                // TODO enable ipv6 and populate ipam
                .enableIPv6(false)
                .checkDuplicate(true)
                .attachable(true)
                .ipam(Ipam.builder()
                        .driver("default")
                        .config(ImmutableList.of(IpamConfig.create(DOCKER_RANGE, DOCKER_BROWSER_RANGE, DOCKER_GATEWAY)))
                        .build())
                .build());
        if (network.warnings() != null) {
            log.warn("Network creation warning: {}", network.warnings());
        }
        networkId = network.id();
        if (config.connectSelfToDockerNetwork()) {
            String selfContainerId = getSelfContainerId();
            log.info("Connecting self {} to selenium network {}", selfContainerId, networkId);
            client.connectToNetwork(networkId, NetworkConnection.builder()
                    .containerId(selfContainerId)
                    .endpointConfig(EndpointConfig.builder()
                            .ipAddress(DOCKER_WEBSERVER_IP)
                            .ipPrefixLen(24)
                            .build())
                    .build());
        }

        dockerStockPile = new StockPile<>("DockerInstance", this::createContainer, configDockerInstanceStockPile, Optional.of(configGlobal::maxBrowserSessionsPerHost));
        serviceManagerProvider.get().addListener(new ServiceManager.Listener() {
            @Override
            public void healthy() {
                dockerStockPile.startUp();
            }
        });
    }

    @Override
    protected void shutDown() throws Exception {
        executorRef.get().ifPresent(ExecutorService::shutdownNow);
        dockerStockPile.shutDown();
        ImmutableList<DockerInstance> stockedInstances = dockerStockPile.drain();
        Iterator<DockerInstance> runningInstancesIterator = Iterators.concat(instances.iterator(), stockedInstances.iterator());
        while (runningInstancesIterator.hasNext()) {
            DockerInstance instance = runningInstancesIterator.next();
            killContainer(instance);
        }
        instances.clear();

        if (config.connectSelfToDockerNetwork()) {
            client.disconnectFromNetwork(getSelfContainerId(), networkId, true);
        }
    }

    @Override
    public DockerInstance getContainer() throws IOException {
        return dockerStockPile.get();
    }

    @Override
    public void killContainer(DockerInstance instance) {
        if (instances.remove(instance)) {
            this.killContainer(instance.getDockerId());
            dockerStockPile.release();
        }
    }

    public DockerInstance createContainer() {
        ContainerCreation container = null;
        try {
            String name = config.instanceNamePrefix() + UUID.randomUUID();
            log.info("Creating container {}", name);
            String imageName = configGlobal.enableBrowserDebug() ? config.imageNameDebug() : config.imageName();
            ContainerConfig.Builder containerConfigBuilder = ContainerConfig.builder();
            Optional<ImmutableMap<String, List<PortBinding>>> portBindingsOpt = Optional.empty();
            if (configGlobal.enableBrowserDebug()) {
                int vncPort = NetworkUtil.findRandomFreePort(49152, 61000);
                containerConfigBuilder.exposedPorts("5900/tcp");
                portBindingsOpt = Optional.of(ImmutableMap.of(
                        "5900/tcp",
                        Collections.singletonList(PortBinding.of("0.0.0.0", vncPort))));
                log.info("Assigning vnc port {} for container {}", vncPort, name);
            }
            containerConfigBuilder.hostConfig(HostConfig.builder()
                    // Required for re-enabling chrome sandbox https://github.com/jessfraz/dotfiles/blob/master/etc/docker/seccomp/chrome.json
                    .securityOpt("seccomp=" + seccompJson)
                    .networkMode(networkId)
                    .dns("8.8.8.8", "8.8.4.4")
                    .nanoCpus(1_000_000_000L)
                    .memory(config.dockerMemorySize())
                    // If set to same value as memory, disables swap
                    .memorySwap(config.dockerMemorySize())
                    .shmSize(config.dockerSharedMemorySize())
                    .oomKillDisable(false)
                    // TODO paying customers should have a lower oom score
                    .oomScoreAdj(1000)
                    .capDrop("ALL")
                    .portBindings(portBindingsOpt.orElse(null))
                    .build());
            // TODO health check doesn't work, do we really need it? (docker doesnt recognize it)
//                    containerConfigBuilder.healthcheck(ContainerConfig.Healthcheck.builder()
//                            .interval(TimeUnit.SECONDS.toNanos(5))
//                            .timeout(TimeUnit.SECONDS.toMicros(3))
//                            .retries(2)
//                            .test(ImmutableList.of("/opt/bin/check-grid.sh"))
//                            .build())
            containerConfigBuilder.image(imageName);
            containerConfigBuilder.env(// And because GRID_* NODE_* env are not working properly, use SE_OPTS:
                    "SE_OPTS="
                            /**
                             * <Integer> in seconds : Specifies the timeout before the server
                             *       automatically kills a session that hasn't had any activity in the last X
                             *       seconds. The test slot will then be released for another test to use.
                             *       This is typically used to take care of client crashes. For grid hub/node
                             *       roles, cleanUpCycle must also be set. If a node does not specify it, the
                             *       hub value will be used.
                             */
                            // Allows us to have a container created for long periods of time
                            // There is no such thing as disabling, need to set the value to really really high
                            // Selenium parses it as a signed integer
                            + " -sessionTimeout " + Integer.MAX_VALUE
                            /**
                             * <Integer> in seconds : number of seconds a browser session is allowed to
                             *       hang while a WebDriver command is running (example: driver.get(url)). If
                             *       the timeout is reached while a WebDriver command is still processing,
                             *       the session will quit. Minimum value is 60. An unspecified, zero, or
                             *       negative value means wait indefinitely. If a node does not specify it,
                             *       the hub value will be used.
                             */
                            + " -browserTimeout 0"
                            + (configGlobal.enableBrowserDebug() || config.enableSeleniumDebugLogging() ? " -debug" : ""),
                    "SCREEN_WIDTH=" + SanitizerScreenDimensions.BROWSER_WIDTH_MAX,
                    "SCREEN_HEIGHT=" + SanitizerScreenDimensions.BROWSER_HEIGHT_MAX,
                    "SCREEN_DEPTH=" + SanitizerScreenDimensions.RESOLUTION_DEPTH);
            container = client.createContainer(containerConfigBuilder.build(), name);
            ImmutableList<String> warnings = container.warnings();
            if (warnings != null && !warnings.isEmpty()) {
                for (String warning : warnings) {
                    log.warn("Container creation warning: {}, image {} id {}",
                            warning, imageName, container.id());
                }
            }
            ContainerInfo containerInfo = client.inspectContainer(container.id());
            String hostname = containerInfo.config().hostname();
            DockerInstance dockerInstance = new DockerInstance(container.id(), hostname, 4444);
            log.info("Created container, starting now, hostname {} image name {} id {}",
                    hostname, imageName, dockerInstance.getDockerId());

            client.startContainer(dockerInstance.getDockerId());

            log.info("Started container, hostname {} id {}", hostname, dockerInstance.getDockerId());

            instances.add(dockerInstance);
            return dockerInstance;
        } catch (Exception ex) {
            if (container != null) {
                killContainer(container.id());
            }
            log.error("Failed to start container", ex);
            throw new RuntimeException(ex);
        }
    }

    public void killContainer(String dockerId) {
        try {
            log.info("Killing container with id {}", dockerId);
            client.killContainer(dockerId);
        } catch (ContainerNotFoundException ex) {
            // Nothing to kill
        } catch (InterruptedException | DockerException ex) {
            log.error("Failed to kill container {}", dockerId, ex);
        }
        try {
            client.removeContainer(dockerId);
        } catch (ContainerNotFoundException ex) {
            // Nothing to remove
        } catch (InterruptedException | DockerException ex) {
            log.error("Failed to remove container, possible leak of {}", dockerId, ex);
        }
    }

    private void startCleaner(boolean enableCleaner) {
        Optional<ScheduledExecutorService> executorOpt = Optional.empty();
        if (enableCleaner) {
            executorOpt = Optional.of(Executors.newSingleThreadScheduledExecutor(new ThreadFactoryBuilder()
                    .setNameFormat("DockerManager-cleaner").build()));
            executorOpt.get().schedule(this::clean, 10, TimeUnit.MINUTES);
        }
        Optional<ScheduledExecutorService> prevExecutor = executorRef.getAndSet(executorOpt);
        prevExecutor.ifPresent(ExecutorService::shutdownNow);
    }

    private void clean() {
        try {
            List<Container> containers = client.listContainers(DockerClient.ListContainersParam.withStatusRunning());
            log.trace("Running containers: {}", containers);
            ImmutableSet<String> used = instances.stream().map(DockerInstance::getDockerId).collect(ImmutableSet.toImmutableSet());
            log.trace("Used containers: {}", used);
            containers.stream()
                    .filter(c -> c.names().stream().anyMatch(name -> name.startsWith("/" + config.instanceNamePrefix())))
                    .filter(c -> Instant.now().minus(1, ChronoUnit.MINUTES).isAfter(Instant.ofEpochMilli(c.created())))
                    .map(Container::id)
                    .filter(Predicates.not(used::contains))
                    .forEach(this::killContainer);
        } catch (Exception ex) {
            log.warn("Cleaner caught exception", ex);
        }
    }

    private static String getSelfContainerId() {
        try (FileReader fr = new FileReader("/proc/self/cgroup");
             BufferedReader br = new BufferedReader(fr)) {
            String line;
            while ((line = br.readLine()) != null) {
                // Expecting something like: "13:name=systemd:/docker/a647b4d1cf619b873695f2f6c6e06bac55a09fd5173eba18afc1c00c00670e65"

                if (!line.contains("docker")) {
                    continue;
                }

                String[] lineSplit = line.split("/", 3);
                if (lineSplit.length != 3) {
                    continue;
                }

                return lineSplit[2];
            }
        } catch (FileNotFoundException ex) {
            log.error("Not running inside a container");
            throw new RuntimeException(ex);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
        throw new RuntimeException("Cannot determine our own container id: Unexpected content of /proc/self/cgroup");
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                install(ConfigSystem.configModule(Config.class));
                install(ConfigSystem.configModule(StockPile.Config.class, Names.named("DockerInstance")));
                bind(DockerManager.class).to(DockerManagerImpl.class).asEagerSingleton();
                addService(DockerManagerImpl.class);
            }
        };
    }
}
