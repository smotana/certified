package com.smotana.veruv.docker;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.Singleton;
import com.kik.config.ice.ConfigSystem;
import com.kik.config.ice.annotations.DefaultValue;
import com.kik.config.ice.annotations.NoDefaultValue;
import com.smotana.veruv.core.AbstractVeruvModule;
import com.smotana.veruv.core.Global;
import com.smotana.veruv.resources.extension.InjectHeaderExtension;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import rx.Observable;

import java.util.logging.Level;
import java.util.stream.Stream;


@Slf4j
@Singleton
public class BrowserCapabilitiesFactory {
    public interface Config {
        @NoDefaultValue
        String browserProxy();

        Observable<String> browserProxyObservable();

        @DefaultValue("Mozilla/5.0 (Veruv/2.0; +https://veruv.com; X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36")
        String userAgent();

        Observable<String> userAgentObservable();

        @DefaultValue("about:blank")
        String homePage();

        Observable<String> homePageObservable();
    }

    @Inject
    private Config config;
    @Inject
    private Global.Config configGlobal;
    @Inject
    private InjectHeaderExtension injectHeaderExtension;

    private volatile Capabilities capabilities;

    @Inject
    protected void setup() {
        Stream.of(
                config.browserProxyObservable(),
                config.userAgentObservable(),
                config.homePageObservable())
                .forEach(o -> o.subscribe(v -> {
                    capabilities = createCapabilities();
                }));
        capabilities = createCapabilities();
    }

    public Capabilities getCapabilities() {
        return capabilities;
    }

    private Capabilities createCapabilities() {
        ChromeOptions chromeOptions = injectHeaderExtension.optionsWithExtension();
        // See various arguments https://chromium.googlesource.com/chromium/src/+/master/chrome/common/chrome_switches.cc
        chromeOptions
                .addArguments("--kiosk")
                .addArguments("--allow-file-access-from-files=false")
                .addArguments("--disable-infobars")
                .addArguments("--user-agent=" + config.userAgent())
                .addArguments("--homepage=" + config.homePage())
                .setPageLoadStrategy(PageLoadStrategy.NONE)
                .setUnhandledPromptBehaviour(UnexpectedAlertBehaviour.DISMISS)
                .setExperimentalOption("prefs", ImmutableMap.of(
                        "credentials_enable_service", false,
                        "profile.password_manager_enabled", false
                ));
        String browserProxy = config.browserProxy();
        if (!Strings.isNullOrEmpty(browserProxy)) {
            chromeOptions.setCapability(CapabilityType.PROXY, new Proxy()
                    .setHttpProxy(browserProxy)
                    .setFtpProxy(browserProxy)
                    .setSslProxy(browserProxy)
                    .setNoProxy(""));
        }
        if (configGlobal.enableBrowserDebug()) {
            LoggingPreferences loggingPrefs = new LoggingPreferences();
            loggingPrefs.enable(LogType.CLIENT, Level.ALL);
            loggingPrefs.enable(LogType.DRIVER, Level.ALL);
            loggingPrefs.enable(LogType.PROFILER, Level.ALL);
            loggingPrefs.enable(LogType.PERFORMANCE, Level.ALL);
            loggingPrefs.enable(LogType.BROWSER, Level.ALL);
            loggingPrefs.enable(LogType.SERVER, Level.ALL);
            chromeOptions.setCapability(CapabilityType.LOGGING_PREFS, loggingPrefs);
        }
        return new DesiredCapabilities(chromeOptions);
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                install(ConfigSystem.configModule(Config.class));
                bind(BrowserCapabilitiesFactory.class).asEagerSingleton();
                addService(DockerManagerImpl.class);
            }
        };
    }
}
