package com.smotana.veruv.docker;

import java.io.IOException;

public interface DockerManager {

    DockerInstance getContainer() throws IOException;

    void killContainer(DockerInstance instance);
}
