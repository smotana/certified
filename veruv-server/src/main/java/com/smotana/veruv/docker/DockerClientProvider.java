package com.smotana.veruv.docker;

import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.kik.config.ice.ConfigSystem;
import com.kik.config.ice.annotations.NoDefaultValue;
import com.smotana.veruv.core.AbstractVeruvModule;
import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;

public class DockerClientProvider implements Provider<DockerClient> {

    public interface Config {
        @NoDefaultValue
        String dockerServerUri();
    }

    @Inject
    private Config config;

    @Override
    @Singleton
    public DockerClient get() {
        return DefaultDockerClient.builder().uri(config.dockerServerUri()).build();
    }

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                bind(DockerClient.class).toProvider(DockerClientProvider.class).asEagerSingleton();
                install(ConfigSystem.configModule(Config.class));
            }
        };
    }
}
