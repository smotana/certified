package com.smotana.veruv.docker;

import lombok.Data;

@Data
public class DockerInstance {
    private final String dockerId;
    private final String host;
    private final long seleniumPort;
}
