package com.google.common.util.concurrent;

import static com.google.common.base.Preconditions.checkArgument;

public class RateLimiters {

    public static RateLimiter createFull(double permitsPerSecond, double maxBurstSeconds) {
        SmoothRateLimiter.SmoothBursty rateLimiter = new SmoothRateLimiter.SmoothBursty(
                RateLimiter.SleepingStopwatch.createFromSystemTimer(),
                maxBurstSeconds);
        rateLimiter.setRate(permitsPerSecond);
        rateLimiter.storedPermits = rateLimiter.maxPermits;
        return rateLimiter;
    }

    public static long requiredWaitTime(RateLimiter rateLimiter, int requiredPermits) {
        if (rateLimiter.tryAcquire(requiredPermits)) {
            return 0L;
        }

        checkArgument(rateLimiter instanceof SmoothRateLimiter);
        SmoothRateLimiter smoothRateLimiter = (SmoothRateLimiter) rateLimiter;

        double storedPermitsToSpend = Math.min(requiredPermits, smoothRateLimiter.storedPermits);
        double freshPermits = requiredPermits - storedPermitsToSpend;
        long waitMicros = smoothRateLimiter.storedPermitsToWaitTime(smoothRateLimiter.storedPermits, storedPermitsToSpend)
                + (long) (freshPermits * smoothRateLimiter.stableIntervalMicros);
        return (long) Math.ceil(waitMicros / 1000d);
    }
}
