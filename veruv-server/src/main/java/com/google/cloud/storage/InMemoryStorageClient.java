package com.google.cloud.storage;

import com.google.api.gax.paging.Page;
import com.google.cloud.Policy;
import com.google.cloud.ReadChannel;
import com.google.cloud.WriteChannel;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.Singleton;
import com.smotana.veruv.core.AbstractVeruvModule;
import com.smotana.veruv.core.VeruvInjector;
import lombok.extern.slf4j.Slf4j;

import java.io.InputStream;
import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static com.google.cloud.storage.spi.v1.StorageRpc.Option.IF_GENERATION_MATCH;
import static com.google.common.base.Preconditions.checkState;

@Slf4j
@Singleton
public class InMemoryStorageClient implements Storage {

    @Inject
    private void setup(VeruvInjector.Environment env) {
        checkState(!env.isProduction(), "Cannot use in memory implementation in production");
    }

    /**
     * bucket_name / object name to content
     */
    private final Set<String> buckets = Sets.newConcurrentHashSet();
    private final Map<String, Blob> blobs = Maps.newConcurrentMap();
    private final Map<BlobId, byte[]> contents = Maps.newConcurrentMap();

    @Override
    public Bucket create(BucketInfo bucketInfo, BucketTargetOption... options) {
        if (buckets.add(bucketInfo.getName())) {
            log.info("Creating bucket {}", bucketInfo.getName());
        } else {
            log.info("Cannot create bucket {}, already exists", bucketInfo.getName());
            throw new StorageException(409, "BucketAlreadyOwnedByYou");
        }
        BucketInfo.BuilderImpl bucketInfoBuilder = (BucketInfo.BuilderImpl) new BucketInfo.BuilderImpl(bucketInfo)
                .setCreateTime(System.currentTimeMillis());
        return new Bucket(this, bucketInfoBuilder);
    }

    @Override
    public Blob create(BlobInfo blobInfo, BlobTargetOption... options) {
        return create(blobInfo, new byte[0], options);
    }

    @Override
    public Blob create(BlobInfo blobInfo, byte[] content, BlobTargetOption... options) {
        BlobInfo.BuilderImpl blobInfoBuilder = (BlobInfo.BuilderImpl) new BlobInfo.BuilderImpl(blobInfo)
                .setCreateTime(System.currentTimeMillis())
                .setUpdateTime(System.currentTimeMillis());
        boolean failIfExists = Arrays.stream(options).anyMatch(o -> o.getRpcOption() == IF_GENERATION_MATCH && o.getValue() == Long.valueOf(0L));
        if (failIfExists) {
            Blob existingValue = blobs.putIfAbsent(getPath(blobInfo), new Blob(this, blobInfoBuilder));
            if (failIfExists && existingValue != null) {
                log.info("Cannot create object {} / {}, already exists", blobInfo.getBucket(), blobInfo.getName());
                throw new StorageException(409, "AlreadyExists");
            }
        } else {
            blobs.compute(getPath(blobInfo), (key, oldValue) -> {
                if (oldValue != null) {
                    blobInfoBuilder.setCreateTime(oldValue.getCreateTime());
                }
                return new Blob(this, blobInfoBuilder);
            });
        }

        // Create blob now since blobInfoBuilder may have had creation time updated above
        Blob blob = new Blob(this, blobInfoBuilder);

        contents.put(blob.getBlobId(), content);
        log.info("Created object {} / {}", blobInfo.getBucket(), blobInfo.getName());
        return blob;
    }

    @Override
    public StorageOptions getOptions() {
        return StorageOptions.getUnauthenticatedInstance();
    }

    @Override
    public Blob get(BlobId blob) {
        return get(blob, null);
    }

    @Override
    public Blob get(BlobId blobId, BlobGetOption... options) {
        Blob blob = blobs.get(getPath(blobId));
        if (blob == null) {
            log.info("Cannot get object {} / {}, doesn't exist", blobId.getBucket(), blobId.getName());
            return null;
        }
        log.info("Getting object {} / {}", blobId.getBucket(), blobId.getName());
        return blob;
    }

    @Override
    public Blob update(BlobInfo blobInfo) {
        return update(blobInfo, null);
    }

    @Override
    public Blob update(BlobInfo blobInfo, BlobTargetOption... options) {
        Blob updatedBlob = blobs.computeIfPresent(getPath(blobInfo), (key, oldValue) -> {
            HashMap<String, String> mergedMetadata = Maps.newHashMap();
            mergedMetadata.putAll(oldValue.getMetadata());
            mergedMetadata.putAll(blobInfo.getMetadata());

            BlobInfo.BuilderImpl blobInfoBuilder = (BlobInfo.BuilderImpl) new BlobInfo.BuilderImpl(blobInfo)
                    .setCreateTime(oldValue.getCreateTime())
                    .setUpdateTime(System.currentTimeMillis())
                    .setMetadata(ImmutableMap.copyOf(mergedMetadata));

            return new Blob(this, blobInfoBuilder);
        });
        if (updatedBlob == null) {
            log.info("Cannot update object {} / {}, doesn't exist", blobInfo.getBucket(), blobInfo.getName());
            throw new StorageException(404, "NotFound");
        }
        log.info("Updated object {} / {}", blobInfo.getBucket(), blobInfo.getName());
        return updatedBlob;
    }

    @Override
    public CopyWriter copy(CopyRequest copyRequest) {
        return new CopyWriter(this.getOptions(), null) {
            public Blob getResult() {
                log.info("Copy object from {} to {}", copyRequest.getSource(), copyRequest.getTarget());
                Blob blob = get(copyRequest.getSource());
                return create(new BlobInfo.BuilderImpl(blob)
                                .setBlobId(BlobId.of(
                                        copyRequest.getTarget().getBlobId().getBucket(),
                                        copyRequest.getTarget().getBlobId().getName()))
                                .build(),
                        blob.getContent(),
                        copyRequest.getTargetOptions().toArray(new BlobTargetOption[]{}));
            }
        };
    }

    @Override
    public boolean delete(BlobId blob) {
        return delete(blob, null);
    }

    @Override
    public boolean delete(BlobId blob, BlobSourceOption... options) {
        boolean deleted = blobs.remove(getPath(blob)) != null;
        contents.remove(blob);
        return deleted;
    }

    @Override
    public byte[] readAllBytes(BlobId blobId, BlobSourceOption... options) {
        byte[] content = contents.get(blobId);
        if (content == null) {
            log.info("Cannot read content {} / {}, doesn't exist", blobId.getBucket(), blobId.getName());
            throw new StorageException(404, "NotFound");
        }
        log.info("Reading content {} / {}", blobId.getBucket(), blobId.getName());
        return content;
    }

    private String getPath(BlobId blobId) {
        return getPath(blobId.getBucket(), blobId.getName());
    }

    private String getPath(BlobInfo blobInfo) {
        return getPath(blobInfo.getBucket(), blobInfo.getName());
    }

    private String getPath(String bucketName, String blobName) {
        return bucketName + "/" + blobName;
    }

    // <editor-fold desc="Not implemented" defaultstate="collapsed">

    @Override
    public Blob create(BlobInfo blobInfo, InputStream content, BlobWriteOption... options) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Bucket get(String bucket, BucketGetOption... options) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Bucket lockRetentionPolicy(BucketInfo bucket, BucketTargetOption... options) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Blob get(String bucket, String blob, BlobGetOption... options) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Page<Bucket> list(BucketListOption... options) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Page<Blob> list(String bucket, BlobListOption... options) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Bucket update(BucketInfo bucketInfo, BucketTargetOption... options) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean delete(String bucket, BucketSourceOption... options) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean delete(String bucket, String blob, BlobSourceOption... options) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Blob compose(ComposeRequest composeRequest) {
        throw new UnsupportedOperationException();
    }

    @Override
    public byte[] readAllBytes(String bucket, String blob, BlobSourceOption... options) {
        throw new UnsupportedOperationException();
    }

    @Override
    public StorageBatch batch() {
        throw new UnsupportedOperationException();
    }

    @Override
    public ReadChannel reader(String bucket, String blob, BlobSourceOption... options) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ReadChannel reader(BlobId blob, BlobSourceOption... options) {
        throw new UnsupportedOperationException();
    }

    @Override
    public WriteChannel writer(BlobInfo blobInfo, BlobWriteOption... options) {
        throw new UnsupportedOperationException();
    }

    @Override
    public URL signUrl(BlobInfo blobInfo, long duration, TimeUnit unit, SignUrlOption... options) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Blob> get(BlobId... blobIds) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Blob> get(Iterable<BlobId> blobIds) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Blob> update(BlobInfo... blobInfos) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Blob> update(Iterable<BlobInfo> blobInfos) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Boolean> delete(BlobId... blobIds) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Boolean> delete(Iterable<BlobId> blobIds) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Acl getAcl(String bucket, Acl.Entity entity, BucketSourceOption... options) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Acl getAcl(String bucket, Acl.Entity entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean deleteAcl(String bucket, Acl.Entity entity, BucketSourceOption... options) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean deleteAcl(String bucket, Acl.Entity entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Acl createAcl(String bucket, Acl acl, BucketSourceOption... options) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Acl createAcl(String bucket, Acl acl) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Acl updateAcl(String bucket, Acl acl, BucketSourceOption... options) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Acl updateAcl(String bucket, Acl acl) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Acl> listAcls(String bucket, BucketSourceOption... options) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Acl> listAcls(String bucket) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Acl getDefaultAcl(String bucket, Acl.Entity entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean deleteDefaultAcl(String bucket, Acl.Entity entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Acl createDefaultAcl(String bucket, Acl acl) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Acl updateDefaultAcl(String bucket, Acl acl) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Acl> listDefaultAcls(String bucket) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Acl getAcl(BlobId blob, Acl.Entity entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean deleteAcl(BlobId blob, Acl.Entity entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Acl createAcl(BlobId blob, Acl acl) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Acl updateAcl(BlobId blob, Acl acl) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Acl> listAcls(BlobId blob) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Policy getIamPolicy(String bucket, BucketSourceOption... options) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Policy setIamPolicy(String bucket, Policy policy, BucketSourceOption... options) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Boolean> testIamPermissions(String bucket, List<String> permissions, BucketSourceOption... options) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ServiceAccount getServiceAccount(String projectId) {
        throw new UnsupportedOperationException();
    }

    // </editor-fold>

    public static Module module() {
        return new AbstractVeruvModule() {
            @Override
            protected void configure() {
                bind(Storage.class).to(InMemoryStorageClient.class).asEagerSingleton();
            }
        };
    }
}
