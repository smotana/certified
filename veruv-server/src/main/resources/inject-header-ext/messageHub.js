var listener = function(event) {
    if(event.data && event.data.veruvSend) {
        chrome.runtime.sendMessage(event.data, function(response) {
            // Shut down communication after setup is ok
            if(response.veruvRecv && response.type === 'setup' && response.msg === 'ok') {
                window.removeEventListener('message', listener, false);
            }
            event.source.postMessage(
                response
                    || {veruvRecv:true, type:'error', msg:chrome.runtime.lastError},
                event.origin);
        });
    }
}
window.addEventListener('message', listener, false);
