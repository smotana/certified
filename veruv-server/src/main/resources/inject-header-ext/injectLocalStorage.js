// Expecting:
// 's' = key-value pair of session storage
// 'l' = key-value pair of local storage

var result;

try{
    Object.entries(l).forEach(function(item){
        window.localStorage.setItem(item[0],item[1]);
    });
    Object.entries(s).forEach(function(item){
        window.sessionStorage.setItem(item[0],item[1]);
    });
    l = undefined;
    s = undefined;
    result = 'ok';
} catch(err) {
    result = JSON.stringify(err);
}

// Return
result
