var headersToInject;

function openTab(url) {
    return new Promise(function (resolve, reject) {
        var timeout = setTimeout(function() {
            reject('Timed out waiting for page load');
        }, 3000);
        chrome.tabs.create({
            active: false,
            url: url
        }, function(tab) {
            if(!tab) {
                return reject('failed to open tab: ' + url);
            }

            var onCompletedListener = function(request) {
                if (request.tabId === tab.id) {
                    resolve(tab);
                    chrome.webRequest.onCompleted.removeListener(onCompletedListener);
                    chrome.webRequest.onErrorOccurred.removeListener(onErrorOccurredListener);
                    clearTimeout(timeout);
                }
            };
            var onErrorOccurredListener = function(request) {
                if (request.tabId === tab.id) {
                    reject('Error occurred loading page in tab: ' + JSON.stringify(request));
                    chrome.tabs.remove(tab.id);
                    chrome.webRequest.onCompleted.removeListener(onCompletedListener);
                    chrome.webRequest.onErrorOccurred.removeListener(onErrorOccurredListener);
                    clearTimeout(timeout);
                }
            };

            chrome.webRequest.onCompleted.addListener(
                onCompletedListener,
                { urls: ['<all_urls>'] });
            chrome.webRequest.onErrorOccurred.addListener(
                onErrorOccurredListener,
                { urls: ['<all_urls>'] });
        });
    });
}
function injectData(localStorage, tab) {
    return new Promise(function (resolve, reject) {
        chrome.tabs.executeScript(tab.id, {
            runAt: "document_start",
            code:
                'var l=' + JSON.stringify(localStorage.localContent) + ';'
                + 'var s=' + JSON.stringify(localStorage.sessionContent) + ';'
                + '"ok"'
        }, function(result) {
            if(result && result.length == 1 && result[0] === "ok") {
                resolve(tab);
            } else {
                reject('failed to inject data into tab: ' + JSON.stringify(result) + ' lastError:' + JSON.stringify(chrome.runtime.lastError));
                chrome.tabs.remove(tab.id);
            }
        });
    });
}
function injectScript(tab, scriptFile) {
    return new Promise(function (resolve, reject) {
        chrome.tabs.executeScript(tab.id, {
            runAt: "document_start",
            file: scriptFile
        }, function(result) {
            if(result && result.length == 1 && result[0] === "ok") {
                resolve();
            } else {
                reject('failed to inject script into tab: ' + JSON.stringify(result) + ' lastError:' + JSON.stringify(chrome.runtime.lastError));
            }
            chrome.tabs.remove(tab.id);
        });
    });
}
function handleSetupMsg(msg, sendResponse) {
    var promisesLocalStorage = [];
    // Set headers
    headersToInject = msg.h;
    // Set local storage
    if(msg.l) {
        for (i = 0; i < msg.l.length; ++i) {
            const localStorage = msg.l[i];
            const url = localStorage.url + '/robots.txt';
            promisesLocalStorage.push(
                openTab(localStorage.url)
                .then(function (tab) {return injectData(localStorage, tab)})
                .then(function (tab) {return injectScript(tab, 'injectLocalStorage.js')}));
        }
    }
    // Set cookies
    var promisesCookie = [];
    if(msg.c) {
        for (i = 0; i < msg.c.length; ++i) {
            var cookie = msg.c[i];
            // See BrowserRequest.java
            var cookieObj = {
                url: (cookie.c ? "https://" : "http://") + cookie.d + "/",
                domain: cookie.i ? cookie.d : undefined,
                name: cookie.n,
                value: cookie.v,
                path: cookie.p || undefined,
                secure: cookie.c,
                httpOnly: cookie.h,
                expirationDate: cookie.s ? undefined : cookie.e
            };
            promisesCookie.push(new Promise(function(resolve, reject) {
                chrome.cookies.set(cookieObj,
                    function(createdCookie) {
                        if (createdCookie) {
                            resolve();
                        } else {
                            reject('Cookie failed to set:' + JSON.stringify(createdCookie) + ', lastError:' + JSON.stringify(chrome.runtime.lastError));
                        }
                    });
            }));
        }
    }
    Promise.all(promisesLocalStorage)
    .catch(function(er) {
        // swallow local storage errors
        console.log('Failed to setup local storage error(s):', er);
    })
    .then(Promise.all(promisesCookie))
    .then(function() {
        // Disable further communication
        chrome.storage.local.set({
            setupComplete: true,
            headersToInject: headersToInject,
        });
        chrome.runtime.onMessage.removeListener(onMessageListener);

        sendResponse({veruvRecv:true,type:'setup',msg:'ok'});
    })
    .catch(function(er) {
        console.log('Failed to setup, error(s):', er);
        sendResponse({veruvRecv:true,type:'setup',msg:JSON.stringify(er)});
        throw(er);
    });
    return true; // Indicate we will call sendResponse asynchronously
}
var onMessageListener = function(data, sender, sendResponse) {
    if (data.veruvSend && data.type) {
        switch (data.type) {
            case 'ping':
                sendResponse({veruvRecv:true,type:'pong',msg:'pong'});
                break;
            case 'setup':
                return handleSetupMsg(data.msg, sendResponse);
            default:
                sendResponse({veruvRecv:true,type:'error',msg:'Unknown msg type: ' + data.type});
                break;
        }
    }
};

function onBeforeSendHeadersInjectHeaders(request) {
    for (var headerName in headersToInject) request.requestHeaders.push({
        name: headerName,
        value: headersToInject[headerName]
    });
    return {
        requestHeaders: request.requestHeaders
    }
}

chrome.webRequest.onBeforeSendHeaders.addListener(
    onBeforeSendHeadersInjectHeaders,
    { urls: ['<all_urls>'] },
    ['requestHeaders', 'blocking']);

chrome.storage.local.get(['setupComplete', 'headersToInject'], function(result) {
    if(result.setupComplete) {
        headersToInject = result.headersToInject;
    } else {
        chrome.runtime.onMessage.addListener(onMessageListener);
    }
});
