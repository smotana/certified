set -ex

INKSCAPE=/Applications/Inkscape.app/Contents/Resources/bin/inkscape 

$INKSCAPE -z $(pwd)/veruv-url.svg -e $(pwd)/veruv-url.png
$INKSCAPE -z $(pwd)/veruv-verified.svg -e $(pwd)/veruv-verified.png
$INKSCAPE -z $(pwd)/veruv-help.svg -e $(pwd)/veruv-help.png
$INKSCAPE -z $(pwd)/veruv-veruv.svg -e $(pwd)/veruv-veruv.png
$INKSCAPE -z $(pwd)/veruv-v.svg -e $(pwd)/veruv-v.png

magick convert veruv-v.png -resize 16x16 veruv-v-16.png
magick convert veruv-v.png -resize 32x32 veruv-v-32.png
magick convert veruv-v.png -resize 48x48 veruv-v-48.png
magick convert veruv-v.png -resize 64x64 veruv-v-64.png
magick convert veruv-v.png -resize 128x128 veruv-v-128.png
magick convert veruv-v-16.png veruv-v-32.png veruv-v-48.png favicon.ico
